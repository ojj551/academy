$('#update-materia').click(function(){
	var requerimientosvl = requerimientos.getData();
	var objetivosvl = objetivos.getData();

	if(requerimientosvl!="" && objetivosvl!=""){
		$.ajax({
			type:'POST',
	       	url : base_url+'alumnosfunc/updateinfobasic',
	       	data: 'id='+encodeURIComponent(MEcryp)+'&reqsvl='+encodeURIComponent(requerimientosvl)+'&objvl='+encodeURIComponent(objetivosvl),
			beforeSend:function(){
				$('#edit-name').modal('hide');
				$('.capaload').fadeIn(600);
			},
			success : function(datav) {
				setTimeout(function(){

					if(datav=="success"){
						location.reload();
					}else{
						$('.capaload').fadeOut(600);
						$.dialog({
							title: '¡Ups! tuvimos problemas para actualizar tus datos',
							content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
						});
					}
				},1000);
			}
		});
	}
});

$('.addsubtema').click(function(){
	var v = $(this).attr('v');
	$('#add-sub-topic').modal('show');
	$('#TopicF').val(v);
	console.log(v);
});
$('.edit-asig').click(function(){
  var cr = $(this).attr('val');
  $.ajax({
    url : base_url+'supervisorfunc/getasig',
    type : 'POST',
    data : 'asig='+encodeURIComponent(cr),
    beforeSend:function(){
    },
    success : function(datav) {
      $('#update-carr').html(datav);
    }
  });
});