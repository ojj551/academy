//////////////TOPICS////////////////////

$('#update-topic').click(function(){
  var name = $('#nametopic-upd').val();
  var idtc = $('#idtc').val();
  if(name!=""){
    $.ajax({
      type:'POST',
          url : base_url+'temasfunc/updatename',
          data: 'nombre='+name+'&idtc='+encodeURIComponent(idtc),
      beforeSend:function(){
        $('#edit-topic').modal('hide');
        $('.capaload').fadeIn(600);
      },
      success : function(datav) {
        setTimeout(function(){

          if(datav=="success"){
            location.reload();
          }else{
            $('.capaload').fadeOut(600);
            $.dialog({
              title: '¡Ups! tuvimos problemas para actualizar tus datos',
              content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
            });
          }
        },1000);
      }
    });
  }
});
$('#update-unidad').click(function(){
  var name = $('#nameunidad-upd').val();
  var idunid = $('#idunid').val();
  if(name!=""){
    $.ajax({
      type:'POST',
          url : base_url+'unidadfunc/updatename',
          data: 'nombre='+name+'&idunid='+encodeURIComponent(idunid),
      beforeSend:function(){
        $('#edit-unidad').modal('hide');
        $('.capaload').fadeIn(600);
      },
      success : function(datav) {
        setTimeout(function(){

          if(datav=="success"){
            location.reload();
          }else{
            $('.capaload').fadeOut(600);
            $.dialog({
              title: '¡Ups! tuvimos problemas para actualizar tus datos',
              content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
            });
          }
        },1000);
      }
    });
  }
});
//agregar material
$('#add-matl').click(function(){
  var idscm = $('#idscm').val();
  var typematvl = $('#typematvl').val();
  var title_matl = $('#title-matl').val();
  var dsc_matl = $('#dsc-matl').val();
  var file_matl = $('#file-matl').prop('files');
  if(idscm!="" && typematvl!="" && title_matl!="" && dsc_matl!=""){
    var formData = new FormData();
    
    formData.append('title_matl', title_matl);
    formData.append('dsc_matl', dsc_matl);
    formData.append('idscm', idscm);
    formData.append('typematvl', typematvl);
    if(typematvl=='4'){
      formData.append('videoyoutube', $('#videoyoutube').val());
      formData.append('file', '');
    }else{
      formData.append('videoyoutube', '');
      formData.append('file', $('#file-matl')[0].files[0]);
    }
    ///
    $.ajax({
      url : base_url+'unidadfunc/creatematerial',
      type : 'POST',
      data : formData,
      processData: false,  // tell jQuery not to process the data
      contentType: false,  // tell jQuery not to set contentType
      beforeSend:function(){
        $('#add-material').modal('hide');
        $('.capaload').fadeIn(600);
      },
      success : function(data) {
        setTimeout(function(){
          if(data=="success"){
            location.reload();
          }else{
            $('.capaload').fadeOut(600);
            $.dialog({
                title: '¡Ups! tuvimos problemas',
                content: data,
            });
          }
        },1000);
      }
    });
    /*
      var data = $('#imgperfil').serialize();
        var formData = new FormData();
        formData.append('file', $('#imgavatar')[0].files[0]);
        $.ajax({
              url : base_url+'cvFunc/updateimg',
              type : 'POST',
              data : formData,
              processData: false,  // tell jQuery not to process the data
              contentType: false,  // tell jQuery not to set contentType
              beforeSend:function(){
          },
          success : function(data) {
            location.reload();
          }
      });
    */
    /*
    $.ajax({
      type:'POST',
      url : base_url+'unidadfunc/creatematerial',
      data: 'title_matl='+title_matl+'&dsc_matl='+dsc_matl+'&idscm='+encodeURIComponent(idscm)+'&typematvl='+typematvl,
      beforeSend:function(){
        $('#add-material').modal('hide');
        $('.capaload').fadeIn(600);
      },
      success : function(datav) {
        setTimeout(function(){

          if(datav=="success"){
            location.reload();
          }else{
            $('.capaload').fadeOut(600);
            $.dialog({
              title: '¡Ups! tuvimos problemas para actualizar tus datos',
              content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
            });
          }
        },1000);
      }
    });*/
  }
});
//////////////////////////////////////
$('#updatenamemat').click(function(){
	var idmat = $('#idmat').val();
	var nombremtupdt = $('#nombremtupdt').val();
	var incmupdt = $('#incmupdt').val();
	var finmupdt = $('#finmupdt').val();
	if(nombremtupdt!="" && idmat!=""){
		$.ajax({
			type:'POST',
	   url : base_url+'materiasfunc/updatename',
	   data: 'nombre='+nombremtupdt+'&idmat='+encodeURIComponent(idmat)+'&incmupdt='+incmupdt+'&finmupdt='+finmupdt,
			beforeSend:function(){
				$('#edit-name').modal('hide');
				$('.capaload').fadeIn(600);
			},
			success : function(datav) {
				setTimeout(function(){

					if(datav=="success"){
						location.reload();
					}else{
						$('.capaload').fadeOut(600);
						$.dialog({
							title: '¡Ups! tuvimos problemas para actualizar tus datos',
							content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
						});
					}
				},1000);
			}
		});
	}
});
$('#updatebannerdata').click(function(){
	var idmat = $('#idmat').val();
	var file = $('#bannermupt').prop('files');
	var colorbmupt = $('#colorbmupt').val();
	var banner;

	if(file!=undefined){
		var typefile = file[0].type;
		if(typefile=="image/png" || typefile=="image/jpg" || typefile=="image/jpeg"){
			//if(file[0].size<=60000){
				var formData = new FormData();
				formData.append('img', $('#bannermupt')[0].files[0]);
				formData.append('color', colorbmupt);
				formData.append('id', idmat);
			//}
		}
	}else{
		var formData = new FormData();
		formData.append('img', "");
		formData.append('color', colorbmupt);
		formData.append('id', idmat);
	}
	$.ajax({
        url : base_url+'materiasfunc/updatebanner',
       	type : 'POST',
       	data : formData,
       	processData: false,  // tell jQuery not to process the data
       	contentType: false,  // tell jQuery not to set contentType
       	beforeSend:function(){
		},
		success : function(data) {
			location.reload();
		}
	});
});
//
$('.descript-mat').click(function(){
	console.log('que pedo');
	
});
var reqmupdvl = CKEDITOR.replace( 'reqmupd' );
	var objmupdvl = CKEDITOR.replace( 'objmupd' );
$('#updatedecrpdata').click(function(){
	var idmat = $('#idmat').val();
	var reqmupd = reqmupdvl.getData();
	var objmupd = objmupdvl.getData();

	if(idmat!="" && reqmupd!="" && objmupd!=""){
		$.ajax({
			type:'POST',
	       	url : base_url+'materiasfunc/updatedescription',
	       	data: 'reqmupd='+encodeURIComponent(reqmupd)+'&idmat='+encodeURIComponent(idmat)+'&objmupd='+encodeURIComponent(objmupd),
			beforeSend:function(){
				$('#edit-description-mat').modal('hide');
				$('.capaload').fadeIn(600);
			},
			success : function(datav) {
				setTimeout(function(){

					if(datav=="success"){
						location.reload();
					}else{
						$('.capaload').fadeOut(600);
						$.dialog({
							title: '¡Ups! tuvimos problemas para actualizar tus datos',
							content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
						});
					}
				},1000);
			}
		});
	}
});

var c = 0;
$('.addtema').click(function(){
  $(".addtema").off('click');
  var bg;
  if(c%2==0){
    bg = '#ffefd2';
  }else{
    bg = '#c7edff';
  }
  $(this).parent().prev().append('<li style="padding: 12px;background:'+bg+';"><input class="form-control form-temario tema-father" v="'+c+'" type="text"/><a class="remove-item"><i class="fa fa-minus-circle" aria-hidden="true"></i></a><ol class="subtemas"></ol></li>');
  //remove
  $(".remove-item").off('click');
  $('.remove-item').click(function(){
    $(this).parent().remove();
  });
  /*
  //add subtema
  $(".addsbtema").off('click');
  $('.addsbtema').click(function(){
    $(this).prev().append('<li><input class="form-control form-temario tema-child" type="text" v="'+c+'"/><a class="remove-sb-item"><i class="fa fa-minus-circle" aria-hidden="true"></i></a><br><br></li>');
    //remove
    $(".remove-sb-item").off('click');
    $('.remove-sb-item').click(function(){
      $(this).parent().remove();
    });
  });*/

  c++;
});

$('#create-topic').click(function(){
  var arrfth = [];
  var section = $('#idsct').val();
  var idmencr = $('#idmencr').val();
  $.each( $('.tema-father'), function( key, value ) {
    var arrcld = [];
    var sb = $(value).next().next().children();
    
    arrfth.push({
      texto : $(value).val(), 
      section : section,
      idmt : idmencr
    });
    
  });
  if(arrfth.length>0){
    var jsonString = JSON.stringify(arrfth);
    console.log(jsonString);
    $.ajax({
        type: "POST",
        url : base_url+'materiasfunc/addtopics',
        data: {data : jsonString}, 
        cache: false,
        beforeSend:function(){
          $('#add-topic').modal('hide');
          $('.capaload').fadeIn(600);
        },
        success: function(data){
          $('.capaload').fadeOut(600);
          if(data=="success"){
            //location.reload();
          }else{
            $.alert({
              title: 'Tuvimos problemas al crear algunos alumnos',
              content: data,
              confirm: function () {
                  //location.reload();
              }
            });
          }
        }
    });
  }
});
/////////////////////////////////SUB-TEMAS////////////////////////////////////////////
var c = 0;
$('.addtemasub').click(function(){
  $(".addtemasub").off('click');
  var bg;
  if(c%2==0){
    bg = '#ffefd2';
  }else{
    bg = '#c7edff';
  }
  $(this).parent().prev().append('<li style="padding: 12px;background:'+bg+';"><input class="form-control form-temario tema-father" v="'+c+'" type="text"/><a class="remove-item"><i class="fa fa-minus-circle" aria-hidden="true"></i></a><ol class="subtemas"></ol></li>');
  //remove
  $(".remove-item").off('click');
  $('.remove-item').click(function(){
    $(this).parent().remove();
  });
  c++;
});
$('#create-sub-topic').click(function(){
  var arrfth = [];
  var TopicF = $('#TopicF').val();
  $.each( $('.tema-father'), function( key, value ) {
    var arrcld = [];
    var sb = $(value).next().next().children();
    
    arrfth.push({
      texto : $(value).val(), 
      Topic : TopicF
    });
    
  });
  if(arrfth.length>0){
    var jsonString = JSON.stringify(arrfth);
    //console.log(jsonString);
    $.ajax({
        type: "POST",
        url : base_url+'materiasfunc/addsbtopics',
        data: {data : jsonString}, 
        cache: false,
        beforeSend:function(){
          $('#add-topic').modal('hide');
          $('.capaload').fadeIn(600);
        },
        success: function(data){
          $('.capaload').fadeOut(600);
          if(data=="success"){
            location.reload();
          }else{
            $.alert({
              title: 'Tuvimos problemas al crear algunos temas',
              content: data,
              confirm: function () {
                  location.reload();
              }
            });
          }
        }
    });
  }
});


/*
var u = 1;
$('.sig-seccion').click(function(){
  u++;
  $('.adds').append('<h3>Unidad '+u+'</h3><div class="padd-left"><ol class="temas"></ol><div style="margin-top: 40px;"><a class="addtema" v="1"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar tema</a></div></div>');
});
*/