$("#typealum1").on('change', function() {
  var typeusr = this.value;
  if(typeusr!=""){
    $.ajax({
        type:'POST',
        url : base_url+'alumnosfunc/TypeUser',
        data: 'typeusr='+encodeURIComponent(typeusr),
        beforeSend:function(){
        },
        success: function(data){
            $('.paintbytype').html(data);
        }
    });
  }
});
//
$('.generateidreference').click(function(){
  $.ajax({
    type:'POST',
    url : base_url+'alumnosfunc/createreference',
    data: 'generate=1',
    beforeSend:function(){
      $('#addalumnos-form').modal('hide');
      $('.capaload').fadeIn(600);
    },
    success : function(data) {
      setTimeout(function(){
        if(data!="error"){
          $('#addalumnos-form').modal('show');
          $('.capaload').fadeOut(600);
          $('#idreference1').val(data);
          $( "#emailalumn" ).focus();
        }else{
          $('.capaload').fadeOut(600);
          $.alert({
            title: '¡Ups! tuvimos problemas para generar tu ID Referencia',
            content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
            confirm: function () {
                $('#addalumnos-form').modal('show');
            }
          });
        }
      },1000);
    }
  });
});
//Crear
$('#createalumnos').click(function(){
	var idref = $('#idreference1').val();
	console.log(idref);
	var email = $('#emailalumn1').val();
	var typealm = $('#typealum1').val();
	$.ajax({
        type: "POST",
        url : base_url+'alumnosfunc/addalumno',
        data: 'idref='+encodeURIComponent(idref)+'&email='+encodeURIComponent(email)+'&typealm='+encodeURIComponent(typealm), 
        beforeSend:function(){
          $('#addalumnos-form').modal('hide');
          $('.capaload').fadeIn(600);
        },
        success: function(data){
          $('.capaload').fadeOut(600);
          if(data=="success"){
            //$(location).attr('href', base_url+'grupos');
          }else{
            $.alert({
              title: 'Tuvimos problemas al crear algunos alumnos',
              content: '<strong>Detalles: </strong>'+data,
              confirm: function () {
                  //$(location).attr('href', base_url+'grupos');
              }
            });
          }
        }
    });
});