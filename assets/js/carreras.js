$('#addmat').click(function(){
  var ctm = $('#ctm').val();
  var namedpto = $('#namedpto').val();
  var leveldpto = $('#leveldpto').val();
  if(namedpto!="" && leveldpto!=""){
    $.ajax({
        type:'POST',
        url : base_url+'supervisorfunc/add',
        data: 'name='+namedpto+'&leveldpto='+leveldpto+'&ctm='+encodeURIComponent(ctm),
        beforeSend:function(){
          $('#adddepto').modal('hide');
          $('.capaload').fadeIn(600);
        },
        success: function(data){
          setTimeout(function(){
            $('.capaload').fadeOut(600);
            if(data=="success"){
              location.reload();
            }else{
              $.alert({
                title: '¡Ups!',
                content: data,
                confirm: function () {
                    $('#adddepto').modal('show');
                }
              }); 
            }
          },1000);
        }
    });
  }
});
$('#crear-materia').click(function(){
  var ctm = $('#ctm').val();
  var nombre = $('#nombre').val();
  var inicia = $('#inicia').val();
  var finaliza = $('#finaliza').val();
  var requerimientos = $('#requerimientos').val();
  var objetivos = $('#objetivos').val();
  ///
  if(nombre!="" && requerimientos!="" && objetivos!=""){
    $.ajax({
      type:'POST',
          url : base_url+'materiasfunc/add',
          data: 'nombre='+nombre+'&inicia='+inicia+'&finaliza='+finaliza+'&requerimientos='+requerimientos+'&objetivos='+objetivos+'&ctm='+encodeURIComponent(ctm),
      beforeSend:function(){
        $('.capaload').fadeIn(600);
      },
      success : function(data) {
        $('.capaload').fadeOut(600);
        setTimeout(function(){ 
          if(data!="error"){
            $(location).attr('href', base_url+'materias');
            //$(location).attr('href', base_url+'materias/admin/?m='+encodeURIComponent(data));
          }else{
            $.dialog({
              title: 'Ups!',
              content: data,
            });
          }
        },700);
      }
    });
  }
});

$("#typealum1").on('change', function() {
    var typeusr = this.value;
    if(typeusr!=""){
      $.ajax({
          type:'POST',
          url : base_url+'alumnosfunc/TypeUser',
          data: 'typeusr='+encodeURIComponent(typeusr),
          beforeSend:function(){
          },
          success: function(data){
              $('.paintbytype').html(data);
              //Generar ID
              $('.generateidreference').click(function(){
                  $.ajax({
                    type:'POST',
                    url : base_url+'alumnosfunc/createreference',
                    data: 'generate=1',
                    beforeSend:function(){
                      $('#addalumnos-form').modal('hide');
                      $('.capaload').fadeIn(600);
                    },
                    success : function(data) {
                      setTimeout(function(){
                        if(data!="error"){
                          $('#addalumnos-form').modal('show');
                          $('.capaload').fadeOut(600);
                          $('#idreference1').val(data);
                          $( "#emailalumn" ).focus();
                        }else{
                          $('.capaload').fadeOut(600);
                          $.alert({
                            title: '¡Ups! tuvimos problemas para generar tu ID Referencia',
                            content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
                            confirm: function () {
                                $('#addalumnos-form').modal('show');
                            }
                          });
                        }
                      },1000);
                    }
                  });
              });
              //Semestres por materia
              $(".carrera").on('change', function() {
                var vlcar = this.value
                var vl = $('#cant').val();
                if(vl!=""){
                  $.ajax({
                      type:'POST',
                      url : base_url+'alumnosfunc/SemestreCarrera',
                      data: 'vlcar='+encodeURIComponent(vlcar),
                      beforeSend:function(){
                      },
                      success: function(data){
                          $('#semestre1').html(data);
                      }
                  });
                }
              });
          }
      });
    }
  });
//
$('.generateidreference').click(function(){
    $.ajax({
      type:'POST',
      url : base_url+'alumnosfunc/createreference',
      data: 'generate=1',
      beforeSend:function(){
        $('#addalumnos-form').modal('hide');
        $('.capaload').fadeIn(600);
      },
      success : function(data) {
        setTimeout(function(){
          if(data!="error"){
            $('#addalumnos-form').modal('show');
            $('.capaload').fadeOut(600);
            $('#idreference1').val(data);
            $( "#emailalumn" ).focus();
          }else{
            $('.capaload').fadeOut(600);
            $.alert({
              title: '¡Ups! tuvimos problemas para generar tu ID Referencia',
              content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
              confirm: function () {
                  $('#addalumnos-form').modal('show');
              }
            });
          }
        },1000);
      }
    });
});
//Crear
$('#createalumnos').click(function(){
    var idref = $('#idreference1').val();
    var email = $('#emailalumn1').val();
    var typealm = $('#typealum1').val();

    var carrera = $('#carrera1').val();
    var semestre = $('#semestre1').val();

    $.ajax({
          type: "POST",
          url : base_url+'alumnosfunc/addalumno',
          data: 'idref='+encodeURIComponent(idref)+'&email='+encodeURIComponent(email)+'&typealm='+encodeURIComponent(typealm)+'&carrera='+carrera+'&semestre='+semestre, 
          beforeSend:function(){
            $('#addalumnos-form').modal('hide');
            $('.capaload').fadeIn(600);
          },
          success: function(data){
            $('.capaload').fadeOut(600);
            if(data=="success"){
              //$(location).attr('href', base_url+'grupos');
            }else{
              $.alert({
                title: 'Tuvimos problemas al crear algunos alumnos',
                content: '<strong>Detalles: </strong>'+data,
                confirm: function () {
                    //$(location).attr('href', base_url+'grupos');
                }
              });
            }
          }
      });
});
//
$('.asiguser').click(function(){
    var typ = $(this).attr('typ');
    var usrid = $(this).attr('usrid');
    $('#asiguser-form').modal('show');
    if(typ!="" && usrid!=""){
      $.ajax({
        url : base_url+'alumnosfunc/getlistalumn',
        type : 'POST',
            data : 'typ='+encodeURIComponent(typ)+'&usrid='+encodeURIComponent(usrid),
            beforeSend:function(){
              console.log('enviando...');
          //$('.capaload').fadeIn(600);
        },
        success : function(datav) {
          $('.paintuserslist').html(datav);
          $('.userlist').selectpicker('refresh');
        }
      });
    }
  });
//
$('#asignarusuario').click(function(){
  var usr = $('#usr_asg').val(); 
  var usr_elg = $('#users_asg').val();
  if(usr!="" && usr_elg!=""){
    $.ajax({
      url : base_url+'alumnosfunc/addasignacion',
      type : 'POST',
          data : 'usr='+encodeURIComponent(usr)+'&usrelg='+encodeURIComponent(usr_elg),
          beforeSend:function(){
        $('.capaload').fadeIn(600);
        $('#asiguser-form').modal('hide');
      },
      success : function(datav) {
        setTimeout(function(){
          $('.capaload').fadeOut(600);
          if(datav=="success"){
            location.reload();
          }else{
            $.dialog({
              title: '¡Ups!',
              content: datav,
            });
          }
        },1000);
      }
    });
  }
});
//
//Actualizar usuario
$('.edit').click(function(){
  var cr = $(this).attr('val');
  $.ajax({
    url : base_url+'supervisorfunc/getcarr',
    type : 'POST',
    data : 'cr='+encodeURIComponent(cr),
    beforeSend:function(){
    },
    success : function(datav) {
      $('#update-carr').html(datav);
    }
  });
});