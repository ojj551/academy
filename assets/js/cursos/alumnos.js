$('#add').click(function(){
	var alm = $('#user-select').val();
	var nm = $( "#user-select option:selected" ).attr('nm');
	var crs = $( "#user-select option:selected" ).attr('crs');
	if(alm!=""){
		$('.miform').show();
		$('.paint-users').append('<h4 class="user-add" v="'+alm+'" crs="'+crs+'">'+nm+' <span class="red dlt-itm"><i class="fa fa-times" aria-hidden="true"></i><span></h4>');
		$('.dlt-itm').click(function(){
			$(this).parent().remove();
		});
	}else{
		$('.miform').hide();
		$('.paint-users').html('');
	}
});
$('#add-usersbd').click(function(){
	var items = [];
	$.each( $('.user-add'), function( key, value ) {
		var data = {};
		data.v = $(value).attr('v');
		data.c = $(value).attr('crs');
		items.push(data);
	});
	$.ajax({
	      type:'POST',
	      url : base_url+'materiasfunc/addalumnos',
	      data : {arry_item:items},
	      //processData: false,  // tell jQuery not to process the data
	      //contentType: false,  // tell jQuery not to set contentType
	      beforeSend:function(){
	        $('.capa-white').fadeIn(600);
	      },
	      success : function(vl) {
	        $('.capa-white').fadeOut(600);
	     	location.reload();
	      }
  	});
});