$('#updatebannerdata').click(function(){
	var idmat = $('#idmat').val();
	var file = $('#bannermupt').prop('files');
	var colorbmupt = $('#colorbmupt').val();
	var banner;

	if(file!=undefined){
		var typefile = file[0].type;
		if(typefile=="image/png" || typefile=="image/jpg" || typefile=="image/jpeg"){
			//if(file[0].size<=60000){
				var formData = new FormData();
				formData.append('img', $('#bannermupt')[0].files[0]);
				formData.append('color', colorbmupt);
				formData.append('id', idmat);
			//}
		}
	}else{
		var formData = new FormData();
		formData.append('img', "");
		formData.append('color', colorbmupt);
		formData.append('id', idmat);
	}
	$.ajax({
        url : base_url+'materiasfunc/updatebanner',
       	type : 'POST',
       	data : formData,
       	processData: false,  // tell jQuery not to process the data
       	contentType: false,  // tell jQuery not to set contentType
       	beforeSend:function(){
		},
		success : function(data) {
			//location.reload();
		}
	});
});

$('#updatedecrpdata').click(function(){
	var idmat = $('#idmat').val();
	var reqmupd = $('#reqmupd').val();
	var objmupd = $('#objmupd').val();

	if(idmat!="" && reqmupd!="" && objmupd!=""){
		$.ajax({
			type:'POST',
	       	url : base_url+'materiasfunc/updatedescription',
	       	data: 'reqmupd='+encodeURIComponent(reqmupd)+'&idmat='+encodeURIComponent(idmat)+'&objmupd='+encodeURIComponent(objmupd),
			beforeSend:function(){
				$('#edit-description-mat').modal('hide');
				$('.capaload').fadeIn(600);
			},
			success : function(datav) {
				setTimeout(function(){

					if(datav=="success"){
						location.reload();
					}else{
						$('.capaload').fadeOut(600);
						$.dialog({
							title: '¡Ups! tuvimos problemas para actualizar tus datos',
							content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
						});
					}
				},1000);
			}
		});
	}
});

$('#guardar_design').click(function(){
	var idmat = $('#idmat').val();
	var fontsize = $('#fontsize').val();
	var color1 = $('#color1').val();
	var color2 = $('#color2').val();
	var color3 = $('#color3').val();
	
	$.ajax({
		type:'POST',
       	url : base_url+'materiasfunc/update_design',
       	data: 'idmat='+encodeURIComponent(idmat)+'&fontsize='+fontsize+'&color1='+color1+'&color2='+color2+'&color3='+color3,
		beforeSend:function(){
			$('.capaload').fadeIn(600);
		},
		success : function(datav) {
			setTimeout(function(){

				if(datav=="success"){
					location.reload();
				}else{
					$('.capaload').fadeOut(600);
					/*
					$.dialog({
						title: '¡Ups! tuvimos problemas para actualizar tus datos',
						content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
					});*/
				}
			},1000);
		}
	});
});