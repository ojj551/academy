$('#create-section').click(function(){ 
  var nameunidad = $('#nameunidad').val();
  var MatSt = $('#MatSt').val();
  $('#idsct').val('');
  if(nameunidad!=""){
      $.ajax({
      type:'POST',
      url : base_url+'materiasfunc/createsetion',
      data: 'MatSt='+encodeURIComponent(MatSt)+'&name='+encodeURIComponent(nameunidad),
      beforeSend:function(){
        $('#add-topic-unidad').modal('hide');
        $('.capaload').fadeIn(600);
      },
      success : function(data) {
        setTimeout(function(){
          if(data!="error"){
            location.reload();
          }else{
            $('.capaload').fadeOut(600);
            $.alert({
              title: '¡Ups! tuvimos problemas para generar la unidad',
              content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
              confirm: function () {
                  $('#add-topic-unidad').modal('show');
              }
            });
          }
        },1000);
      }
    });
  }
});
//Editar leccion
$('.edit-lesson').change(function() {
    var c = $(this);
    $.when(c.focusout()).then(function() {
        var v = c.attr('v');
    var name = c.val();
    if(v!="" && name!=""){
      $.ajax({
          type:'POST',
          url : base_url+'materiasfunc/editname',
          data : 't='+encodeURIComponent(v)+'&temaname='+name,
          beforeSend:function(){
            $('.capa-white').fadeIn(600);
          },
          success : function(vl) {
            if(vl=='success'){
              setTimeout(function(){ $('.capa-white').fadeOut(600); }, 300);
              
            }
          }
      });
    }
    });
});
//Agregar leccion
$(".div-lecciones").on("keypress", ".lesson", function(e){
  var input = $(this);
  if(e.which == 13) {
    var vlt = $(this).val();
    if(vlt!=""){
      var c = $(this).attr('v');
      var u = $(this).attr('u');
      var p = $(this).attr('p');
      $.ajax({
          type:'POST',
          url : base_url+'materiasfunc/addlesson',
          data : 'c='+encodeURIComponent(c)+'&temaname='+vlt+'&u='+encodeURIComponent(u)+'&p='+encodeURIComponent(p),
          beforeSend:function(){
            $('.capa-white').fadeIn(600);
          },
          success : function(vl) {
            $('.capa-white').fadeOut(600);
            location.reload();
            //editar
            
          }
      });

    }
  }
});

//ver sub temas
$('.vpaint').click(function(){
  $('.allpaint').html('');
  var v = $(this).attr('v');
  var shw = $(this).attr('shw');
  $.ajax({
    type:'POST',
    url : base_url+'materiasfunc/subtopics',
    data : 'v='+encodeURIComponent(v),
    beforeSend:function(){
      $('.capa-white').fadeIn(600);
    },
    success : function(vl) {
      
      setTimeout(function(){ 
        $('.capa-white').fadeOut(600);
        $('.paint_'+shw).html(vl); 

        //Editar leccion
        $('.edit-lesson').change(function() {
            var c = $(this);
            $.when(c.focusout()).then(function() {
                var v = c.attr('v');
            var name = c.val();
            if(v!="" && name!=""){
              $.ajax({
                  type:'POST',
                  url : base_url+'materiasfunc/editname',
                  data : 't='+encodeURIComponent(v)+'&temaname='+name,
                  beforeSend:function(){
                    $('.capa-white').fadeIn(600);
                  },
                  success : function(vl) {
                    if(vl=='success'){
                      setTimeout(function(){ $('.capa-white').fadeOut(600); }, 300);
                      
                    }
                  }
              });
            }
            });
        });
        
      }, 300);
        
    
    }
  });
});

//Re-ordenar temas
$(function() {
  $(".sortable").sortable({
      stop: function( ) {
        var items = [];
        $.each($('ul.sortable > li'), function( index, value ) {
          var data = {};
          data.v = $(value).attr('v');
          data.o = $(value).attr('o');
          items.push(data);
      //console.log($(value).attr('v'));
    });
    $.ajax({
          type:'POST',
          url : base_url+'materiasfunc/reordentopics',
          data : {arry_item:items},
          //processData: false,  // tell jQuery not to process the data
          //contentType: false,  // tell jQuery not to set contentType
          beforeSend:function(){
            $('.capa-white').fadeIn(600);
          },
          success : function(vl) {
            $('.capa-white').fadeOut(600);
         
          }
      });
        //console.log(items);
        //alert('cambio');
      }
  });
  $(".sortable").disableSelection();
});