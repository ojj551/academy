$("#typealum1").on('change', function() {
    var typeusr = this.value;
    if(typeusr!=""){
      $.ajax({
          type:'POST',
          url : base_url+'alumnosfunc/TypeUser',
          data: 'typeusr='+encodeURIComponent(typeusr),
          beforeSend:function(){
          },
          success: function(data){
              $('.paintbytype').html(data);
              //Generar ID
              $('.generateidreference').click(function(){
                  $.ajax({
                    type:'POST',
                    url : base_url+'alumnosfunc/createreference',
                    data: 'generate=1',
                    beforeSend:function(){
                      $('#addalumnos-form').modal('hide');
                      $('.capaload').fadeIn(600);
                    },
                    success : function(data) {
                      setTimeout(function(){
                        if(data!="error"){
                          $('#addalumnos-form').modal('show');
                          $('.capaload').fadeOut(600);
                          $('#idreference1').val(data);
                          $( "#emailalumn" ).focus();
                        }else{
                          $('.capaload').fadeOut(600);
                          $.alert({
                            title: '¡Ups! tuvimos problemas para generar tu ID Referencia',
                            content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
                            confirm: function () {
                                $('#addalumnos-form').modal('show');
                            }
                          });
                        }
                      },1000);
                    }
                  });
              });
              //Semestres por materia
              $(".carrera").on('change', function() {
                var vlcar = this.value
                var vl = $('#cant').val();
                if(vl!=""){
                  $.ajax({
                      type:'POST',
                      url : base_url+'alumnosfunc/SemestreCarrera',
                      data: 'vlcar='+encodeURIComponent(vlcar),
                      beforeSend:function(){
                      },
                      success: function(data){
                          $('#semestre1').html(data);
                      }
                  });
                }
              });
          }
      });
    }
  });
//
$('.generateidreference').click(function(){
    $.ajax({
      type:'POST',
      url : base_url+'alumnosfunc/createreference',
      data: 'generate=1',
      beforeSend:function(){
        $('#addalumnos-form').modal('hide');
        $('.capaload').fadeIn(600);
      },
      success : function(data) {
        setTimeout(function(){
          if(data!="error"){
            $('#addalumnos-form').modal('show');
            $('.capaload').fadeOut(600);
            $('#idreference1').val(data);
            $( "#emailalumn" ).focus();
          }else{
            $('.capaload').fadeOut(600);
            $.alert({
              title: '¡Ups! tuvimos problemas para generar tu ID Referencia',
              content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
              confirm: function () {
                  $('#addalumnos-form').modal('show');
              }
            });
          }
        },1000);
      }
    });
});
//Crear
$('#createalumnos').click(function(){
    var client1 = $('#client1').val();
    var idref = $('#idreference1').val();
    var email = $('#emailalumn1').val();
    var typealm = $('#typealum1').val();

    var carrera = $('#carrera1').val();
    var semestre = $('#semestre1').val();

    if(idref!="" && email!="" && typealm!=""){
      var emailok;
      var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
      if (regex.test($('#emailalumn1').val().trim())) {
        emailok = 1;
      }else{
        emailok = 0;
      }
      if(emailok==1){
        $.ajax({
            type: "POST",
            url : base_url+'alumnosfunc/addalumno',
            data: 'ctmr='+encodeURIComponent(client1)+'&idref='+encodeURIComponent(idref)+'&email='+encodeURIComponent(email)+'&typealm='+encodeURIComponent(typealm)+'&carrera='+carrera+'&semestre='+semestre, 
            beforeSend:function(){
              $('#addalumnos-form').modal('hide');
              $('.capaload').fadeIn(600);
            },
            success: function(data){
              $('.capaload').fadeOut(600);
              if(data=="success"){
                //$(location).attr('href', base_url+'grupos');
                  location.reload();
              }else{
                $.alert({
                  title: 'Tuvimos problemas al crear algunos alumnos',
                  content: '<strong>Detalles: </strong>'+data,
                  confirm: function () {
                      //$(location).attr('href', base_url+'grupos');
                      location.reload();
                  }
                });
              }
            }
        });
      }else{
        $.dialog({
          title: '<b class="red"><i class="fa fa-exclamation" aria-hidden="true"></i> Campo de correo invalido</b>',
          content: '',
          type: 'red',
         
        });
      }
    }else{
      $.alert({
        title: '<b class="red"><i class="fa fa-exclamation" aria-hidden="true"></i> Campos incompletos</b>',
        content: '<span class="medium">Los campos marcados con <b class="medium red">*</b> son obligatorios</span>',
        type: 'red',
        confirm: function () {
            //$(location).attr('href', base_url+'grupos');
        }
      });
    }
});
//Asignar cursos a prof
$('#addmattoprof').click(function(){
  var ctm = $('#ctm').val();
  var mat = $('#matprof').val();
  var prof = $('#profmat').val();
  var star = $('#star').val();
  var end = $('#end').val();
  if(mat!="" && prof!="" && end > star){
    $.ajax({
        type: "POST",
        url : base_url+'alumnosfunc/addmattoprof',
        data: 'ctm='+encodeURIComponent(ctm)+'&mat='+encodeURIComponent(mat)+'&prof='+encodeURIComponent(prof)+'&star='+star+'&end='+end, 
        beforeSend:function(){
          $('#addalumnos-form').modal('hide');
          $('.capaload').fadeIn(600);
        },
        success: function(data){
          $('.capaload').fadeOut(600);
          if(data=="success"){
            location.reload();
          }else{
            $.alert({
              title: 'Ups',
              content: data,
              confirm: function () {
                //location.reload();
              }
            });
          }
        }
    });
  }else{
      if(mat==""){
          $.dialog({
              title: 'Verificar',
              content: 'El campo de selecci&oacute;n no tiene valor seleccionado',
              confirm: function () {
              }});
      }else if(end < star){
          $.dialog({
              title: 'Verificar',
              content: 'Favor de revisar las fechas',
              confirm: function () {
              }});
          $('alertAddMatSelectDate').text('Favor de revisar las fechas');
      }
  }
});
//Asignamer usuarios
$('#addsubordn').click(function(){
  var ctm = $('#ctm').val();
  var almtoasig = $('#almtoasig').val(); 
  var usr = $('#usr').val();
  if(almtoasig!="" && ctm!="" && usr!=""){
    $.ajax({
        type: "POST",
        url : base_url+'alumnosfunc/asgme',
        data: 'ctm='+encodeURIComponent(ctm)+'&almtoasig='+encodeURIComponent(almtoasig)+'&usr='+encodeURIComponent(usr), 
        beforeSend:function(){
          $('#addalumnos-form').modal('hide');
          $('.capaload').fadeIn(600);
        },
        success: function(data){
          $('.capaload').fadeOut(600);
          if(data=="success"){
            location.reload();
          }else{
            $.alert({
              title: 'Ups',
              content: data,
              confirm: function () {
                //location.reload();
              }
            });
          }
        }
    });
  }
});
//
$('#addsubordntouser').click(function(){
  var ctm = $('#ctm').val();
  var almtoasig = $('#touser').val(); 
  var usr = $('#fromusr').val();
  if(almtoasig!="" && ctm!="" && usr!=""){
    $.ajax({
        type: "POST",
        url : base_url+'alumnosfunc/asgme',
        data: 'ctm='+encodeURIComponent(ctm)+'&almtoasig='+encodeURIComponent(almtoasig)+'&usr='+encodeURIComponent(usr), 
        beforeSend:function(){
          $('#addalumnos-form').modal('hide');
          $('.capaload').fadeIn(600);
        },
        success: function(data){
          $('.capaload').fadeOut(600);
          if(data=="success"){
            location.reload();
          }else{
            $.alert({
              title: 'Ups',
              content: data,
              confirm: function () {
                //location.reload();
              }
            });
          }
        }
    });
  }
});
//
$('#fromusr').on('change', function() {
  var ctm = $('#ctm').val();
  var userid = this.value;
  if(userid){
    $.ajax({
      type:'POST',
      url : base_url+'alumnosfunc/SubUsers',
      data: 'userid='+encodeURIComponent(userid)+'&ctm='+ctm,
      beforeSend:function(){
      },
      success: function(data){
        $('#touser').html(data);
      }
    });
  }
});
//actualizar usuarios