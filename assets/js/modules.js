$('#save-module').click(function(){
	var name = $('#name').val();
	var clase = $('#clase').val();
	var font = $('#font').val();
	var padre = $('#padre').val();
	if(name!="" && font!="" && padre!=""){
		var datos = $('#form').serialize();
		$.ajax({
	    	type:'POST',
	       	url : base_url+'modulosfunc/addModule',
	       	data: datos,
			beforeSend:function(){
				$('.capmodal').show();
				$('.sending').show();
			},
			success : function(data) {
				$('.capmodal').hide();
				$('.sending').hide();
				if(data=="error"){
	           		$.alert({
		           		theme: 'material',
					    title: 'Tuvimos problemas al crear el modúlo',
					    content: 'Intentalo nuevamente y de ser continuo el error, :( revisa el codigo'
					});
	           	}else{
		           	$.alert({
		           		theme: 'material',
					    title: '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Modúlo agregado',
					    content: 'El modúlo se creó correctamente.',
					    confirm: function(){
					        location.reload();
					    }
					});
	           }
			}
		});
	}else{
		$.alert({
       		theme: 'material',
		    title: '<i class="fa fa-exclamation" aria-hidden="true"></i> Campos incompletos',
		    content: 'Asegurate de llenar todos los campos para poder continuar.',
		    confirm: function(){
		    }
		});
	}
});