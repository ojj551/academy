<?php
function random_code($random_code_min = 6, $random_code_max = 15)
{
//$random_code_min=10;// minimum length of password
//$random_code_max=15;// maximum length of password
    $random_code_pwd = "";   //to store generated password
    for ($random_code_i = 0; $random_code_i < rand($random_code_min, $random_code_max); $random_code_i++) {
        $random_code_num = rand(48, 122);
        if (($random_code_num > 97 && $random_code_num < 122)) {

            $random_code_pwd .= chr($random_code_num);

        } else if (($random_code_num > 65 && $random_code_num < 90)) {

            $random_code_pwd .= chr($random_code_num);

        } else if (($random_code_num > 48 && $random_code_num < 57)) {

            $random_code_pwd .= chr($random_code_num);

        } else if ($random_code_num == 95) {

            $random_code_pwd .= chr($random_code_num);

        } else {
            $random_code_i--;
        }
    }
    return preg_replace("[^A-Za-z0-9]", "", $random_code_pwd);
}

//clase para encriptar y desencriptar contraseña
class BN_EnDe
{

    /*

    $ende = new BN_EnDe('secret key');
    $e01e = base64_encode($ende->encrypt('hola'));
    $e02d = $ende->decrypt(base64_decode($e01e));

     */

    var $key;
    var $data;

    var $td;
    var $iv;
    var $init = false;

    function BN_EnDe($key = '', $data = '')
    {
        if ($key != '') {
            $this->init($key);
        }
        $this->data = $data;
    }

    function init(&$key)
    {
        $this->td = mcrypt_module_open('des', '', 'ecb', '');
        $this->key = substr($key, 0, mcrypt_enc_get_key_size($this->td));
        $iv_size = mcrypt_enc_get_iv_size($this->td);
        $this->iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $this->init = true;
    }

    function setKey($key)
    {
        $this->init($key);
    }

    function setData($data)
    {
        $this->data = $data;
    }

    function & getKey()
    {
        return $this->key;
    }

    function & getData()
    {
        return $this->data;
    }

    function & encrypt($data = '')
    {
        return $this->_crypt('encrypt', $data);
    }

    function & decrypt($data = '')
    {
        return $this->_crypt('decrypt', $data);
    }

    function close()
    {
        mcrypt_module_close($this->td);
    }

    function & _crypt($mode, &$data)
    {
        if ($data != '') {
            $this->data = $data;
        }

        if ($this->init) {
            if (mcrypt_generic_init($this->td, $this->key, $this->iv) != -1) {
                if ($mode == 'encrypt') {
                    $this->data = mcrypt_generic($this->td, $this->data);
                } elseif ($mode == 'decrypt') {
                    $this->data = mdecrypt_generic($this->td, $this->data);
                }

                mcrypt_generic_deinit($this->td);

                return $this->data;
            } else {
                trigger_error('Error initialising ' . $mode . 'ion handle', E_USER_ERROR);
            }
        } else {
            trigger_error('Key not set. Use setKey() method', E_USER_ERROR);
        }
    }
}

//Codigo de activación de cuenta
function BN_guid($namespace = '')
{
    static $guid = '';
    $uid = uniqid("", true);
    $data = $namespace;
    $data .= $_SERVER['REQUEST_TIME'];
    $data .= $_SERVER['HTTP_USER_AGENT'];
    $data .= $_SERVER['SERVER_ADDR'];
    $data .= $_SERVER['SERVER_PORT'];
    $data .= $_SERVER['REMOTE_ADDR'];
    $data .= $_SERVER['REMOTE_PORT'];
    $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
    $guid = '' .
        substr($hash, 0, 8) .

        substr($hash, 8, 4) .

        substr($hash, 12, 4) .

        substr($hash, 16, 4) .

        substr($hash, 20, 12) .
        '';
    return $guid;
}

function RandomString()
{
    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
    srand((double)microtime() * 1000000);
    $i = 0;
    $pass = '';

    while ($i <= 7) {
        $num = rand() % 33;
        $tmp = substr($chars, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }

    return $pass;
}

function random_color_part()
{
    return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
}

function random_color()
{
    return random_color_part() . random_color_part() . random_color_part();
}

function encodeURIComponent($str)
{
    $revert = array('%21' => '!', '%2A' => '*', '%27' => "'", '%28' => '(', '%29' => ')');
    return strtr(rawurlencode($str), $revert);
}

function send_recovery_password($name, $email, $code)
{
    /*
    ini_set("SMTP","smtp.gmail.com");
    ini_set("smtp_port","465");
    ini_set("sendmail_from","contacto@xpertcad.com");
    $subject = '¡Ingresa a XpertCAD!';
    $message = '
          <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta name="viewport" content="width=device-width" />

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>ZURBemails</title>



            </head>
            <style>
                * {
                    margin:0;
                    padding:0;
                }
                * { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

                img {
                    max-width: 100%;
                }
                .collapse {
                    margin:0;
                    padding:0;
                }
                body {
                    -webkit-font-smoothing:antialiased;
                    -webkit-text-size-adjust:none;
                    width: 100%!important;
                    height: 100%;
                }

                a { color: #2BA6CB;}

                table.social {
                    background-color: #ebebeb;

                }
                .social .soc-btn {
                    padding: 3px 7px;
                    font-size:12px;
                    margin-bottom:10px;
                    text-decoration:none;
                    color: #FFF;font-weight:bold;
                    display:block;
                    text-align:center;
                }
                a.fb { background-color: #3B5998!important; }
                a.tw { background-color: #1daced!important; }
                a.gp { background-color: #DB4A39!important; }
                a.ms { background-color: #000!important; }

                .sidebar .soc-btn {
                    display:block;
                    width:100%;
                }

                table.body-wrap { width: 100%;}


                table.footer-wrap { width: 100%;    clear:both!important;
                }
                .footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
                .footer-wrap .container td.content p {
                    font-size:10px;
                    font-weight: bold;

                }


                h1,h2,h3,h4,h5,h6,a,p {
                font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
                }
                h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

                h1 { font-weight:200; font-size: 44px;}
                h2 { font-weight:200; font-size: 37px;}
                h3 { font-weight:500; font-size: 27px;}
                h4 { font-weight:500; font-size: 23px;}
                h5 { font-weight:900; font-size: 17px;}
                h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

                .collapse { margin:0!important;}

                p, ul {
                    margin-bottom: 10px;
                    font-weight: normal;
                    font-size:14px;
                    line-height:1.6;
                }
                p.lead { font-size:17px; }
                p.last { margin-bottom:0px;}

                ul li {
                    margin-left:5px;
                    list-style-position: inside;
                }

                ul.sidebar {
                    background:#ebebeb;
                    display:block;
                    list-style-type: none;
                }
                ul.sidebar li { display: block; margin:0;}
                ul.sidebar li a {
                    text-decoration:none;
                    color: #666;
                    padding:10px 16px;

                    margin-right:10px;

                    cursor:pointer;
                    border-bottom: 1px solid #777777;
                    border-top: 1px solid #FFFFFF;
                    display:block;
                    margin:0;
                }
                ul.sidebar li a.last { border-bottom-width:0px;}
                ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}

                .container {
                    display:block!important;
                    max-width:600px!important;
                    margin:0 auto!important;
                    clear:both!important;
                }

                .content {
                    padding:15px;
                    max-width:600px;
                    margin:0 auto;
                    display:block;
                }

                .content table { width: 100%; }


                .column {
                    width: 300px;
                    float:left;
                }
                .column tr td { padding: 15px; }
                .column-wrap {
                    padding:0!important;
                    margin:0 auto;
                    max-width:600px!important;
                }
                .column table { width:100%;}
                .social .column {
                    width: 280px;
                    min-width: 279px;
                    float:left;
                }

                .clear { display: block; clear: both; }


                @media only screen and (max-width: 600px) {

                    a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

                    div[class="column"] { width: auto!important; float:none!important;}

                    table.social div[class="column"] {
                        width:auto!important;
                    }

                }
            </style>
            <body bgcolor="#e2e2e2">


            <table class="body-wrap">
                <tr>
                    <td></td>
                    <td class="container" bgcolor="#1f2858">

                        <div class="content">
                        <table >
                            <tr><td align="center"><img src="logodemo.png" style="width: 260px;"></td></tr>
                        </table>
                        <table>
                            <tr>
                                <td>

                                    <h3 style="color: #FFFFFF;">Hola '.$name.',</h3>
                                    <p style="color: #FFFFFF;" class="lead">No te quedes sin tu plataforma de aprendizaje.</p>


                                    <p><img src="3.gif" /></p>

                                    <table class="social" width="100%">
                                        <tr>
                                            <td>

                                                <table align="left" class="column">
                                                    <tr>
                                                        <td>

                                                            <h5 class="">Recupera tu contraseña</h5>
                                                            <p class=""><a href="'.base_url().'login/recoverypassword/?c='.$code.'" class="soc-btn fb">Click aquí</a> </p>


                                                        </td>
                                                    </tr>
                                                </table>

                                                <table align="left" class="column">
                                                    <tr>
                                                        <td>

                                                            <h5 class="">Dudas:</h5>
                            Email: <strong><a href="emailto:hseldon@trantor.com">contacto@xpertcad.com</a></strong></p>

                                                        </td>
                                                    </tr>
                                                </table>

                                                <span class="clear"></span>

                                            </td>
                                        </tr>
                                    </table>


                                </td>
                            </tr>
                        </table>
                        </div>

                    </td>
                    <td></td>
                </tr>
            </table>

            <table class="footer-wrap">
                <tr>
                    <td></td>
                    <td class="container">

                            <div class="content">
                            <table>
                            <tr>
                                <td align="center">
                                    <p>
                                        <a href="#">Terminos y condiciones</a> |
                                        <a href="#">Privacidad</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                            </div>

                    </td>
                    <td></td>
                </tr>
            </table>

            </body>
            </html>
  ';

   $headers  = 'MIME-Version: 1.0' . "\r\n";
   $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
   $headers .= "To: ".$email."\r\n";
   $headers .= "From: contacto@xpertcad.com" . "\r\n";

   $mail_sent = @mail($email, $subject, $message, $headers );
   if($mail_sent){
        return true;
   }else{
        return false;
   }*/
    //Ahorita que no sirve enviar correo
    $message = '
          <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta name="viewport" content="width=device-width" />

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>ZURBemails</title>
                


            </head>
            <style>
                * { 
                    margin:0;
                    padding:0;
                }
                * { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

                img { 
                    max-width: 100%; 
                }
                .collapse {
                    margin:0;
                    padding:0;
                }
                body {
                    -webkit-font-smoothing:antialiased; 
                    -webkit-text-size-adjust:none; 
                    width: 100%!important; 
                    height: 100%;
                }

                a { color: #2BA6CB;}

                table.social {
                    background-color: #ebebeb;
                    
                }
                .social .soc-btn {
                    padding: 3px 7px;
                    font-size:12px;
                    margin-bottom:10px;
                    text-decoration:none;
                    color: #FFF;font-weight:bold;
                    display:block;
                    text-align:center;
                }
                a.fb { background-color: #3B5998!important; }
                a.tw { background-color: #1daced!important; }
                a.gp { background-color: #DB4A39!important; }
                a.ms { background-color: #000!important; }

                .sidebar .soc-btn { 
                    display:block;
                    width:100%;
                }

                table.body-wrap { width: 100%;}


                table.footer-wrap { width: 100%;    clear:both!important;
                }
                .footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
                .footer-wrap .container td.content p {
                    font-size:10px;
                    font-weight: bold;
                    
                }


                h1,h2,h3,h4,h5,h6,a,p {
                font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
                }
                h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

                h1 { font-weight:200; font-size: 44px;}
                h2 { font-weight:200; font-size: 37px;}
                h3 { font-weight:500; font-size: 27px;}
                h4 { font-weight:500; font-size: 23px;}
                h5 { font-weight:900; font-size: 17px;}
                h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

                .collapse { margin:0!important;}

                p, ul { 
                    margin-bottom: 10px; 
                    font-weight: normal; 
                    font-size:14px; 
                    line-height:1.6;
                }
                p.lead { font-size:17px; }
                p.last { margin-bottom:0px;}

                ul li {
                    margin-left:5px;
                    list-style-position: inside;
                }

                ul.sidebar {
                    background:#ebebeb;
                    display:block;
                    list-style-type: none;
                }
                ul.sidebar li { display: block; margin:0;}
                ul.sidebar li a {
                    text-decoration:none;
                    color: #666;
                    padding:10px 16px;

                    margin-right:10px;

                    cursor:pointer;
                    border-bottom: 1px solid #777777;
                    border-top: 1px solid #FFFFFF;
                    display:block;
                    margin:0;
                }
                ul.sidebar li a.last { border-bottom-width:0px;}
                ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}

                .container {
                    display:block!important;
                    max-width:600px!important;
                    margin:0 auto!important; 
                    clear:both!important;
                }

                .content {
                    padding:15px;
                    max-width:600px;
                    margin:0 auto;
                    display:block; 
                }

                .content table { width: 100%; }


                .column {
                    width: 300px;
                    float:left;
                }
                .column tr td { padding: 15px; }
                .column-wrap { 
                    padding:0!important; 
                    margin:0 auto; 
                    max-width:600px!important;
                }
                .column table { width:100%;}
                .social .column {
                    width: 280px;
                    min-width: 279px;
                    float:left;
                }

                .clear { display: block; clear: both; }


                @media only screen and (max-width: 600px) {
                    
                    a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

                    div[class="column"] { width: auto!important; float:none!important;}
                    
                    table.social div[class="column"] {
                        width:auto!important;
                    }

                }
            </style>
            <body bgcolor="#e2e2e2">


            <table class="body-wrap">
                <tr>
                    <td></td>
                    <td class="container" bgcolor="#1f2858">

                        <div class="content">
                        <table >
                            <tr><td align="center"><img src="http://adecuellar.com/images/logodemo.png" style="width: 260px;"></td></tr>
                        </table>
                        <table>
                            <tr>
                                <td>
                                    
                                    <h3 style="color: #FFFFFF;">Hola ' . $name . ',</h3>
                                    <p style="color: #FFFFFF;" class="lead">No te quedes sin tu plataforma de aprendizaje.</p>
                                    

                                    <p><img src="http://adecuellar.com/images/3.gif" /></p>

                                    <table class="social" width="100%">
                                        <tr>
                                            <td>

                                                <table align="left" class="column">
                                                    <tr>
                                                        <td>                
                                                            
                                                            <h5 class="">Recupera tu contraseña</h5>
                                                            <p class=""><a href="' . base_url() . 'login/recoverypassword/?' . $code . '" class="soc-btn fb">Click aquí</a> </p>
                                    
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                                <table align="left" class="column">
                                                    <tr>
                                                        <td>                
                                                                                        
                                                            <h5 class="">Dudas:</h5>                                                
                            Email: <strong><a href="emailto:hseldon@trantor.com">contacto@xpertcad.com</a></strong></p>
                            
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                                <span class="clear"></span> 
                                                
                                            </td>
                                        </tr>
                                    </table>
                                
                                
                                </td>
                            </tr>
                        </table>
                        </div>
                                                
                    </td>
                    <td></td>
                </tr>
            </table>

            <table class="footer-wrap">
                <tr>
                    <td></td>
                    <td class="container">
                        
                            <div class="content">
                            <table>
                            <tr>
                                <td align="center">
                                    <p>
                                        <a href="#">Terminos y condiciones</a> |
                                        <a href="#">Privacidad</a> 
                                    </p>
                                </td>
                            </tr>
                        </table>
                            </div>
                            
                    </td>
                    <td></td>
                </tr>
            </table>

            </body>
            </html>
  ';
    return $message;
}

function groupArray($array, $groupkey)
{
    if (count($array) > 0) {
        $keys = array_keys($array[0]);
        $removekey = array_search($groupkey, $keys);
        if ($removekey === false)
            return array("Clave \"$groupkey\" no existe");
        else
            unset($keys[$removekey]);
        $groupcriteria = array();
        $return = array();
        foreach ($array as $value) {
            $item = null;
            foreach ($keys as $key) {
                $item[$key] = $value[$key];
            }
            $busca = array_search($value[$groupkey], $groupcriteria);
            if ($busca === false) {
                $groupcriteria[] = $value[$groupkey];
                $return[] = array($groupkey => $value[$groupkey], 'groupeddata' => array());
                $busca = count($return) - 1;
            }
            $return[$busca]['groupeddata'][] = $item;
        }
        return $return;
    } else
        return array();
}

///**************Textos********************//
function texto($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {

            if ($subv['ValorType'] == 'textarea') {

                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 padding0" style="margin-bottom:10px;">
                    <label>' . $subv['Type'] . '</label>
                    <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px; ' . $bg . '">' . $subv['Valor'] . '</textarea>
                </div>   
                ';
            }

            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }
    return $contenido;
}

function texto2($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="text" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';


            }


            if ($subv['ValorType'] == 'textarea') {

                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 padding0" style="margin-bottom:10px;">
                    <label>' . $subv['Type'] . '</label>
                    <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px; ' . $bg . '">' . $subv['Valor'] . '</textarea>
                </div>   
                ';
            }

            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }
    return $contenido;
}

function texto_centrado($arr)
{
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {

            $setting = json_decode($subv['Setting']);
            $bg = $setting[0]->background;
            $pdg = $setting[0]->padding;
            $alg = $setting[0]->aling;
            $clr = $setting[0]->color;

            //return $setting[0]->background;

            if ($subv['Type'] == 'description') {
                $contenido = '

                
                <div class="col-xs-12 col-sm-12 col-lg-12" style="background:' . $bg . '; padding:' . $pdg . '; text-align:' . $alg . ';">
          
                        <h1 class="tlt-tmp_txt-heading editor_edit itm' . $subv['OrdenSection'] . '" v="' . $subv['DocEntry'] . '" style="color:' . $clr . ';">' . $subv['Valor'] . '</h1>
                   
                </div>
            
                ';
            }
        }
    }
    return $contenido;
}

function default_well($arr)
{
    $contenido = '<div class="col-xs-12 col-sm-12 col-lg-12 margen_templates"><div class="">';
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'title') {
                $contenido .= '
                
                <h4 class="editor_edit itm' . $subv['OrdenSection'] . '" style="font-size: 30px;"  v="' . $subv['DocEntry'] . '">' . $subv['Valor'] . '</h4>

                ';
            }
            if ($subv['Type'] == 'description') {
                $contenido .= '<label class="editor_edit itm' . $subv['OrdenSection'] . ' texto-tabs" v="' . $subv['DocEntry'] . '">' . $subv['Valor'] . '</label>';
            }
        }
    }
    $contenido .= '</div></div>';
    return $contenido;
}

function two_columns($arr)
{
    $contenido = '<div class="col-xs-12 col-sm-12 col-lg-12">';
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            $contenido .= '
                <div class="col-xs-6 col-sm-12 col-lg-6 padding0">
                    <label style="font-size:18px;">' . $subv['Valor'] . '</label>
                </div>
            ';
        }
    }
    $contenido .= '</div>';
    return $contenido;

}

function texto_simple($arr)
{
    $contenido = '<div class="col-xs-12 col-sm-12 col-lg-12"><div class="">';
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'description') {
                $contenido .= '
                    <label class="editor_edit itm' . $subv['OrdenSection'] . '" v="' . $subv['DocEntry'] . '" style="font-size: 18px;">' . $subv['Valor'] . '</label>
                ';
            }
        }
    }
    $contenido .= '</div></div>';
    return $contenido;
}

function texto_izquierda($arr)
{
    $contenido = '<div class="col-xs-12 col-sm-12 col-lg-12"><div class="container">';
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'description') {
                $contenido .= '
                    <h1 class="tlt-tmp_txt-heading editor_edit itm' . $subv['OrdenSection'] . '" v="' . $subv['DocEntry'] . '">' . $subv['Valor'] . '</h1>
                ';
            }
        }
    }
    $contenido .= '</div></div>';
    return $contenido;
}

function texto_derecha($arr)
{
    $contenido = '<div class="col-xs-12 col-sm-12 col-lg-12 text-right"><div class="">';
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'description') {
                $contenido .= '
                    <h1 class="tlt-tmp_txt-heading editor_edit itm' . $subv['OrdenSection'] . '" v="' . $subv['DocEntry'] . '">' . $subv['Valor'] . '</h1>
                ';
            }
        }
    }
    $contenido .= '</div></div>';
    return $contenido;
}

function embed_justificado($arr)
{
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'description') {
                $contenido = '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0">
                        <div class="col-xs-12 col-sm-12 col-lg-12 enunciados1-div">
                            <div class="col-xs-2 col-sm-12 col-lg-2 padding0"></div>
                            <div class="col-xs-8 col-sm-12 col-lg-8 text-justify">
                                <label style="font-size:23px;font-weight: bold;" class="editor_edit itm' . $subv['OrdenSection'] . '" v="' . $subv['DocEntry'] . '">' . $subv['Valor'] . '</label>
                            </div>
                        </div>
                    </div>
                ';
            }
        }
    }
    return $contenido;
}

function enunciado_linea($arr)
{
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'description') {
                $contenido = '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0">
                        <div class="col-xs-12 col-sm-12 col-lg-12 enunciados1-div">
                            <div class="col-xs-2 col-sm-12 col-lg-2 padding0"></div>
                            <div class="col-xs-8 col-sm-12 col-lg-8 text-justify">
                                <div class="col-xs-4 col-sm-12 col-lg-4 padding0"></div>
                                <div class="col-xs-4 col-sm-12 col-lg-4 tmp-line-enununciados2"></div>
                                <div class="col-xs-12 col-sm-12 col-lg-12 tmp-des-enununciados2">
                                    <label class="fontsize_unu12 editor_edit itm' . $subv['OrdenSection'] . '"  v="' . $subv['DocEntry'] . '">' . $subv['Valor'] . '</label>
                                </div>
                            </div>
                        </div>
                    </div>
                ';
            }
        }
    }
    return $contenido;
}

function enunciado_palabras($arr)
{
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'description') {
                $contenido = '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0">
                        <div class="col-xs-12 col-sm-12 col-lg-12 enunciados2-div">
                            <div class="col-xs-2 col-sm-12 col-lg-2 padding0"></div>
                            <div class="col-xs-8 col-sm-12 col-lg-8 text-left">
                                <label class="editor_edit itm' . $subv['OrdenSection'] . '"  v="' . $subv['DocEntry'] . '" style="font-size:27px;">' . $subv['Valor'] . '</label>
                            </div>
                        </div>
                    </div>
                ';
            }
        }
    }
    return $contenido;
}

function cita_left($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        $subv = $value['groupeddata'];
        $contenido .= '
        <div class="col-md-12 col-sm-12 col-lg-12 ">
            <div class="col-md-2 col-sm-12 col-lg-2"></div>
            <div class="col-md-8 col-sm-12 col-lg-8 text-center">
        ';
        if ($subv[1]['Type'] == 'description') {
            $contenido .= '
                <div class="col-md-4 col-sm-12 col-lg-4"></div>
                <div class="col-md-4 col-sm-12 col-lg-4 line-quoq"></div>
                <h1 class="editor_edit itm' . $subv[1]['OrdenSection'] . '" v="' . $subv[1]['DocEntry'] . '">' . $subv[1]['Valor'] . '</h1>
            ';
        }
        if ($subv[0]['Type'] == 'title') {
            $contenido .= '
            <div class="col-md-12 col-sm-12 col-lg-12 text-right">
                <h4 class="editor_edit itm' . $subv[0]['OrdenSection'] . '" v="' . $subv[0]['DocEntry'] . '"><b>-' . $subv[0]['Valor'] . '</b></h4>
            </div>
            ';
        }
        $contenido .= '
            </div>
        </div>
        ';
    }
    /*
    $contenido = '<div class="col-xs-12 col-sm-12 col-lg-12"><div class="container">';
    foreach ($arr as $key => $value) {
        $subv = $value['groupeddata'];

            if($subv[1]['Type']=='description'){
                $contenido .= '
                    <label class="editor_edit itm'.$subv[1]['OrdenSection'].'"  v="'.$subv[1]['DocEntry'].'" style="font-size:18px;">'.$subv[1]['Valor'].'</label>
                ';
            }
            if($subv[0]['Type']=='title'){
                $contenido .= '<div class="text-left"><cite class="editor_edit itm'.$subv[0]['OrdenSection'].'"  v="'.$subv[0]['DocEntry'].'">'.$subv[0]['Valor'].'</cite></div>';
            }

    }
    $contenido .= '</div></div>';
    */
    return $contenido;
}

function cita_right($arr)
{
    $contenido = '<div class="col-xs-12 col-sm-12 col-lg-12">';
    foreach ($arr as $key => $value) {
        $subv = $value['groupeddata'];
        $contenido .= '
            <div class="col-xs-4 col-sm-12 col-lg-4"></div>
            <div class="col-xs-4 col-sm-12 col-lg-4">
            ';
        if ($subv[1]['Type'] == 'description') {
            $contenido .= '
                    <div class="text-right">
                    <label class="editor_edit itm' . $subv[1]['OrdenSection'] . '"  v="' . $subv[1]['DocEntry'] . '" style="font-size:18px;">' . $subv[1]['Valor'] . '</label></div>
                ';
        }
        if ($subv[0]['Type'] == 'title') {
            $contenido .= '<div class="text-right"><b><cite class="editor_edit itm' . $subv[0]['OrdenSection'] . '"  v="' . $subv[0]['DocEntry'] . '">' . $subv[0]['Valor'] . '</b></div>';
        }
        $contenido .= '</div>';

    }
    $contenido .= '</div>';
    return $contenido;
}

///*************Progreso*************************///
//Progreso 1
function progreso1($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="text" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';


            }
            if ($subv['ValorType'] == 'file') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="file" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';
            }

            if ($subv['ValorType'] == 'textarea') {

                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 padding0" style="margin-bottom:10px;">
                    <label>' . $subv['Type'] . '</label>
                    <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px; ' . $bg . '">' . $subv['Valor'] . '</textarea>
                </div>   
                ';
            }

            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }
    return $contenido;
}

//Progreso 1 (template)
function progreso_1($arr)
{

    $TemplateContent = '
        <div style="margin-top: 20px;margin-bottom: 20px;">
        <div class="swiper-container swpprogress">
        <div class="swiper-wrapper">
    ';
    $content = '';
    foreach ($arr as $key => $value) {
        $content .= '<div class="swiper-slide" style="padding-left:60px;padding-right:60px;"><div class="col-xs-12 col-sm-12 col-lg-12"><div class="">';

        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'title') {
                $content .= '
                    <h1 class="editor_edit itm' . $subv['OrdenSection'] . '" v="' . $subv['DocEntry'] . '">' . $subv['Valor'] . '</h1><br>
                ';
            }
            if ($subv['Type'] == 'description') {
                $content .= '
                    <label class="editor_edit itm' . $subv['OrdenSection'] . '" v="' . $subv['DocEntry'] . '" style="font-size:21px;">' . $subv['Valor'] . '</label>
                ';
            }
            if ($subv['Type'] == 'imagen') {
                $content .= '
                <div class="text-center">
                    <img src="' . site_url('../files/' . $subv['Valor']) . '"style="width:50%;">
                </div>
                ';
            }
            if ($subv['Type'] == 'video') {
                $content .= '
                <video style="width:50%;height:50%;margin:0 auto;">
                    <source src="' . site_url('../files/' . $subv['Valor']) . '" type="video/mp4">
                </video>
                ';
            }

        }
        $content .= '</div></div></div>';

    }

    $TemplateContent .= '
        
        ' . $content . '
        
        </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-next"><h1 class="cl_primario"><i class="fa fa-chevron-right" aria-hidden="true"></i><h1></div>
            <div class="swiper-button-prev"><h1 class="cl_primario"><i class="fa fa-chevron-left" aria-hidden="true"></i></h1></div>
        </div>
        </div>
    ';
    return $TemplateContent;

}

///*************Video***************************///
//Video 1
function video1($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'file') {
                $contenido .= '
                    <div class="">
                      <input type="file" class="form-control" id="' . $subv['Type'] . $value['Orden'] . '" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                    </div>
                    ';

            }
            if ($subv['ValorType'] == 'radio') {
                $inputs = '';
                if ($subv['Valor'] == 'c') {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio1" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="c" checked>
                        <span class="outer">
                          <span class="inner"></span></span> Chico
                    </label>
                    ';
                } else {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio1" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="c">
                        <span class="outer">
                          <span class="inner"></span></span> Chico
                    </label>
                    ';
                }
                if ($subv['Valor'] == 'm') {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio2" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="m" checked>
                        <span class="outer">
                          <span class="inner"></span></span> Mediano
                    </label>
                    ';
                } else {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio2" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="m">
                        <span class="outer">
                          <span class="inner"></span></span> Mediano
                    </label>
                    ';
                }
                if ($subv['Valor'] == 'g') {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio2" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="g" checked>
                        <span class="outer">
                          <span class="inner"></span></span> Grande
                    </label>
                    ';
                } else {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio2" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="g">
                        <span class="outer">
                          <span class="inner"></span></span> Grande
                    </label>
                    ';
                }
                $contenido .= '
                <div id="myForm">
                    <label>Tamaño del video</label><br>
                    <div class="form-animate-radio">
                        
                        ' . $inputs . '
                        
                    </div>
                </div>  
                ';
            }

            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }
    return $contenido;
}

//video  1 (template)
function video_1($arr)
{
    $contenido = '
    <div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top:10px;margin-bottom:10px;padding-top: 30px;padding-bottom: 20px;">
    
    ';
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'tam') {
                if ($subv['Valor'] == 'c') {
                    $wd = "50%";
                    $ht = "100%";
                    $lf = "25%";
                }
                if ($subv['Valor'] == 'm') {
                    $wd = "70%";
                    $ht = "100%";
                    $lf = "15%";
                }
                if ($subv['Valor'] == 'g') {
                    $wd = "100%";
                    $ht = "100%";
                    $lf = "0%";
                }

                $contenido .= '
                <script>
                    $(".cont-video").width("' . $wd . '");
                    $(".cont-video").height("' . $ht . '");
                    $(".cont-video").css("left","' . $lf . '");
                </script>';
            }
            $contenido .= '<div class="cont-video" style="position: relative;">';
            if ($subv['Type'] == 'media') {
                $contenido .= '
                <video style="width:100%;height:100%;">
                    <source src="' . site_url('../files/' . $subv['Valor']) . '" type="video/mp4">
                </video>';
            }
            $contenido .= '</div>';
        }
    }
    $contenido .= '
    </div>
    ';
    return $contenido;
}

//Video 2
function video2($arr)
{

    $contenido = '
    <div class="col-md-12 panel text-center">
        
    ';
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {

                $contenido .= '
                <div class="form-group form-animate-text">
                  <input type="text" class="form-text" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '">
                  <span class="bar"></span>
                  <label>Pega la url del video</label>
                </div>
                ';
            }
            if ($subv['ValorType'] == 'radio') {
                $inputs = '';
                if ($subv['Valor'] == 'c') {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio1" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="c" checked>
                        <span class="outer">
                          <span class="inner"></span></span> Chico
                    </label>
                    ';
                } else {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio1" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="c">
                        <span class="outer">
                          <span class="inner"></span></span> Chico
                    </label>
                    ';
                }
                if ($subv['Valor'] == 'm') {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio2" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="m" checked>
                        <span class="outer">
                          <span class="inner"></span></span> Mediano
                    </label>
                    ';
                } else {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio2" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="m">
                        <span class="outer">
                          <span class="inner"></span></span> Mediano
                    </label>
                    ';
                }
                if ($subv['Valor'] == 'g') {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio2" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="g" checked>
                        <span class="outer">
                          <span class="inner"></span></span> Grande
                    </label>
                    ';
                } else {
                    $inputs .= '
                    <label class="radio">
                        <input id="radio2" type="radio" name="' . $subv['Type'] . $value['Orden'] . '" value="g">
                        <span class="outer">
                          <span class="inner"></span></span> Grande
                    </label>
                    ';
                }
                $contenido .= '
                <div id="myForm">
                    <label>Tamaño del video</label><br>
                    <div class="form-animate-radio">
                        
                        ' . $inputs . '
                        
                    </div>
                </div>  
                ';
            }

            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido;
}

//Video 2 (template)
function video_2($arr)
{

    $contenido = '<iframe ';

    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'url') {
                $expld = explode("?v=", $subv['Valor']);
                $url = "https://www.youtube.com/embed/" . $expld[1];

                $contenido .= '
                    src="' . $url . '"
                ';
            }
            if ($subv['Type'] == 'tam') {
                if ($subv['Valor'] == 'c') {
                    $style = "position: relative;width:50%;height:300px;left:25%;";
                }
                if ($subv['Valor'] == 'm') {
                    $style = "position: relative;width:70%;height:500px;left:15%;";
                }
                if ($subv['Valor'] == 'g') {
                    $style = "position: relative;width:100%;height:900px;";
                }
                $contenido .= 'style="' . $style . '"';
            }

        }
    }
    $contenido .= '></iframe>';

    return $contenido;
}

//Video vimeo (template)
function video_3($arr)
{

    $contenido = '<iframe ';

    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'url') {
                $expld = explode("https://vimeo.com/", $subv['Valor']);
                $url = $expld[1];

                $contenido .= '
                    src="https://player.vimeo.com/video/' . $url . '"
                ';
            }
            if ($subv['Type'] == 'tam') {
                if ($subv['Valor'] == 'c') {
                    $style = "position: relative;width:50%;height:300px;left:25%;border: 0px;";
                }
                if ($subv['Valor'] == 'm') {
                    $style = "position: relative;width:70%;height:500px;left:15%;border: 0px;";
                }
                if ($subv['Valor'] == 'g') {
                    $style = "position: relative;width:100%;height:900px;border: 0px;";
                }
                $contenido .= 'style="' . $style . '"';
            }

        }
    }
    $contenido .= '></iframe>';

    return $contenido;
}

///*************Linea del tiempo***************************///
//Línea 1
function linea1($arr)
{
    $contenido = '';

    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">' . $subv['Valor'] . '</textarea>';

            }
            if ($subv['ValorType'] == 'date') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">' . $subv['Valor'] . '</textarea>';
            }

            if ($subv['ValorType'] == 'textarea') {

                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 padding0" style="margin-bottom:10px;">
                    <label>' . $subv['Type'] . '</label>
                    <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px; ' . $bg . '">' . $subv['Valor'] . '</textarea>
                </div>   
                ';
            }

            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }
    return $contenido;
}

//Línea 1(template)
function linea_1($arr)
{

    $TemplateContent = '
        <div class="col-md-12 padding0">
            <div class="panel">
                <div class="panel-heading"><h3>Línea del tiempo</h3></div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <ul class="timeline">
    ';
    $content = '';
    foreach ($arr as $key => $value) {
        if ($key % 2 == 0) {
            $direction = '';

        } else {
            $direction = 'timeline-inverted';
        }
        $content .= '<li class="' . $direction . '">';

        foreach ($value['groupeddata'] as $subv) {
            $content .= '
                <div class="timeline-badge bg_primario"></div>
                <div class="timeline-panel">
                <div class="timeline-heading">
            ';
            if ($subv['Type'] == 'title') {
                $content .= '
                    <h4 class="timeline-title editor_edit itm' . $subv['OrdenSection'] . '" v="' . $subv['DocEntry'] . '">' . $subv['Valor'] . '</h4>
                ';
            }
            if ($subv['Type'] == 'date') {
                $content .= '
                    
                        <small class="text-muted"><i class="glyphicon glyphicon-time"></i></small><label class="editor_edit itm' . $subv['OrdenSection'] . '" v="' . $subv['DocEntry'] . '"> ' . $subv['Valor'] . '</label>
                    
                ';
            }
            //$content .= '</div>';
            if ($subv['Type'] == 'description') {
                $content .= '
                    <div class="timeline-body">
                      <span class="editor_edit itm' . $subv['OrdenSection'] . '" v="' . $subv['DocEntry'] . '">' . $subv['Valor'] . '</span>
                    </div>
                ';
            }
            $content .= '</div>';

        }
        $content .= '</li>';

    }

    $TemplateContent .= '
        ' . $content . '
                </ul>
                    </div>
                </div>
            </div>
        </div>
    ';
    return $TemplateContent;
}

///*************Complementos***************************///
//Embed
function embed1($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                    <div class="col-md-12">
                    <div class="col-md-12">
                        <h4>Puedes agregar contenido web de las siguientes plataformas:</h4>
                        <span><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</span>
                        <span><i class="fa fa-soundcloud" aria-hidden="true"></i> Soundcloud</span>
                        <span><i class="fa fa-youtube" aria-hidden="true"></i> Youtube</span>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group form-animate-text" style="margin-top:40px !important;">
                            <input type="text" class="form-text mask-date" id="embed" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '">
                            <span class="bar"></span>
                            <label>Pega la url</label>
                        </div>
                    </div>
                </div>
                ';

            }


            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido;
}

//puntos sencibles
function puntoschck($imgvl, $arrg)
{
    $contenido = '
        <label>Selecciona imagen</label>
        <input type="file" name="imgpunto" /><br>
        <hr>
        ';
    foreach ($arrg as $value) {

        foreach ($value['groupeddata'] as $subv) {


            $contenido .= '
            <label>Titulo</label>
            <input type="text" class="form-control" name="title' . $value['Orden'] . '" value="' . $subv['Title'] . '">
            <label>Descripción</label>
            <textarea class="animate form-construnctor editor_edit" name="description' . $value['Orden'] . '" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px;">' . $subv['Description'] . '</textarea>
            ';


        }
    }

    return $contenido;
}

//**************Listas*******************************///
//Lista 1
function lista1($arr)
{
    $contenido = '';

    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0 cnt' . $subv['Type'] . $value['Orden'] . '" style="margin-bottom:10px;">
                        <label>' . $subv['Type'] . '</label>
                        <input type="text" class="form-control editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                    </div>
                    ';
            }
            if ($subv['ValorType'] == 'date') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="text" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';
            }

            if ($subv['ValorType'] == 'textarea') {

                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 padding0" style="margin-bottom:10px;">
                    <label>' . $subv['Type'] . '</label>
                    <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px; ' . $bg . '">' . $subv['Valor'] . '</textarea>
                </div>   
                ';

            }

            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido;
}

function lista_1($arr)
{
    $TemplateContent = '
        <div class="col-md-12 tabs-area ">
        <ul id="tabs-demo5" class="nav nav-tabs nav-tabs-v4" role="tablist">
    ';
    $ul = '';
    $cont = 1;
    foreach ($arr as $key => $value) {
        if ($cont == 1) {
            $activado = 'active';
        } else {
            $activado = '';
        }
        $ul .= '<li role="presentation" class="' . $activado . '">';

        foreach ($value['groupeddata'] as $subv) {

            if ($subv['Type'] == 'description') {
                $ul .= '
                    <a href="#tabs-demo5-area' . $cont . '" id="tabs-demo5-' . $cont . '" role="tab" data-toggle="tab" aria-expanded="true"><span class="">' . $cont . '</span></a>
                ';
            }
            $cont++;
        }
        $ul .= '</li>';

    }

    $TemplateContent .= '
        
        ' . $ul . '
        </ul>
        <div id="tabsDemo5Content" class="tab-content tab-content-v4">
    ';
    $content = '';
    $cont2 = 1;
    foreach ($arr as $key => $value) {
        if ($cont2 == 1) {
            $activado = 'active in';
        } else {
            $activado = '';
        }
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'description') {
                $content .= '
                    <div role="tabpanel" class="tab-pane fade ' . $activado . '" id="tabs-demo5-area' . $cont2 . '" aria-labelledby="tabs-demo5-area' . $cont2 . '">
                        <label style="font-size: 17px;">' . $subv['Valor'] . '</label>
                    </div>
                ';
            }
            $cont2++;
        }
    }
    $TemplateContent .= $content . '
        </div>
    </div>
    ';

    return $TemplateContent;
}

//Lista 2
function lista_2($arr)
{
    $TemplateContent = '
        <div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 20px;margin-bottom: 20px;">
            <div class="container">
    ';
    $content = '';
    $cont = 1;
    foreach ($arr as $key => $value) {
        $content .= '<div class="col-xs-12 col-sm-12 col-lg-12" style="padding:20px;">';

        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'description') {
                $content .= '
                    <div class="col-xs-3 col-sm-12 col-lg-3 text-center div-list2 bg_primario">
                        <strong>' . $cont . '</strong>
                    </div>
                    <div class="col-xs-9 col-sm-12 col-lg-9">
                        <label style="font-size: 17px;">' . $subv['Valor'] . '</label>
                    </div>
                ';
            }

        }
        $content .= '</div>';
        $cont++;
    }

    $TemplateContent .= '
        
        ' . $content . '
        </div>
        </div>
    ';
    return $TemplateContent;
}

//****************IMAGENES***********************////
function imagen1($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0 cnt' . $subv['Type'] . $value['Orden'] . '" style="margin-bottom:10px;">
                        <label>' . $subv['Type'] . '</label>
                        <input type="text" class="form-control" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                    </div>
                    ';
            }
            if ($subv['ValorType'] == 'file') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="file" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';
            }

            if ($subv['ValorType'] == 'textarea') {

                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 padding0" style="margin-bottom:10px;">
                    <label>' . $subv['Type'] . '</label>
                    <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px; ' . $bg . '">' . $subv['Valor'] . '</textarea>
                </div>   
                ';

            }

            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }
    return $contenido;
}

function imagen_1($arr)
{
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'media') {
                $contenido = '
                <div class="text-center">
                    <img src="' . site_url('../files/' . $subv['Valor']) . '"style="position:relative;width:70%;margin:0 auto;">
                </div>
                ';
            }
        }
    }
    return $contenido;
}

function imagen_2($arr)
{
    $contenido = '<div class="col-xs-12 col-sm-12 col-lg-12 img-and-text">';
    foreach ($arr as $key => $value) {
        $subv = $value['groupeddata'];
        //foreach ($value['groupeddata'] as $keysb => $subv) {

        if ($subv[1]['Type'] == 'media') {
            $contenido .= '
                <div class="col-xs-6 col-sm-12 col-lg-6 padding0">
                    <div class="text-center">
                        <img src="' . site_url('../files/' . $subv[1]['Valor']) . '"style="width:80%;">
                    </div>
                </div>
                ';
        }

        if ($subv[0]['Type'] == 'title') {
            $sbcontent = '';
            if ($subv[0]['Type'] == 'title') {
                $sbcontent .= '<h3>' . $subv[0]['Valor'] . '</h3>';
            }
            if(isset($subv[2])) {
                if ($subv[2]['Type'] == 'description') {
                    $sbcontent .= '<label class="texto-tabs"><p>' . $subv[2]['Valor'] . '</p> </label>';
                }
            }else{
                $sbcontent .= '<label class="texto-tabs"><p> </p> </label>';
            }
            $contenido .= '
                <div class="col-xs-6 col-sm-12 col-lg-6" style="padding-right: 0px;">
                    ' . $sbcontent . '
                </div>
                ';
        }
        //}

    }
    $contenido .= '</div>';
    return $contenido;
}

function imagen_3($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        //$subv = $value['groupeddata'];
        $img = '';
        $text = '';
        foreach ($value['groupeddata'] as $keysb => $subv) {

            if ($subv['Type'] == 'media') {
                $img = site_url("../files/" . $subv['Valor']);
            }
            if ($subv['Type'] == 'title') {
                $text .= $subv['Valor'];
            }
            if ($subv['Type'] == 'description') {
                $text .= $subv['Valor'];
            }

        }
        $contenido .= '
            <div class="col-xs-12 col-sm-12 col-lg-12 tmpl-img-bg" style="background: url(' . $img . ');">
                <div class="cap-tmpl-img-bg">
                    <div class="col-xs-6 col-sm-12 col-lg-6 subdiv-tmpl-img">
                        <div class="col-xs-8 col-sm-12 col-lg-8 sub-subdiv-tmpl-img"></div>
                        <label class="descrp-bg-img">
                            ' . $text . '
                        </label>
                    </div>
                </div>
            </div>
            ';


    }
    return $contenido;
}

//****************GALERIA***********************////
function galeria1($arr)
{
    $contenido = '';
    $galery = '';

    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0 cnt' . $subv['Type'] . $value['Orden'] . '" style="margin-bottom:10px;">
                        <label>' . $subv['Type'] . '</label>
                        <input type="text" class="form-control editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                    </div>
                    ';
            }
            if ($subv['ValorType'] == 'file') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="file" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';
                $contenido .= '
                    <img src="' . site_url('../files/' . $subv['Valor']) . '"style="width:20%;">
                ';
            }

            if ($subv['ValorType'] == 'textarea') {

                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 padding0" style="margin-bottom:10px;">
                    <label>' . $subv['Type'] . '</label>
                    <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px; ' . $bg . '">' . $subv['Valor'] . '</textarea>
                </div>   
                ';


            }

            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido . '<div class="col-xs-12 col-sm-12 col-lg-12 padding0">' . $galery . '</div>';
}

function galeria_1($arr)
{

    $TemplateContent = '
    <div class="col-md-2 col-sm-12 col-xs-12"></div>
    <div class="col-md-8 col-sm-12 col-xs-12">
        <div class="panel">
            <div class="panel-heading"><center><h3>Galería</h3></center></div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div>
                <div class="swiper-container swgalery">
                <div class="swiper-wrapper">
    ';
    $content = '';
    foreach ($arr as $key => $value) {

        //$content .= '<div class="swiper-slide" style="background:url('.site_url('../files/'.$subv['Valor']).')no-repeat 100% 100%;background-size:cover;height:500px;">';
        $content .= '<div class="swiper-slide" >';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'media') {
                $content .= '
                    <img src="' . site_url('../files/' . $subv['Valor']) . '" style="position:relative;width:70%;height:70%;left:15%;">
                ';
            }
        }
        $content .= '</div>';

    }

    $TemplateContent .= '
        
        ' . $content . '
        
        </div>
            <div class="swiper-pagination"></div>
        </div>
        </div></div></div></div>
    ';
    return $TemplateContent;
}

function galeria_2($arr)
{

    $TemplateContent = '
    <div class="col-md-2 col-sm-12 col-xs-12"></div>
    <div class="col-md-8 col-sm-12 col-xs-12">
        <div class="panel">
            <div class="panel-heading"><center><h3>Galería</h3></center></div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div>
                <div class="swiper-container swipertres">
                <div class="swiper-wrapper">
    ';
    $content = '';
    foreach ($arr as $key => $value) {

        $content .= '<div class="swiper-slide" >';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'media') {
                $content .= '
                    <img src="' . site_url('../files/' . $subv['Valor']) . '" style="width:100%;height:100%;">
                ';
            }
        }
        $content .= '</div>';

    }

    $TemplateContent .= '
        
        ' . $content . '
        
        </div>
            <div class="swiper-pagination"></div>
        </div>
        </div></div></div></div>
    ';
    return $TemplateContent;
}

/*
function galeria_2($arr){
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if($subv['Type']=='media'){
                $contenido = '
                <div class="text-center">
                    <img src="'.site_url('../files/'.$subv['Valor']).'"style="width:80%;">
                </div>
                ';
            }
        }
    }
    return $contenido;
}
*/
//****************SCORM***********************////
function scorm($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0 cnt' . $subv['Type'] . $value['Orden'] . '" style="margin-bottom:10px;">
                        <label>' . $subv['Type'] . '</label>
                        <input type="text" class="form-control" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                    </div>
                    ';
            }
            if ($subv['ValorType'] == 'file') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="file" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';
            }


            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido;
}

function scorm_t($arr)
{
    $contenido = '
    <div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top:10px;margin-bottom:10px;padding-top: 30px;padding-bottom: 20px;">
    
    ';
    foreach ($arr as $key => $value) {
        $NameScorm = $value['groupeddata'][0]['Valor'];
        $Url = $value['groupeddata'][1]['Valor'];
        $contenido .= '
                <iframe src="' . site_url('../files/' . $Url . '/' . $NameScorm . '.html') . '" height="550" style="width:100%;"></iframe>';
    }
    $contenido .= '
    </div>
    ';
    return $contenido;
}

function scorm_site($arr)
{
    $contenido = '
    <div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top:10px;margin-bottom:10px;padding-top: 30px;padding-bottom: 20px;">
    ';
    foreach ($arr as $key => $value) {
        $NameScorm = $value['groupeddata'][0]['Valor'];
        $Url = $value['groupeddata'][1]['Valor'];
        $contenido .= '
                <iframe src="' . site_url('../files/' . $Url . '/scormcontent/index.html') . '" height="1050" style="width:100%;"></iframe>';
    }
    $contenido .= '
    </div>
    ';
    return $contenido;
}

//****************MULTIMEDIA***********************////
//Audio
function audio1($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0 cnt' . $subv['Type'] . $value['Orden'] . '" style="margin-bottom:10px;">
                        <label>' . $subv['Type'] . '</label>
                        <input type="text" class="form-control" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                    </div>
                    ';
            }
            if ($subv['ValorType'] == 'file') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="file" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';
            }


            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido;
}

function audio_1($arr)
{
    $contenido = '
    <div class="col-xs-12 col-sm-12 col-lg-12 wdt-mtl">
    
    ';
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'title') {
                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 title-text-audio">
                    <h3>' . $subv['Valor'] . '</h3>
                </div>
                ';
            }
            if ($subv['Type'] == 'media') {
                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <audio src="' . site_url('../files/' . $subv['Valor']) . '"></audio>
                </div>
                ';
            }
        }
    }
    $contenido .= '
    </div>
    ';
    return $contenido;
}

//Archivo
function archivo1($arr)
{
    $contenido = '';

    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0 cnt' . $subv['Type'] . $value['Orden'] . '" style="margin-bottom:10px;">
                        <label>' . $subv['Type'] . '</label>
                        <input type="text" class="form-control" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                    </div>
                    ';
            }
            if ($subv['ValorType'] == 'file') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="file" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';
            }


            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido;
}

function archivo_1($arr)
{
    $img_file = site_url('assets/img/file_icon.png');
    $contenido = '

    <div class="col-md-12 col-sm-12 col-lg-12 border-tmpl-file wdt-mtl">
        <div class="col-md-4 col-sm-12 col-lg-4">
            <img src="' . $img_file . '" />
        </div>
        <div class="col-md-8 col-sm-12 col-lg-8">
    ';
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'title') {
                $contenido .= '<h4>' . $subv['Valor'] . '</h4>';
            }
            if ($subv['Type'] == 'media') {
                $contenido .= '
                    <a href="' . site_url('../files/' . $subv['Valor']) . '" target="_blank">Ver/Descargar archivo</a>
                ';
            }
        }
    }
    $contenido .= '</div></div>';
    return $contenido;
}

//****************INTERACTIVO***********************//
function acordion($arr)
{
    $contenido = '';

    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                if ($subv['Type'] != "url") {
                    $contenido .= '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0 cnt' . $subv['Type'] . $value['Orden'] . '" style="margin-bottom:10px; ' . $bg . '">
                        <label>' . $subv['Type'] . '</label>
                        <input type="text" class="form-control" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                    </div>
                    ';
                }
            }
            if ($subv['ValorType'] == 'date') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="text" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';
            }

            if ($subv['ValorType'] == 'textarea') {

                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 padding0" style="margin-bottom:10px;">
                    <label>' . $subv['Type'] . '</label>
                    <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px; ' . $bg . '">' . $subv['Valor'] . '</textarea>
                </div>   
                ';

            }
            if ($subv['ValorType'] == 'file') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="file" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';
            }

            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido;
}

function acordion_tabs($arr)
{
    $contenido = '';

    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                if ($subv['Type'] != "url") {
                    $contenido .= '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0 cnt' . $subv['Type'] . $value['Orden'] . '" style="margin-bottom:10px; ' . $bg . '">
                        <label>' . $subv['Type'] . '</label>
                        <input type="text" class="form-control" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                    </div>
                    ';
                }
            }


            if ($subv['ValorType'] == 'textarea') {

                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 padding0" style="margin-bottom:10px;">
                    <label>' . $subv['Type'] . '</label>
                    <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px; ' . $bg . '">' . $subv['Valor'] . '</textarea>
                </div>   
                ';

            }
            if ($subv['ValorType'] == 'file') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="file" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';
            }

            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido;
}

function acordion_t($arr)
{
    $TemplateContent = '
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

    ';
    $ul = '';
    $cont = 1;
    foreach ($arr as $key => $value) {
        if ($cont == 1) {
            $activado = 'in';
        } else {
            $activado = '';
        }
        $ul .= '<div class="card panel panel-default">';
        $subv = $value['groupeddata'];
        //foreach ($value['groupeddata'] as $subv) {
        if (count($subv) >= 1) {
            if ($subv[0]['Type'] == 'title') {
                //if($subv['Type']!="url"){
                $ul .= '
                            <div class="card-header panel-heading" role="tab" id="heading' . $cont . '">
                              <h5 class="mb-0 panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse' . $cont . '" aria-expanded="true" aria-controls="collapse' . $cont . '">
                                  ' . $subv[0]['Valor'] . '
                                </a>
                              </h5>
                            </div>
                        ';
                //}
            }

            if (count($subv) >= 2) {
                $ul .= '
                <div id="collapse' . $cont . '" class="panel-collapse collapse ' . $activado . '" role="tabpanel" aria-labelledby="heading' . $cont . '">
                  <div class="card-block panel-body"> 
                ';
                if (count($subv) >= 2) {
                    if ($subv[1]['Type'] == 'description') {
                        $ul .= '<label class="texto-tabs">' . $subv[1]['Valor'] . '</label>';
                    }
                }
                if (count($subv) >= 3) {
                    if ($subv[2]['Type'] == 'imagen') {
                        $expl = explode(".", $subv[2]['Valor']);

                        $ul .= '<img src="' . site_url('../files/' . $subv[2]['Valor']) . '"style="position:relative;width:30%;left:35%;">';

                    }
                    if ($subv[2]['Type'] == 'video') {
                        $ul .= '
                        <div class="col-lg-3 col-md-3 col-sm-12"></div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <video style="position:relative;width:100% !important;height:auto !important;">
                                <source src="' . site_url('../files/' . $subv[2]['Valor']) . '" type="video/mp4">
                            </video>
                        </div>
                        ';
                    }
                    if ($subv[2]['Type'] == 'audio') {
                        $ul .= '
                        <audio src="' . site_url('../files/' . $subv[2]['Valor']) . '"></audio>
                        ';
                    }
                    if ($subv[2]['Type'] == 'download') {
                        $ul .= '
                        <a href="' . site_url('../files/' . $subv[2]['Valor']) . '" target="_blank">Ver archivo</a>
                        ';
                    }
                }
                $ul .= '</div>
                </div>';
            }

            $cont++;
            $ul .= '</div>';
        }

    }
    $TemplateContent .= $ul . '</div>';


    return $TemplateContent;
}

//tabs
function tabs_t($arr)
{
    $TemplateContent = '
        <div class="col-md-12 tabs-area ">
        <ul id="tabs-demo4" class="nav nav-tabs tabs_tmpl1" role="tablist">
    ';
    $ul = '';
    $cont = 1;
    foreach ($arr as $key => $value) {
        if ($cont == 1) {
            $activado = 'active';
            $lett_act = 'cl_primario';
        } else {
            $activado = '';
            $lett_act = '';
        }
        $ul .= '<li role="presentation" class=" ' . $activado . '">';

        foreach ($value['groupeddata'] as $subv) {

            if ($subv['Type'] == 'title') {
                $ul .= '
                    <a href="#tabs-demo4-area' . $cont . '" id="tabs-demo4-' . $cont . '" role="tab" data-toggle="tab" aria-expanded="true" class="listas-ltt cl_secundario"><span>' . $subv['Valor'] . '</span></a>
                ';
            }

        }
        $cont++;
        $ul .= '</li>';

    }

    $TemplateContent .= '
        
        ' . $ul . '
        </ul>
        <div id="tabsDemo5Content" class="tab-content tab-content-v4">
    ';
    $content = '';
    $cont2 = 1;
    foreach ($arr as $key => $value) {
        if ($cont2 == 1) {
            $activado = 'active in';
        } else {
            $activado = '';
        }
        $content .= '<div role="tabpanel" class="tab-pane fade ' . $activado . '" id="tabs-demo4-area' . $cont2 . '" aria-labelledby="tabs-demo4-area' . $cont2 . '">';
        foreach ($value['groupeddata'] as $subv) {


            if ($subv['Type'] == 'description') {
                $content .= '
                    
                        <label class="texto-tabs">' . $subv['Valor'] . '</label>
                    
                ';
            }
            /*
            if($subv['Type']=='media'){
                $content .= '
                <div class="text-center">
                    <img class="img-tabs" src="'.site_url('../files/'.$subv['Valor']).'">
                </div>
                ';
            }*/
            if ($subv['Type'] == 'imagen') {
                $content .= '
                <div class="text-center">
                    <img src="' . site_url('../files/' . $subv['Valor']) . '"style="width:50%;">
                </div>
                ';
            }
            if ($subv['Type'] == 'video') {
                $content .= '
                <div class="col-lg-3 col-md-3 col-sm-12"></div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <video style="width:100% !important;height:auto !important;">
                        <source src="' . site_url('../files/' . $subv['Valor']) . '" type="video/mp4">
                    </video>
                </div>
                ';
            }


        }
        $content .= '</div>';
        $cont2++;
    }
    $TemplateContent .= $content . '
        </div>
    </div>
    ';

    return $TemplateContent;
}

//puntos sensibles
function puntos_t($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        $imgbg = '';

        $modals = '<div class="speech-bubble vwm' . $key . ' text-left" ><a class="close-dc-point"><i class="fa fa-times" aria-hidden="true"></i></a>';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'position') {
                $expl = explode(';', $subv['Valor']);
                $sbexpl = explode(':', $expl[0]);
                $sbexpl2 = explode(':', $expl[1]);
                $lf = $sbexpl[1] - 16.4;
                $tp = $sbexpl2[1] - 15.7;
                $contenido .= '<div class="points bg_primario" style="' . $subv['Valor'] . '" lf="' . $lf . '" tp="' . $tp . '" v="' . $key . '">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>';
            }
            if ($subv['Type'] == 'background') {
                $imgbg = site_url('../files/' . $subv['Valor']);
                $contenido .= '<div class="paddg-dv-puntos"><img class="imagenps" src="' . $imgbg . '" /></div>';
            }
            if ($subv['Type'] == 'title') {
                $modals .= '<h3 class="gris">' . $subv['Valor'] . '</h3>';
            }
            if ($subv['Type'] == 'description') {
                $modals .= '<label class="texto-tabs gris" style="display: block;">' . $subv['Valor'] . '</label>';
            }
            if ($subv['Type'] == 'video') {
                $modals .= '
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <video style="width:100% !important;height:auto !important;">
                        <source src="' . site_url('../files/' . $subv['Valor']) . '" type="video/mp4">
                    </video>
                </div>';
            }
            if ($subv['Type'] == 'audio') {
                $modals .= '
                <div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top:30px;margin-bottom:30px;">
                <h3>Audio</h3>
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <audio src="' . site_url('../files/' . $subv['Valor']) . '"></audio>
                </div>';
            }

        }

        $modals .= '</div>';
        $contenido .= $modals;

    }

    return $contenido;
}

//Flashcard grid ///////////////////PENDIENTE
function flashcardupdate($arr)
{
    $contenido = '';
    $actual_link = "http://$_SERVER[HTTP_HOST]";
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            $DocEntry = $subv['DocEntry'];
            $Type = $subv['Type'];
            $ValorType = $subv['ValorType'];

            $expl = explode('_', $Type);
            if ($Type[0] == 'f') {
                if ($expl[2] == 'a') {
                    if ($ValorType == 'textarea') {
                        $contenido .= '
                        <div class="">
                            <textarea class="editor_edit update-flashcd" name="' . $Type . '" style="min-height:120px;" vl="' . $DocEntry . '">' . $subv['Valor'] . '</textarea>
                        </div>
                        ';
                    }
                    if ($ValorType == 'multimedia') {
                        $img = $actual_link . '/files/' . $subv['Valor'];
                        $contenido .= '
                        <div class="" >
                            <input type="file update-flashcd"  name="' . $Type . '" vl="' . $DocEntry . '"/>
                            <span>' . $img . '</span>
                        </div>
                        ';
                    }
                    //$contenido .= 'Frontent '.$ValorType.'<br>';
                }

            }
            if ($Type[0] == 'b') {
                if ($expl[2] == 'a') {
                    if ($ValorType == 'textarea') {
                        $contenido .= '
                        <div class="">
                            <textarea class="editor_edit update-flashcd" name="' . $Type . '" style="min-height:120px;" vl="' . $DocEntry . '">' . $subv['Valor'] . '</textarea>
                        </div>
                        ';
                    }
                    if ($ValorType == 'multimedia') {
                        $img = $actual_link . '/files/' . $subv['Valor'];
                        $contenido .= '
                        <div class="" >
                            <input type="file" class="update-flashcd" name="' . $Type . '" vl="' . $DocEntry . '"/>
                            <span>' . $img . '</span>
                        </div>
                        ';
                    }
                    //$contenido .= 'Backend '.$ValorType.'<br>';
                }
            }
        }

    }
    return $contenido;
}

/*
function flashcard($arr){
    $contenido = '';
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if($key%2==0){
            $bg = 'background: #f1fdff;';
        }else{
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="'.$bg.'">';
        for ($ic=0; $ic < 2; $ic++) {
            if($ic==0){
                $msgtitle = 'Tarjeta de frente';
                $abrv = 'f';
            }else{
                $msgtitle = 'Tarjeta parte trasera';
                $abrv = 'b';
            }

            $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <h4>'.$msgtitle.'</h4>
                </div>
               <select class="form-control flash-crd">
                <option>Seleccionar...</option>
                <option value="'.$abrv.'_t_'.$value['Orden'].'">Texto</option>
                <option value="'.$abrv.'_m_'.$value['Orden'].'">Imagen</option>
               </select>
               ';
            foreach ($value['groupeddata'] as $subv) {

                if($subv['ValorType']=='textarea'){
                    $contenido .= '
                    <div class="">
                        <textarea class="editor_edit" style="min-height:120px;" name="'.$abrv.'_'.'t_'.$value['Orden'].'"></textarea>
                    </div>
                    ';
                }
                if($subv['ValorType']=='file'){
                    $contenido .= '
                    <div class="" >
                        <input type="file" name="'.$abrv.'_'.'m_'.$value['Orden'].'" />
                    </div>
                    ';
                }
                $dlt .= '&valor='.$subv['DocEntry'];
                if($subv['Valor']!=""){
                    $dvlors = 1;
                }

            }
            if($dvlors==1){
                $contenido .= '<div><a class="dlt-item" v="'.$dlt.'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
            }

        }
    }
    return $contenido;
}
*/
function flashcard_t($arr)
{
    $contenido = '<div class="row" style="padding-top: 40px; padding-bottom: 100px;padding-left:80px;padding-right:80px;">';
    $actual_link = "http://$_SERVER[HTTP_HOST]";
    foreach ($arr as $key => $value) {
        $contenido .= '
        <div class="col-sm-6 text-center" style="margin-top:15px;margin-bottom:15px;">      
        <div class="flip">
        <div class="card">';
        //$subv = $value['groupeddata'];
        foreach ($value['groupeddata'] as $subv) {
            $Type = $subv['Type'];
            $expl = explode('_', $Type);


            if ($expl[0] == 'f') {
                $contenido .= ' 
                <div class="face front" style="padding: 0px;">
                <div class="inner">';
                if ($expl[1] == 't') {
                    $contenido .= '<h1>' . $subv['Valor'] . '</h1>';
                }
                $contenido .= '</div></div>';
                if ($expl[1] == 'm') {
                    $img = $actual_link . '/files/' . $subv['Valor'];
                    $contenido .= '
                      <div class="face front" > 
                        <div class="inner text-center"> 
                          <img src="' . $img . '" style="width:60%;" />
                        </div>
                      </div>
                    ';
                }

            }
            if ($expl[0] == 'b') {
                $contenido .= ' 
                  <div class="face back" style="padding: 0px;"> 
                    <div class="inner"> ';
                if ($expl[1] == 't') {
                    $contenido .= '<h1>' . $subv['Valor'] . '</h1>';
                }
                $contenido .= '</div></div>';
                if ($expl[1] == 'm') {
                    $img = $actual_link . '/files/' . $subv['Valor'];
                    $contenido .= '
                      <div class="face back"> 
                        <div class="inner text-center"> 
                          <img src="' . $img . '" style="width:60%;" />
                        </div>
                      </div>
                    ';
                }

            }
        }
        $contenido .= '</div></div></div>';
    }
    $contenido .= '</div>';
    return $contenido;
    /*
    $contenido = '<div class="row" style="padding-top: 40px; padding-bottom: 100px;padding-left:80px;padding-right:80px;">';
    if(count($arr)==1){
        $grd = '12';
    }
    if(count($arr)==2){
        $grd = '6';
    }
    if(count($arr)>=3){
        $grd = '4';
    }

    foreach ($arr as $key => $value) {
        $contenido .= '<div class="col-sm-'.$grd.' text-center" style="margin-top:15px;margin-bottom:15px;">';

        $subv = $value['groupeddata'];

        $contenido .= '
          <div class="flip">
            <div class="card">';
            if($subv[0]['Type']=='f_media'){
                $img = site_url('../files/'.$subv[0]['Valor']);
                $contenido .='
                  <div class="face back" style="background:url('.$img.')no-repeat 100% 100%;background-size: cover;height: 300px;">
                    <div class="inner text-center">
                      <img src="'.$img.'" style="width:60%;" />
                    </div>
                  </div>
                ';
            }
            if($subv[0]['Type']=='f_description'){
                $contenido .= '
                  <div class="face front">
                    <div class="inner">
                      <h1>'.$subv[0]['Valor'].'</h1>
                    </div>
                  </div>';
            }
            if($subv[1]['Type']=='b_media'){
                $img = site_url('../files/'.$subv[1]['Valor']);
                $contenido .='
                  <div class="face back" style="background:url('.$img.')no-repeat 100% 100%;background-size: cover;height: 300px;">
                    <div class="inner text-center">
                      <img src="'.$img.'" style="width:60%;" />
                    </div>
                  </div>
                ';
            }
            if($subv[1]['Type']=='b_description'){
                $contenido .= '
                  <div class="face back">
                    <div class="inner">
                      <h1>'.$subv[1]['Valor'].'</h1>
                    </div>
                  </div>';
            }
            $contenido .= '
            </div>
          </div>
        ';

        $contenido .= '</div>';
    }
    $contenido .= '</div>';
    return $contenido;
    */
}

//botones
function botones($arr)
{
    $contenido = '';

    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }

        //Incremento

        $inc = $value['Orden'];

        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0 cnt' . $subv['Type'] . $value['Orden'] . '" style="margin-bottom:10px; ' . $bg . '">
                        <label>' . $subv['Type'] . '</label>
                        <input type="text" class="form-control " name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;' . $bg . '">
                    </div>
                    ';
            }
            if ($subv['ValorType'] == 'date') {
                $contenido .= '
                <label>' . $subv['Type'] . '</label>
                <input type="text" class="animate form-construnctor" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="' . $bg . '">';
            }

            if ($subv['ValorType'] == 'textarea') {
                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 padding0" style="margin-bottom:10px;">
                    <label>' . $subv['Type'] . '</label>
                    <textarea class="animate form-construnctor editor_edit" name="' . $subv['Type'] . $value['Orden'] . '" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px; ' . $bg . '">' . $subv['Valor'] . '</textarea>
                </div>   
                ';
            }
            if ($subv['ValorType'] == 'url') {
                $contenido .= '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0 style="' . $bg . '">
                        <label>Escribir url</label>
                        <input type="text" class="form-control" value="' . $subv['Valor'] . '" name="' . $subv['Type'] . $value['Orden'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;' . $bg . '">
                    </div>';
            }


            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }


    }

    return $contenido;
}

function botones_t($arr)
{


    $contenido = '
    
    ';
    foreach ($arr as $key => $value) {
        $contenido .= '<div class="col-xs-12 col-sm-12 col-lg-12 wdt-mtl2">';
        //foreach ($value['groupeddata'] as $subv) {
        $subv = $value['groupeddata'];
        if ($subv[1]['Type'] == 'description') {
            $contenido .= '
                    <div class="col-xs-6 col-sm-12 col-lg-6 text-right">
                        <label class="texto-tabs">' . $subv[1]['Valor'] . '</label>
                    </div>
                ';
        }
        $contenido .= '
            <div class="col-xs-1 col-sm-12 col-lg-1"></div>
                <div class="col-xs-5 col-sm-12 col-lg-5 text-left content-bttn">
            ';
        if ($subv[0]['Type'] == 'title') {

            $contenido .= '
                        <a class="btton-go" href="' . $subv[2]['Valor'] . '" target="_blank">' . $subv[0]['Valor'] . '</a>
                ';

        }
        /*
            if($subv[2]['Type']=='url'){
                $contenido .= '';
            }
            */
        $contenido .= '</div>';
        /*
            if($subv[0]['Type']=='title'){

                $contenido .= '
                    <div class="col-xs-6 col-sm-12 col-lg-6 text-right">
                        <button class="btn-5 gourl" hf="'.$subv[0]['Valor'].'">'.$subv[0]['Valor'].'</button>
                    </div>
                ';
            }
            */

        //}
        $contenido .= '</div>';
    }
    $contenido .= '';
    return $contenido;
}

////////////*************DIVISIONES***************************////////////////////
function divisiones($arr)
{
    $contenido = '';
    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;
        if ($key % 2 == 0) {
            $bg = 'background: #f1fdff;';
        } else {
            $bg = 'background: #f1fdff;';
        }
        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12" style="' . $bg . '">';
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['ValorType'] == 'text') {
                $contenido .= '
                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0 cnt' . $subv['Type'] . $value['Orden'] . '" style="margin-bottom:10px;">
                        <label>' . $subv['Type'] . '</label>
                        <input type="text" class="form-control" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;' . $bg . '">
                    </div>
                    ';
            }
            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Valor'] != "") {
                $dvlors = 1;
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido;
}

//template divide 1
function continuar_t($arr)
{
    /*<div class="col-xs-12 col-sm-12 col-lg-12 dividercontinue text-center">*/
    $contenido = '
    <div class="col-xs-12 col-sm-12 col-lg-12 line-continuar">
        ';
    foreach ($arr as $key => $value) {
        foreach ($value['groupeddata'] as $subv) {
            if ($subv['Type'] == 'title') {
                if (strlen($subv['Valor']) <= 5) {
                    $contenido .= '
                    <div class="col-xs-5 col-sm-12 col-lg-5"></div>
                    <div class="col-xs-2 col-sm-12 col-lg-2 text-center">
                        <div class="col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-xs-12 col-sm-12 col-lg-12 bg_primario" >
                                <h2 style="color:#fff;">' . $subv['Valor'] . '</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-12 col-lg-5"></div>
                    ';
                } else {
                    $contenido .= '
                    <div class="col-xs-2 col-sm-12 col-lg-2"></div>
                    <div class="col-xs-8 col-sm-12 col-lg-8 text-center">
                        <div class="col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-xs-12 col-sm-12 col-lg-12 bg_primario" >
                                <h2 style="color:#fff;">' . $subv['Valor'] . '</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-12 col-lg-2"></div>
                    ';
                }
            }
        }
    }
    $contenido .= '
    </div>
    ';
    return $contenido;
}

///////////*******************ACTIVIDADES*******************//////////
function ordenar($arr)
{
    $contenido = '';

    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;

        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12">';
        foreach ($value['groupeddata'] as $subv) {
            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Type'] == "title") {
                $contenido .= '
                <div class="col-xs-8 col-sm-12 col-lg-8">
                    <label>' . $subv['Type'] . '</label>
                    <input type="text" class="form-control" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                </div>
                ';
            }
            if ($subv['Type'] == "orden") {
                $contenido .= '
                <div class="col-xs-4 col-sm-12 col-lg-4">
                    <label>' . $subv['Type'] . '</label>
                    <input type="number" class="form-control" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                </div>
                ';
            }
        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido;
}

function ordenar_t($arr)
{

    $img_file = site_url('assets/img/ejercicio.png');
    $contenido = '
    <div class="col-md-12 col-sm-12 col-lg-12 border-tmpl-file wdt-mtl">
        <div class="col-md-4 col-sm-12 col-lg-4">
            <img src="' . $img_file . '" />
        </div>
        <div class="col-md-8 col-sm-12 col-lg-8">
            <h4>Ejercicio</h4>
            <label>[ordenar por jerarquia]</label>
        </div>
    </div>
    ';
    return $contenido;

}

function opcion_m($arr)
{
    $contenido = '';

    foreach ($arr as $key => $value) {
        $dlt = '';
        $dvlors = 0;

        $contenido .= '<div class="form-group divitems-c col-xs-12 col-sm-12 col-lg-12">';
        foreach ($value['groupeddata'] as $subv) {
            $dlt .= '&valor=' . $subv['DocEntry'];
            if ($subv['Type'] == "description") {
                $contenido .= '
                <div class="col-xs-12 col-sm-12 col-lg-12 padding0">
                    <label>Pregunta</label>
                    <textarea class="form-control" name="' . $subv['Type'] . $value['Orden'] . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;"></textarea>
                   
                </div>
                ';
            }
            if ($subv['Type'] == "option") {
                for ($i = 1; $i < 5; $i++) {

                    $contenido .= '
                    <label>' . $subv['Type'] . ' ' . $i . '</label>
                    <input type="text" class="form-control" name="' . $subv['Type'] . $i . '" value="' . $subv['Valor'] . '" style="border-left: 0px;border-right: 0px;border-top: 0px;">
                    ';
                }
            }

        }
        if ($dvlors == 1) {
            $contenido .= '<div><a class="dlt-item" v="' . $dlt . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
        }
        $contenido .= '</div>';
    }

    return $contenido;
}


function actual_link()
{
    return "http://$_SERVER[HTTP_HOST]";
}

//categoria demoR
function demoRo()
{
    return "test <input type='hidden' value='.' name='description1'  /><input type='hidden' value='.title' name='title1'  />";
}

function demo_Ro()
{
    //éste es para visualizacion con datos de la base de datos
    $contenido = '<div class="col-xs-12 col-sm-12 col-lg-12 text-right"><div class="">test';
    $contenido .= '</div></div>';
    return $contenido;
}

function test()
{
    echo 'something';
}

?>