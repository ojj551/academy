<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-header'); ?>
<div class="conteiner-right col-xs-12 col-sm-12 col-lg-12">
	<h1 class="tpg-logo title-section"><?php echo $CourseName; ?></h1>
	<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 40px;">
		<div class="col-xs-12 col-sm-2 col-lg-2"></div>
		<div class="col-xs-12 col-sm-8 col-lg-8">
			<div class="col-xs-2 col-sm-12 col-lg-2 text-center pasos paso-do">
				<div class="paso">
					<a href="<?php echo base_url() ?>materias/adminp1/?m=<?php echo $MateriaIDEncryp; ?>" class="tpg-relawey">Información <br> básica</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-2 col-lg-2 pasos-linea"></div>
			<div class="col-xs-12 col-sm-2 col-lg-2 text-center pasos paso-do">
				<div class="paso">
					<a href="<?php echo base_url() ?>materias/adminp2/?m=<?php echo $MateriaIDEncryp; ?>" class="tpg-relawey">Diseño de la materia</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-2 col-lg-2 pasos-linea-peding"></div>
			<div class="col-xs-12 col-sm-2 col-lg-2 text-center pasos-peding">
				<div class="paso">
					<a href="<?php echo base_url() ?>materias/prevista/?m=<?php echo $MateriaIDEncryp; ?>" class="tpg-relawey">Vista de la <br> materia</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-2 col-lg-2 text-right">
				<a data-toggle="modal" data-target="#stephelp"><i class="fa fa-question-circle" aria-hidden="true"></i></a>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 60px;">
				<label class="tpg-relawey addmat">Imagen de la materia</label>
				<input class="form-control" type="file" name="Thumbnail" id="Thumbnail">
				<label class="tpg-relawey addmat">Banner de la materia</label>
				<input class="form-control" type="file" name="Banner" id="Banner">
				<label class="tpg-relawey addmat">Video promocional de la materia</label>
				<input class="form-control" type="file" name="VideoMat" id="VideoMat">
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 30px;">
				<a type="button" class="btn btn-secondary savemateria" id="save-materia">Guardar</a>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('footer'); ?>