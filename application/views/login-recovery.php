<?php $this->load->view('cabecera'); ?>
<video class="login-video" autoplay loop>
  <source src="<?php echo site_url('assets/video/Spacious.mp4'); ?>" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
<div class="login-conteiner">
  <div class="col-xs-12 col-sm-12 col-lg-12 text-center">
    <!---<img class="login-logo" src="<?php echo site_url('assets/img/logodemo.png'); ?>">-->
    <div class="col-xs-12 col-sm-12 col-lg-12">
      <div class="col-xs-2 col-sm-12 col-lg-2"></div>
      <div class="col-xs-8 col-sm-12 col-lg-8 text-center">
        <?php 
        if($continue){
        ?>
        <img class="login-logo" src="<?php echo site_url('assets/img/logodemo.png'); ?>">
        <div class="col-xs-12 col-sm-12 col-lg-12">
          <div class="col-xs-3 col-sm-12 col-lg-3"></div>
          <div class="col-xs-6 col-sm-12 col-lg-6 login-form">
            <input class="form-control login-inputs" type="password" name="password" id="password" placeholder="Contraseña">
            <input class="form-control login-inputs" type="password" name="passwordr" id="passwordr" placeholder="Repite Contraseña">
            <button type="button" class="btn btn-secondary" id="savepaswword">Guardar nueva contraseña</button>
          </div>
        </div>
        <?php
        }else{ ?>
        <h1 class="activacion-listo tpg-logo blanco">Problemas al cambiar contraseña</h1>
        <h1 class="tpg- blanco ">favor de comunicarte a <b>contacto@xpertcad.com</b></h1>
        <?php
        }
        ?>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('footer'); ?>
<script>
  $('#savepaswword').click(function(){
    var password = $('#password').val();
    var passwordr = $('#passwordr').val();
    var u = "<?php echo $U; ?>";
    if(password==""){
      $('#password').addClass('animated shake');
      setTimeout(function(){ $('#password').removeClass('animated shake'); },600);
    }
    if(passwordr==""){
      $('#passwordr').addClass('animated shake');
      setTimeout(function(){ $('#passwordr').removeClass('animated shake'); },600);
    }

    if(password!="" && passwordr!=""){
      $.ajax({
          type:'POST',
          url : base_url+'login/changepassword',
          data: 'pssw='+password+'&psswr='+passwordr+'&u='+encodeURIComponent(u),
          beforeSend:function(){
            $('.capaload').fadeIn(600);
          },
          success : function(data) {
            $('.capaload').fadeOut(600);
            if(data!="error"){
              $(location).attr('href', base_url+'inicio');
            }else{
              $.dialog({
                  title: '¡Lo sentimos! <i class="fa fa-frown-o" aria-hidden="true"></i>',
                  content: data,
              });
            }
          }
      });
    }
  });
</script>