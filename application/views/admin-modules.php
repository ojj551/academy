<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php $this->load->view('header-left'); ?>
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Modulos</h3>

          </div>
      </div>                    
    </div>  
	<div class="col-xs-12 col-sm-12 col-lg-12 marginfirst">
		<div class="col-xs-12 col-sm-12 col-lg-12 ">
			<div class="col-xs-4 col-sm-12 col-lg-4">
				<form id="form">
					<label>Nombre de la página</label>
					<input type="text" name="name" id="name" class="form-control">
					<label>Clase (url)</label>
					<input type="text" name="clase" id="clase" class="form-control">
					<label>Font Awesome</label>
					<input type="text" name="font" id="font" class="form-control">
					<label>Se visualizará en </label>
					<input type="text" name="font" id="font" class="form-control">
					<label>Página padre</label>
					<select name="padre" class="form selectpicker" id="padre" data-live-search="true">
						<option value="0">Ningúno</option>
						<?php echo $selectModule; ?>
					</select>
				</form>
				<div class="text-right">
					<button type="button" class="btn btn-primary btng" id="save-module">Guardar</button>
				</div>
			</div>
			<div class="col-xs-8 col-sm-12 col-lg-8">
				<div class="col-xs-12 col-sm-12 col-lg-12 headtablemodules">
					<div class="col-xs-1 col-sm-12 col-lg-1">
						<label>#</label>
					</div>
					<div class="col-xs-3 col-sm-12 col-lg-3">
						<label>Modúlo</label>
					</div>
					<div class="col-xs-3 col-sm-12 col-lg-3">
						<label>Clase</label>
					</div>
					<div class="col-xs-2 col-sm-12 col-lg-2">
						<label>Partent</label>
					</div>
					<div class="col-xs-3 col-sm-12 col-lg-3">
						<label>Ajustes</label>
					</div>
					<div class="col-xs-3 col-sm-12 col-lg-3">
						
					</div>
				</div>
				<?php echo $listmodules; ?>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('footer-admin'); ?>  
<script src="<?php echo site_url('assets/js/modules.js'); ?>"></script>