<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php //$this->load->view('header-left'); ?>
<style>
	.opener-left-menu{display: none;}
</style>
<div class="col-xs-12 col-sm-12 col-lg-12 padding0 info-user">

	<div class="col-xs-12 col-sm-12 col-lg-9 sub-info-user">
		<h2 class="tpg-relawey" style="display: inline;"><?php echo $NameTopic; ?> </h2>
		<a class="edittopic" v="<?php echo $TopicID; ?>" >Editar</a>
		<a class="dlt" t="<?php echo $tabla ?>" id="<?php echo $id ?>" vl="<?php echo $TopicID ?>" style=" color: #F44336;">Eliminar</a>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12 padding0 tpc" v="<?php echo $TopicID; ?>">
	<?php if($loads=='vacio'){ ?>
	<div class="col-xs-12 col-sm-12 col-lg-12 padding0 items-section" i="1">
		<div class="col-xs-12 col-sm-12 col-lg-12 padding0 sectionstm section1 text-center" i="1">
			<span data-editable data-name="article"></span>
			<label>Sección 1</label>
			<a class="additem addi" i="1">Agregar item<i class="fa fa-plus" aria-hidden="true"></i></a>
			
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<a class="addsection"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
	</div>
	<?php }else{ ?>
	<div class="col-xs-12 col-sm-12 col-lg-12 padding0 items-section" i="1">
		<div class="col-xs-12 col-sm-12 col-lg-12 padding0 sectionstm section1 text-center sectionstmnpdd sction-have" i="1" vl="<?php echo $loads; ?>">
			<span data-editable data-name="article"></span>
			<div class="have-section hsc1">

			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<a class="addsection"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
	</div>
	<?php
	} ?>
</div>
<div id="edit-topic" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar tema</h4>
      </div>
      <div class="modal-body paint-topic">
        <input type="hidden" id="idtc">
        <label>Nombre del tema</label>
        <input class="form-control" type="text" id="nametopic-upd"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="update-topic">Aceptar</button>
      </div>
    </div>

  </div>
</div>
<div class="menu-items-add animate">
	<div class="col-xs-12 col-sm-12 col-lg-12 text-right div-close-mnct">
		<a class="additem close-menu-items"><i class="fa fa-times" aria-hidden="true"></i></a>
	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12 padding0">
		<?php echo $categorias; ?>
	</div>
</div>
<div class="sbmenu-items-add animate">

</div>
<?php $this->load->view('footer-admin'); ?>
<script>
	var tpc = $('.tpc').attr('v');
	var section = 0;
	var i = $('.items-section').attr('i');
	//pintar sections existentes
	/*
	Primera version
	$.each($('.sction-have'), function( value ) {
		var vl = $(this).attr('vl');
		var i = $(this).attr('i');
		if(vl!=""){
			var result = vl.split('?t=');
			$(".section"+i).attr('tmp', result[1]);
			$(".hsc"+i).load("<?php echo base_url(); ?>temas/"+vl+'&tpc='+encodeURIComponent(tpc));
		}
	});*/
	//Agregar tipo de template
	$('.addi').click(function(){
		section++;
	});
	$('.additem').click(function(){
		$('.menu-items-add').toggleClass('menu-items-add-show');
	});
	$('.list-categ-template').click(function(){
		$('.list-categ-template').removeClass('active-cat-tmpl');
		$(this).addClass('active-cat-tmpl');
		$('.sbmenu-items-add').addClass('sbmenu-items-add-show');

		var ct = $(this).attr('vl');
		$.ajax({
	      type:'POST',
	      url : base_url+'temasfunc/categorys',
	      data: 'ct='+encodeURIComponent(ct),
	      beforeSend:function(){
	      },
	      success : function(vl) {
	      	$('.sbmenu-items-add').html(vl);
	      	$('.imgtemplate').click(function(){
	      		var t = $(this).attr('v');
	      		$.ajax({
			      type:'POST',
			      url : base_url+'temasfunc/template',
			      data: 't='+encodeURIComponent(t),
			      beforeSend:function(){
			      },
			      success : function(resp) {
			      	if(resp!=""){
			      		$('.sectionstm').addClass('sectionstmnpdd');
			      		$('.menu-items-add').toggleClass('menu-items-add-show');
						$('.list-categ-template').removeClass('active-cat-tmpl');
						$('.sbmenu-items-add').removeClass('sbmenu-items-add-show');
						var result = resp.split('?t=');
						$(".section"+section).attr('tmp', result[1]);
			      		$(".section"+section).load("<?php echo base_url(); ?>temas/"+resp+'&tpc='+encodeURIComponent(tpc));

			      	}
			      	//console.log(resp);
			      }
			    });
	      	});
	      }
	    });
	});
	//Agregar otra sección
	$('.addsection').click(function(){
		i++;
		$('.items-section').append('<div class="col-xs-12 col-sm-12 col-lg-12 padding0 sectionstm section'+i+' text-center" i="'+i+'"><label>Sección '+i+'</label><a class="additem addi" i="'+i+'">Agregar item<i class="fa fa-plus" aria-hidden="true"></i></a></div>');
		
	});
	$('.close-menu-items').click(function(){
		$('.list-categ-template').removeClass('active-cat-tmpl');
		$('.sbmenu-items-add').removeClass('sbmenu-items-add-show');
	});
	$('.plus-mat').click(function(){
		$('.typeadd').toggle();
	});
	//
	$('.admtl').click(function(){
		var v = $(this).attr('v');
		var idtpcf = $('#idtpcf').val();
		
		$('#add-material').modal('show');

		$('#idtpc').val(idtpcf);
		$('#typematvl').val(v);

		if(v=="1"){
			$('.namemat').html('Archivo');
		}
		if(v=="2"){
			$('.namemat').html('Imagen');
		}
		if(v=="3"){
			$('.namemat').html('Video');
		}
		if(v=="4"){
			$('.namemat').html('Zip');
		}
	});
	//
	$('.edittopic').click(function(){
		console.log('entro');
		var v = $(this).attr('v');
		$('#edit-topic').modal('show');
		$.ajax({
	      type:'POST',
	      url : base_url+'temasfunc/bytema',
	      data: 'v='+encodeURIComponent(v),
	      beforeSend:function(){
	      },
	      success : function(vl) {
	      	$('#idtc').val(v);
	      	$('#nametopic-upd').val(vl);
	      }
	    });
	});
	$('.saveplanti').click(function(){
		console.log('aloha');
	});
</script>