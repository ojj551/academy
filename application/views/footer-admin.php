<div id="right-menu">
    <div class="col-md-6 col-sm-6 text-right" style="padding-left:10px;">
      <h3 style="color:#DDDDDE;"><span class="fa  fa-map-marker"></span> Querétaro</h3>
      <h1 style="margin-top: -10px;color: #ddd;">30<sup>o</sup></h1>
    </div>
    <div class="col-md-6 col-sm-6">
       <div class="wheather">
        <div class="stormy rainy animated pulse infinite">
          <div class="shadow">
            
          </div>
        </div>
        <div class="sub-wheather">
          <div class="thunder">
            
          </div>
          <div class="rain">
              <div class="droplet droplet1"></div>
              <div class="droplet droplet2"></div>
              <div class="droplet droplet3"></div>
              <div class="droplet droplet4"></div>
              <div class="droplet droplet5"></div>
              <div class="droplet droplet6"></div>
            </div>
        </div>
      </div>
    </div>
</div> 



<div class="capaload">
  <div class="sk-folding-cube">
    <div class="sk-cube1 sk-cube"></div>
    <div class="sk-cube2 sk-cube"></div>
    <div class="sk-cube4 sk-cube"></div>
    <div class="sk-cube3 sk-cube"></div>
  </div>
</div>
<!--
<div id="modalgeneral" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body viewsgeneral">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
-->
<script src="<?php echo site_url('asset/js/jquery.ui.min.js'); ?>" ></script>
<script src="<?php echo site_url('asset/js/bootstrap.min.js'); ?>" ></script>



<!-- plugins -->
<script>var base_url = '<?php echo base_url(); ?>';</script>
<script src="<?php echo site_url('asset/js/plugins/moment.min.js'); ?>" ></script>
<script src="<?php echo site_url('asset/js/plugins/fullcalendar.min.js'); ?>" ></script>
<script src="<?php echo site_url('asset/js/plugins/jquery.nicescroll.js'); ?>" ></script>
<script src="<?php echo site_url('asset/js/plugins/jquery.vmap.min.js'); ?>" ></script>
<script src="<?php echo site_url('asset/js/plugins/maps/jquery.vmap.world.js'); ?>" ></script>
<script src="<?php echo site_url('asset/js/plugins/jquery.vmap.sampledata.js'); ?>" ></script>
<script src="<?php echo site_url('assets/js/Chart.bundle.js'); ?>"></script>
<!--<script src="<?php echo site_url('asset/js/plugins/chart.min.js'); ?>" ></script>-->
<script src="<?php echo site_url('asset/js/main.js'); ?>" ></script>
<script src="<?php echo site_url('assets/js/jquery-confirm.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/bootstrap.file-input.js'); ?>"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<script src="<?php echo site_url('assets/js/jquery.dataTables.min.js'); ?>"></script>


<script src="<?php echo site_url('assets/js/swiper.min.js'); ?>"></script>


<script src="<?php echo site_url('asset/js/plugins/moment.min.js'); ?>"></script>
<script src="<?php echo site_url('asset/js/plugins/jquery.knob.js'); ?>"></script>
<script src="<?php echo site_url('asset/js/plugins/ion.rangeSlider.min.js'); ?>"></script>
<script src="<?php echo site_url('asset/js/plugins/bootstrap-material-datetimepicker.js'); ?>"></script>
<script src="<?php echo site_url('asset/js/plugins/jquery.mask.min.js'); ?>"></script>
<script src="<?php echo site_url('asset/js/plugins/select2.full.min.js'); ?>"></script>
<script src="<?php echo site_url('asset/js/plugins/nouislider.min.js'); ?>"></script>
<script src="<?php echo site_url('asset/js/plugins/jquery.validate.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/Video/video.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Video/videojs-ie8.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/Video/videojs-resolution-switcher.js"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/jquery.dataTables.min.js'); ?>" ></script>
<script src="<?php echo site_url('asset/js/plugins/mediaelement-and-player.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/skrollr.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/jquery-ui.js'); ?>"></script>
<script src="<?php //echo site_url('assets/js/modals.js'); ?>"></script>
<script>
  //skr
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      // some code..
    }else{
        var s = skrollr.init({forceHeight: false});
    }
    //inputs file
    $('input[type=file]').bootstrapFileInput();
    $('.file-inputs').bootstrapFileInput();


</script>
<script>

  //
  $('.asiguser').click(function(){
    var typ = $(this).attr('typ');
    var usrid = $(this).attr('usrid');
    $('#asiguser-form').modal('show');
    if(typ!="" && usrid!=""){
      $.ajax({
        url : base_url+'alumnosfunc/getlistalumn',
        type : 'POST',
            data : 'typ='+encodeURIComponent(typ)+'&usrid='+encodeURIComponent(usrid),
            beforeSend:function(){
              console.log('enviando...');
          //$('.capaload').fadeIn(600);
        },
        success : function(datav) {
          $('.paintuserslist').html(datav);
          $('.userlist').selectpicker('refresh');
        }
      });
    }
  });
  //
  $('#asignarusuario').click(function(){
    var usr = $('#usr_asg').val();
    var usr_elg = $('#users_asg').val();
    if(usr!="" && usr_elg!=""){
      $.ajax({
        url : base_url+'alumnosfunc/addasignacion',
        type : 'POST',
            data : 'usr='+encodeURIComponent(usr)+'&usrelg='+encodeURIComponent(usr_elg),
            beforeSend:function(){
          $('.capaload').fadeIn(600);
          $('#asiguser-form').modal('hide');
        },
        success : function(datav) {
          setTimeout(function(){
            $('.capaload').fadeOut(600);
            if(datav=="success"){
              location.reload();
            }else{
              $.dialog({
                title: '¡Ups!',
                content: datav,
              });
            }
          },1000);
        }
      });
    }
  });
  //
  /*
  $(document).ready(function() {
      $('.tablessearch').DataTable();
  } );*/
  $('body').on('click','.dlt',function(){
    var t = $(this).attr('t');
    var id = $(this).attr('id');
    var vl = $(this).attr('vl');


    $.confirm({
        title: 'Eliminar registro',
        content: '¿Seguro que quieres eliminar el registro?',
        confirm: function(){
          if(t!="" && id!="" && vl!=""){
          $.ajax({
                url : base_url+'generalesfunc/delete',
                type : 'POST',
                data : 't='+encodeURIComponent(t)+'&id='+encodeURIComponent(id)+'&vl='+encodeURIComponent(vl),
                beforeSend:function(){
              $('.capaload').fadeIn(600);
            },
            success : function(datav) {
              setTimeout(function(){
                $('.capaload').fadeOut(600);
                if(datav=="success"){
                  location.reload();
                }else{
                  $.dialog({
                    title: '¡Ups! tuvimos problemas para actualizar tus datos',
                    content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
                  });
                }
              },1000);
            }
          });
        }
        }
    });


  });
</script>
<script>
  var toggleMenu = function(){
    if (swiper.previousIndex == 0)
      swiper.slidePrev()
  }
  , menuButton = document.getElementsByClassName('menu-button')[0]
  , swiper = new Swiper('.swipmenu', {
    slidesPerView: 'auto'
    , initialSlide: 1
    , resistanceRatio: .00000000000001
    , onSlideChangeStart: function(slider) {
      if (slider.activeIndex == 0) {
        menuButton.classList.add('cross')
        menuButton.removeEventListener('click', toggleMenu, false)
      } else
        menuButton.classList.remove('cross')
    }
    , onSlideChangeEnd: function(slider) {
      if (slider.activeIndex == 0)
        menuButton.removeEventListener('click', toggleMenu, false)
      else
        menuButton.addEventListener('click', toggleMenu, false)
    }
    , slideToClickedSlide: true
  })
</script>
 </body>
</html>