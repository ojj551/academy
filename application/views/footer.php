<div class="capaload">
	<div class="sk-folding-cube">
	  <div class="sk-cube1 sk-cube"></div>
	  <div class="sk-cube2 sk-cube"></div>
	  <div class="sk-cube4 sk-cube"></div>
	  <div class="sk-cube3 sk-cube"></div>
	</div>
</div>


</body>
<script>
	var base_url = '<?php echo base_url(); ?>'
</script>
<script src="<?php echo site_url('assets/js/jquery-3.1.0.min.js'); ?>" ></script>
<script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/jquery-confirm.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/editor/ckeditor.js"></script>
<script src="<?php echo site_url('assets/js/bootstrap.file-input.js'); ?>"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<script src="<?php echo site_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/modals.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/materias.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/skrollr.min.js'); ?>"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	//skr
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	 	// some code..
	}else{
    	var s = skrollr.init({forceHeight: false});
	}
	
	$('.asiguser').click(function(){
		var typ = $(this).attr('typ');
		var usrid = $(this).attr('usrid');
		$('#asiguser-form').modal('show');
		if(typ!="" && usrid!=""){
			$.ajax({
				url : base_url+'alumnosfunc/getlistalumn',
				type : 'POST',
		       	data : 'typ='+encodeURIComponent(typ)+'&usrid='+encodeURIComponent(usrid),
		       	beforeSend:function(){
		       		console.log('enviando...');
					//$('.capaload').fadeIn(600);
				},
				success : function(datav) {
					$('.paintuserslist').html(datav);
					$('.userlist').selectpicker('refresh');
				}
			});
		}
	});
	//
	$('#asignarusuario').click(function(){
		var usr = $('#usr_asg').val(); 
		var usr_elg = $('#users_asg').val();
		if(usr!="" && usr_elg!=""){
			$.ajax({
				url : base_url+'alumnosfunc/addasignacion',
				type : 'POST',
		       	data : 'usr='+encodeURIComponent(usr)+'&usrelg='+encodeURIComponent(usr_elg),
		       	beforeSend:function(){
					$('.capaload').fadeIn(600);
					$('#asiguser-form').modal('hide');
				},
				success : function(datav) {
					setTimeout(function(){
						$('.capaload').fadeOut(600);
						if(datav=="success"){
							location.reload();
						}else{
							$.dialog({
								title: '¡Ups!',
								content: datav,
							});
						}
					},1000);
				}
			});
		}
	});
	//
	$(document).ready(function() {
	    $('.tablessearch').DataTable();
	} );
	//
	$('.edit').click(function(){
		$('#update').modal('show'); 
		//
		var t = $(this).attr('t');
		var id = $(this).attr('id');
		var vl = $(this).attr('vl');
		if(vl!=""){
			$.ajax({
				url : base_url+'generalesfunc/updateform',
				type : 'POST',
		       	data : 't='+encodeURIComponent(t)+'&id='+encodeURIComponent(id)+'&vl='+encodeURIComponent(vl),
		       	beforeSend:function(){
		       		console.log('enviando...');
					//$('.capaload').fadeIn(600);
				},
				success : function(datav) {
					console.log(datav);
				}
			});
		}
	});
	//
	
	$('.linkmenu').click(function(){
		$('.menu-lateral-sb').addClass('showmenu');
		$('.sk-folding-cube').hide();
		$('.capaload').fadeIn(300);
		var v = $(this).attr('v');
		if(v=="profile"){
			$('.paint-menu').show();
			$(".paint-menu").load("<?php echo base_url().'menu/profile'; ?>");
		}
		if(v=="cursos"){
			$('.paint-menu').show();
			$(".paint-menu").load("<?php echo base_url().'menu/cursos'; ?>");
		}
		if(v=="alumnos"){
			$('.paint-menu').show();
			$(".paint-menu").load("<?php echo base_url().'menu/alumnos'; ?>");
		}
		if(v=="admin"){
			$('.paint-menu').show();
			$(".paint-menu").load("<?php echo base_url().'menu/admin'; ?>");
		}
	});
	$('.items-menu-sb').click(function(){
		$('.capaload').fadeOut(300);
		$('.sk-folding-cube').show(600);
		$('.menu-lateral-sb').removeClass('showmenu');
		$('.paint-menu').hide();
		//$(".paint-menu").load("");
	});
	///
	$('input[type=file]').bootstrapFileInput();
	$('.file-inputs').bootstrapFileInput();
	//
	$('body').('click','.dlt',function(){
		var t = $(this).attr('t');
		var id = $(this).attr('id');
		var vl = $(this).attr('vl');

		$.confirm({
		    title: 'Eliminar registro',
		    content: '¿Seguro que quieres eliminar el registro?',
		    confirm: function(){
		    	if(t!="" && id!="" && vl!=""){
					$.ajax({
				        url : base_url+'generalesfunc/delete',
				       	type : 'POST',
				       	data : 't='+encodeURIComponent(t)+'&id='+encodeURIComponent(id)+'&vl='+encodeURIComponent(vl),
				       	beforeSend:function(){
							$('.capaload').fadeIn(600);
						},
						success : function(datav) {
							setTimeout(function(){
								$('.capaload').fadeOut(600);
								if(datav=="success"){
									location.reload();
								}else{
									$.dialog({
										title: '¡Ups! tuvimos problemas para actualizar tus datos',
										content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
									});
								}
							},1000);
						}
					});
				}
		    }
		});

		
	});
</script>
</html>
