<?php $this->load->view('profesor/header-left'); ?> 
<?php echo link_tag('assets/js/editor-master/css/medium-editor.css'); ?>
<?php echo link_tag('assets/js/editor-master/css/themes/default.css'); ?>
<style>
	.navbar{position: absolute !important;}
	.capa-white{
		display: block;
	}
	.opener-left-menu{display: none;}
	.sbmenu-items-add {
	    position: fixed;
	    width: 20%;
	    left: 0%;
	    top: 0px;
	    height: 100%;
	    background: #f1f1f1;
	    z-index: 99999;
	    overflow-y: scroll;
	}
	.show-sbmenu{
		left: 0%;
	}
	.div-pregunts{
		padding-top: 15px;
	    padding-bottom: 15px;
	    border-bottom: 1px solid #e4e4e4;
	}
	.addquestion{
		background: #2196f3 !important;
	}
	.div-question{
		width: 
		padding-left: 20%;
	}
	.div-question{
		width: 60%;
	    position: relative;
	    left: 30%;
	    top: 75px;
	}
	.capa-white{display: none;}
	.opcion-op{
		padding-top: 20px;
	    padding-bottom: 0px;
	    border-bottom: 1px solid #ccc;
	    background: #f1eded;
	}
	.dlt-option,.dlt-option-tmp{
		opacity: 0;
		font-size: 20px;
	}
	.opcion-op:hover .dlt-option{
		opacity: 1;
	}
	.opcion-op:hover .dlt-option-tmp{
		opacity: 1;
	}
</style>
<div class="sbmenu-items-add animate">
	<?php echo $questions; ?>
	<!--
	<div class="col-xs-12 col-sm-12 col-lg-12 div-pregunts" v="<?php echo $CourseIDEnCrypt; ?>" i="1">
		<a>Pregunta 1</a>
	</div>-->
	<div class="menu-questions">
		
	</div>
	<div class="col-xs-12 col-sm-12 text-center" style="margin-top: 30px;">
		<button type="button" class="btn btn-info addquestion" v="<?php echo $QuizIDEnCryp; ?>">Agregar pregunta<i class="fa fa-chevron-down" aria-hidden="true"></i></button>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12">
    <div class="container">
    	<div class="div-question print-question">

        </div>
    </div>
</div>
<div class="capa-white"></div>
<?php $this->load->view('footer-admin'); ?>
<script src="<?php echo site_url('assets/js/editor-master/js/medium-editor.js'); ?>"></script>
<script>
	//Agregar otra pregunta
	var i = $('.div-pregunts').size();
	$('.addquestion').click(function(){
		var v = $(this).attr('v');
		i++;
		//$('.menu-questions').append('<div class="col-xs-12 col-sm-12 col-lg-12 div-pregunts" v="'+v+'" i="'+i+'" q=""><a>Pregunta '+i+'</a></div>');

		$('.menu-questions').append('<div class="col-xs-12 col-sm-12 col-lg-12"><div class="col-xs-10 col-sm-10 col-lg-10 div-pregunts" v="'+v+'" i="'+i+'" q=""><a>Pregunta '+i+'</a></div><div class="col-xs-1 col-sm-1 col-lg-1" style="padding-top:15px;"><a class="dltqs"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div>');
		//Delete question
		$('.dltqs').click(function(){
			$(this).parent().parent().hide();
		});

		//Ver pregunta
		$('.div-pregunts').click(function(){
			var vq = $(this).attr('v');
			var iq = $(this).attr('i');
			$.ajax({
		      type:'POST',
		      url : base_url+'materiasfunc/getquestion',
		      //data : 'quiz='+encodeURIComponent(vq)+'&i='+iq+'&q='+q+'&t=',
		      data : 'quiz='+encodeURIComponent(vq)+'&i='+iq,
		      success : function(vl) {
		      	$('.print-question').html(vl);
		      	var editor = new MediumEditor('.editor_edit', {
			    toolbar: {
			        allowMultiParagraphSelection: true,
			        //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
			        buttons: ['bold', 'italic', 'anchor'],
			        diffLeft: 0,
			        diffTop: -10,
			        firstButtonClass: 'medium-editor-button-first',
			        lastButtonClass: 'medium-editor-button-last',
			        relativeContainer: null,
			        standardizeSelectionStart: false,
			        static: false,

			        align: 'center',
			        sticky: false,
			        updateOnEmptySelection: false
			    }
				});
				//Guardar pregunta
				$('.save-qst').click(function(){
					var ip = $(this).attr('i');;
					var qv = $(this).attr('v');
					var qname = $('#title-q').text();
					if(qname){
						var dat;
						if(qv==1){
							dat = 'q='+encodeURIComponent(vq)+'&i='+ip+'&name='+qname+'&new=0';
						}else{
							dat = 'q='+encodeURIComponent(vq)+'&i='+ip+'&name='+qname+'&new=1';
						}
						$.ajax({
					      type:'POST',
					      url : base_url+'materiasfunc/savequestion',
					      data : dat,
					      success : function(sbvl) {
					      	location.reload();
					      }
					    });
					}
				});
				

		      }
		    });
		});
	});
	//Agregar otra opción
	/*
	$('.add-opcion').click(function(){
		console.log('si quiere agregar nuevo');
		c++;
		//console.log('hola');
		$('.all-options').append('<div class="col-xs-12 col-sm-12 col-lg-12 opcion-op"><div class="col-xs-1 col-sm-2 col-lg-1"><div class="form-animate-radio"><label class="radio"><input  type="radio" name="radios" value="'+c+'" q=""><span class="outer"><span class="inner"></span></span></label></div></div><div class="col-xs-10 col-sm-2 col-lg-10"><label class="editor_edit resp-opcion" i="'+c+'">Opción '+c+'</label></div></div><div class="col-xs-12 col-sm-12 col-lg-12 text-center padding0">');
		var editor = new MediumEditor('.editor_edit', {
	    toolbar: {
	        allowMultiParagraphSelection: true,
	        //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
	        buttons: ['bold', 'italic', 'anchor'],
	        diffLeft: 0,
	        diffTop: -10,
	        firstButtonClass: 'medium-editor-button-first',
	        lastButtonClass: 'medium-editor-button-last',
	        relativeContainer: null,
	        standardizeSelectionStart: false,
	        static: false,

	        align: 'center',
	        sticky: false,
	        updateOnEmptySelection: false
	    }
		});
	});*/
	//Ver pregunta
	$('.div-pregunts').click(function(){
		var vq = $(this).attr('v');
		var qv = $(this).attr('q');
		var iq = $(this).attr('i');
		$.ajax({
	      type:'POST',
	      url : base_url+'materiasfunc/getquestion',
	      //data : 'quiz='+encodeURIComponent(vq)+'&i='+iq+'&q='+q+'&t=',
	      data : 'quiz='+encodeURIComponent(vq)+'&i='+iq+'&question='+encodeURIComponent(qv),
	      success : function(vl) {
	      	$('.print-question').html(vl);
	      	var editor = new MediumEditor('.editor_edit', {
		    toolbar: {
		        allowMultiParagraphSelection: true,
		        //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
		        buttons: ['bold', 'italic', 'anchor'],
		        diffLeft: 0,
		        diffTop: -10,
		        firstButtonClass: 'medium-editor-button-first',
		        lastButtonClass: 'medium-editor-button-last',
		        relativeContainer: null,
		        standardizeSelectionStart: false,
		        static: false,

		        align: 'center',
		        sticky: false,
		        updateOnEmptySelection: false
		    }
			});
			//Guardar pregunta
			$('.save-qst').click(function(){
				var ip = $(this).attr('i');;
				var qv = $(this).attr('v');
				var qname = $('#title-q').text();
				if(qname){
					var dat;
					if(qv==1){
						dat = 'q='+encodeURIComponent(vq)+'&i='+ip+'&name='+qname+'&new=0';
					}else{
						dat = 'q='+encodeURIComponent(vq)+'&i='+ip+'&name='+qname+'&new=1';
					}
					$.ajax({
				      type:'POST',
				      url : base_url+'materiasfunc/savequestion',
				      data : dat,
				      success : function(sbvl) {
				      	location.reload();
				      }
				    });
				}
			});
			//Opción multiple
			$(".checkbox").change(function() {
				var c = 1;
			    if(this.checked) {
			        $('.opciones-q').html('<div class="col-xs-12 col-sm-12 col-lg-12 all-options"><div class="col-xs-12 col-sm-12 col-lg-12 opcion-op"><div class="col-xs-1 col-sm-2 col-lg-1"><div class="form-animate-radio"><label class="radio"><input  type="radio" name="radios" value="1" q=""><span class="outer"><span class="inner"></span></span></label></div></div><div class="col-xs-10 col-sm-2 col-lg-10"><label class="editor_edit resp-opcion" i="1">Opción 1</label></div></div></div><div class="col-xs-12 col-sm-12 col-lg-12 text-center padding0"><button type="button" class="btn btn-primary add-opcion"><i class="fa fa-plus" aria-hidden="true"></i></button></div></div>');
			        
					//Agregar otra opción
					//var c = 1;
					$('.add-opcion').click(function(){
						c++;
						$('.all-options').append('<div class="col-xs-12 col-sm-12 col-lg-12 opcion-op"><div class="col-xs-1 col-sm-2 col-lg-1"><div class="form-animate-radio"><label class="radio"><input  type="radio" name="radios" value="'+c+'" q=""><span class="outer"><span class="inner"></span></span></label></div></div><div class="col-xs-10 col-sm-2 col-lg-10"><div class="col-xs-11 col-sm-12 col-lg-11"><label class="editor_edit resp-opcion" i="'+c+'">Opción '+c+'</label></div><div class="col-xs-1 col-sm-12 col-lg-1 text-right"><a class="dlt-option-tmp"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div></div>');
						//console.log('hola');
						$('.dlt-option-tmp').click(function(){
							$(this).parent().parent().parent().addClass('animated bounceOut');
							setTimeout(function(){ $('.bounceOut').hide(); }, 700);
							//c--;
						});

						var editor = new MediumEditor('.editor_edit', {
					    toolbar: {
					        allowMultiParagraphSelection: true,
					        //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
					        buttons: ['bold', 'italic', 'anchor'],
					        diffLeft: 0,
					        diffTop: -10,
					        firstButtonClass: 'medium-editor-button-first',
					        lastButtonClass: 'medium-editor-button-last',
					        relativeContainer: null,
					        standardizeSelectionStart: false,
					        static: false,

					        align: 'center',
					        sticky: false,
					        updateOnEmptySelection: false
					    }
						});
					});

					var editor = new MediumEditor('.editor_edit', {
				    toolbar: {
				        allowMultiParagraphSelection: true,
				        //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
				        buttons: ['bold', 'italic', 'anchor'],
				        diffLeft: 0,
				        diffTop: -10,
				        firstButtonClass: 'medium-editor-button-first',
				        lastButtonClass: 'medium-editor-button-last',
				        relativeContainer: null,
				        standardizeSelectionStart: false,
				        static: false,

				        align: 'center',
				        sticky: false,
				        updateOnEmptySelection: false
				    }
					});
					///Guardar
					$('.save-qstnm').click(function(){
						var q = $(this).attr('q');
						var title_q = $('#title-q').html();
						if(title_q!=""){
							var array = [];
							$.each( $( ".resp-opcion" ), function( key, value ) {
								var data = {};
								data.qst=q;
								data.qsnm=title_q;
								data.opc=$(this).html();
					            data.ord=$(this).attr('i');
					            array.push(data);
							});
							$.ajax({
						        type:'POST',
						        url : base_url+'materiasfunc/saveoptions',
						        data : {files: array},
						        dataType: "json",
						        beforeSend:function(){
						        	$('.capa-white').fadeIn(600);
						        	setTimeout(function(){ $('.capa-white').fadeOut(600); }, 300);
						        	
						        },
						        success : function(vl) {
						          	location.reload();
						        }
						    });
						}
					});
			    }else{
			    	$('.opciones-q').html('');
			    }
			});
			///Guardar
					
			$('.save-qstnm').click(function(){
				var q = $(this).attr('q');
				var title_q = $('#title-q').html();
				if(title_q!=""){
					var array = [];
					$.each( $( ".resp-opcion" ), function( key, value ) {
						var data = {};
						data.qst=q;
						data.qsnm=title_q;
						data.opc=$(this).html();
			            data.ord=$(this).attr('i');
			            array.push(data);
					});
					$.ajax({
				        type:'POST',
				        url : base_url+'materiasfunc/saveoptions',
				        data : {files: array},
				        dataType: "json",
				        beforeSend:function(){
				        	$('.capa-white').fadeIn(600);
						    setTimeout(function(){ $('.capa-white').fadeOut(600); }, 300);
				        },
				        success : function(vl) {
				          location.reload();
				        }
				    });
				}
			});

			//Seleccionar respuesta
			$('input:radio[name=radios]').change(function() {
				var q = $(this).attr('q');
				if(q!=""){
					$.ajax({
				      type:'POST',
				      url : base_url+'materiasfunc/saveanswer',
				      data : 'question='+encodeURIComponent(q)+'&anws='+encodeURIComponent(this.value),
				      beforeSend:function(){
				      	
				      },
				      success : function(vl) {
				      	
				      }
				    });
			        //alert(this.value);
		    	}
		    });
		    //Borrar pregunta
			$('.dlt-option').click(function(){
				$(this).parent().parent().parent().addClass('animated bounceOut');
				setTimeout(function(){ $('.bounceOut').hide(); }, 700);
				var v = $(this).attr('v');
				$.ajax({
			      type:'POST',
			      url : base_url+'materiasfunc/deleteoption',
			      data : 'q='+encodeURIComponent(v),
			      success : function(sbvl) {
			      	
			      	//location.reload();
			      }
			    });
			});
			//agregar otra pregunta
			var cn = $('.opcion-op').size();
			$('.add-opcion').click(function(){
				cn++;
				$('.opciones-q').append('<div class="col-xs-12 col-sm-12 col-lg-12 opcion-op"><div class="col-xs-1 col-sm-2 col-lg-1"><div class="form-animate-radio"><label class="radio"><input  type="radio" name="radios" value="'+cn+'" q=""><span class="outer"><span class="inner"></span></span></label></div></div><div class="col-xs-10 col-sm-2 col-lg-10"><div class="col-xs-11 col-sm-12 col-lg-11"><label class="editor_edit resp-opcion" i="'+cn+'">Opción '+cn+'</label></div><div class="col-xs-1 col-sm-12 col-lg-1 text-right"><a class="dlt-option-tmp"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div></div>');
				var editor = new MediumEditor('.editor_edit', {
			    toolbar: {
			        allowMultiParagraphSelection: true,
			        //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
			        buttons: ['bold', 'italic', 'anchor'],
			        diffLeft: 0,
			        diffTop: -10,
			        firstButtonClass: 'medium-editor-button-first',
			        lastButtonClass: 'medium-editor-button-last',
			        relativeContainer: null,
			        standardizeSelectionStart: false,
			        static: false,

			        align: 'center',
			        sticky: false,
			        updateOnEmptySelection: false
			    }
				});
				//
				$('.dlt-option-tmp').click(function(){
					$(this).parent().parent().parent().addClass('animated bounceOut');
					setTimeout(function(){ $('.bounceOut').hide(); }, 700);
					//cn--;
				});
			});
	      }
	    });
	});

	
	/*
	//Guardar pregunta
	$('.save-qst').click(function(){
		var i = $(this).attr('i');
		var c = $(this).attr('c');
		$.ajax({
	      type:'POST',
	      url : base_url+'materiasfunc/savequestion',
	      data : 'c='+encodeURIComponent(c)+'&i='+i,
	      beforeSend:function(){
	      	//$('.capa-white').fadeIn(600);
	      },
	      success : function(vl) {
	      	location.reload();
	      }
	    });
	});*/
	var editor = new MediumEditor('.editor_edit', {
    toolbar: {
        allowMultiParagraphSelection: true,
        //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
        buttons: ['bold', 'italic', 'anchor'],
        diffLeft: 0,
        diffTop: -10,
        firstButtonClass: 'medium-editor-button-first',
        lastButtonClass: 'medium-editor-button-last',
        relativeContainer: null,
        standardizeSelectionStart: false,
        static: false,
        align: 'center',
        sticky: false,
        updateOnEmptySelection: false
    }
	});

	
</script>