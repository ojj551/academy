<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta name="description" content="Miminium Admin Template v.1">
	<meta name="author" content="Isna Nur Azis">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Xpertcad-academy</title>
 
    <!-- start: Css -->

    <!-- plugins -->
    <?php echo link_tag('assets/css/style.css'); ?>
    <?php echo link_tag('asset/css/bootstrap.min.css'); ?>
    <?php echo link_tag('assets/css/animate.css'); ?>
    <?php echo link_tag('assets/css/jquery-confirm.css'); ?>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    <?php echo link_tag('asset/css/plugins/simple-line-icons.css'); ?>
    <?php echo link_tag('asset/css/plugins/animate.min.css'); ?>
    <?php echo link_tag('asset/css/plugins/fullcalendar.min.css'); ?>
    
    <?php echo link_tag('assets/css/swiper.min.css'); ?>
    <?php echo link_tag('assets/css/jquery-ui.css'); ?>
    <?php echo link_tag('asset/css/style.css'); ?>
    <?php //echo link_tag('assets/js/editortools/sandbox/sandbox.css'); ?>
    <?php echo link_tag('assets/js/editortools/build/content-tools.min.css'); ?>
    <?php echo link_tag('asset/css/plugins/bootstrap-material-datetimepicker.css'); ?>
    <?php echo link_tag('asset/css/plugins/mediaelementplayer.css'); ?>
    <?php echo link_tag('assets/css/video-js.min.css'); ?>
    <?php echo link_tag('assets/css/videojs-resolution-switcher.css'); ?>
    <?php echo link_tag('assets/css/admin.css'); ?>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Nunito:300,400,700|Open+Sans:300,400,700|Roboto:300,400,700" rel="stylesheet">
    <script src="<?php echo site_url('asset/js/jquery.min.js'); ?>" ></script>
	<!-- end: Css -->

	<!--<link rel="shortcut icon" href="asset/img/logomi.png">-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
   <body id="mimin" class="dashboard">