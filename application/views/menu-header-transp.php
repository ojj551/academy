<div class="col-xs-12 col-sm-12 col-lg-12 padding0 header-transparent">
	<div class="col-xs-12 col-sm-3 col-lg-3">
		<img src="<?php echo site_url('assets/img/escuelas/unaq/unaq.png'); ?>" style="width: 169px;">
	</div>
	<div class="col-xs-12 col-sm-9 col-lg-9 text-right div-links-he">
		<?php if($UserTypeID=="1"){ ?>
			<a class="linkmenu tpg-relawey"><i class="fa fa-home font-header" aria-hidden="true"></i> Inicio</a>
			<a class="linkmenu tpg-relawey" v="cursos"><i class="fa fa-book font-header" aria-hidden="true"></i> Cursos</a>
			<a class="linkmenu tpg-relawey" v="alumnos"><i class="fa fa-user font-header" aria-hidden="true"></i> Alumnos</a>
			<a class="linkmenu tpg-relawey"><i class="fa fa-calendar-o font-header" aria-hidden="true"></i> Calendario</a>

			<a class="linkmenu tpg-relawey" v="profile"><i class="fa fa-user font-header" aria-hidden="true"></i> <?php echo $this->session->userdata('UserFirstName'); ?></a>
		<?php } ?>
	</div>
</div>