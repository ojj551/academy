<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php $this->load->view('header-left'); ?> 
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Lista de alumnos</h3>

          </div>
      </div>                    
    </div>  
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="">
			<a class="tpg-relawey add-curso-directo" data-toggle="modal" data-target="#addalumnos-form"><i class="fa fa-plus" aria-hidden="true"></i> Dar de alta un alumno</a>
		</div>
		<table class="table table-bordered">
		    <thead>
		      <tr>
		      	<th>Fecha de creación</th>
		        <th>ID Referencia</th>
		        <th>Email</th>
		        <th>Nombre</th>
		        <th>Apellido</th>
		        <th>Customer</th>
		        <th>Status</th>
		        <th>-</th>
		      </tr>
		    </thead>
		    <tbody>
		    	<?php echo $list; ?>
		    </tbody>
		  </table>
	</div>
	<!--Alumnos-->
	<div id="addalumnos-form" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Dar de alta alumnos</h4>
	      </div>
	      <div class="modal-body">
            <div class="form-group form-animate-text">
	        	<a>4</a>
		        <span>Institución/Empresa</span>
		        <select class="form-control cliente" id="client1">
		          <option value="">Selecciona escuela a la que pertenece</option>
		          <?php echo $listclients; ?>
		        </select>
	        </div>
            <div class="form-group form-animate-text">
            	<a>3</a>
		        <span>Tipo de usuario</span>
		        <select class="form-control tipouser" id="typealum1">
		          <option>Selecciona un tipo de usuario</option>
		          <?php echo $listtype; ?>
		        </select>
	        </div>
	        <div class="paintbytype">
        	
        	</div>
	      </div>
	      <div class="modal-footer">
	        <div class="modal-footer">
		        <button type="button" class="btn btn-primary" id="createalumnos"><?php echo $FormGuardar; ?></button>
		        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
		     </div>
	      </div>
	    </div>

	  </div>
	</div>
</div>
<?php $this->load->view('clientes/modal-add-user-notadmin'); ?>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer-admin'); ?>  
<script src="<?php echo site_url('assets/js/admin.js'); ?>"></script>
