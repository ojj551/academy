<?php $this->load->view('cabecera'); ?>
<video class="login-video" autoplay loop>
  <source src="<?php echo site_url('assets/video/Wall-Sketching.mp4'); ?>" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
<div class="login-conteiner">
	<div class="col-xs-12 col-sm-12 col-lg-12 text-center">
		<img class="login-logo" src="<?php echo site_url('assets/img/logodemo.png'); ?>">
		<div class="col-xs-12 col-sm-12 col-lg-12 text-left divbacklogin" style="display: none;">
			<div class="col-xs-4 col-sm-12 col-lg-4"></div>
			<div class="col-xs-4 col-sm-12 col-lg-4">
				<a class="login-opc backlogin" style="font-size: 11px;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Regresar a login</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-12">
			<div class="col-xs-4 col-sm-12 col-lg-4"></div>
			<div class="col-xs-4 col-sm-12 col-lg-4 login-form">
				<form action="<?php echo base_url() ?>login/login" method="post" id="loginform">
					<input class="form-control login-inputs" type="email" name="email" id="email" placeholder="Email" required>
					<input class="form-control login-inputs" type="password" name="password" id="password" placeholder="Contraseña" required>
                    <button type="submit" class="btn btn-secondary" id="entrar">Entrar</button>
                </form>
			</div>
			<div class="col-xs-4 col-sm-12 col-lg-4 create-form" style="display: none;">
				<input class="form-control login-inputs" type="text" name="matricula" id="matriculac" placeholder="Matricula">
				<input class="form-control login-inputs" type="text" name="nombrec" id="nombrec" placeholder="Nombre(s)">
				<input class="form-control login-inputs" type="text" name="apellidosc" id="apellidosc" placeholder="Apellidos">
				<input class="form-control login-inputs" type="text" name="email" id="emailc" placeholder="Email">
				<input class="form-control login-inputs" type="password" name="passwordc" id="passwordc" placeholder="Contraseña">
				<input class="form-control login-inputs" type="password" name="passwordc2" id="passwordc2" placeholder="Confirmación de Contraseña">
				<button type="button" class="btn btn-secondary" id="create">Crear cuenta</button>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-12 more-details">
			<div class="col-xs-4 col-sm-12 col-lg-4"></div>
			<div class="col-xs-4 col-sm-12 col-lg-4">
				<div class="col-xs-5 col-sm-12 col-lg-5 text-left">
					<a class="login-opc" data-toggle="modal" data-target="#forget">Olvidé mi contraseña</a>
				</div>
				<div class="col-xs-2 col-sm-12 col-lg-2 text-center divpoint"><label style="color: #fff;"><i class="fa fa-circle" aria-hidden="true"></i></label></div>
				<div class="col-xs-5 col-sm-12 col-lg-5 text-right">
					<a class="login-opc" id="create-account">Crear una cuenta</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="forget" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content print-nw">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Olvidé mi contraseña</h4>
      </div>
      <div class="modal-body">
      	<label class="text-forget">Ingresa tu correo eléctronico</label>
        <input class="form-control" type="text" id="email-forget" style="border-left: 0px;border-top: 0px;border-right: 0px;box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075);border-bottom: 2px solid #1c84c6;" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="send-forget">Enviar</button>
        <a class="" data-dismiss="modal">O cancelar</a>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('footer'); ?>
<script>

	$('#create-account').click(function(){
		$('.login-form').addClass('animated bounceOutUp');
		$('.more-details').hide();
		setTimeout(function(){ 
			$('.create-form').show();
			$('.create-form').removeClass('bounceOutUp');
			$('.create-form').addClass('animated bounceInDown');
			$('.login-form').hide();

			$('.divbacklogin').show();
		}, 700);
	});
	$('.backlogin').click(function(){
		$('.create-form').addClass('animated bounceOutUp');
		$('.divbacklogin').hide();
		setTimeout(function(){ 
			$('.login-form').show();
			$('.login-form').removeClass('bounceOutUp');
			$('.login-form').addClass('animated bounceInDown');
			$('.create-form').hide(); 
			$('.more-details').show();
		}, 700);
	});
	//Crear cuenta
	$('#create').click(function(){
		console.log('entro al create');
		var matriculac = $('#matriculac').val();
		var nombrec = $('#nombrec').val();
		var apellidosc = $('#apellidosc').val();
		var emailc = $('#emailc').val();
		var passwordc = $('#passwordc').val();
		var passwordc2 = $('#passwordc2').val();
		/*
		if(passwordc!="" && passwordc2!=""){
			if(passwordc!=passwordc2){
				$.dialog({
					    title: 'Contraseñas mal escritas',
					    content: 'La confirmación de tu contraseña no coincide con tu contraseña ingresada',
				});
			}
		}*/
		if(matriculac!="" && nombrec!="" && apellidosc!="" && emailc!="" && passwordc!="" && passwordc2!=""){
			if(passwordc==passwordc2){
				$.ajax({
					type:'POST',
			       	url : base_url+'login/createaccount',
			       	data: 'matriculac='+matriculac+'&nombrec='+nombrec+'&apellidosc='+apellidosc+'&emailc='+emailc+'&passwordc='+passwordc+'&passwordc2='+passwordc2,
					beforeSend:function(){
						console.log('enviando...');
					},
					success : function(data) {
						if(data!="success"){
							$.dialog({
							    title: '¡Problemas al crear tu cuenta! <i class="fa fa-frown-o" aria-hidden="true"></i>',
							    content: data,
							});
						}else{
							$.dialog({
							    title: 'Tu registro se ha generado exitosamente',
							    content: '<strong>Solo te falta un paso más</strong> te hemos enviado un correo para que actives tu cuenta',
							});
						}
					}
				});
			}else{
				$.dialog({
				    title: 'Contraseñas mal escritas',
				    content: 'La confirmación de tu contraseña no coincide con tu contraseña ingresada',
				});
			}
		}
	});
	//Login
	/*$('#entrar1').click(function(){
		var email = $('#email').val();
		var password = $('#password').val();
		//
		if(email!="" && password!=""){
			$( "#loginform" ).submit();
			/!*
			$.ajax({
				type:'POST',
		       	url : base_url+'login/login',
		       	data: 'email='+email+'&password='+password,
				beforeSend:function(){
				},
				success : function(data) {
					
					if(data!="success"){
						$.dialog({
						    title: 'Email y/o contraseña no estan correctas <i class="fa fa-frown-o" aria-hidden="true"></i>',
						    content: '',
						});
					}else{
						$('.login-logo').addClass('animated bounceOutUp');
						setTimeout(function(){ 
							$('#email').addClass('animated bounceOutUp');
							$('#password').addClass('animated bounceOutUp');
							$('#entrar').addClass('animated bounceOutUp');
							setTimeout(function(){ 
								$('.login-opc').addClass('animated bounceOutUp');
								setTimeout(function(){ 
									$('.divpoint').addClass('animated bounceOutUp');
									$(location).attr('href', base_url+'inicio');
								},600);
							},600);
						},600);
					}
					
				}
			});
			*!/
		}
	});*/


	//Olvide contraseña
	$('#send-forget').click(function(){
		var email = $('#email-forget').val();
		if(email!=""){
			var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
			if (regex.test(email.trim())) {
				$.ajax({
					type:'POST',
			       	url : base_url+'login/recovery',
			       	data: 'email='+email,
					beforeSend:function(){
						$('.capaload').fadeIn(600);
					},
					success : function(data) {
						$('.capaload').fadeOut(600);
						if(data!="error"){
							$('.print-nw').html();
							$('.print-nw').html(data);
						}else{
							$.dialog({
							    title: '¡No se hemos encontrado tu correo! <i class="fa fa-frown-o" aria-hidden="true"></i>',
							    content: data,
							});
						}
					}
				});
			}else{
				$('#email-forget').addClass('animated shake');
				setTimeout(function(){ 
					$('#email-forget').removeClass('animated shake');
				},600);
			}
		}else{
			$('.text-forget').addClass('animated shake');
			$('#email-forget').addClass('animated shake');
				setTimeout(function(){ 
					$('.text-forget').removeClass('animated shake');
					$('#email-forget').removeClass('animated shake');
			},600);
		}
	});

	<?php if($err): ?>
    $.dialog({
        title: 'Email y/o contraseña no estan correctos',
        content: ''
    });
    <?php endif; ?>
</script>