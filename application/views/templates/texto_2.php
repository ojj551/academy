<?php $this->load->view('templates/cabecera'); ?>
<div class="col-md-12 bod">
    <div class="padding0 margin0" style="background: transparent;">
        <div class="panel-body padding0 margin0">
          <blockquote class="dtos editable" data-editable data-name="article" tpc="<?php echo $Topic; ?>" tmpl="<?php echo $Template; ?>" ord="<?php echo $Orden; ?>">
          	<?php
	        	if($content!=""){
	        		$textos = $content;
	        	}else{
	        		$textos = '
	        			<p class="editable" etiqueta="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
	        		';
	        	}
	        	echo $textos;
	        ?>
            
          </blockquote>
        </div>
    </div>
</div>
<?php $this->load->view('templates/footer'); ?>
<script>
	//Tamaño de iframe 
	$( document ).ready(function() {
		var ord = $('.dtos').attr('ord');
		var height = $('.bod').height();
		parent.$(parent.document).trigger('heightfunc',[height,ord]);
	});
</script>