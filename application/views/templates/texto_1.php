<!--Estos archivos de templates no se estan utilizando-->
<?php $this->load->view('templates/cabecera'); ?>
<div class="col-md-12 bod" >
    <div class="padding0 margin0">
        <div class="dtos" style="padding:0px;padding-left: 19px;padding-right: 19px;margin-bottom: 0px;" data-editable data-name="article" tpc="<?php echo $Topic; ?>" tmpl="<?php echo $Template; ?>" ord="<?php echo $Orden; ?>">
	        <?php
	        	if($content!=""){
	        		$textos = $content;
	        	}else{
	        		$textos = '
	        			<h4 class="article__by-line editable" etiqueta="title" style="font-size: 30px;">Default well</h4>
			            <p class="editable" etiqueta="description" style="font-size: 20px;">Sed libero. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis.

			            		Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vitae tortor. Nullam sagittis. Vestibulum eu odio.</p>
	        		';
	        	}
	        	echo $textos;
	        ?>
            
        </div>
    </div>

</div>
<?php $this->load->view('templates/footer'); ?>
<script>
	//Tamaño de iframe 
	$( document ).ready(function() {
		var ord = $('.dtos').attr('ord');
		var height = $('.bod').height();
		parent.$(parent.document).trigger('heightfunc',[height,ord]);
	});
</script>