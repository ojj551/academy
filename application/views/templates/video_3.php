<?php $this->load->view('templates/cabecera'); ?>
<div class="col-md-12 dts" tpc="<?php echo $Topic; ?>" tmpl="<?php echo $Template; ?>" ord="<?php echo $Orden; ?>">
	<?php
    	if($content!=""){
    		$textos = $content;

    		echo '
    			<div class="edit-multimedia">
    				<a>	<i class="fa fa-pencil" aria-hidden="true"></i>	</a>
    			</div>
    		';
    	}else{
    		$textos = '
    			<div class="col-md-12 panel">
				    <div class="col-md-12 panel-body">
				    	<div class="col-md-2"></div>
					    <div class="col-md-8 text-center" style="padding-top: 30px;">
				            <div class="form-group form-animate-text">
				              <input type="text" class="form-text" id="validate_url" name="validate_url" required>
				              <span class="bar"></span>
				              <label>Pega la url del video</label>
				            </div>
				            <div id="myForm">
				            	<label>Tamaño del video</label><br>
				            	<div class="form-animate-radio">
			                      	<label class="radio">
			                        <input id="radio1" type="radio" name="tam" value="c">
			                        <span class="outer">
			                          <span class="inner" style="background:#00ADEF !important;"></span></span> Chico
			                        </label>
			                        <label class="radio">
			                        <input id="radio2" type="radio" name="tam" value="m">
			                        <span class="outer">
			                          <span class="inner" style="background:#00ADEF !important;"></span></span> Mediano
			                        </label>
			                        <label class="radio">
			                        <input id="radio2" type="radio" name="tam" value="g">
			                        <span class="outer">
			                          <span class="inner" style="background:#00ADEF !important;"></span></span> Grande
			                        </label>
			                    </div>
				            </div>
				             <button class="btn btn-danger" id="save-video" style="background:#00ADEF !important;">Guardar video</button>
					    </div>
				  	</div>
				</div>
    		';
    	}
    	echo $textos;
    ?> 
  	
</div>
<?php $this->load->view('templates/footer'); ?>
<script>
	function isUrlValid(url) {
	    if(/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(url)){
	    	return true;
		} else {
	   		return false;
		}
	}
	$('#save-video').click(function(){
		var tam = $('input[name=tam]:checked', '#myForm').val();
		var tpc = $('.dts').attr('tpc');
		var tmpl = $('.dts').attr('tmpl');
		var ord = $('.dts').attr('ord');
		var validate_url = $('#validate_url').val();
		if(validate_url!="" && tam!=""){
			if(isUrlValid(validate_url)){
				$.ajax({
			      type:'POST',
			      url : base_url+'temasfunc/addvideo',
			      data: 'url='+encodeURIComponent(validate_url)+'&type=3'+'&tpc='+encodeURIComponent(tpc)+'&tmpl='+encodeURIComponent(tmpl)+'&ord='+encodeURIComponent(ord)+'&tam='+tam+'&opc=add',
			      beforeSend:function(){
			      	$('.capaload').fadeIn(600);
			      },
			      success : function(vl) {
			      	$('.capaload').fadeOut(600);
			      	
			      }
			    });
			}else{
				console.log('No se puede guardar no es url');
			}
		}else{
			console.log('Url vacia');
		}
	});
	//Tamaño de iframe 
	$( document ).ready(function() {
		var ord = $('.dts').attr('ord');
		var height = $(this).height();
		parent.$(parent.document).trigger('heightfunc',[height,ord]);
	});
	//Editar video
	$('.edit-multimedia').click(function(){
		

		var src =$('iframe').attr('src');
		var tam = $('input[name=tam]:checked', '#myForm').val();
		var tpc = $('.dts').attr('tpc');
		var tmpl = $('.dts').attr('tmpl');
		var ord = $('.dts').attr('ord');

		var content = '<div class="text-center"><div class="form-group form-animate-text"><input type="text" class="form-text" id="validate_url" name="validate_url" value="'+src+'" required><span class="bar"></span><label>Pega la url del video</label></div><div id="myForm"><label>Tamaño del video</label><br><div class="form-animate-radio"><label class="radio"><input id="radio1" type="radio" name="tam" value="c"><span class="outer"><span class="inner"></span></span> Chico</label><label class="radio"><input id="radio2" type="radio" name="tam" value="m"><span class="outer"><span class="inner"></span></span> Mediano</label><label class="radio"><input id="radio2" type="radio" name="tam" value="g"><span class="outer"><span class="inner"></span></span> Grande</label><br><button class="btn btn-danger" id="save-video">Actualizar video</button></div></div>';

		parent.$(parent.document).trigger('edt-item',[src,tam,tpc,tmpl,ord,content]);
	});
</script>