<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php $this->load->view('header-left'); ?> 
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Avance <i><?php echo $CustomerName; ?></i></h3>

          </div>
      </div>                    
    </div>  
    <div class="col-md-12">
      <div class="col-md-12">
          
	      <div class="col-md-12 tabs-area">

	         <div class="col-md-6">
              <div class="panel">
                   <div class="panel-heading-white panel-heading text-center">
                      <h4>Avance total de mi institución</h4>
                    </div>
                    <div class="panel-body">
                      <center>
                        <canvas id="myChart" class="doughnut-chart"></canvas>
                        <strong>Avanzado: </strong><span><?php echo $avanceglobal; ?>%</span><br>
                        <strong>Por avanzar: </strong><span><?php echo $poravanzarglobal; ?>%</span><br>
                      </center>
                    </div>
              </div>
            </div>
            <div class="col-md-<?php echo $grig; ?>">
                <div class="panel">
                       <div class="panel-heading-white panel-heading">
                          <h4>Avance por carrera</h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                             <canvas id="carreras" width="1300" height="1000"></canvas>
                            </div>
                        </div>
                  </div>
            </div>
	      </div>
      </div>
  </div>
</div>

<!--MODALES-->
<?php $this->load->view('clientes/modal-add-carrera'); ?>
<?php $this->load->view('clientes/modal-add-materia'); ?>
<?php $this->load->view('footer-admin'); ?>  
<script src="<?php echo site_url('assets/js/admin.js'); ?>"></script>
<script>
    var avance = <?php echo $avanceglobal; ?>;
    avance = parseFloat(avance);
    var poravanzar = 100-avance;
    var doughnutData = [
      {
          value: avance,
          color:"#2196F3",
          highlight: "#3d9de8",
          label: "Avance"
      },
      {
          value: poravanzar,
          color: "#f52a2a",
          highlight: "#ea4444",
          label: "Por avanzar"
      }
    ];
    var ctx = document.getElementById("myChart").getContext('2d');

    var myChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: ["Avance", "Por avanzar"],
          datasets: [{
              label: '# of Votes',
              data: [avance, poravanzar],
              backgroundColor: [
                  '#ff7800',
                  '#b5b5b5',
              ]
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
  });
  ///
  var list = <?php echo json_encode($NamesCarreras); ?>;
  var progress = <?php echo json_encode($AVGCarreras); ?>;
  var ctx2 = document.getElementById("carreras");
  var data = {
      labels: list,
      datasets: [
          {
              label: "Avance",
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(26, 197, 72, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(26, 197, 72, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(75, 192, 192, 1)',
              ],
              borderWidth: 1,
              data: progress,
          }
      ]
  };
  new Chart(ctx2, {
      data: data,
      type: "bar",
      options: {
          scales: {
            xAxes: [{
                display: true,
                ticks: {
                    suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                    // OR //
                    beginAtZero: true,   // minimum value will be 0.
                    suggestedMax: 100
                }
            }]
        }
      }
  });
</script>
<script>
  /*
  var avance = <?php echo $avanceglobal; ?>;
  avance = parseFloat(avance);
  var poravanzar = 100-avance;
  var doughnutData = [
    {
        value: avance,
        color:"#2196F3",
        highlight: "#3d9de8",
        label: "Avance"
    },
    {
        value: poravanzar,
        color: "#f52a2a",
        highlight: "#ea4444",
        label: "Por avanzar"
    }
  ];
  var ctx = $(".doughnut-chart")[0].getContext("2d");
  window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
      responsive : true,
      showTooltips: true
  });
  //Bar
  var NamesCarreras = <?php echo json_encode($NamesCarreras); ?>;
  var AVGCarreras = <?php echo json_encode($AVGCarreras); ?>;
  var barData = {
      labels: NamesCarreras,
      datasets: [
          {
              label: "My First dataset",
              fillColor: "rgba(21,186,103,0.5)",
              data: AVGCarreras
          }
      ]
  };
  var ctx4 = $(".bar-chart")[0].getContext("2d");
  window.myBar = new Chart(ctx4).Bar(barData, {
      responsive : true,
      showTooltips: true
  });
  */
</script>