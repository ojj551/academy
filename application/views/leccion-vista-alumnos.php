<?php $this->load->view('cabecera-admin'); ?>
<link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet">
<style>
    body{
       font-family: 'Open Sans', sans-serif;
    }
</style>
<?php echo link_tag('assets/css/style-alumn.css'); ?>
<div class="col-md-12 col-sm-12 col-lg-12  "> 
 
    <div class="menu-users-vw">
      <?php echo $list; ?>
    </div>
    <div class="contet-users-vw">
      <div class="col-md-12 col-sm-12 col-lg-12 title-lesson-alm">
          <a class="hmb-users-vw"><i class="fa fa-bars" aria-hidden="true"></i></a>
          <h2><?php echo $NameTopic; ?></h2>
      </div>
      <div class="col-md-12 col-sm-12 col-lg-12 padding0 contenidos-load">
        <?php echo $loads; ?>
      </div>
    </div>

</div>
<div id="progress" data-0="width:0%;" data-end="width:100%;" class="skrollable skrollable-between" style="position: fixed;top: 0px;left: 0px;z-index: 999999;height: 5px;background: orange;"></div>
<?php $this->load->view('footer-admin'); ?>

<script>
    
	  var swpprogress = new Swiper(".swpprogress", {
        paginationType: "progress"
    });
    var swgalery = new Swiper(".swgalery", {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        autoplay: 1400
    });

    //Porcentaje de avance conforme a scroll
    var porcent = 0;
    $(window).scroll(function (event) {
        porcent = $("#progress").width() / $('#progress').parent().width() * 100;
    });
    //Condición si contenido es menor al scroll
    var hgwnd = $(window).height();
    var hgcon = $('.contenidos-load').height();
    if(porcent<=0){
      if(hgcon<=hgwnd){
        porcent = 100;
      }
    }
    //condición si hay contenido enviar avance si no no
    var loads = "<?php echo $havecontent; ?>";
    if(loads=="1"){
      //salir de topic 
      var tpc = "<?php echo $TopicID ?>";
      window.onbeforeunload = function(event) {
          //event.returnValue = "";
          //console.log(porcent);
          $.ajax({
            type:'POST',
            url : base_url+'temasfunc/avancetopic',
            data: 'p='+porcent+'&tpc='+encodeURIComponent(tpc),
            beforeSend:function(){
            },
            success : function(data) {
            }
          });
      };
    }

    $('.points').click(function(){
    	var v = $(this).attr('v');
    	var lf = $(this).attr('lf');
    	var tp = $(this).attr('tp');
    	$('.speech-bubble').fadeOut(300);
    	$('.vwm'+v).fadeIn(300);
    	$('.vwm'+v).css('left',lf+'%');
    	$('.vwm'+v).css('top',tp+'%');
    	
    });
    $('.close-dc-point').click(function(){
    	$(this).parent().fadeOut(300);
    });
    //close menu
    $('.hmb-users-vw').click(function(){
      $('.menu-users-vw').toggle();
      $('.contet-users-vw').toggleClass('cont-w-vw');
    });
</script>
<script type="text/javascript">
      $(document).ready(function(){
          $('video,audio').mediaelementplayer({
            alwaysShowControls: true,
            videoVolume: 'vertical',
            features: ['playpause','progress','volume','fullscreen']
          });
      });
</script>