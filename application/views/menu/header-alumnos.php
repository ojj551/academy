<div class="header-userprofile">
	<div class="capa">
		<div class="col-xs-12 col-sm-12 col-lg-12 text-center div-cont-header">
			<h1 class="tpg-logo title-menu blanco" style="line-height: 31px;">Alumnos y grupos</h1>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-12 div-cont-h" style="padding-top: 20px;">
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<i>Alumnos</i>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<a class="tpg-relawey add-curso-directo" data-toggle="modal" data-target="#addalumnos-form"><i class="fa fa-plus" aria-hidden="true"></i> Dar de alta un alumno</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<a class="tpg-relawey add-curso-directo" data-toggle="modal" data-target="#addalumnos-file"><i class="fa fa-plus" aria-hidden="true"></i> Dar de alta alumnos por archivo</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<a class="tpg-relawey add-curso-directo" href="<?php echo base_url() ?>alumnos"><i class="fa fa-list" aria-hidden="true"></i> Mis alumnos</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<i>Grupos</i>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<a class="tpg-relawey add-curso-directo" href="<?php echo base_url() ?>grupos/crear"><i class="fa fa-plus" aria-hidden="true"></i> Crear grupo</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<a class="tpg-relawey add-curso-directo" href="<?php echo base_url() ?>grupos"><i class="fa fa-list" aria-hidden="true"></i> Mis grupos</a>
			</div>
		</div>
	</div>
</div>
<script>
	$('.add-curso-directo').click(function(){
	    $.ajax({
	        type:'POST',
	        url : base_url+'alumnosfunc/ShowTypeUsers',
	        data: 'generate=1',
	        beforeSend:function(){
	        },
	        success: function(data){
	            $('.tipouser').html(data);
	        }
	    });
	    $.ajax({
	        type:'POST',
	        url : base_url+'alumnosfunc/ShowCarreras',
	        data: 'generate=1',
	        beforeSend:function(){
	        },
	        success: function(data){
	            $('.carrera').html(data);
	        }
	    });
	    /*
	    $.ajax({
	        type:'POST',
	        url : base_url+'alumnosfunc/SemestreCarrera',
	        data: 'generate=1',
	        beforeSend:function(){
	        },
	        success: function(data){
	            $('.semestre').html(data);
	        }
	    });*/
	});
</script>