<div class="header-userprofile">
	<div class="capa">
		<div class="col-xs-12 col-sm-12 col-lg-12 text-center div-cont-header">
			<h1 class="tpg-logo title-menu blanco">Materias</h1>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-12 div-cont-h">
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<?php
				if($UserTypeID=="1"){ ?>
					<a class="tpg-relawey add-curso-directo" href="<?php echo base_url() ?>materias/crear"><i class="fa fa-plus" aria-hidden="true"></i> Agregar materia</a>
				<?php
				}
				?>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 20px;">
				<?php echo $materias; ?>
			</div>

		</div>
	</div>
</div>