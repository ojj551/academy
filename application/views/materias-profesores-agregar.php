<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('menu-header'); ?>
<div class="conteiner-right  col-xs-12 col-sm-12 col-lg-12">
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<h1 class="tpg-logo title-section">Materia - Profesor</h1>

	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="col-xs-2 col-sm-12 col-lg-2"></div>
		<div class="col-xs-8 col-sm-12 col-lg-8">
			<div class="col-xs-12 col-sm-12 col-lg-12" >
				<label class="tpg-relawey addmat">Materia</label>
				<select class="form selectpicker" name="materiaprf" id="materiaprf" data-live-search="true">
					<option>Seleccionar...</option>
					<?php echo $materias; ?>
				</select>
			</div>

			<div class="col-xs-12 col-sm-12 col-lg-12" >
				<label class="tpg-relawey addmat">Profesor</label>
				<select class="form selectpicker" name="prfmat" id="prfmat" data-live-search="true">
					<option>Seleccionar...</option>
					<?php echo $alumnos; ?>
				</select>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12" >
				<div class="col-xs-6 col-sm-12 col-lg-6 padding0">
					<label class="tpg-relawey addmat">Inicia</label>
					<input class="form-control form-add-mat" type="date" name="inicia" id="inicia">
				</div>
				<div class="col-xs-6 col-sm-12 col-lg-6 padding0">
					<label class="tpg-relawey addmat">Termina</label>
					<input class="form-control form-add-mat" type="date" name="finaliza" id="finaliza">
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 30px;">
				<a type="button" class="btn btn-secondary savemateria" id="crear-prof-mat">Crear</a>
			</div>
		</div>

	</div>
</div>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer'); ?>
<script>
	$('#crear-prof-mat').click(function(){
		var materiaprf = $('#materiaprf').val();
		var prfmat = $('#prfmat').val();
		var inicia = $('#inicia').val();
		var finaliza = $('#finaliza').val();

		if(materiaprf!="" && prfmat!="" && inicia!="" && finaliza!=""){
			$.ajax({
				type:'POST',
		       	url : base_url+'asignaturas/add',
		       	data: 'mat='+encodeURIComponent(materiaprf)+'&prfmat='+encodeURIComponent(prfmat)+'&inc='+inicia+'&end='+finaliza,
				beforeSend:function(){
					$('.capaload').fadeIn(600);
				},
				success : function(data) {
					$('.capaload').fadeOut(600);
					setTimeout(function(){ 
						if(data=="sucess"){
							$(location).attr('href', base_url+'n');
						}else{
							
							$.dialog({
								title: 'Ups!',
								content: data,
							});
						}
					},700);
				}
			});
		}
	});
</script>