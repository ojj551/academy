<!-- Ayuda pasos -->
<div id="stephelp" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- Editar prevista de materia -->
<!--
<div id="edit-name" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar datos de materia</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idmat" id="idmat" value="<?php echo $MateriaIDEncryp; ?>">
        <label class="tpg-relawey">Nombre</label>
        <input class="form-control" type="text" name="nombremtupdt" id="nombremtupdt" value="<?php echo $CourseName; ?>">
        <label class="tpg-relawey">Fecha inicio</label>
        <input class="form-control" type="date" name="incmupdt" id="incmupdt" value="<?php echo $Inc; ?>">
        <label class="tpg-relawey">Fecha fin</label>
        <input class="form-control" type="date" name="finmupdt" id="finmupdt" value="<?php echo $Fin; ?>">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="updatenamemat">Aceptar</button>
      </div>
    </div>

  </div>
</div>
-->
<div id="edit-banner" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar datos del banner</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idmat" id="idmat" value="<?php echo $MateriaIDEncryp; ?>">
        <label class="tpg-relawey addmat">Banner de la materia</label>
        <input class="form-control" type="file" name="bannermupt" id="bannermupt">
        <label class="tpg-relawey addmat">Color banner</label>
        <input class="form-control" type="color" name="colorbmupt" id="colorbmupt" style="width: 50px;height: 50px;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="updatebannerdata">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<div id="edit-description-mat" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar datos</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idmat" id="idmat" value="<?php echo $MateriaIDEncryp; ?>">
        <label class="tpg-relawey addmat">Requerimientos</label>
        <textarea class="form-control" name="reqmupd" id="reqmupd" style="min-height: 70px;"><?php echo $CourseRequirements; ?></textarea>
        <label class="tpg-relawey addmat">Objetivos</label>
        <textarea class="form-control" name="objmupd" id="objmupd" style="min-height: 70px;"><?php echo $CourseGoals; ?></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="updatedecrpdata">Aceptar</button>
      </div>
    </div>

  </div>
</div>
<!--Alumnos
<div id="addalumnos-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Dar de alta alumnos</h4>
      </div>
      <div class="modal-body">
        <label>ID Referencia alumno</label>
        <input class="form-control idreference" type="text" name="idreference1" id="idreference1">
        <span>No tengo ID Referencia,</span><a class="generateidreference" v="1">generar un ID Referencia unico</a><br>
        <label>Correo del alumno</label>
        <input class="form-control emailreference" type="text" name="emailalumn1" id="emailalumn1">
        <label>Tipo de usuario</label>
        <select class="form-control tipouser" id="typealum1">
          
        </select>
        <label>Carrera</label>
        <select class="form-control carrera" id="carrera1">
        </select>
        <label>Semestre</label><select class="form-control semestre" id="semestre1"></select><br><hr>
        <div class="moreitems">
          
        </div>
        <div class="text-right">
          <input type="hidden" name="cant" id="cant" value="1">
          <a class="addmorealumn">Agregar otro alumnos</a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="createalumnos">Aceptar</button>
      </div>
    </div>

  </div>
</div>
-->
<!--Alumnos-->
<div id="AddMatToUser" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Asignar materia</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idencr" id="idencr" value="<?php echo $UserEncryp; ?>">
        <label>Materia</label>
        <select class="form selectpicker" data-live-search="true" id="mat">
          <option value="">Selecciona materia...</option>
          <?php echo $materias; ?>
        </select>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="asignarmateria">Aceptar</button>
      </div>
    </div>

  </div>
</div>
<!-- TOPICS -->
<div id="add-topic-unidad" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Crear temario</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idmencr" id="idmencr" value="<?php echo $MateriaIDEncryp; ?>">
        
        <div class="sections section-1">
          <label>Orden</label>
          <input class="form-control" type="number" id="numberunidad" min="1">
          <label>Nombre de la unidad</label>
          <input class="form-control" type="text" id="nameunidad">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="create-section">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<div id="add-topic" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Crear temario de la unidad</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="MatSt" value="<?php echo $MateriaIDEncryp; ?>">
        <input type="hidden" id="idsct">
        <div class="sections section-1">
          <div class="padd-left">
              <ol class="temas"></ol>
              <div style="margin-top: 40px;">
                <a class="addtema" v="1"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar tema</a>
              </div>
          </div>

        </div>
        
        <div class="sections adds">

        </div>
        <!--
        <div>
          <a class="sig-seccion"><i class="fa fa-plus-circle" aria-hidden="true"></i> Siguiente unidad</a>
        </div>-->
        

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="create-topic">Aceptar</button>
      </div>
    </div>

  </div>
</div>
<div id="add-sub-topic" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Crear subtema</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="TopicF" value="">
        <div class="sections section-1">
          <div class="padd-left">
              <ol class="temas"></ol>
              <div style="margin-top: 40px;">
                <a class="addtemasub" v="1"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar otro subtema</a>
              </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="create-sub-topic">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<div id="edit-topic" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar tema</h4>
      </div>
      <div class="modal-body paint-topic">
        <input type="hidden" id="idtc">
        <label>Nombre del tema</label>
        <input class="form-control" type="text" id="nametopic-upd"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="update-topic">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<div id="edit-unidad" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar unidad</h4>
      </div>
      <div class="modal-body paint-topic">
        <input type="hidden" id="idunid">
        <label>Nombre de la unidad</label>
        <input class="form-control" type="text" id="nameunidad-upd"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="update-unidad">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<div id="add-material" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar <span class="namemat"></span></h4>
      </div>
      <div class="modal-body paint-topic">
        <input type="hidden" id="idscm">
        <input type="hidden" id="typematvl">

        <label>Titúlo del material</label>
        <input class="form-control" type="text" id="title-matl">
        <label>Descripción del material</label>
        <textarea class="form-control" id="dsc-matl" style="min-height: 60px;"></textarea>
        <div class="filevdo">
          <label>Archivo</label>
          <input class="form-control" type="file" id="file-matl">
        </div>
        <div class="videoytb" style="display: none;">
          <label>Video YouTube</label>
          <input class="form-control" type="text" id="videoyoutube">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="add-matl">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<script>
///////////////////////////Materias temario///////////////////////////////////////////
$('#create-section').click(function(){ 
  var nameunidad = $('#nameunidad').val();
  var numberunidad = $('#numberunidad').val();
  var MatSt = $('#MatSt').val();
  $('#idsct').val('');
  if(nameunidad!="" && numberunidad!=""){
      $.ajax({
      type:'POST',
      url : base_url+'materiasfunc/createsetion',
      data: 'MatSt='+encodeURIComponent(MatSt)+'&name='+encodeURIComponent(nameunidad)+'&number='+numberunidad,
      beforeSend:function(){
        $('#add-topic-unidad').modal('hide');
        $('.capaload').fadeIn(600);
      },
      success : function(data) {
        setTimeout(function(){
          if(data!="error"){
            $('#add-topic').modal('show');
            $('.capaload').fadeOut(600);
            $('#idsct').val(data);
          }else{
            $('.capaload').fadeOut(600);
            $.alert({
              title: '¡Ups! tuvimos problemas para generar la unidad',
              content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
              confirm: function () {
                  $('#add-topic-unidad').modal('show');
              }
            });
          }
        },1000);
      }
    });
  }
});

var c = 0;
$('.addtema').click(function(){
  $(".addtema").off('click');
  var bg;
  if(c%2==0){
    bg = '#ffefd2';
  }else{
    bg = '#c7edff';
  }
  $(this).parent().prev().append('<li style="padding: 12px;background:'+bg+';"><input class="form-control form-temario tema-father" v="'+c+'" type="text"/><a class="remove-item"><i class="fa fa-minus-circle" aria-hidden="true"></i></a><ol class="subtemas"></ol></li>');
  //remove
  $(".remove-item").off('click');
  $('.remove-item').click(function(){
    $(this).parent().remove();
  });
  /*
  //add subtema
  $(".addsbtema").off('click');
  $('.addsbtema').click(function(){
    $(this).prev().append('<li><input class="form-control form-temario tema-child" type="text" v="'+c+'"/><a class="remove-sb-item"><i class="fa fa-minus-circle" aria-hidden="true"></i></a><br><br></li>');
    //remove
    $(".remove-sb-item").off('click');
    $('.remove-sb-item').click(function(){
      $(this).parent().remove();
    });
  });*/

  c++;
});

/////////////////////////////////SUB-TEMAS////////////////////////////////////////////
var c = 0;
$('.addtemasub').click(function(){
  $(".addtemasub").off('click');
  var bg;
  if(c%2==0){
    bg = '#ffefd2';
  }else{
    bg = '#c7edff';
  }
  $(this).parent().prev().append('<li style="padding: 12px;background:'+bg+';"><input class="form-control form-temario tema-father" v="'+c+'" type="text"/><a class="remove-item"><i class="fa fa-minus-circle" aria-hidden="true"></i></a><ol class="subtemas"></ol></li>');
  //remove
  $(".remove-item").off('click');
  $('.remove-item').click(function(){
    $(this).parent().remove();
  });
  c++;
});
$('#create-sub-topic').click(function(){
  var arrfth = [];
  var TopicF = $('#TopicF').val();
  $.each( $('.tema-father'), function( key, value ) {
    var arrcld = [];
    var sb = $(value).next().next().children();
    
    arrfth.push({
      texto : $(value).val(), 
      Topic : TopicF
    });
    
  });
  if(arrfth.length>0){
    var jsonString = JSON.stringify(arrfth);
    //console.log(jsonString);
    $.ajax({
        type: "POST",
        url : base_url+'materiasfunc/addsbtopics',
        data: {data : jsonString}, 
        cache: false,
        beforeSend:function(){
          $('#add-topic').modal('hide');
          $('.capaload').fadeIn(600);
        },
        success: function(data){
          $('.capaload').fadeOut(600);
          if(data=="success"){
            location.reload();
          }else{
            $.alert({
              title: 'Tuvimos problemas al crear algunos temas',
              content: data,
              confirm: function () {
                  location.reload();
              }
            });
          }
        }
    });
  }
});
</script>