<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php //$this->load->view('header-left'); ?>
<?php echo link_tag('assets/js/editor-master/css/medium-editor.css'); ?>
<?php echo link_tag('assets/js/editor-master/css/themes/default.css'); ?>
<?php echo link_tag('assets/css/templates.css'); ?>
<style>
	.navbar{position: absolute !important;}
	.capa-white{
		display: block;
	}
	.opener-left-menu{display: none;}
	body{
		font-family: <?php echo $CourseFont; ?> !important;
	}
	.mejs-container{width: 100% !important;    height: auto !important;}
	
	.bg_tercero{
		background: <?php echo $ColorPalette3; ?> !important;
	}
	.cl_tercero{
		color: <?php echo $ColorPalette3; ?> !important;
	}
	.bg_secundario{
		background: <?php echo $ColorPalette2; ?> !important;
	}
	.cl_secundario{
		color: <?php echo $ColorPalette2; ?> !important;
	}
	.bg_primario{
		background: <?php echo $ColorPalette1; ?> !important;
		background-color: <?php echo $ColorPalette1; ?> !important;
	}
	.cl_primario{
		color: <?php echo $ColorPalette1; ?> !important;
	}

	/*tabs*/
	#tabs-demo4 > .active a{
		color: <?php echo $ColorPalette1; ?> !important;
	}
	/*enununciados*/
	.tmp-line-enununciados2{
		border-bottom: 3px solid <?php echo $ColorPalette1; ?> !important;
	}
	/*progress*/
	.swiper-pagination-progress .swiper-pagination-progressbar{
		background: <?php echo $ColorPalette1; ?>;
	}
	/*listas*/
	.nav-tabs.nav-tabs-v4 li a{
		color: <?php echo $ColorPalette1; ?>;
		border: 1px solid <?php echo $ColorPalette2; ?> !important;
	}
	#tabs-demo5 > .active a{
		background: <?php echo $ColorPalette2; ?> !important;
	}
	/*img*/
	.sub-subdiv-tmpl-img{
		    border-top: 4px solid <?php echo $ColorPalette1; ?>;
	}
	/*continuar*/
	.line-continuar{
		border-bottom: 1px solid <?php echo $ColorPalette1; ?>;
	}
	/*pop up*/
	.points{
		background: <?php echo $ColorPalette1; ?>;
		border: 3px solid <?php echo $ColorPalette1; ?>;
	}
</style>
<div class="col-xs-12 col-sm-12 col-lg-12 padding0 info-user">

	<div class="col-xs-9 col-sm-12 col-lg-9 sub-info-user">
		<h2 class="tpg-relawey" style="display: inline;"><?php echo $NameTopic; ?> </h2>
	</div>
	<div class="col-xs-3 col-sm-12 col-lg-3 sub-info-user">
		<a class="btn btn-primary" href="<?php echo base_url().'vista/?l='.$TopicID; ?>" target="_blank">Vista previa</a>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12 padding0 tpc" v="<?php echo $TopicID; ?>">
	<div class="container">
		
		<?php if($loads=='vacio'){ ?>
		<div class="col-xs-12 col-sm-12 col-lg-12 padding0 items-section" i="1">
			
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-12">
		</div>
		<?php }else{ ?>
		<div class="col-xs-12 col-sm-12 col-lg-12 items-section" i="1">
			<?php echo $loads; ?>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-12">
		</div>
		<?php
		} ?>
	</div>
</div>
<div id="edit-topic" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar tema</h4>
      </div>
      <div class="modal-body paint-topic">
        <input type="hidden" id="idtc">
        <label>Nombre del tema</label>
        <input class="form-control" type="text" id="nametopic-upd"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="update-topic">Aceptar</button>
      </div>
    </div>

  </div>
</div>
<div class="menu-items-add animate">
	<div class="col-xs-12 col-sm-12 col-lg-12 text-right div-close-mnct">
		<a class="additem close-mn"><i class="fa fa-times" aria-hidden="true"></i></a>
	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12 padding0">
		<?php echo $categorias; ?>
	</div>
</div>
<div class="sbmenu-items-add animate">

</div>
<div class="menu-edit-items animate">
	<div class="col-xs-12 col-sm-12 col-lg-12 text-right div-close-mnct">
		<!--<a class="close-edit close-edit-l"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>-->
		<a class="close-edit close-edit-l"><i class="fa fa-times" aria-hidden="true"></i></a>
	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12" id="print-value-items" style="position: relative;">
		
	</div>
	<div class="capaload"></div>
</div>
<div class="sbmenu-items-edit animate">
	<form method="post" action="<?php echo base_url() ?>temasfunc/addsettings">
		<div class="paint-form">
			
		</div>
		<input class="btn btn-primary" type="submit" name="Guardar settings" >
	</form>
</div>
<div class="capa-load-lesson close-edit-l"></div>
<div class="capa-white close-menu-items"></div>

<?php $this->load->view('footer-admin'); ?>
<script src="<?php echo site_url('assets/js/editor-master/js/medium-editor.js'); ?>"></script>
 <script>
 var editor = new MediumEditor('.editor_edit', {
    toolbar: {
        allowMultiParagraphSelection: true,
        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
        //buttons: ['bold', 'italic', 'anchor'],
        diffLeft: 0,
        diffTop: -10,
        firstButtonClass: 'medium-editor-button-first',
        lastButtonClass: 'medium-editor-button-last',
        relativeContainer: null,
        standardizeSelectionStart: false,
        static: false,

        /* options which only apply when static is true */
        align: 'center',
        sticky: false,
        updateOnEmptySelection: false
    }
	});
 	$('.flip').hover(function(){
        $(this).find('.card').toggleClass('flipped');

    });
 	//
    var swpprogress = new Swiper(".swpprogress", {
        nextButton: ".swiper-button-next",
        prevButton: ".swiper-button-prev",
        pagination: ".swiper-pagination",
        paginationType: "progress",
        paginationClickable: false,
        freeMode: false
    });

    var swgalery = new Swiper(".swgalery", {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        autoplay: 2400
    });
    ///
    $(document).ready(function(){
          $('video,audio').mediaelementplayer({
            alwaysShowControls: true,
            videoVolume: 'vertical',
            features: ['playpause','progress','volume','fullscreen']
          });
      });
    //Go url botton template
    $('.gourl').click(function(){
    	var url = $(this).attr('hf');
    	window.open(url);
    });
    //
    $('.points').draggable(
    {
    	containment: $('.imagenps'),
        drag: function(){
            var offset = $(this).offset();
            var xPos = offset.left;
            var yPos = offset.top;

            var LeftPorcent = parseInt(xPos) / ($('.imagenps').width() / 100)+"%";
            var TopPorcent = parseInt(yPos) / ($('.imagenps').height() / 100)+"%";
        },
        stop : function(){
        	var lf = $(this).css('left');
			var tp = $(this).css('top');

        	var vlsb  = $(this).attr('v');
        	var itm = $(this).attr('itm');
        	var ipt = $(this).attr('i');
			$('.menu-edit-items').addClass('menu-items-add-show');
			$.ajax({
		      type:'POST',
		      url : base_url+'temasfunc/onlypunto',
		      data : 'v='+encodeURIComponent(vlsb)+'&i='+ipt,
		      success : function(vl) {
		      	//Pintar resultados y reactivar edición
		      	$('#print-value-items').html(vl);
		      	var editor = new MediumEditor('.editor_edit', {
			    toolbar: {
			        allowMultiParagraphSelection: true,
			        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
			        //buttons: ['bold', 'italic', 'anchor'],
			        diffLeft: 0,
			        diffTop: -10,
			        firstButtonClass: 'medium-editor-button-first',
			        lastButtonClass: 'medium-editor-button-last',
			        relativeContainer: null,
			        standardizeSelectionStart: false,
			        static: false,

			        /* options which only apply when static is true */
			        align: 'center',
			        sticky: false,
			        updateOnEmptySelection: false
			    }
				});

				//Save result
				$('.save-point').click(function(){
					var vlsv = $(this).attr('v');
					var titlep = $('#titlep').val();
					

					var LeftPorcent = parseInt(lf) / ($('.imagenps').width() / 100)+"%";
		            var TopPorcent = parseInt(tp) / ($('.imagenps').height() / 100)+"%";
		            var postionstyle = "left:"+LeftPorcent+";top:"+TopPorcent+";";

					var descriptionp = $('#descriptionp').val();
					var formData = new FormData();
					if(titlep!="" &&descriptionp!=""){
						formData.append('vl', vlsv);
						formData.append('itm', itm);
						formData.append('title', titlep);
						formData.append('description', descriptionp);
						formData.append('pos', postionstyle);
						formData.append('i', ipt);
						formData.append('video', $('#video')[0].files[0]);
						formData.append('audio', $('#audio')[0].files[0]);
					    $.ajax({
					      	type:'POST',
					      	url : base_url+'temasfunc/savepoint',
					      	data : formData,
					      	contentType: 'multipart/form-data',
					       	processData: false,  // tell jQuery not to process the data
					       	contentType: false,  // tell jQuery not to set contentType
					      	beforeSend:function(){
					      		$('.capa-white').fadeIn(600);
					      	},
					      	success : function(vl) {
					      		location.reload();
					      	}
					    });
					    
					}
				});
				//Update 
				$('.update-point').click(function(){
					//var vlsv = $(this).attr('v');
					var titlep = $('#titlep').val();
					

					var LeftPorcent = parseInt(lf) / ($('.imagenps').width() / 100)+"%";
		            var TopPorcent = parseInt(tp) / ($('.imagenps').height() / 100)+"%";
		            var postionstyle = "left:"+LeftPorcent+";top:"+TopPorcent+";";

					var descriptionp = $('#descriptionp').val();
					var formData = new FormData();
					if(titlep!="" &&descriptionp!=""){
						formData.append('itm', vlsb);
						formData.append('title', titlep);
						formData.append('description', descriptionp);
						formData.append('pos', postionstyle);
						formData.append('i', ipt);
						formData.append('video', $('#video')[0].files[0]);
						formData.append('audio', $('#audio')[0].files[0]);
						
					    $.ajax({
					      	type:'POST',
					      	url : base_url+'temasfunc/updatepoint',
					      	data : formData,
					      	contentType: 'multipart/form-data',
					       	processData: false,  // tell jQuery not to process the data
					       	contentType: false,  // tell jQuery not to set contentType
					      	beforeSend:function(){
					      		$('.capa-white').fadeIn(600);
					      	},
					      	success : function(vl) {
					      		location.reload();
					      	}
					    });

					}
				});
				//borrar item padre
				$('.dlt-punto').click(function(){
					var vl = $(this).attr('vl');
					var ord = $(this).attr('ord');

					$.confirm({
						title: 'Eliminar punto',
					    content: '¿Seguro que quieres eliminar este punto?',
					    confirm: function(){
					    	if(vl!="" && ord!=""){
					    		$.ajax({
							        url : base_url+'temasfunc/delete_point',
							       	type : 'POST',
							       	data : 'vl='+encodeURIComponent(vl)+'&ord='+ord,
							       	beforeSend:function(){
										$('.capaload').fadeIn(600);
									},
									success : function(datav) {
										setTimeout(function(){
											$('.capaload').fadeOut(600);
											if(datav=="success"){
												location.reload();
											}else{
												$.dialog({
													title: '¡Ups! tuvimos problemas para actualizar tus datos',
													content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
												});
											}
										},1000);
									}
								});
					    	}
					    }
					});
				});

				//borrar sub items
				$('.dlt').click(function(){
					var t = $(this).attr('t');
					var id = $(this).attr('id');
					var vl = $(this).attr('vl');

					$.confirm({
					    title: 'Eliminar registro',
					    content: '¿Seguro que quieres eliminar el registro?',
					    confirm: function(){
					    	if(t!="" && id!="" && vl!=""){
								$.ajax({
							        url : base_url+'generalesfunc/delete',
							       	type : 'POST',
							       	data : 't='+encodeURIComponent(t)+'&id='+encodeURIComponent(id)+'&vl='+encodeURIComponent(vl),
							       	beforeSend:function(){
										$('.capaload').fadeIn(600);
									},
									success : function(datav) {
										setTimeout(function(){
											$('.capaload').fadeOut(600);
											if(datav=="success"){
												location.reload();
											}else{
												$.dialog({
													title: '¡Ups! tuvimos problemas para actualizar tus datos',
													content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
												});
											}
										},1000);
									}
								});
							}
					    }
					});

					
				});
		      }
		    });

        }
    });
	$('.points').click(function(){	        
        var offset = $(this).offset();
        var xPos = offset.left;
        var yPos = offset.top;

        var LeftPorcent = parseInt(xPos) / ($('.imagenps').width() / 100)+"%";
        var TopPorcent = parseInt(yPos) / ($('.imagenps').height() / 100)+"%";

   
    	var lf = $(this).css('left');
		var tp = $(this).css('top');

    	var vlsb  = $(this).attr('v');
    	var itm = $(this).attr('itm');
    	var ipt = $(this).attr('i');

    	
		$('.menu-edit-items').addClass('menu-items-add-show');
		$.ajax({
	      type:'POST',
	      url : base_url+'temasfunc/onlypunto',
	      data : 'v='+encodeURIComponent(vlsb)+'&i='+ipt,
	      success : function(vl) {
	      	//Pintar resultados y reactivar edición
	      	$('#print-value-items').html(vl);
	      	var editor = new MediumEditor('.editor_edit', {
		    toolbar: {
		        allowMultiParagraphSelection: true,
		        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
		        //buttons: ['bold', 'italic', 'anchor'],
		        diffLeft: 0,
		        diffTop: -10,
		        firstButtonClass: 'medium-editor-button-first',
		        lastButtonClass: 'medium-editor-button-last',
		        relativeContainer: null,
		        standardizeSelectionStart: false,
		        static: false,


		        align: 'center',
		        sticky: false,
		        updateOnEmptySelection: false
		    }
			});

			//Save result
			$('.save-point').click(function(){
				var vlsv = $(this).attr('v');
				var titlep = $('#titlep').val();
				

				var LeftPorcent = parseInt(lf) / ($('.imagenps').width() / 100)+"%";
	            var TopPorcent = parseInt(tp) / ($('.imagenps').height() / 100)+"%";
	            var postionstyle = "left:"+LeftPorcent+";top:"+TopPorcent+";";

				var descriptionp = $('#descriptionp').val();
				var formData = new FormData();
				if(titlep!="" &&descriptionp!=""){
					formData.append('vl', vlsv);
					formData.append('itm', vlsb);
					formData.append('title', titlep);
					formData.append('description', descriptionp);
					formData.append('pos', postionstyle);
					formData.append('i', ipt);
					formData.append('video', $('#video')[0].files[0]);
					formData.append('audio', $('#audio')[0].files[0]);
				    $.ajax({
				      	type:'POST',
				      	url : base_url+'temasfunc/savepoint',
				      	data : formData,
				      	contentType: 'multipart/form-data',
				       	processData: false,  // tell jQuery not to process the data
				       	contentType: false,  // tell jQuery not to set contentType
				      	beforeSend:function(){
				      		$('.capa-white').fadeIn(600);
				      	},
				      	success : function(vl) {
				      		location.reload();
				      	}
				    });
				}
			});
			//Update 
			$('.update-point').click(function(){
				//var vlsv = $(this).attr('v');
				var titlep = $('#titlep').val();
				

				var LeftPorcent = parseInt(lf) / ($('.imagenps').width() / 100)+"%";
	            var TopPorcent = parseInt(tp) / ($('.imagenps').height() / 100)+"%";
	            var postionstyle = "left:"+LeftPorcent+";top:"+TopPorcent+";";

				var descriptionp = $('#descriptionp').val();
				var formData = new FormData();
				if(titlep!="" &&descriptionp!=""){
					formData.append('itm', vlsb);
					formData.append('title', titlep);
					formData.append('description', descriptionp);
					formData.append('pos', postionstyle);
					formData.append('i', ipt);
					formData.append('video', $('#video')[0].files[0]);
					formData.append('audio', $('#audio')[0].files[0]);
				    $.ajax({
				      	type:'POST',
				      	url : base_url+'temasfunc/updatepoint',
				      	data : formData,
				      	contentType: 'multipart/form-data',
				       	processData: false,  // tell jQuery not to process the data
				       	contentType: false,  // tell jQuery not to set contentType
				      	beforeSend:function(){
				      		$('.capa-white').fadeIn(600);
				      	},
				      	success : function(vl) {
				      		location.reload();
				      	}
				    });
				}
			});
			//borrar item padre
			$('.dlt-punto').click(function(){
				var vl = $(this).attr('vl');
				var ord = $(this).attr('ord');

				$.confirm({
					title: 'Eliminar punto',
				    content: '¿Seguro que quieres eliminar este punto?',
				    confirm: function(){
				    	if(vl!="" && ord!=""){
				    		$.ajax({
						        url : base_url+'temasfunc/delete_point',
						       	type : 'POST',
						       	data : 'vl='+encodeURIComponent(vl)+'&ord='+ord,
						       	beforeSend:function(){
									$('.capaload').fadeIn(600);
								},
								success : function(datav) {
									setTimeout(function(){
										$('.capaload').fadeOut(600);
										if(datav=="success"){
											location.reload();
										}else{
											$.dialog({
												title: '¡Ups! tuvimos problemas para actualizar tus datos',
												content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
											});
										}
									},1000);
								}
							});
				    	}
				    }
				});
			});

			//borrar sub items
			$('.dlt').click(function(){
				var t = $(this).attr('t');
				var id = $(this).attr('id');
				var vl = $(this).attr('vl');

				$.confirm({
				    title: 'Eliminar registro',
				    content: '¿Seguro que quieres eliminar el registro?',
				    confirm: function(){
				    	if(t!="" && id!="" && vl!=""){
							$.ajax({
						        url : base_url+'generalesfunc/delete',
						       	type : 'POST',
						       	data : 't='+encodeURIComponent(t)+'&id='+encodeURIComponent(id)+'&vl='+encodeURIComponent(vl),
						       	beforeSend:function(){
									$('.capaload').fadeIn(600);
								},
								success : function(datav) {
									setTimeout(function(){
										$('.capaload').fadeOut(600);
										if(datav=="success"){
											location.reload();
										}else{
											$.dialog({
												title: '¡Ups! tuvimos problemas para actualizar tus datos',
												content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
											});
										}
									},1000);
								}
							});
						}
				    }
				});

				
			});
			
	      }
	    });

    
});
    //Agregar otro punto
	//var totalnow = $('.points').size();
	$('.addpoint').click(function(){
		var totalnow = $(this).attr('tl');
    	totalnow++;
    	

    	var v = $(this).attr('v');
    	$('.mis-items').append('<div class="points point'+totalnow+'" v="'+v+'" i="'+totalnow+'" style="top:10%;left:40%;"><div class="double-bounce1"></div><div class="double-bounce2"></div></div>');
    	var LeftPorcent; var TopPorcent;
    	$('.points').draggable(
	    {
	    	containment: $('.imagenps'),
	        drag: function(){
	            var offset = $(this).offset();
	            var xPos = offset.left;
	            var yPos = offset.top;

	            var LeftPorcent = parseInt(xPos) / ($('.imagenps').width() / 100)+"%";
	            var TopPorcent = parseInt(yPos) / ($('.imagenps').height() / 100)+"%";

	        },
	        stop : function(){
	        	var lf = $(this).css('left');
				var tp = $(this).css('top');

	        	var vlsb  = $(this).attr('v');
	        	var itm = $(this).attr('itm');
	        	var ipt = $(this).attr('i');

	        	
				$('.menu-edit-items').addClass('menu-items-add-show');
				$.ajax({
			      type:'POST',
			      url : base_url+'temasfunc/onlypunto',
			      data : 'v='+encodeURIComponent(vlsb)+'&i='+ipt,
			      success : function(vl) {
			      	//Pintar resultados y reactivar edición
			      	$('#print-value-items').html(vl);
			      	var editor = new MediumEditor('.editor_edit', {
				    toolbar: {
				        allowMultiParagraphSelection: true,
				        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
				        //buttons: ['bold', 'italic', 'anchor'],
				        diffLeft: 0,
				        diffTop: -10,
				        firstButtonClass: 'medium-editor-button-first',
				        lastButtonClass: 'medium-editor-button-last',
				        relativeContainer: null,
				        standardizeSelectionStart: false,
				        static: false,


				        align: 'center',
				        sticky: false,
				        updateOnEmptySelection: false
				    }
					});

					//Save result
					$('.save-point').click(function(){
						var vlsv = $(this).attr('v');
						var titlep = $('#titlep').val();
						

						var LeftPorcent = parseInt(lf) / ($('.imagenps').width() / 100)+"%";
			            var TopPorcent = parseInt(tp) / ($('.imagenps').height() / 100)+"%";
			            var postionstyle = "left:"+LeftPorcent+";top:"+TopPorcent+";";

						var descriptionp = $('#descriptionp').val();
						var formData = new FormData();
						if(titlep!="" &&descriptionp!=""){
							formData.append('vl', vlsv);
							formData.append('itm', vlsb);
							formData.append('title', titlep);
							formData.append('description', descriptionp);
							formData.append('pos', postionstyle);
							formData.append('i', ipt);
							formData.append('video', $('#video')[0].files[0]);
							formData.append('audio', $('#audio')[0].files[0]);
						    $.ajax({
						      	type:'POST',
						      	url : base_url+'temasfunc/savepoint',
						      	data : formData,
						      	contentType: 'multipart/form-data',
						       	processData: false,  // tell jQuery not to process the data
						       	contentType: false,  // tell jQuery not to set contentType
						      	beforeSend:function(){
						      		$('.capa-white').fadeIn(600);
						      	},
						      	success : function(vl) {
						      		location.reload();
						      	}
						    });
						}
					});
					//Update 
					$('.update-point').click(function(){
						//var vlsv = $(this).attr('v');
						var titlep = $('#titlep').val();
						

						var LeftPorcent = parseInt(lf) / ($('.imagenps').width() / 100)+"%";
			            var TopPorcent = parseInt(tp) / ($('.imagenps').height() / 100)+"%";
			            var postionstyle = "left:"+LeftPorcent+";top:"+TopPorcent+";";

						var descriptionp = $('#descriptionp').val();
						var formData = new FormData();
						if(titlep!="" &&descriptionp!=""){
							formData.append('itm', vlsb);
							formData.append('title', titlep);
							formData.append('description', descriptionp);
							formData.append('pos', postionstyle);
							formData.append('i', ipt);
							formData.append('video', $('#video')[0].files[0]);
							formData.append('audio', $('#audio')[0].files[0]);
						    $.ajax({
						      	type:'POST',
						      	url : base_url+'temasfunc/updatepoint',
						      	data : formData,
						      	contentType: 'multipart/form-data',
						       	processData: false,  // tell jQuery not to process the data
						       	contentType: false,  // tell jQuery not to set contentType
						      	beforeSend:function(){
						      		$('.capa-white').fadeIn(600);
						      	},
						      	success : function(vl) {
						      		location.reload();
						      	}
						    });
						}
						/*
						if(titlep!="" &&descriptionp!=""){
							$.ajax({
						      type:'POST',
						      url : base_url+'temasfunc/updatepoint',
						      data : 'itm='+encodeURIComponent(vlsb)+'&i='+encodeURIComponent(ipt)+'&title='+titlep+'&description='+encodeURIComponent(descriptionp)+'&pos='+encodeURIComponent(postionstyle),
						      beforeSend:function(){
						      	$('.capa-white').fadeIn(600);
						      },
						      success : function(vl) {
						      	location.reload();
						      }
						    });
						}
						*/
					});
					
			      }
			    });

	        }
	    });
		
	    
    });
    //cambiar imagen de puntos
    $('.changimg').click(function(){
    	var v = $(this).attr('v');
    	$('.menu-edit-items').addClass('menu-items-add-show');

    	$('#print-value-items').html('<input type="file" id="bgpoints" /><br><br><button type="button" class="btn btn-primary savebgpoint">Guardar</button> ');

    	$('.savebgpoint').click(function(){
    		var bgpoints = $('#bgpoints').val();
    		var formData = new FormData();
			formData.append('file', $('#bgpoints')[0].files[0]);
			formData.append('vl', v);

    		$.ajax({
		      	type:'POST',
		      	url : base_url+'temasfunc/savebgpoint',
		      	data : formData,
				processData: false,  // tell jQuery not to process the data
				contentType: false,  // tell jQuery not to set contentType
		     	beforeSend:function(){
		      		$('.capa-white').fadeIn(600);
		      	},
		      	success : function(vl) {
		      		location.reload();
		      	}
		    });
    	});
    });

	/********************Generales de Leccion*****************************/
	$('.dlt-items').click(function(){
	    var vl = $(this).attr('vl');
	    $.confirm({
	        title: 'Eliminar registro',
	        content: '¿Seguro que quieres eliminar el registro?',
	        confirm: function(){
				$.ajax({
			      type:'POST',
			      url : base_url+'temasfunc/deletefatheritem',
			      data : 'vl='+encodeURIComponent(vl),
			      beforeSend:function(){
			      	$('.capa-white').fadeIn(600);
			      },
			      success : function(vl) {
			      	location.reload();
			      }
			    });
			}
		});
	});
	jQuery(window).load(function() {
		$('.capa-white').fadeOut(600);
	});
	//Editar nombre de lesson
	$('.edittopic').click(function(){
		console.log('entro');
		var v = $(this).attr('v');
		$('#edit-topic').modal('show');
		$.ajax({
	      type:'POST',
	      url : base_url+'temasfunc/bytema',
	      data: 'v='+encodeURIComponent(v),
	      beforeSend:function(){
	      },
	      success : function(vl) {
	      	$('#idtc').val(v);
	      	$('#nametopic-upd').val(vl);
	      }
	    });
	});
	$('.saveplanti').click(function(){
		console.log('aloha');
	});
	
	$('.close-edit-l').click(function(){
		$('.capa-load-lesson').fadeOut(600);
		$('.menu-edit-items').removeClass('menu-items-add-show');
		$('.sbmenu-items-edit').removeClass('sbmenu-items-edit-show');
	});

	/**************Items tipo articulate******************************/
	var tpc = $('.tpc').attr('v');
	//var section = $( ".sectionstm" ).length;
	var i = $('.items-section').attr('i');
	var section = 1;
	$.each( $( ".sectionstm" ), function( key, value ) {
		section = $(this).attr('i');
	});
	

	$('.additem').click(function(){
		$('.capa-white').fadeIn(500);
		$('.menu-items-add').toggleClass('menu-items-add-show');

	});

	//Agregar otra sección
	$('.addsection').click(function(){
		$('.capa-white').fadeIn(500);
		section++;
		$('.items-section').append('<div class="col-xs-12 col-sm-12 col-lg-12 padding0 sectionstm section'+section+' text-center" i="'+section+'" data-editable data-name="article"><label>Sección '+section+'</label><a class="additem addi" i="'+section+'">Agregar item<i class="fa fa-plus" aria-hidden="true"></i></a></div>');
		$('.addi').click(function(){
			section++;
		});
		$('.menu-items-add').toggleClass('menu-items-add-show');
		console.log(section);
	});
	//Cerrar menu items
	$('.close-menu-items').click(function(){
		$('.capa-white').fadeOut(500);
		$('.list-categ-template').removeClass('active-cat-tmpl');
		$('.sbmenu-items-add').removeClass('sbmenu-items-add-show');
		$('.menu-items-add').toggleClass('menu-items-add-show');
	});
	$('.close-mn').click(function(){
		$('.capa-white').fadeOut(500);
		$('.list-categ-template').removeClass('active-cat-tmpl');
		$('.sbmenu-items-add').removeClass('sbmenu-items-add-show');
		$(this).parent().toggleClass('menu-items-add-show');
	});
	//
	$('.plus-mat').click(function(){
		$('.typeadd').toggle();
	});
	//
	$('.admtl').click(function(){
		var v = $(this).attr('v');
		var idtpcf = $('#idtpcf').val();
		
		$('#add-material').modal('show');

		$('#idtpc').val(idtpcf);
		$('#typematvl').val(v);

		if(v=="1"){
			$('.namemat').html('Archivo');
		}
		if(v=="2"){
			$('.namemat').html('Imagen');
		}
		if(v=="3"){
			$('.namemat').html('Video');
		}
		if(v=="4"){
			$('.namemat').html('Zip');
		}
	});
	
	//*****************Editar items multimedia************************************
	function isUrlValid(url) {
	    if(/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(url)){
	    	return true;
		} else {
	   		return false;
		}
	}

	$(document).on('edt-item', function(event, src,tam,tpc,tmpl,ord,content) { 
	//$('.edt-item').click(function(){
		$('#print-value-items').html('');
		$('.capa-load-lesson').fadeIn(600);
		$('.menu-edit-items').addClass('menu-items-add-show');

		//var content = content;
		$('#print-value-items').html(content);

		$('#save-video').click(function(){
			var tam = $('input[name=tam]:checked', '#myForm').val();
			var validate_url = $('#validate_url').val();
			if(validate_url!="" && tam!=""){

				if(isUrlValid(validate_url)){
					$.ajax({
				      type:'POST',
				      url : base_url+'temasfunc/addvideo',
				      data: 'url='+encodeURIComponent(validate_url)+'&type=2'+'&tpc='+encodeURIComponent(tpc)+'&tmpl='+encodeURIComponent(tmpl)+'&ord='+encodeURIComponent(ord)+'&tam='+tam+'&opc=upd',
				      beforeSend:function(){
				      	$('.capaload').fadeIn(600);
				      },
				      success : function(vl) {
				      	$('.capaload').fadeOut(600);
				      	
				      }
				    });
				}else{
					console.log('No se puede guardar no es url');
				}
			}else{
				console.log('Url vacia');
			}
		});

	});
	/**********************Cambiar orden******************/
	$('.change-orden').click(function(){
		var v = $(this).attr('v');
		var oth = $(this).attr('oth');
		var i = $(this).attr('i');
		var typ = $(this).attr('typ');

		$.ajax({
	      type:'POST',
	      url : base_url+'temasfunc/change_orden',
	      data: 'v='+encodeURIComponent(v)+'&oth='+oth+'&i='+i+'&typ='+typ,
	      beforeSend:function(){
	      	$('.capa-white').fadeIn(500);
	      },
	      success : function(vl) {
	      	$('.capa-white').fadeOut(500);
	      	location.reload();
	      }
	    });
	    /*
	    var ky = $(this).attr('ky');
		if(typ=='down'){
			var next = parseInt(ky)+1;
			$('.orden'+ky).insertAfter('.orden'+next);
			console.log('.orden'+ky);
			console.log('.orden'+next);
		}
		if(typ=='up'){
			var prev = parseInt(ky)-1;
			if(prev>=0){
				$('.orden'+ky).insertBefore('.orden'+prev);
				console.log('.orden'+ky);
				console.log('.orden'+prev);
			}
		}*/
		
	});
	/*******************Segunda opción de agregar mas items**************/
	$('.add-sction').click(function(){
		var nxt = $(this).attr('nxt');
		var tpc = $(this).attr('tpc');

		$('.capa-white').fadeIn(500);
		$('.menu-items-add').toggleClass('menu-items-add-show');

		$('.list-categ-template').click(function(){
			$('.list-categ-template').removeClass('active-cat-tmpl');
			$(this).addClass('active-cat-tmpl');
			$('.sbmenu-items-add').addClass('sbmenu-items-add-show');

			var ct = $(this).attr('vl');

			$.ajax({
		      type:'POST',
		      url : base_url+'temasfunc/categorys',
		      data: 'ct='+encodeURIComponent(ct),
		      beforeSend:function(){
		      },
		      success : function(vl) {
		      	$('.sbmenu-items-add').html(vl);
		      	$('.imgtemplate').click(function(){
		      		var t = $(this).attr('v');
		      		$.ajax({
				      type:'POST',
				      url : base_url+'temasfunc/addtemplatelienzo',
				      data: 'nxt='+encodeURIComponent(nxt)+'&tpc='+encodeURIComponent(tpc)+'&tp='+encodeURIComponent(t),
				      beforeSend:function(){
				      },
				      success : function(resp) {
				      	$('.capa-white').fadeOut(500);
	      				location.reload();
				      }
				    });
		      	});
		      }
		    });
		});
	});
	/*******************Segunda opción para guardar cambios en items*********/
	$('.save-item').click(function(){
		var v = $(this).attr('v');
		var i = $(this).attr('i');
		//
		var array = [];
		$('.itm'+i).each(function(k,v){
			var txt = $(this).html();
			var vlr = $(this).attr('v');

			var data = {};
			data.txt=txt;
            data.vlr=vlr;
            array.push(data);
			//console.log($(this).html());
		});
		$.ajax({
	        type:'POST',
	        url : base_url+'temasfunc/savetext',
	        data : {files: array},
	        dataType: "json",
	        beforeSend:function(){
	        },
	        success : function(vl) {
	          
	        }
	    });

	});
	/***********************Style***********************/
	$(".sectionstm").mouseover(function(){
        $(this).next().show();
    });
    $(".sectionstm").mouseout(function(){
    	$(this).next().hide();
    	$(".bttonsedit").mouseover(function(){
    		$(this).show();
    	});
    	$(".bttonsedit").mouseout(function(){
    		$(this).hide();
    	});
        //$(this).next().hide();
    });
    //Hacer grande iframe
	$(function(){
		$(document).on('heightfunc', function(event, h,i) { 
			console.log(h+'--'+i);
	    	$('.mif'+i).height(h);
	    });
    	$(document).on('eventhandler', function(event, i,h) {               
	        $('.capa-white').fadeIn(500);
	        if(h<400){
				$('.mif'+i).height(400);
			}else{
				$('.mif'+i).height(h);
			}
			$('.mif'+i).addClass('myiframezz');
	    });
	    $(document).on('closem', function(event, sbi) {               
	        $('.capa-white').fadeOut(500);
			$('.mif'+sbi).removeClass('myiframezz');
	    });
	    
	});
	/**************************EDITAR ITEMS***************************/
	$('.edit-item').click(function(){
		$('#print-value-items').html('');
		$('.capa-load-lesson').fadeIn(600);
		$('.menu-edit-items').addClass('menu-items-add-show');

		
		var v = $(this).attr('v');
		var i = $(this).attr('i');
		var ct = $(this).attr('ct');

		$.ajax({
	      type:'POST',
	      url : base_url+'temasfunc/backinfoedit',
	      data: 'v='+encodeURIComponent(v)+'&i='+i+'&ct='+encodeURIComponent(ct),
	      success : function(vl) {
	      	$('#print-value-items').html(vl);
	      	var editor = new MediumEditor('.editor_edit', {
			    toolbar: {
			        allowMultiParagraphSelection: true,
			        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
			        //buttons: ['bold', 'italic', 'anchor'],
			        diffLeft: 0,
			        diffTop: -10,
			        firstButtonClass: 'medium-editor-button-first',
			        lastButtonClass: 'medium-editor-button-last',
			        relativeContainer: null,
			        standardizeSelectionStart: false,
			        static: false,

			        align: 'center',
			        sticky: false,
			        updateOnEmptySelection: false
			    }

			});
			//Poder agregar más items
			var ism = i; var nowi;
			var nowi = $('.save-item-sg').attr('i');
			//Ver ajustes de diseño
			$('.getsettings').click(function(){
				$('.sbmenu-items-edit').addClass('sbmenu-items-edit-show');
				var vs = $(this).attr('v');
				var cts = $(this).attr('ct');
				$.ajax({
			      type:'POST',
			      url : base_url+'temasfunc/settings',
			      data    : 'v='+encodeURIComponent(vs)+'&ct='+encodeURIComponent(cts),
			      success : function(vlors) {
			      	$('.paint-form').html(vlors);
			      	//location.reload();
			      }
			    });
			});
			//addsubitem
			$('.addotheritem').click(function(){
				//var nowi = $(this).attr('i');
				nowi = parseInt(nowi)+1;
				$('.save-item-sg').attr('i',nowi);
				$('.save-item-flash').attr('i',nowi);
				$.ajax({
					type:'POST',
	      			url : base_url+'temasfunc/backadditems',
	      			data: 'v='+encodeURIComponent(v)+'&i='+nowi+'&ct='+encodeURIComponent(ct),
	      			success : function(resp) {
	      				$('.moreitems').append(resp);
	      				var editor = new MediumEditor('.editor_edit', {
						    toolbar: {
						        allowMultiParagraphSelection: true,
						        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
						        //buttons: ['bold', 'italic', 'anchor'],
						        diffLeft: 0,
						        diffTop: -10,
						        firstButtonClass: 'medium-editor-button-first',
						        lastButtonClass: 'medium-editor-button-last',
						        relativeContainer: null,
						        standardizeSelectionStart: false,
						        static: false,

						        align: 'center',
						        sticky: false,
						        updateOnEmptySelection: false
						    }
						});
	      				//Cambiar si es flashcard
						$('.flash-crd').on('change', function() {
							var vslc = $(this).val();
							var expl = vslc.split('_');
							$('.fscd'+expl[1]).hide();
							$('.'+vslc).show();
						});
						//Cambiar si es botones
						$('.chgradio').on('change', function() {
							var vslc = $(this).val();
							var expl = vslc.split('_');
							$('.alls'+expl[1]).hide();
							$('.'+vslc).show();
						});
						//Borrar item
						$('.dlt-item-print').click(function(){
							var itm = $(this).attr('v');
							$('.dvcat'+itm).addClass('animated bounceOutLeft');
							setTimeout(function(){ $('.dvcat'+itm).remove() }, 600);
						});

	      			}
				});
				
			});
			//Cambiar si es flashcard
			$('.flash-crd').on('change', function() {
				var vslc = $(this).val();
				var expl = vslc.split('_');
				var chghide;
				if(expl[1]=='t'){
					chghide = 'm';
				}else{
					chghide = 't';
				}
				$('.'+expl[0]+'_'+chghide+'_'+expl[2]).hide();
				$('.'+vslc).show();
			});
			//Cambiar si es botones
			$('.chgradio').on('change', function() {
				var vslc = $(this).val();
				var expl = vslc.split('_');
				$('.alls'+expl[1]).hide();
				$('.'+vslc).show();
			});
			//Guardar general
			$('.save-item-sg').click(function(){
				var v = $(this).attr('v');
				var ct = $(this).attr('ct');
				var nowi = $(this).attr('i');
				var fr = $(this).attr('fr');
				//
				var txt = $('#itemseditados').serialize();


				var form = $('#itemseditados');
			    var formData = new FormData();
			    var formParams = form.serializeArray();
			    
			    $.each(form.find('input[type="file"]'), function(i, tag) {
			      $.each($(tag)[0].files, function(i, file) {
			        formData.append(tag.name, file);
			        
			      });
			    });

			    $.each(formParams, function(i, val) {
			      formData.append(val.name, val.value);
			    });
			    formData.append('v', v);
			    formData.append('ct', ct);
			    formData.append('tam', nowi);
			    formData.append('i', i);
			    formData.append('fr', fr);
			    $.ajax({
			       url : base_url+'temasfunc/save_item_options_file',
			       type : 'POST',
			       data : formData,
			       processData: false,  // tell jQuery not to process the data
			       contentType: false,  // tell jQuery not to set contentType
			       beforeSend:function(){
			       	$('.capa-white').fadeIn(300);
			       },
			       success : function(data) {
			       	location.reload();
			       }
			    });

			});
			//Guardar flashcard
			$('.save-item-flash').click(function(){
				var v = $(this).attr('v');
				var ct = $(this).attr('ct');
				var nowi = $(this).attr('i');
				var fr = $(this).attr('fr');
				var totalinputs = $('#itemseditados .flahscard-sw').size();

				var form = $('#itemseditados');
			    var formData = new FormData();
			    var formParams = form.serializeArray();

				$.each(form.find('input[type="file"]'), function(i, tag) {
			      $.each($(tag)[0].files, function(i, file) {
			        formData.append(tag.name, file);
			        
			      });
			    });

			    $.each(formParams, function(i, val) {
			      formData.append(val.name, val.value);
			    });
			    formData.append('v', v);
			    formData.append('ct', ct);
			    formData.append('tam', nowi);
			    formData.append('ttinputs', totalinputs);
			    formData.append('i', i);
			    formData.append('fr', fr);
			    $.ajax({
			       url : base_url+'temasfunc/save_item_flashcard',
			       type : 'POST',
			       data : formData,
			       processData: false,  // tell jQuery not to process the data
			       contentType: false,  // tell jQuery not to set contentType
			       beforeSend:function(){
			       	$('.capaload').fadeIn(300);
			       },
			       success : function(data) {
			       	location.reload();
			       }
			    });
			});
			//Guardar embed
			$('.save-item-embed').click(function(){
				var v = $(this).attr('v');
				var ct = $(this).attr('ct');
				var nowi = $(this).attr('i');
				//
				var src = $('#embed').val();
		    	var embed = encodeURIComponent($('#embed').val());
		    	var url = 'https://noembed.com/embed?url='+embed;

		    	if(isUrlValid($('#embed').val())){
			    	var exp1=$('#embed').val().split('https://');
			    	var exp2 = exp1[1].split('/');

			    	if(exp2[0]=='twitter.com' || exp2[0]=='twitter.com' || exp2[0]=='soundcloud.com' || exp2[0]=='youtube.com'){
				    	if(url!=""){
				    		$.ajax({
						      type:'GET',
						      url : url,
						      data: 'url='+encodeURIComponent(src),
						      dataType: "text",
						      success : function(vl) {
						      	var json = $.parseJSON(vl);
						      	if(json.html!=undefined){
							      	//$('.capaload').fadeOut(600);
							      	$.ajax({
							      		  type:'POST',
									      url : base_url+'temasfunc/save_embed',
									      data: 'v='+encodeURIComponent(v)+'&ct='+encodeURIComponent(ct)+'&i='+i+'&iframe='+encodeURIComponent(json.html)+'&urlsave='+embed,
									      success : function(vl) {
									      	$('.capa-white').fadeOut(500);
									      	location.reload();
									      }
							      	});
						      	}else{
						      		$.dialog({
									    title: 'Este dominio de url no es permitido',
										content: 'Nuestros dominios permitidos son: <ul><li>twitter</li><li>soundcloud</li><li>youtube</li></ul>',
									});
						      	}
						      }
						    });
				    	}
			    	}else{
			    		$.dialog({
						    title: 'Este dominio de url no es permitido',
							content: 'Nuestros dominios permitidos son: <ul><li>wikipedia</li><li>twitter</li><li>soundcloud</li><li>youtube</li></ul>',
						});
			    	}
		    	}else{
		    		$.dialog({
						    title: 'Este dominio de url no es permitido',
							content: 'Nuestros dominios permitidos son: <ul><li>wikipedia</li><li>twitter</li><li>soundcloud</li><li>youtube</li></ul>',
						});
		    	}
				
			});
			//Eliminar items
			$('.dlt-item').click(function(){
				var v = $(this).attr('v');
				var explo = v.split('&valor=');
				var arrsend = [];

				$(this).parent().parent().addClass('animated bounceOutLeft');
				setTimeout(function(){ $('.bounceOutLeft').hide(); }, 600);

				for(i=0; i<explo.length; i++){
					arrsend.push(explo[i]);
				}

				$.ajax({
			      type:'POST',
			      url : base_url+'temasfunc/deleteitems',
			      data    : {result:JSON.stringify(arrsend)},
			      success : function(vl) {
			      	//location.reload();
			      }
			    });
			});

			//Actividades (preguntas opción multiple)
			//agregar mas opciones de respuestas
			var con_opt = 3;
			$('.add-more-options').click(function(){
				$('.paint-more-options').append('<div class="col-xs-12 col-sm-12 col-lg-12 padding0"><div class="col-xs-10 col-sm-12 col-lg-10 padding0" ><label>Opción</label>	<input class="form-control options-actv" num="'+con_opt+'" type="text" placeholder="Escribe opción" /></div><div class="col-xs-2 col-sm-12 col-lg-2 text-center" style="padding-top:25px;"><input type="radio" name="awnswer" value="'+con_opt+'" /></div><div class="col-xs-12 col-sm-12 col-lg-12 padding0"><a class="delete-option"><i class="fa fa-minus" aria-hidden="true"></i> Quitar opción</a><br></div></div></div>');
				con_opt++;
				$('.delete-option').click(function(){
					$(this).parent().parent().remove();
					con_opt--;
				});
			});
			//guardar pregunta y respuestas
			$('.save-questions').click(function(){
				//alert('hpña');
				var preg = $('#pregunta').val();
				var itm = $('#itm').val();
				var awns = $("input[type='radio']:checked").val();

				var arr_options = [];
				$.each( $('.options-actv'), function( key, value ) {
					var dat = {};
					dat.qst = $(value).val();
					dat.preg = preg;
					dat.itm = itm;
					dat.awns = awns;
					arr_options.push(dat);
				});
				
				$.ajax({
			        type:'POST',
			        url : base_url+'temasfunc/save_question',
			        data : {files: arr_options},
			        dataType: "json",
			        beforeSend:function(){
			        },
			        success : function(vl) {
			          
			        }
			    });
				
			});


	      }
	    });
		

		
	});
	/*************EDITAR PUNTOS*****************/
	$('.edit-item-puntos').click(function(){
		/*
		$('#print-value-items').html('');
		$('.capa-load-lesson').fadeIn(600);
		$('.menu-edit-items').addClass('menu-items-add-show');

		
		var v = $(this).attr('v');
		var i = $(this).attr('i');

		$.ajax({
	      type:'POST',
	      url : base_url+'temasfunc/puntos',
	      data: 'v='+encodeURIComponent(v)+'&i='+i,
	      success : function(vl) {
	      	$('#print-value-items').html(vl);
	      	var editor = new MediumEditor('.editor_edit', {
			    toolbar: {
			        allowMultiParagraphSelection: true,
			        //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
			        buttons: ['bold', 'italic', 'anchor'],
			        diffLeft: 0,
			        diffTop: -10,
			        firstButtonClass: 'medium-editor-button-first',
			        lastButtonClass: 'medium-editor-button-last',
			        relativeContainer: null,
			        standardizeSelectionStart: false,
			        static: false,

			        align: 'center',
			        sticky: false,
			        updateOnEmptySelection: false
			    }
			});
			//Poder agregar más items
			var ism = i; var nowi;
			var nowi = $('.save-item-sg').attr('i');
			
			$('.addotheritem').click(function(){
				//var nowi = $(this).attr('i');
				nowi = parseInt(nowi)+1;
				$('.save-item-sg').attr('i',nowi);
				$('.save-item-flash').attr('i',nowi);
				$.ajax({
					type:'POST',
	      			url : base_url+'temasfunc/backadditems',
	      			data: 'v='+encodeURIComponent(v)+'&i='+nowi+'&ct='+encodeURIComponent(ct),
	      			success : function(resp) {
	      				$('.moreitems').append(resp);
	      				var editor = new MediumEditor('.editor_edit', {
						    toolbar: {
						        allowMultiParagraphSelection: true,
						        //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
						        buttons: ['bold', 'italic', 'anchor'],
						        diffLeft: 0,
						        diffTop: -10,
						        firstButtonClass: 'medium-editor-button-first',
						        lastButtonClass: 'medium-editor-button-last',
						        relativeContainer: null,
						        standardizeSelectionStart: false,
						        static: false,

						        align: 'center',
						        sticky: false,
						        updateOnEmptySelection: false
						    }
						});
	      				//Cambiar si es flashcard
						$('.flash-crd').on('change', function() {
							var vslc = $(this).val();
							var expl = vslc.split('_');
							$('.fscd'+expl[1]).hide();
							$('.'+vslc).show();
						});
						//Cambiar si es botones
						$('.chgradio').on('change', function() {
							var vslc = $(this).val();
							var expl = vslc.split('_');
							$('.alls'+expl[1]).hide();
							$('.'+vslc).show();
						});
						//Borrar item
						$('.dlt-item-print').click(function(){
							var itm = $(this).attr('v');
							$('.dvcat'+itm).addClass('animated bounceOutLeft');
							setTimeout(function(){ $('.dvcat'+itm).remove() }, 600);
						});
	      			}
				});
				
			});
			
			//Guardar general
			$('.save-item-sg').click(function(){
				var v = $(this).attr('v');
				var ct = $(this).attr('ct');
				var nowi = $(this).attr('i');
				var fr = $(this).attr('fr');
				//
				var txt = $('#itemseditados').serialize();


				var form = $('#itemseditados');
			    var formData = new FormData();
			    var formParams = form.serializeArray();
			    
			    $.each(form.find('input[type="file"]'), function(i, tag) {
			      $.each($(tag)[0].files, function(i, file) {
			        formData.append(tag.name, file);
			        
			      });
			    });

			    $.each(formParams, function(i, val) {
			      formData.append(val.name, val.value);
			    });
			    formData.append('v', v);
			    formData.append('ct', ct);
			    formData.append('tam', nowi);
			    formData.append('i', i);
			    formData.append('fr', fr);
			    $.ajax({
			       url : base_url+'temasfunc/save_item_options_file',
			       type : 'POST',
			       data : formData,
			       processData: false,  // tell jQuery not to process the data
			       contentType: false,  // tell jQuery not to set contentType
			       success : function(data) {
			       	location.reload();
			       }
			    });

			});
			
			//Eliminar items
			$('.dlt-item').click(function(){
				var v = $(this).attr('v');
				var explo = v.split('&valor=');
				var arrsend = [];

				$(this).parent().parent().addClass('animated bounceOutLeft');
				setTimeout(function(){ $('.bounceOutLeft').hide(); }, 600);

				for(i=0; i<explo.length; i++){
					arrsend.push(explo[i]);
				}

				$.ajax({
			      type:'POST',
			      url : base_url+'temasfunc/deleteitems',
			      data    : {result:JSON.stringify(arrsend)},
			      success : function(vl) {
			      	//location.reload();
			      }
			    });
			});

	      }
	    });
	    */
	});
	/**************EDITAR FLASH CARD***********/
	$('.edit-item-flash').click(function(){
		$('#print-value-items').html('');
		$('.capa-load-lesson').fadeIn(600);
		$('.menu-edit-items').addClass('menu-items-add-show');

		
		var v = $(this).attr('v');
		var i = $(this).attr('i');
		var ct = $(this).attr('ct');

		$.ajax({
	      	type:'POST',
	      	url : base_url+'temasfunc/editflashcard',
	      	data: 'v='+encodeURIComponent(v)+'&i='+i+'&ct='+encodeURIComponent(ct),
	      	success : function(vl) {
	      		$('#print-value-items').html(vl);
	      		var editor = new MediumEditor('.editor_edit', {
			    toolbar: {
				        allowMultiParagraphSelection: true,
				        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
				        //buttons: ['bold', 'italic', 'anchor'],
				        diffLeft: 0,
				        diffTop: -10,
				        firstButtonClass: 'medium-editor-button-first',
				        lastButtonClass: 'medium-editor-button-last',
				        relativeContainer: null,
				        standardizeSelectionStart: false,
				        static: false,

				        align: 'center',
				        sticky: false,
				        updateOnEmptySelection: false
				    }
				});
				//Guardar flashcard
				$('.save-item-flash').click(function(){
					var v = $(this).attr('v');
					var ct = $(this).attr('ct');
					var nowi = $(this).attr('i');
					var fr = $(this).attr('fr');
					var totalinputs = $('#itemseditados .flahscard-sw').size();

					var form = $('#itemseditados');
				    var formData = new FormData();
				    var formParams = form.serializeArray();

					$.each(form.find('input[type="file"]'), function(i, tag) {
				      $.each($(tag)[0].files, function(i, file) {
				        formData.append(tag.name, file);
				        
				      });
				    });

				    $.each(formParams, function(i, val) {
				      formData.append(val.name, val.value);
				    });
				    formData.append('v', v);
				    formData.append('ct', ct);
				    formData.append('tam', nowi);
				    formData.append('ttinputs', totalinputs);
				    formData.append('i', i);
				    formData.append('fr', fr);
				    $.ajax({
				       url : base_url+'temasfunc/update_item_flashcard',
				       type : 'POST',
				       data : formData,
				       processData: false,  // tell jQuery not to process the data
				       contentType: false,  // tell jQuery not to set contentType
				       success : function(data) {
				       	//location.reload();
				       }
				    });
				});
	    	}
	    });
	});
</script>