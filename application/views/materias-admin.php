<?php $this->load->view('cabecera'); ?>
<div class="col-xs-12 col-sm-12 col-lg-12 padding0">
	<div class="col-xs-12 col-sm-12 col-lg-12 materias-banner padding0 text-center" style="background: url(<?php echo site_url('assets/img/escuelas/unaq/'.$Banner) ?>)no-repeat;">
		<div class="capa div-cap-banner" style="background: rgba(117, 117, 117, 0.6);" >
			<div class="col-xs-12 col-sm-12 col-lg-12 padding0 div-name" v="name">
				<h1 class="tpg-logo title-section editables" >
					<?php echo $CourseName; ?>
					<br><span style="font-size: 31px;"><?php echo "(".$Inc." - ".$Fin.")"; ?></span>
				</h1><!--
				<a class="tpg-relawey edits edit-name blanco" data-toggle="modal" data-target="#edit-name"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>-->
			</div>

			<a class="tpg-relawey edits edit-banner blanco" data-toggle="modal" data-target="#edit-banner" style="position: absolute;right: 10px;bottom: 10px;"><i class="fa fa-pencil" aria-hidden="true"></i> Editar Banner</a>

		</div>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12 sub-info-user text-right" style="padding-top: 20px;padding-bottom: 0px;">
	
    <?php echo $btn_preview.$btn_exam; ?>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 30px;">
	<div class="col-xs-2 col-sm-12 col-lg-2"></div>
	<div class="col-xs-8 col-sm-12 col-lg-8">
		<strong class="tpg-relawey descrip-mat1">Requerimientos: </strong> <p class="tpg-relawey descrip-mat2"><?php echo $CourseRequirements; ?></p><br>
		<strong class="tpg-relawey descrip-mat1">Objetivos: </strong> <p class="tpg-relawey descrip-mat2"><?php echo $CourseGoals; ?></p>
		<div class="col-xs-12 col-sm-12 col-lg-12 text-right" style="margin-top: 30px;">
			<a class="tpg-relawey descript-mat" data-toggle="modal" data-target="#edit-description-mat"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
		</div>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12">
	<div class="col-xs-2 col-sm-12 col-lg-2"></div>
	<div class="col-xs-8 col-sm-12 col-lg-8">
		<strong class="tpg-relawey descrip-mat1">Temario: </strong> <br>
		<?php echo $list; ?>
		<!--
		<label>Nombre de la unidad</label>
        <input class="form-control" type="text" id="nameunidad">-->
		<div class="col-xs-12 col-sm-12 col-lg-12 text-right" style="margin-top: 30px;">
		
			<a class="tpg-relawey" data-toggle="modal" data-target="#add-topic-unidad"><i class="fa fa-pencil" aria-hidden="true"></i> Agregar tu primer unidad</a>
		</div>
	</div>
</div>

<!--modals-->

<div id="edit-banner" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar datos del banner</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idmat" id="idmat" value="<?php echo $MateriaIDEncryp; ?>">
        <label class="tpg-relawey addmat">Banner de la materia</label>
        <input class="form-control" type="file" name="bannermupt" id="bannermupt">
        <label class="tpg-relawey addmat">Color banner</label>
        <input class="form-control" type="color" name="colorbmupt" id="colorbmupt" style="width: 50px;height: 50px;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="updatebannerdata">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<div id="edit-description-mat" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar datos</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idmat" id="idmat" value="<?php echo $MateriaIDEncryp; ?>">
        <label class="tpg-relawey addmat">Requerimientos</label>
        <textarea class="form-control" name="reqmupd" id="reqmupd" style="min-height: 70px;"><?php echo $CourseRequirements; ?></textarea>
        <label class="tpg-relawey addmat">Objetivos</label>
        <textarea class="form-control" name="objmupd" id="objmupd" style="min-height: 70px;"><?php echo $CourseGoals; ?></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="updatedecrpdata">Aceptar</button>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('modals'); ?>  
<?php $this->load->view('footer'); ?>  
<script>
	/////
	$('#nameunidad').keypress(function(e) {
		if(e.which == 13) {
		  var nameunidad = $('#nameunidad').val();
		  //var numberunidad = $('#numberunidad').val();
		  var MatSt = "<?php echo $MateriaIDEncryp; ?>";
		  $('#idsct').val('');
		  if(nameunidad!=""){
		      $.ajax({
		      type:'POST',
		      url : base_url+'materiasfunc/createsetion',
		      data: 'MatSt='+encodeURIComponent(MatSt)+'&name='+encodeURIComponent(nameunidad),
		      beforeSend:function(){
		        $('#add-topic-unidad').modal('hide');
		       	$('.capaload').fadeIn(600);
		      },
		      success : function(data) {
		      	
		        setTimeout(function(){
		          if(data!="error"){
		            $('#add-topic').modal('show');
		            $('.capaload').fadeOut(600);
		            $('#idsct').val(data);
		          }else{
		            $('.capaload').fadeOut(600);
		            $.alert({
		              title: '¡Ups! tuvimos problemas para generar la unidad',
		              content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
		              confirm: function () {
		                  $('#add-topic-unidad').modal('show');
		              }
		            });
		          }
		        },1000);
		      }
		    });
		  }
		}
	});
	/////
	$( ".div-name" )
	  .mouseover(function() {
	    var v = $(this).attr('v');
	    $('.edit-'+v).show();
	  })
	  .mouseout(function() {
	  	var v = $(this).attr('v');
	    $('.edit-'+v).hide();
	  });

	$( ".materias-banner" )
	  .mouseover(function() {
	    $('.edit-banner').show();
	  })
	  .mouseout(function() {
	    $('.edit-banner').hide();
	  });

	  $('.addtm').click(function(){
	  	var v = $(this).attr('v');
	  	$('#add-topic').modal('show');
	  	$('#idsct').val(v);
	  });
</script>