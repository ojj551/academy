<div id="content" class="profile-v1">
    <div class="col-md-12 col-sm-12 profile-v1-wrapper">
        <div class="col-md-12  profile-v1-cover-wrap" style="padding-right:0px;">
            <div class="profile-v1-pp">
                <h2><?php echo $Nombre; ?> (alumno)</h2>
            </div>
            <div class="col-md-12 profile-v1-cover">
                <img src="<?php echo site_url('asset/img/bgperfil.png'); ?>" class="img-responsive">
            </div>

        </div>


    </div>
    <div class="col-md-12 col-sm-12 profile-v1-body">
        <div class="col-md-7">
            <div class="panel box-v7">
                <div class="panel-body">
                    <div class="col-md-12 padding-0 box-v7-header">
                        <div class="col-md-12 padding-0">
                            <div class="col-md-10" style="padding-top: 20px;padding-bottom: 30px;">
                                <div class="col-md-12 padding-0">
                                    <div class="col-md-4">
                                        <h4 class="tpg-relawey">Nombre(s):</h4>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="inputperfil"><h4><?php echo $Nombre; ?></h4></div>
                                    </div>
                                </div>
                                <div class="col-md-12 padding-0">
                                    <div class="col-md-4">
                                        <h4 class="tpg-relawey">Apellidos:</h4>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="inputperfil"><h4><?php echo $Apellido; ?></h4></div>
                                    </div>
                                </div>
                                <div class="col-md-12 padding-0">
                                    <div class="col-md-4">
                                        <h4 class="tpg-relawey">Correo:</h4>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="inputperfil"><h4><?php echo $UserEmail; ?></h4></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-right" style="padding-top: 25px;">
                                <a data-toggle="modal" data-target="#edit"><i class="fa fa-pencil"
                                                                              aria-hidden="true"></i> Editar</a><br><br><br><br>
                                <a class="red dlt" t="<?php echo $tabla; ?>" id="<?php echo $id; ?>"
                                   vl="<?php echo $usr; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Dar de baja</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-12 padding-0">
                <div class="panel box-v3">
                    <div class="panel-heading bg-white border-none">
                        <h4>Avance general</h4>
                    </div>
                    <div class="panel-body">
                        <canvas class="doughnut-chart" id="myChart"></canvas>
                        <strong>Avanzado: </strong><span><?php echo $avance_g; ?>%</span><br>
                        <strong>Por avanzar: </strong><span><?php echo $falt_avance_g ?>%</span><br>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="panel box-v3">

                <div class="panel-heading bg-white border-none">
                    <h4>Reporte de cursos</h4>
                </div>
                <div class="panel-body">
                    <?php echo $materias_list; ?>

                </div>
                <div class="panel-footer bg-white border-none">
                    <?php if ($UserTypeSlugPadre == "profe") { ?>
                        <a v="<?php echo $UserEncryp; ?>" data-toggle="modal" data-target="#AddMatToUser"><i
                                    class="fa fa-plus" aria-hidden="true"></i> Agregar curso</a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="AddMatToUser" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Asignar curso</h4>
            </div>

            <div class="modal-body">

                <div class="row">
                    <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6">
                        <input type="hidden" name="idencr" id="idencr" value="<?php echo $UserEncryp; ?>">
                        <label>Curso</label>
                        <select class="form selectpicker" data-live-search="true" id="mat">
                            <option value="">Selecciona curso...</option>
                            <?php echo $materias; ?>
                        </select>
                        <div class="alert alert-danger alertSelect " style="display: none;">
                            Necesita seleccionar un curso
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="asignarmateria">Aceptar</button>
            </div>
        </div>

    </div>
</div>
<div id="edit" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar</h4>
            </div>
            <form action="<?php echo base_url() ?>alumnosfunc/updateuser" method="post">
                <div class="modal-body">
                    <input type="hidden" name="usr" value="<?php echo $usr; ?>">
                    <label>Nombre</label>
                    <input class="form-control" type="text" name="name" value="<?php echo $Nombre; ?>">
                    <label>Apellidos</label>
                    <input class="form-control" type="text" name="app" value="<?php echo $Apellido; ?>">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                    <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
                </div>
            </form>
        </div>

    </div>
</div>
<?php //$this->load->view('modals'); ?>
<?php $this->load->view('footer-admin'); ?>
<script src="<?php echo site_url('assets/js/admin.js'); ?>"></script>
<script>
    $('#asignarmateria').click(function () {
        var mat = $('#mat').val();
        var idencr = $('#idencr').val();
        if (mat != "") {
            $.ajax({
                url: base_url + 'materiasfunc/addmateriastoalumn',
                type: 'POST',
                data: 'mat=' + encodeURIComponent(mat) + '&idencr=' + encodeURIComponent(idencr),
                beforeSend: function () {
                    $('#AddMatToUser').modal('hide');
                    $('.capaload').fadeIn(600);
                },
                success: function (datav) {
                    setTimeout(function () {
                        if (datav == "success") {

                            location.reload();
                        } else {
                            $('.capaload').fadeOut(600);
                            $.dialog({
                                title: '¡Ups! tuvimos problemas dar de alta la materia',
                                content: datav,
                            });
                        }
                    }, 1000);
                }
            });
        }else{
            $('.alertSelect').show();
        }
    });
    $('.addusers').click(function () {
        var v = $(this).attr('v');
        if (v != "") {
            $('#modalgeneral').modal('show');
            $(".viewsgeneral").load("<?php echo base_url() . 'generalesfunc/modaladduser'; ?>/?v=" + encodeURIComponent(v));

        }
    });
    //Grafica
    var avance = <?php echo $avance_g; ?>;
    avance = parseFloat(avance);
    var poravanzar = 100 - avance;
    var data = {
        datasets: [{
            data: [
                avance,
                poravanzar
            ],
            backgroundColor: [
                "#ff7800",
                "#b5b5b5",
            ],
            label: 'My dataset' // for legend
        }],
        labels: [
            "Avance logrado " + avance + "%",
            "Avance por lograr " + poravanzar + "%",
        ]
    };
    var ctx = document.getElementById("myChart");
    new Chart(ctx, {
        data: data,
        type: "doughnut"
    });
</script>