<div id="content" class="profile-v1">
     <div class="col-md-12 col-sm-12 profile-v1-wrapper">
        <div class="col-md-9  profile-v1-cover-wrap" style="padding-right:0px;">
            <div class="profile-v1-pp">
              <h2><?php echo $Nombre; ?> (profesor)</h2>
            </div>
            <div class="col-md-12 profile-v1-cover">
              <img src="<?php echo site_url('asset/img/bgperfil.png'); ?>" class="img-responsive">
            </div>
        </div>
        <div class="col-md-3 col-sm-12 padding-0 profile-v1-right">
            <div class="col-md-6 col-sm-4 profile-v1-right-wrap padding-0"  style="box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);">
              <div class="col-md-12 padding-0 sub-profile-v1-right text-center sub-profile-v1-right1">
                  <h2><?php echo $total_materias; ?></h2>
                  <p>Materias</p>
              </div>
            </div>
            <div class="col-md-6 col-sm-4 profile-v1-right-wrap padding-0">
                <div class="col-md-12 sub-profile-v1-right text-center sub-profile-v1-right2">
                   <h3><?php echo $total_alumnos; ?></h3>
                   <p>Alumnos a su cargo</p>
                </div>
            </div>
        </div>
     </div>
     <div class="col-md-12 col-sm-12 profile-v1-body">
        <div class="col-md-7">
           <div class="panel box-v7">
                <div class="panel-body">
                  <div class="col-md-12 padding-0 box-v7-header">
                      <div class="col-md-12 padding-0">
                          <div class="col-md-10" style="padding-top: 20px;padding-bottom: 30px;">
                            <div class="col-md-12 padding-0">
                              <div class="col-md-4">
                                <h4 class="tpg-relawey">Nombre(s):</h4>
                              </div>
                              <div class="col-md-8">
                                <div class="inputperfil"><h4><?php echo $Nombre; ?></h4></div>
                              </div>
                            </div>
                            <div class="col-md-12 padding-0">
                              <div class="col-md-4">
                                <h4 class="tpg-relawey">Apellidos:</h4>
                              </div>
                              <div class="col-md-8">
                                <div class="inputperfil"><h4><?php echo $Apellido; ?></h4></div>
                              </div>
                            </div>
                            <div class="col-md-12 padding-0">
                              <div class="col-md-4">
                                <h4 class="tpg-relawey">Correo:</h4>
                              </div>
                              <div class="col-md-8">
                                <div class="inputperfil"><h4><?php echo $UserEmail; ?></h4></div>
                              </div>
                            </div>
                            <div class="col-md-12 text-right" style="padding-top: 25px;">
                              <a data-toggle="modal" data-target="#edit"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a><br><br><br><br>
                              <a class="red dlt" t="<?php echo $tabla; ?>" id="<?php echo $id; ?>" vl="<?php echo $usr; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Dar de baja</a>
                            </div>
                          </div>
                      </div>
                  </div>
        
                </div>
            </div>
            <div class="col-md-12 padding-0">
                  <div class="panel box-v3">
                      <div class="panel-heading bg-white border-none">
                        <h4>Avance general del grupo del supervisor</h4>
                      </div>
                      <div class="panel-body">
                          <canvas class="doughnut-chart" id="myChart"></canvas>
                          <strong>Avanzado: </strong><span><?php echo $avance_g; ?>%</span><br>
                          <strong>Por avanzar: </strong><span><?php echo $falt_avance_g ?>%</span><br>
                      </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
             <div class="panel box-v3">
                <div class="panel-heading bg-white border-none">
                  <h4>Reporte de cursos</h4>
                </div>
                <div class="panel-body">
                    <?php echo $materias_list; ?>
                  
                </div>
                <div class="panel-footer bg-white border-none">
                  <a class="tpg-relawey" data-toggle="modal" data-target="#asigmat-form"><i class="fa fa-plus" aria-hidden="true"></i> Agregar curso</a>
                </div>
            </div>
        </div>
     </div>
      <div class="col-xs-12 col-sm-12 col-lg-12 ">
              <div class="panel box-v7">
                <div class="panel-body">
            <div class="col-xs-12 col-sm-12 col-lg-12" style="padding-top: 50px;">
              <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 30px;padding-bottom: 30px;">
                  <?php echo $addmore; ?>
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-12" style="padding-bottom: 30px;"/>
                  <table id="example" class="table table-striped table-bordered headtablemodules tablessearch" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><label>Fecha de alta</label></th>
                                <th><label>Nombre</label></th>
                                <th><label>Apellido</label></th>
                                <th><label>Email</label></th>
                                <th>-</th>
                                <th>-</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php echo $listmyuser; ?>
                        </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

<div id="asigme-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Asignarme alumnos</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="ctm" value="<?php echo $ctm; ?>">
        <input type="hidden" id="usr" value="<?php echo $usr; ?>">
        <label>Alumnos</label>
        <select class="form-control" id="almtoasig">
          <option value="">Selecciona...</option>
          <?php echo $subordinds ?>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addsubordn"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>
<div id="asigmat-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Asignar materias</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="ctm" value="<?php echo $ctm; ?>">
        <input type="hidden" id="profmat" value="<?php echo $usr; ?>">
        <label>Materia</label>
        <select class="form-control" id="matprof">
          <option value="">Selecciona...</option>
          <?php echo $asignaturas ?>
        </select><br>
        <label>Ciclo escolar empieza</label>
        <div class="input-group input-append date">
            <input type="date" class="form-control" name="star" id="star" required />
            <span class="input-group-addon add-on"><i class="fa fa-calendar" aria-hidden="true"></i></span>
        </div><br>
        <label>Ciclo escolar termina</label>
        <div class="input-group input-append date">
            <input type="date" class="form-control" name="end" id="end" required />
            <span class="input-group-addon add-on"><i class="fa fa-calendar" aria-hidden="true"></i></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addmattoprof"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>
<div id="edit" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar</h4>
      </div>
      <form action="<?php echo base_url() ?>alumnosfunc/updateuser" method="post">
        <div class="modal-body">
          <input type="hidden" name="usr" value="<?php echo $usr; ?>">
          <label>Nombre</label>
          <input class="form-control" type="text" name="name" value="<?php echo $Nombre; ?>">
          <label>Apellidos</label>
          <input class="form-control" type="text" name="app" value="<?php echo $Apellido; ?>">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Actualizar</button>
          <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
        </div>
      </form>
    </div>

  </div>
</div>
<?php $this->load->view('footer-admin'); ?>
<script src="<?php echo site_url('assets/js/admin.js'); ?>"></script>
<script>
  var avance = <?php echo $avance_g; ?>;
    avance = parseFloat(avance);
    var poravanzar = 100-avance;
    var doughnutData = [
      {
          value: avance,
          color:"#2196F3",
          highlight: "#3d9de8",
          label: "Avance"
      },
      {
          value: poravanzar,
          color: "#f52a2a",
          highlight: "#ea4444",
          label: "Por avanzar"
      }
    ];
    var ctx = document.getElementById("myChart").getContext('2d');

    var myChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: ["Avance", "Por avanzar"],
          datasets: [{
              label: '# of Votes',
              data: [avance, poravanzar],
              backgroundColor: [
                  '#ff7800',
                  '#b5b5b5',
              ]
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
  });
</script>