<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('menu-header'); ?>
<div class="col-xs-12 col-sm-12 col-lg-12">
	<div class="">

		   <a href="<?php echo base_url() ?>alumnos/lista/?t=1" class="btn btn-default">Mis creados</a>
		   <a href="<?php echo base_url() ?>alumnos/lista/?t=2" class="btn btn-default">Mis responsables</a>

		<?php echo $adduser; ?>
		
	</div>
	<table class="table table-bordered">
	    <thead>
	    <?php
	    if($t==1){
	    	echo '
	    		<tr>
			      	<th>Fecha de registro</th>
			      	<th>ID Referencia</th>
			        <th>Nombre</th>
			        <th>Apellido</th>
			        <th>Email</th>
			        <th>Departamento</th>
			        <th>Status</th>
			        <th>-</th>
			     </tr>
	    	';
	    }
	    if($t==2){
	    	echo '
		      <tr>
		      	<th>Fecha de registro</th>
		        <th>Nombre</th>
		        <th>Apellido</th>
		        <th>Email</th>
		        <th>Departamento</th>
		        <th>-</th>
		      </tr>
		    ';
		}
	    ?>
	    </thead>
	    <tbody>
	    	<?php echo $alumnos; ?>
	    </tbody>
	  </table>
</div>
<!--Alumnos-->

<div id="addalumnos-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Dar de alta usuarios</h4>
      </div>
      <div class="modal-body">
        <label>ID Referencia usuario</label>
        <input class="form-control idreference" type="text" name="idreference1" id="idreference1">
        <span>No tengo ID Referencia,</span><a class="generateidreference" v="1">generar un ID Referencia unico</a><br>
        <label>Correo del usuario</label>
        <input class="form-control emailreference" type="text" name="emailalumn1" id="emailalumn1">
        <label>Tipo de usuario</label>
        <select class="form-control tipouser" id="typealum1">
          <?php echo $listtype; ?>
        </select>
        <div class="paintbytype">
        	
        </div>
        <!--
        <label>Carrera</label>
        <select class="form-control carrera" id="carrera1">
        	<?php echo $carr; ?>
        </select>
        <label>Semestre</label><select class="form-control semestre" id="semestre1"></select><br><hr>
        <div class="moreitems">
          
        </div>
        <div class="text-right">
          <input type="hidden" name="cant" id="cant" value="1">
          <a class="addmorealumn">Agregar otro alumnos</a>
        </div>
        -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="createalumnos">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer'); ?>
<script>

	$("#typealum1").on('change', function() {
	  var typeusr = this.value;
	  if(typeusr!=""){
	    $.ajax({
	        type:'POST',
	        url : base_url+'alumnosfunc/TypeUser',
	        data: 'typeusr='+encodeURIComponent(typeusr),
	        beforeSend:function(){
	        },
	        success: function(data){
	            $('.paintbytype').html(data);
	        }
	    });
	  }
	});
	//
	$('.generateidreference').click(function(){
	  $.ajax({
	    type:'POST',
	    url : base_url+'alumnosfunc/createreference',
	    data: 'generate=1',
	    beforeSend:function(){
	      $('#addalumnos-form').modal('hide');
	      $('.capaload').fadeIn(600);
	    },
	    success : function(data) {
	      setTimeout(function(){
	        if(data!="error"){
	          $('#addalumnos-form').modal('show');
	          $('.capaload').fadeOut(600);
	          $('#idreference1').val(data);
	          $( "#emailalumn" ).focus();
	        }else{
	          $('.capaload').fadeOut(600);
	          $.alert({
	            title: '¡Ups! tuvimos problemas para generar tu ID Referencia',
	            content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
	            confirm: function () {
	                $('#addalumnos-form').modal('show');
	            }
	          });
	        }
	      },1000);
	    }
	  });
	});
	//Crear
	$('#createalumnos').click(function(){
		var idref = $('#idreference1').val();
		console.log(idref);
		var email = $('#emailalumn1').val();
		var typealm = $('#typealum1').val();
		$.ajax({
	        type: "POST",
	        url : base_url+'alumnosfunc/addalumno',
	        data: 'idref='+encodeURIComponent(idref)+'&email='+encodeURIComponent(email)+'&typealm='+encodeURIComponent(typealm), 
	        beforeSend:function(){
	          $('#addalumnos-form').modal('hide');
	          $('.capaload').fadeIn(600);
	        },
	        success: function(data){
	          $('.capaload').fadeOut(600);
	          if(data=="success"){
	            //$(location).attr('href', base_url+'grupos');
	          }else{
	            $.alert({
	              title: 'Tuvimos problemas al crear algunos alumnos',
	              content: '<strong>Detalles: </strong>'+data,
	              confirm: function () {
	                  //$(location).attr('href', base_url+'grupos');
	              }
	            });
	          }
	        }
	    });
	});
</script>