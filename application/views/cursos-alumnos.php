<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php $this->load->view('header-left'); ?>
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Alumnos curso <?php echo $CourseName; ?></h3>

          </div>
      </div>                    
    </div> 
	<div class="col-xs-12 col-sm-12 col-lg-12">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Fecha de registro</th>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>Email</th>
          <th>-</th>
        </tr>
      </thead>
      <tbody>
        <?php echo $alumnos; ?>
      </tbody>
			
	</div>

</div>
<?php $this->load->view('footer-admin'); ?> 