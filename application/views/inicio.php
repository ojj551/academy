<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php $this->load->view('header-left'); ?>
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">
              <h3 class="animated fadeInLeft">Hola <strong><?php echo $this->session->userdata('UserFirstName'); ?>.</strong></h3>
          </div>
      </div>                    
    </div>
    <div class="col-xs-12 col-sm-12 col-lg-12">
    	<div class="col-md-12">
          <div class="panel">
            <div class="panel-heading-white panel-heading">
              <h4>Mi institución</h4>
            </div>
            <div class="panel-body">
              <canvas id="myChart" width="1300" height="400"></canvas>
            </div>
          </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer-admin'); ?>
<script>
  var list = <?php echo json_encode($list); ?>;
  var progress = <?php echo json_encode($progress); ?>;
  /*
var r1 = 23;
  var r2 = 11;
  var r3 = 87;
  var r4 = 12;
  var r5 = 95;*/
  var ctx = document.getElementById("myChart");
  var data = {
      labels: list,
      datasets: [
          {
              label: "Avance",
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(26, 197, 72, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(26, 197, 72, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(75, 192, 192, 1)',
              ],
              borderWidth: 1,
              data: progress,
          }
      ]
  };
  new Chart(ctx, {
          data: data,
          type: "bar",
          options: {
              scales: {
                xAxes: [{
                    display: true,
                    ticks: {
                        suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                        // OR //
                        beginAtZero: true,   // minimum value will be 0.
                        suggestedMax: 100
                    }
                }]
            }
          }
      });
</script>