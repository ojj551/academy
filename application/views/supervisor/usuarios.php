<?php $this->load->view('supervisor/header-left'); ?> 
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Usuarios</h3>

          </div>
      </div>                    
    </div>
    <div class="col-md-12 col-sm-12 col-lg-12">
      <div class="col-md-6 col-sm-12 col-lg-6">
        <a class="tpg-relawey add-curso-directo" data-toggle="modal" data-target="#addalumnos-form"><i class="fa fa-plus" aria-hidden="true"></i> Dar de alta un usuario</a>
      </div>
      <div class="col-md-6 col-sm-12 col-lg-6 text-right">
        <b class="dline-b">Busqueda: </b>
        <form class="dline-b" method="get">
          <input class="form-control" type="text" name="s" id="s">
        </form>
        
      </div>
    </div>  
    <div class="col-md-12 margin-content">
      <table class="table">
        <thead>
          <tr>
            <th>Registro</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Email</th>
            <th>Tipo</th>
            <th>Status</th>
            <th>-</th>
          </tr>
        </thead>
        <tbody>
          <?php echo $usuarios; ?>
        </tbody>
      </table>
      <?php echo $pagination; ?>
  </div>
  <!-- Trigger the modal with a button -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Usuario aún sin activar</h4>
        </div>
        <div class="modal-body">
          
          <b>Detalles: </b><br>
          <p class="intrc"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
        </div>
      </div>

    </div>
  </div>
  <div id="addalumnos-form" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Dar de alta usuarios</h4>
        </div>
        <div class="modal-body">
          <!--
          <label>ID Referencia usuario</label>
          <input class="form-control idreference" type="text" name="idreference1" id="idreference1">
          <span>No tengo ID Referencia,</span><a class="generateidreference" v="1">generar un ID Referencia unico</a><br>
          <label>Correo del usuario</label>
          <input class="form-control emailreference" type="text" name="emailalumn1" id="emailalumn1">-->
          <label>Tipo de usuario <b class="red">*</b></label>
          <select class="form-control tipouser" id="typealum1">
            <?php echo $listtype; ?>
          </select>
          <div class="paintbytype">
            
          </div>
        </div>
        <div class="modal-footer">
          
          <button type="button" class="btn btn-primary" id="createalumnos">Aceptar</button>
          <a data-dismiss="modal">Cancelar</a>
        </div>
      </div>

    </div>
  </div>


</div>
<?php $this->load->view('footer-admin'); ?> 
<!--
<script type="text/javascript" src="<?php echo site_url('assets/js/adds.js'); ?>"></script>-->
<script src="<?php echo site_url('assets/js/admin.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/sup/actions.js'); ?>"></script>