<?php $this->load->view('supervisor/header-left'); ?> 
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">
              <h3 class="animated fadeInLeft">Materias</i></h3>
          </div>
      </div>                    
    </div>  
    <div class="col-md-12">
      <a class="tpg-relawey add-curso-directo" data-toggle="modal" data-target="#addmaterias-form"><i class="fa fa-plus" aria-hidden="true"></i> Nueva materia</a>
      <table class="table">
        <thead>
          <tr>
            <th>Ultima actualización</th>
            <th>Materia</th>
            <th>Usuarios</th>
            <th>-</th>
          </tr>
        </thead>
        <tbody>
          <?php echo $content; ?>
        </tbody>
      </table>
  </div>




</div>

<div id="addmaterias-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar Materia</h4>
      </div>
      <form action="<?php echo base_url() ?>supervisorfunc/addmat" method="post">
        <div class="modal-body">
          <input type="hidden" name="ctm" value="<?php echo $ctm; ?>">
          <label class="tpg-relawey addmat">Nombre de la materia</label>
          <input class="form-control" type="text" name="nombre" id="nombre" required>

          <label class="tpg-relawey addmat">Requerimientos</label>
          <textarea class="form-control" name="requerimientos" id="requerimientos" style="min-height: 90px;"></textarea>

          <label class="tpg-relawey addmat">Objetivos</label>
          <textarea class="form-control" name="objetivos" id="objetivos" style="min-height: 90px;"></textarea>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="crear-materia">Guardar</button>
          <a class="" data-dismiss="modal">Cancelar</a>
        </div>
      </form>
    </div>

  </div>
</div>
<div id="update" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar asignatura</h4>
      </div>
      <form action="<?php echo base_url() ?>supervisorfunc/updatemat" method="post">
        <div class="modal-body">
          <div id="update-carr"></div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Actualizar</button>
          <a class="" data-dismiss="modal">Cancelar</a>
        </div>
      </form>
    </div>

  </div>
</div>
<?php $this->load->view('footer-admin'); ?>
<script src="<?php echo site_url('assets/js/materias.js'); ?>"></script>