<?php $this->load->view('supervisor/header-left'); ?> 
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">
              <h3 class="animated fadeInLeft">Carreras</i></h3>
          </div>
      </div>                    
    </div>  
    <div class="col-md-12">
      <a class="tpg-relawey add-curso-directo" data-toggle="modal" data-target="#addalumnos-form"><i class="fa fa-plus" aria-hidden="true"></i> Nueva carrera</a>
      <table class="table">
        <thead>
          <tr>
            <th>Ultima actualización</th>
            <th>Carrera</th>
            <th>Niveles</th>
            <th>Usuarios</th>
            <th>-</th>
          </tr>
        </thead>
        <tbody>
          <?php echo $content; ?>
        </tbody>
      </table>
  </div>




</div>
<div id="addalumnos-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Dar de alta carrera</h4>
      </div>
      <form action="<?php echo base_url() ?>supervisorfunc/add" method="post">
      <div class="modal-body">
        <input type="hidden" name="ctm" value="<?php echo $ctm ?>">
        <label>Nombre de la carrera</label>
        <input class="form-control" type="text" name="name" id="namedpto" required>
        <label>Total Periodos escolares</label>
        <input class="form-control" type="number" name="leveldpto" id="leveldpto" required>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Guardar</button>
        <a class="" data-dismiss="modal">Ó Cancelar</a>
      </div>
      </form>
    </div>

  </div>
</div>

<div id="updatealumnos-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar carrera</h4>
      </div>
      <form action="<?php echo base_url() ?>supervisorfunc/update" method="post">
        <div class="modal-body">
          <div id="update-carr"></div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="updatealumnos">Actualizar</button>
          <a class="" data-dismiss="modal">Ó Cancelar</a>
        </div>
      </form>
    </div>

  </div>
</div>
<?php $this->load->view('footer-admin'); ?>
<script src="<?php echo site_url('assets/js/carreras.js'); ?>"></script>