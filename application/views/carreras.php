<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('menu-header'); ?>
<div class="conteiner-right  col-xs-12 col-sm-12 col-lg-12">
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<h1 class="tpg-logo title-section"><?php echo $Depto; ?></h1>
		<a data-toggle="modal" data-target="#adddepto"><?php echo $ModalAdd; ?> <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12">
  <table id="example" class="table table-striped table-bordered headtablemodules tablessearch" cellspacing="0" width="100%">
      <thead>
          <tr>
            <th><label><?php echo $Depto; ?></label></th>
            <th><label><?php echo $TypeNivelName; ?></label></th>
            <th>-</th>
          </tr>
      </thead>
      <tbody>
        <?php echo $carreras; ?>
      </tbody>
  </table>
</div>
<?php $this->load->view('clientes/modal-add-carrera'); ?>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer'); ?>
<script src="<?php echo site_url('assets/js/carreras.js'); ?>"></script>