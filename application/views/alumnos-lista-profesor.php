<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('menu-header'); ?>
<div class="col-xs-12 col-sm-12 col-lg-12">
	<div class="">
		<a class="tpg-relawey add-curso-directo" data-toggle="modal" data-target="#addalumnos-form"><i class="fa fa-plus" aria-hidden="true"></i> Dar de alta un alumno</a>
	</div>
	<table class="table table-bordered">
	    <thead>
	      <tr>
	      	<th>Fecha de creación</th>
	        <th>ID Referencia</th>
	        <th>Email</th>
	        <th>Nombre</th>
	        <th>Apellido</th>
	        <th>Status</th>
	        <th>-</th>
	      </tr>
	    </thead>
	    <tbody>
	    	<?php echo $list; ?>
	    </tbody>
	  </table>
</div>
<!--Alumnos-->
<div id="addalumnos-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Dar de alta alumnos</h4>
      </div>
      <div class="modal-body">
        <label>ID Referencia alumno</label>
        <input class="form-control idreference" type="text" name="idreference1" id="idreference1">
        <span>No tengo ID Referencia,</span><a class="generateidreference" v="1">generar un ID Referencia unico</a><br>
        <label>Correo del alumno</label>
        <input class="form-control emailreference" type="text" name="emailalumn1" id="emailalumn1">
        <label>Tipo de usuario</label>
        <select class="form-control tipouser" id="typealum1">
          <?php echo $listtype; ?>
        </select>
        <label>Carrera</label>
        <select class="form-control carrera" id="carrera1">
        	<?php echo $carr; ?>
        </select>
        <label>Semestre</label><select class="form-control semestre" id="semestre1"></select><br><hr>
        <div class="moreitems">
          
        </div>
        <div class="text-right">
          <input type="hidden" name="cant" id="cant" value="1">
          <a class="addmorealumn">Agregar otro alumnos</a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="createalumnos">Aceptar</button>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer'); ?>
<script>
	///////////////////ALUMNOS////////////////////////
	$(".carrera").on('change', function() {
	  var vlcar = this.value
	  var vl = $('#cant').val();
	  if(vl!=""){
	    $.ajax({
	        type:'POST',
	        url : base_url+'alumnosfunc/SemestreCarrera',
	        data: 'vlcar='+encodeURIComponent(vlcar),
	        beforeSend:function(){
	        },
	        success: function(data){
	            $('#semestre1').html(data);
	        }
	    });
	  }
	});
	//generar id reference
	$('.generateidreference').click(function(){
	  var v = $(this).attr('v');
	  $.ajax({
	    type:'POST',
	    url : base_url+'alumnosfunc/createreference',
	    data: 'generate=1',
	    beforeSend:function(){
	      $('#addalumnos-form').modal('hide');
	      $('.capaload').fadeIn(600);
	    },
	    success : function(data) {
	      setTimeout(function(){
	        if(data!="error"){
	          $('#addalumnos-form').modal('show');
	          $('.capaload').fadeOut(600);
	          $('#idreference'+v).val(data);
	          $( "#emailalumn" ).focus();
	        }else{
	          $('.capaload').fadeOut(600);
	          $.alert({
	            title: '¡Ups! tuvimos problemas para generar tu ID Referencia',
	            content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
	            confirm: function () {
	                $('#addalumnos-form').modal('show');
	            }
	          });
	        }
	      },1000);
	    }
	  });
	});
	//otro alumnos

	$('.addmorealumn').click(function(){
	  var vl = $('#cant').val();
	  var n = (parseInt(vl))+(1);
	  $('#cant').val(n);
	  $('.moreitems').append('<div class="items'+n+'"><div class="text-right"><a class="quitar" v="'+n+'">Quitar formulario</a></div><label>ID Referencia alumno</label><input class="form-control idreference" type="text" name="idreference'+n+'" id="idreference'+n+'"><span>No tengo ID Referencia,</span><a class="generateidreference" v="'+n+'">generar un ID Referencia unico</a><br><label>Correo del alumno</label><input class="form-control emailreference" type="text" name="emailalumn'+n+'" id="emailalumn'+n+'"><label>Tipo de usuario</label><select class="form-control tipouser" id="typealum'+n+'"></select><label>Materia</label><select class="form-control carrera" id="carrera'+n+'"></select><label>Semestre</label><select class="form-control semestre" id="semestre'+n+'"></select><br><hr></div>');

	  //quitar
	  $('.quitar').click(function(){
	    var vl2 = $(this).attr('v');
	    $('.items'+vl2).remove();
	  });
	  //generar id reference
	  $('.generateidreference').click(function(){
	    var v = $(this).attr('v');
	    console.log(v);
	    $.ajax({
	      type:'POST',
	      url : base_url+'alumnosfunc/createreference',
	      data: 'generate=1',
	      beforeSend:function(){
	        $('#addalumnos-form').modal('hide');
	        $('.capaload').fadeIn(600);
	      },
	      success : function(data) {
	        setTimeout(function(){
	          if(data!="error"){
	            $('#addalumnos-form').modal('show');
	            $('.capaload').fadeOut(600);
	            $('#idreference'+v).val(data);
	            $( "#emailalumn" ).focus();
	          }else{
	            $('.capaload').fadeOut(600);
	            $.alert({
	              title: '¡Ups! tuvimos problemas para generar tu ID Referencia',
	              content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
	              confirm: function () {
	                  $('#addalumnos-form').modal('show');
	              }
	            });
	          }
	        },1000);
	      }
	    });
	  });
	  //agregar el tipo de usuario a los items
	  $.ajax({
	      type:'POST',
	      url : base_url+'alumnosfunc/ShowTypeUsers',
	      data: 'generate=1',
	      beforeSend:function(){
	      },
	      success: function(data){
	          $('#typealum'+n).html(data);
	      }
	  });

	  $.ajax({
	      type:'POST',
	      url : base_url+'alumnosfunc/ShowCarreras',
	      data: 'generate=1',
	      beforeSend:function(){
	      },
	      success: function(data){
	          $('#carrera'+n).html(data);
	      }
	  });
	  $('#carrera'+n).on('change', function() {
			$.ajax({
			      type:'POST',
			      url : base_url+'alumnosfunc/SemestreCarrera',
			      data: 'vlcar='+encodeURIComponent(this.value),
			      beforeSend:function(){
			      },
			      success: function(data){
			          $('#semestre'+n).html(data);
			      }
			});
		});

	});
	//Crear alumnos
	$('#createalumnos').click(function(){
	  var total = $( ".idreference" ).length;

	  var arr = [];
	  var status=0;
	  for (var i = 1; i <= total; i++) {
	    var n = $('#idreference'+i).val();
	    var e = $('#emailalumn'+i).val();
	    var t = $('#typealum'+i).val();
	    var c = $('#carrera'+i).val();
	    var lv = $('#semestre'+i).val();

	    if(n!="" && e!="" && t!="" && c!="" && lv!=""){
	      arr.push({
	          idref : n, 
	          Email : e,
	          tipo : t,
	          carr : c,
	          lv : lv
	      });
	    }else{
	      status++;
	    }
	  }
	  if(status>0){
	    $('#addalumnos-form').modal('hide');
	    $.alert({
	      title: 'Campos incompletos',
	      content: 'Lo sentimos tienes que llenar todos los campos para poder agregar un alumno',
	      confirm: function () {
	          $('#addalumnos-form').modal('show');
	      }
	    });
	  }else{

	    var jsonString = JSON.stringify(arr);
	    console.log(jsonString);
	    $.ajax({
	        type: "POST",
	        url : base_url+'alumnosfunc/addalumnos',
	        data: {data : jsonString}, 
	        cache: false,
	        beforeSend:function(){
	          $('#addalumnos-form').modal('hide');
	          $('.capaload').fadeIn(600);
	        },
	        success: function(data){
	          $('.capaload').fadeOut(600);
	          if(data=="success"){
	            //$(location).attr('href', base_url+'grupos');
	          }else{
	            $.alert({
	              title: 'Tuvimos problemas al crear algunos alumnos',
	              content: data,
	              confirm: function () {
	                  //$(location).attr('href', base_url+'grupos');
	              }
	            });
	          }
	        }
	    });
	  }
	});
	//Asignar materia a alumno
	$('#asignarmateria').click(function(){
	  var mat = $('#mat').val();
	  var usr = $('#idencr').val();
	  if(mat!=""){
	    $.ajax({
	      type:'POST',
	      url : base_url+'alumnosfunc/asignarmateria',
	      data: 'mat='+encodeURIComponent(mat)+'&usr='+encodeURIComponent(usr),
	      beforeSend:function(){
	        $('#AddMatToUser').modal('hide');
	        $('.capaload').fadeIn(600);
	      },
	      success : function(send) {
	        setTimeout(function(){
	          if(send=="success"){
	            location.reload();
	          }else{
	            $('.capaload').fadeOut(600);
	            $.alert({
	              title: '¡Ups! tuvimos problemas para asignar la materia',
	              content: send,
	              confirm: function () {
	                  $('#AddMatToUser').modal('show');
	              }
	            });
	          }
	        },1000);
	      }
	    });
	  }
	});
</script>