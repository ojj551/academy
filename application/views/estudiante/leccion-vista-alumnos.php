<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>
<?php echo link_tag('assets/css/style-alumn.css'); ?>
<?php echo link_tag('assets/css/templates.css'); ?>
<style>
  #content img{cursor:-webkit-zoom-in;}
  .navbar{position: absolute !important;}
  .capa-white{
    display: block;
  }
  .opener-left-menu{display: none;}
  body{
    font-family: <?php echo $CourseFont; ?> !important;
  }
  .mejs-container{width: 100% !important;    height: auto !important;}
  .bg_tercero{
    background: <?php echo $ColorPalette3; ?> !important;
  }
  .cl_tercero{
    color: <?php echo $ColorPalette3; ?> !important;
  }
  .bg_secundario{
    background: <?php echo $ColorPalette2; ?> !important;
  }
  .cl_secundario{
    color: <?php echo $ColorPalette2; ?> !important;
  }
  .bg_primario{
    background: <?php echo $ColorPalette1; ?> !important;
    background-color: <?php echo $ColorPalette1; ?> !important;
  }
  .cl_primario{
    color: <?php echo $ColorPalette1; ?> !important;
  }

  /*tabs*/
  #tabs-demo4 > .active a{
    color: <?php echo $ColorPalette1; ?> !important;
  }
  /*enununciados*/
  .tmp-line-enununciados2{
    border-bottom: 3px solid <?php echo $ColorPalette1; ?> !important;
  }
  /*progress*/
  .swiper-pagination-progress .swiper-pagination-progressbar{
    background: <?php echo $ColorPalette1; ?>;
  }
  /*listas*/
  .nav-tabs.nav-tabs-v4 li a{
    color: <?php echo $ColorPalette1; ?>;
    border: 1px solid <?php echo $ColorPalette2; ?> !important;
  }
  #tabs-demo5 > .active a{
    background: <?php echo $ColorPalette2; ?> !important;
  }
  /*img*/
  .sub-subdiv-tmpl-img{
        border-top: 4px solid <?php echo $ColorPalette1; ?>;
  }
  /*continuar*/
  .line-continuar{
    border-bottom: 1px solid <?php echo $ColorPalette1; ?>;
  }
  /*pop up*/
  .points{
    background: <?php echo $ColorPalette1; ?>;
    border: 3px solid <?php echo $ColorPalette1; ?>;
  }
  /*quoq*/
  .line-quoq{
    border-bottom:2px solid <?php echo $ColorPalette1; ?>;
  }


  

</style>
<div id="left-menu">
    <div class="sub-left-menu scroll" >
      <div class="banner-curso text-center" style="background: url(<?php echo $imgcurso; ?>)no-repeat;height: 200px;position: relative;background-position: 100% 100%;background-size: cover;">
        <div class="capagrp" style="background: rgba(0, 0, 0, 0.65);padding-top: 45%;">
          <a href="<?php echo $CourseEncryp ?>" style="color:white;"><?php echo $CourseName; ?></a>
        </div>
      </div>
      <?php echo $topics; ?>
    </div>
</div>
<div id="content">
  <div class="panel"></div>
  <div class="col-md-12 col-sm-12 col-lg-12  "> 
    <div class="col-md-12 col-sm-12 col-lg-12 title-lesson-alm">
        <div class="col-md-10 col-sm-12 col-lg-10">
          <h2><?php echo $NameTopic; ?></h2>
        </div>
        <div class="col-md-2 col-sm-12 col-lg-2 text-right">
          <?php if($typ_user=='student'){ ?>
          <a href="<?php echo base_url() ?>estudiante/materias" class="btn btn-primary">Salir de vista</a>
          <?php }else{ ?> <!--<a href="<?php echo base_url().'temas/?t='.$typ_user; ?>" class="btn btn-primary">Salir de vista</a>--> <?php } ?>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-lg-12 padding0 contenidos-load">
      <?php echo $loads; ?>
    </div>
  </div>
</div>
<div id="modal-images" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content paint-image">
      
      
    </div>

  </div>
</div>
<!--modal respuestas-->
<div id="modalanw" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content paint-anws">
      
    </div>

  </div>
</div>
<div id="progress" data-0="width:0%;" data-end="width:100%;" class="skrollable skrollable-between" style="position: fixed;top: 0px;left: 0px;z-index: 999999;height: 5px;background: orange;"></div>

<?php $this->load->view('footer-admin'); ?>

<script>
    //agregar
    $('#content img').click(function(){
      var src = $(this).attr('src');
      if(src!=""){
        $('#modal-images').modal('show');
        $('.paint-image').html('<img class="img-modal" src="'+src+'">');
      }
    });

    var swiper = new Swiper('.swiperquestion', {
      nextButton: ".swiper-button-next",
        prevButton: ".swiper-button-prev",
        pagination: ".swiper-pagination",
        paginationType: "progress",
        paginationClickable: false,
        freeMode: false
    });

	  var swpprogress = new Swiper(".swpprogress", {
        nextButton: ".swiper-button-next",
        prevButton: ".swiper-button-prev",
        pagination: ".swiper-pagination",
        paginationType: "progress",
        paginationClickable: false,
        freeMode: false
    });
    //
    var swiper = new Swiper('.swipertres', {
      slidesPerView: 3,
      spaceBetween: 30,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });
    var swgalery = new Swiper(".swgalery", {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        autoplay: 1400
    });

    //Porcentaje de avance conforme a scroll
    var porcent = 0;
    var cont_porcent = 0;
    $(window).scroll(function (event) {
        cont_porcent =+ parseFloat($("#progress").width() / $('#progress').parent().width() * 100);
        if(cont_porcent>=90){
          porcent = 100;
        }
    });

    //Condición si contenido es menor al scroll
    var hgwnd = $(window).height();
    var hgcon = $('.contenidos-load').height();
    if(cont_porcent<=0){
      if(hgcon<=hgwnd){
        porcent = 100;
      }
    }
    //condición si hay contenido enviar avance si no no
    var porcent_send;
    var loads = "<?php echo $havecontent; ?>";
    //console.log(loads);
    if(loads=="1"){
      //salir de topic 
      var tpc = "<?php echo $TopicID ?>";
      window.onbeforeunload = function(event) {
          //event.returnValue = "";
          if(porcent!=100){
            porcent_send = cont_porcent;
          }else{porcent_send = porcent;}
          //console.log(porcent_send);
          $.ajax({
            type:'POST',
            url : base_url+'temasfunc/avancetopic',
            data: 'p='+porcent_send+'&tpc='+encodeURIComponent(tpc),
            beforeSend:function(){
            },
            success : function(data) {
            }
          });
      };
    }

    $('.points').click(function(){
      //console.log('aloha');
    	var v = $(this).attr('v');
    	var lf = $(this).attr('lf');
    	var tp = $(this).attr('tp');
    	$('.speech-bubble').fadeOut(300);
    	$('.vwm'+v).fadeIn(300);
    	$('.vwm'+v).css('left',lf+'%');
    	$('.vwm'+v).css('top',tp+'%');
    	
    });
    $('.close-dc-point').click(function(){
    	$(this).parent().fadeOut(300);
    });
    //close menu
    $('.hmb-users-vw').click(function(){
      $('.menu-users-vw').toggle();
      $('.contet-users-vw').toggleClass('cont-w-vw');
    });

      $(document).ready(function(){
          $('video,audio').mediaelementplayer({
            alwaysShowControls: true,
            videoVolume: 'vertical',
            features: ['playpause','progress','volume','fullscreen']
          });
      });



    //Re-ordenar temas
    
    $(function() {
        $(".sortable").sortable({
            stop: function( ) {
            }
          });
        $(".sortable").disableSelection();
    });
    
    $('.ver-actv').click(function(){
      var items = [];
      var itm = $(this).attr('itm');
      $.each($('ul.sortable > li'), function( index, value ) {
        var data = {};
        data.v = $(value).text();
        data.itm = itm;
        items.push(data);
      });
      //console.log(items);
      $.ajax({
          type:'POST',
          url : base_url+'actividades/ordenar',
          data : {arry_item:items},
          //processData: false,  // tell jQuery not to process the data
          //contentType: false,  // tell jQuery not to set contentType
          beforeSend:function(){
            $('.capa-white').fadeIn(600);
          },
          success : function(vl) {
            $('.capa-white').fadeOut(600);
            $('#modalanw').modal('show');
            $('.paint-anws').html(vl);
          }
      });
    });

    //guardar respuestas multiples
    $('.save-anws').click(function(){
		var arrToSend=[];
		$(this).closest('div').siblings('.swiper-container').find('.swiper-slide').each(function(){
			if($(this).find('input:checked').val()){
				arrToSend.push({'resp':$(this).find('input:checked').val(),'qt':$(this).find('input:checked').attr('qt')});
			}else{
				arrToSend.push({'resp':'','qt':$(this).find('input:radio').attr('qt')});
			}
		});
		
		$.ajax({
			type:'POST',
			url : base_url+'actividades/saveaws',
			datatype: 'json',
			data: {'datos':JSON.stringify(arrToSend)},
			beforeSend:function(){
			},
			success : function(vl) {
				$('.verModal').show();
				$('#modalanw').modal('show');
				$('.paint-anws').html(vl);
			}
		});
    });
	
</script>