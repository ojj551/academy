<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>
<div id="left-menu">
  	<div class="sub-left-menu scroll" style="padding-top: 70px;">

	    <?php echo $topics; ?>

    </div>
</div>
<div id="content">
	<div class="panel " style="background: url(<?php echo $imgcurso; ?>)no-repeat;height: 345px;position: relative;background-position: 100%;background-size: cover;">
		<div class="capagrp" style="background: rgba(0, 0, 0, 0.65);" data-0="padding-top: 10%;" data-466="padding-top: 0%;">
			<div class="text-right" style="position: absolute;right: 15px;top: 30px;">
				<a href="<?php echo base_url() ?>estudiante/materias" class="btn btn-primary">Salir de vista</a>
			</div>
			<div class="text-center">
				<h1 class="blanco tpg-relawey title-curso-alumn"><?php echo $CourseName; ?></h1>
				<i class="blanco"><?php echo $Inc; ?> - <?php echo $Fin; ?></i><br><br>
				<i class="blanco"><b>Profesor:</b> <?php echo $ProfesorName; ?></i><br>
			</div>
			
		</div>
	</div>
    <div class="col-md-12">
		<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 30px;">
			<div class="col-xs-2 col-sm-12 col-lg-2"></div>
			<div class="col-xs-8 col-sm-12 col-lg-8">
				<strong class="tpg-relawey descrip-mat1">Requerimientos: </strong> <p class="tpg-relawey descrip-mat2"><?php echo $CourseRequirements; ?></p><br>
				<strong class="tpg-relawey descrip-mat1">Objetivos: </strong> <p class="tpg-relawey descrip-mat2"><?php echo $CourseGoals; ?></p>
			</div>
		</div>
    </div>
</div>
<?php $this->load->view('footer-admin'); ?>
