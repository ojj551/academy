<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php $this->load->view('header-left'); ?> 
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Lista de alumnos</h3>

          </div>
      </div>                    
    </div> 
	<div class="col-xs-12 col-sm-12 col-lg-12 padding0 info-user">
		<div class="col-xs-12 col-sm-2 col-lg-2"></div>
		<div class="col-xs-12 col-sm-8 col-lg-8 sub-info-user">
			<h2 class="tpg-relawey"><?php echo $Nombre; ?></h2>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-12 text-right">
			<a class="dlt" t="<?php echo $TableEncrypt ?>" id="<?php echo $IDEncrypt ?>" vl="<?php echo $ValueEncrypt ?>">Dar de baja usuario</a>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-top: 50px;">
			<div class="col-xs-12 col-sm-4 col-lg-4 text-center" >
				<h3 class="tpg-relawey">Acerca del alumno:</h3>
			</div>
			<div class="col-xs-12 col-sm-8 col-lg-8">
				<strong class="tpg-relawey">Nombre: </strong> <span><?php echo $Nombre; ?></span><br>
				<strong class="tpg-relawey">Correo: </strong> <span><?php echo $UserEmail; ?></span><br>
				<strong class="tpg-relawey">Semestre: </strong> <span><?php //echo $UserEmail; ?></span><br>
			</div>
		</div>
		<?php
		if($UserTypeSlug=='alumn'){
		?>
		<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-top: 50px;">
			<div class="col-xs-12 col-sm-4 col-lg-4 text-center" >
				<h3 class="tpg-relawey">Materias del alumno:</h3>
			</div>
			<div class="col-xs-12 col-sm-8 col-lg-8">
				<?php echo $cursos; ?>
				<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 60px;">
					<a v="<?php echo $UserEncryp; ?>" data-toggle="modal" data-target="#AddMatToUser"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar materia</a>
				</div>
			</div>
		</div>
		<?php
		}
		?>
	</div>
	<?php 
	if($UserTypeSlug!='alumn'){
	 ?>
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-top: 50px;">
			<div class="col-xs-12 col-sm-4 col-lg-4 text-center" >
				<h3 class="tpg-relawey">Personas a su cargo</h3>
			</div>
			<div class="col-xs-12 col-sm-8 col-lg-8">
				<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 30px;padding-bottom: 30px;">
					<?php echo $addmore; ?>
				</div>
				<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-bottom: 30px;"/>
					<table id="example" class="table table-striped table-bordered headtablemodules tablessearch" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				                <th><label>Fecha de alta</label></th>
				                <th><label>Nombre</label></th>
				                <th><label>Email</label></th>
				                <th><label>Carrera</label></th>
				                <th><label>Semestre</label></th>
				                <th>-</th>
				            </tr>
				        </thead>
				        <tbody>
				        	<?php echo $listmyuser; ?>
				        </tbody>
				    </table>
			    </div>
			</div>
		</div>
	</div>
	<?php
	}
	?>
</div>
<div id="asigme-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Asignarme usuarios</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="ctm" value="<?php echo $ctm; ?>">
        <input type="hidden" id="usr" value="<?php echo $usr; ?>">
        <label>Alumnos</label>
        <select class="form-control" id="almtoasig">
          <option value="">Selecciona...</option>
          <?php echo $subordinds ?>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addsubordn"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer-admin'); ?>
<script src="<?php echo site_url('assets/js/admin.js'); ?>"></script>
<script>
	$('#asignarmateria').click(function(){
		var mat = $('#mat').val();
		var idencr = $('#idencr').val();
		if(mat!=""){
			$.ajax({
		      url : base_url+'materiasfunc/addmateriastoalumn',
		      type : 'POST',
		      data : 'mat='+encodeURIComponent(mat)+'&idencr='+encodeURIComponent(idencr),
		      beforeSend:function(){
		        $('#AddMatToUser').modal('hide');
		        $('.capaload').fadeIn(600);
		      },
		      success : function(datav) {
		        setTimeout(function(){
			        if(datav=="success"){
			            location.reload();
			        }else{
			            $('.capaload').fadeOut(600);
			            $.dialog({
			              title: '¡Ups! tuvimos problemas dar de alta la materia',
			              content: datav,
			            });
			        }
		        },1000);
		      }
		    });
		}
	});
	$('.addusers').click(function(){
		var v = $(this).attr('v');
		if(v!=""){
			$('#modalgeneral').modal('show'); 
			$(".viewsgeneral").load("<?php echo base_url().'generalesfunc/modaladduser'; ?>/?v="+encodeURIComponent(v));
			
		}
	});
</script>