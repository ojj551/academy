<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php $this->load->view('header-left'); ?> 
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Lista de clientes</h3>

          </div>
      </div>                    
    </div>  
    <div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="">
			<a class="tpg-relawey add-curso-directo" data-toggle="modal" data-target="#addalumnos-form"><i class="fa fa-plus" aria-hidden="true"></i> Dar de alta nuevo cliente</a>
		</div>
		<table class="table table-bordered">
		    <thead>
		      <tr>
		      	<th>Fecha de creación</th>
		        <th>Nombre</th>
		        <th>Suscripción</th>
		        <th>Ver</th>
		      </tr>
		    </thead>
		    <tbody>
		    	<?php echo $list; ?>
		    </tbody>
		  </table>
	</div>
	<!--Alumnos-->
	<div id="addalumnos-form" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Dar de alta cliente</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group form-animate-text" style="margin-bottom:10px !important;">
	            <input type="text" class="form-text mask-date idreference" name="idreference1" id="idreference1" required>
	            <span class="bar"></span>
	            <label>Nombre cliente</label>

	        </div>
	      </div>
	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" id="createcustomer"><?php echo $FormGuardar; ?></button>
	        <a data-dismiss="modal"><?php echo $FormCancel; ?></button>
	       
	      </div>
	    </div>

	  </div>
	</div>
</div>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer-admin'); ?>  