<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('menu-header'); ?>
<div class="col-xs-12 col-sm-12 col-lg-12 padding0 info-user">
	<div class="col-xs-12 col-sm-1 col-lg-1"></div>
	<div class="col-xs-12 col-sm-9 col-lg-9 sub-info-user">
		<strong class="tpg-relawey">Tema: </strong>
		<h2 class="tpg-relawey"><?php echo $NameSection; ?> </h2>
		<a class="edituni" v="<?php echo $SectionID; ?>" >Editar</a><br>
		<a class="dlt" t="<?php echo $tabla ?>" id="<?php echo $id ?>" vl="<?php echo $SectionID ?>" style=" color: #F44336;">Eliminar</a>
	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12 text-right">
		<!--<a class="dlt" t="<?php echo $TableEncrypt ?>" id="<?php echo $IDEncrypt ?>" vl="<?php echo $ValueEncrypt ?>">Dar de baja usuario</a>-->
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12">
	<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-top: 50px;">
		<div class="col-xs-12 col-sm-5 col-lg-5 text-center">
			
				<a class="plus-mat"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
				<div class="typeadd">
					<input type="hidden" id="idtpcf" value="<?php echo $SectionID; ?>">
					<a class="tpg-relawey admtl" v="1">Archivo</a>
					<a class="tpg-relawey admtl" v="2">Imagen</a>
					<a class="tpg-relawey admtl" v="3">Video</a>
					<a class="tpg-relawey admtl" v="4">Video Youtube</a>
					<a class="tpg-relawey admtl" v="5">Zip</a>
				</div>
			
		</div>
	</div>
</div>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer'); ?>
<script>
	$('.plus-mat').click(function(){
		$('.typeadd').toggle();
	});
	//
	$('.admtl').click(function(){
		var v = $(this).attr('v');
		var idtpcf = $('#idtpcf').val();
		
		$('#add-material').modal('show');

		$('#idscm').val(idtpcf);
		$('#typematvl').val(v);

		if(v=="1"){
			$('.namemat').html('Archivo');
		}
		if(v=="2"){
			$('.namemat').html('Imagen');
		}
		if(v=="4"){
			$('.filevdo').hide();
			$('.videoytb').show();
			$('.namemat').html('Video Youtube');
		}else{
			$('.videoytb').hide();
			$('.filevdo').show();
		}
		if(v=="5"){
			$('.namemat').html('Zip');
		}
	});
	//
	$('.edituni').click(function(){
		console.log('entro');
		var v = $(this).attr('v');
		$('#edit-unidad').modal('show');
		$.ajax({
	      type:'POST',
	      url : base_url+'unidadfunc/bysection',
	      data: 'v='+encodeURIComponent(v),
	      beforeSend:function(){
	      },
	      success : function(vl) {
	      	$('#idunid').val(v);
	      	$('#nameunidad-upd').val(vl);
	      }
	    });
	});
</script>