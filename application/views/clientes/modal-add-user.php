<div id="addalumnos-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Dar de alta usuarios</h4>
      </div>
      <div class="modal-body">
        
        <label>Tipo de usuario</label>
        <select class="form-control tipouser" id="typealum1">
          <?php echo $listtype; ?>
        </select>
        <div class="paintbytype">
        	
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="createalumnos"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>