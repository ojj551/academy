<div id="addmateria" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $ModalAdd; ?> Materia/Curso</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="ctm" value="<?php echo $ctm; ?>">
        <label class="tpg-relawey addmat">Nombre de la materia</label>
        <input class="form-control" type="text" name="nombre" id="nombre">

        <!--
        <div class="col-xs-12 col-sm-12 col-lg-12" >
          <div class="col-xs-6 col-sm-12 col-lg-6 padding0">
            <label class="tpg-relawey addmat">Inicia</label>
            <input class="form-control form-add-mat" type="date" name="inicia" id="inicia">
          </div>
          <div class="col-xs-6 col-sm-12 col-lg-6 padding0">
            <label class="tpg-relawey addmat">Termina</label>
            <input class="form-control form-add-mat" type="date" name="finaliza" id="finaliza">
          </div>
        </div>-->

        <label class="tpg-relawey addmat">Requerimientos</label>
        <textarea class="form-control" name="requerimientos" id="requerimientos" style="min-height: 90px;"></textarea>

        <label class="tpg-relawey addmat">Objetivos</label>
        <textarea class="form-control" name="objetivos" id="objetivos" style="min-height: 90px;"></textarea>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="crear-materia"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>