<div id="adddepto" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $ModalAdd; ?> <?php echo $Depto; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="ctm" value="<?php echo $ctm; ?>">
        <label><?php echo $FormName; ?></label>
        <input class="form-control" type="text" name="namedpto" id="namedpto">
        <label><?php echo $TypeNivelName; ?></label>
        <input class="form-control" type="number" name="leveldpto" id="leveldpto">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addmat"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>