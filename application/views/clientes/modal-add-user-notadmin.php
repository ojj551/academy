<div id="asiguser-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Asignar usuario</h4>
      </div>
      <div class="modal-body">
        <label>Usuarios</label>
        <div class="paintuserslist"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="asignarusuario"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>