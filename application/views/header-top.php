<!-- start: Header -->
  <nav class="navbar navbar-default header navbar-fixed-top" style="background-color: #cacaca;">
    <div class="col-md-12 nav-wrapper">
      <div class="navbar-header" style="width:100%;">
        <div class="opener-left-menu is-open" style="display: block;opacity: 1;">
          <span class="top"></span>
          <span class="middle"></span>
          <span class="bottom"></span>
        </div>
          <a href="index.html" class="navbar-brand"> 
           
          </a>

        <ul class="nav navbar-nav navbar-right user-nav" style="padding-right: 20px;">
          <li class="user-name"><span><?php echo $this->session->userdata('UserFirstName'); ?></span></li>
            <li class="dropdown avatar-dropdown" style="color: #000;">
              <img src="<?php echo site_url("asset/img/avatar.jpg"); ?>" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
               <ul class="dropdown-menu user-dropdown">
                 <li role="separator" class="divider"></li>
                 <li class="more">
                  <div class="text-center">
                    <a href="<?php echo base_url(); ?>login/logout"><span class="fa fa-power-off "> Cerrar sesion</span></a>
                  </div>
                </li>
            </ul>
             <?php
              //$this->load->helper('picture_helper');
              //echo menu_top();
              ?>
            </li>
        </ul>
      </div>
    </div>
  </nav>
<!-- end: Header -->
<div class="container-fluid mimin-wrapper">