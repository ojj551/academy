<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="language" content="ES">
	<title>Xpertcad-academy</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
	<?php echo link_tag('assets/css/bootstrap.css'); ?>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
	<?php echo link_tag('assets/css/animate.css'); ?>
	<?php echo link_tag('assets/css/jquery-confirm.css'); ?>
	<?php echo link_tag('assets/css/styleadmin.css'); ?>
	<?php echo link_tag('assets/css/style.css'); ?>

	<!--
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,500,700,900,900i" rel="stylesheet">-->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Nunito:300,400,700|Open+Sans:300,400,700|Roboto:300,400,700" rel="stylesheet">
	<script src="<?php echo site_url('assets/js/jquery-3.1.0.min.js'); ?>"></script>
</head>
<body>