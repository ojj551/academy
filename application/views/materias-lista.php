<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-header'); ?>
<div class="col-xs-12 col-sm-12 col-lg-12">
<h3>Materias</h3> <a href="<?php echo base_url() ?>materias/crear"><i class="fa fa-plus" aria-hidden="true"></i> Agregar</a>

<table id="example" class="table table-striped table-bordered headtablemodules tablessearch" cellspacing="0" width="100%">
	<thead>
		<th><label>Fecha de alta</label></th>
		<th><label>Asignatura</label></th>
	</thead>
	<tbody>
		<?php echo $mt; ?>
	</tbody>
</table>
</div>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('footer'); ?>