<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('menu-header'); ?>
<?php $this->load->view('modals'); ?>
<div class="col-xs-12 col-sm-12 col-lg-12 marginfirst">
	<div class="col-xs-2 col-sm-12 col-lg-2 "></div>
	<div class="col-xs-8 col-sm-12 col-lg-8 ">
		<div class="col-xs-4 col-sm-12 col-lg-4">
			<form id="form">
				<label>Idioma</label>
				<input type="text" name="nameidom" id="nameidom" class="form-control">
				<label>Abreviación del idioma ejm(en,es...)</label>
				<input type="text" name="abrevidom" id="abrevidom" class="form-control">
			</form>
			<div class="text-right">
				<button type="button" class="btn btn-primary btng" id="save-idiom">Guardar</button>
			</div>
		</div>
		<div class="col-xs-8 col-sm-12 col-lg-8">
			<table id="example" class="table table-striped table-bordered headtablemodules tablessearch" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		                <th><label>#</label></th>
		                <th><label>Nombre del idioma</label></th>
		                <th><label>Slug del idioma</label></th>
		                <th><label>Status</label></th>
		                <th><label>Ajustes</label></th>
		            </tr>
		        </thead>
		        <tbody>
		        	<?php echo $list; ?>
		        </tbody>
		    </table>
		</div>
	</div>
</div>
<?php $this->load->view('footer'); ?>
<script>
	$('#save-idiom').click(function(){
		var nameidom = $('#nameidom').val();
		var abrevidom = $('#abrevidom').val();

		if(nameidom!="" && abrevidom!=""){
			$.ajax({
		        type:'POST',
		        url : base_url+'idiomas/add',
		        data: 'nameidom='+nameidom+'&abrevidom='+abrevidom,
		        beforeSend:function(){
		        	$('.capaload').fadeIn(600);
		        },
		        success: function(data){
		        	$('.capaload').fadeOut(600);
		        	if(data=="success"){
		        		location.reload();
		        	}else{
		        		$.alert({
			              title: 'Tuvimos problemas al crear el dioma',
			              content: '<strong>Detalles: </strong>'+data,
			              confirm: function () {
			                  //$(location).attr('href', base_url+'grupos');
			              }
			            });
		        	}
		        }
		    });
		}
	});
</script>