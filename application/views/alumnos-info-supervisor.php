<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>
<?php $this->load->view('header-left'); ?>
<div id="content" class="profile-v1">
     <div class="col-md-12 col-sm-12 profile-v1-wrapper">
        <div class="col-md-9  profile-v1-cover-wrap" style="padding-right:0px;">
            <div class="profile-v1-pp">
              <img src="<?php echo site_url('asset/img/avatar.jpg'); ?>"/>
              <h2><?php echo $Nombre; ?> <i>(supervisor)</i></h2>
            </div>
            <div class="col-md-12 profile-v1-cover">
              <img src="<?php echo site_url('asset/img/bgperfil.png'); ?>" class="img-responsive">
            </div>
        </div>
        <div class="col-md-3 col-sm-12 padding-0 profile-v1-right">
            <div class="col-md-12 col-sm-4 profile-v1-right-wrap padding-0">
                <div class="col-md-12 sub-profile-v1-right text-center sub-profile-v1-right2">
                   <h1><?php echo $total_alumnos; ?></h1>
                   <p>Profesores a su cargo</p>
                </div>
            </div>
            <div class="col-md-12 col-sm-4 profile-v1-right-wrap padding-0">
                <div class="col-md-12 sub-profile-v1-right text-center sub-profile-v1-right3">
                  <h3>Calificado por los profesores</h3>
                  <span>&#9733; </span><span>&#9733; </span><span>&#9733;</span>
                </div>
            </div>
        </div>
     </div>
    <div class="col-md-12 col-sm-12 profile-v1-body">
        <div class="col-md-7">
            <div class="panel box-v7">
                <div class="panel-body">
                  <div class="col-md-12 padding-0 box-v7-header">
                      <div class="col-md-12 padding-0">
                          <div class="col-md-10" style="padding-top: 20px;padding-bottom: 30px;">
                          	<div class="col-md-12 padding-0">
	                          	<div class="col-md-4">
	                          		<h4 class="tpg-relawey">Nombre(s):</h4>
	                          	</div>
	                          	<div class="col-md-8">
	                          		<div class="inputperfil"><h4><?php echo $Nombre; ?></h4></div>
	                          	</div>
                          	</div>
                          	<div class="col-md-12 padding-0">
	                          	<div class="col-md-4">
	                          		<h4 class="tpg-relawey">Apellidos:</h4>
	                          	</div>
	                          	<div class="col-md-8">
	                          		<div class="inputperfil"><h4><?php echo $Apellido; ?></h4></div>
	                          	</div>
                          	</div>
                          	<div class="col-md-12 padding-0">
	                          	<div class="col-md-4">
	                          		<h4 class="tpg-relawey">Correo:</h4>
	                          	</div>
	                          	<div class="col-md-8">
	                          		<div class="inputperfil"><h4><?php echo $UserEmail; ?></h4></div>
	                          	</div>
                          	</div>
                          </div>
                          <!--
                          <div class="col-md-2 padding-0">
                            <div class="btn-group pull-right">
	                            <a><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>

                          	</div>
                          </div>-->
                      </div>
                  </div>

                </div>
            </div>

        </div>
        <div class="col-md-5">
            <div class="panel box-v3">
                <div class="panel-heading bg-white border-none">
                  <h4>Avance general del grupo del supervisor</h4>
                </div>
                <div class="panel-body">
                    <canvas class="doughnut-chart"></canvas>
                    <strong>Avanzado: </strong><span><?php echo $avance_g; ?>%</span><br>
                    <strong>Por avanzar: </strong><span><?php echo $falt_avance_g ?>%</span><br>
              	</div>
        	</div>
    	</div>
   		<div class="col-xs-12 col-sm-12 col-lg-12 ">
            	<div class="panel box-v7">
	            	<div class="panel-body">
						<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-top: 50px;">
							<div class="col-xs-12 col-sm-12 col-lg-12">
								<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 30px;padding-bottom: 30px;">
									<?php echo $addmore; ?>
								</div>
								<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-bottom: 30px;">
									<table id="example" class="table table-striped table-bordered headtablemodules tablessearch" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								                <th><label>Fecha de alta</label></th>
								                <th><label>Nombre</label></th>
								                <th><label>Apellido</label></th>
								                <th><label>Email</label></th>
								                <th>-</th>
								                <th>-</th>
								            </tr>
								        </thead>
								        <tbody>
								        	<?php echo $listmyuser; ?>
								        </tbody>
								    </table>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>

<div id="asigme-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Asignarme alumnos</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="ctm" value="<?php echo $ctm; ?>">
        <input type="hidden" id="usr" value="<?php echo $usr; ?>">
        <label>Alumnos</label>
        <select class="form-control" id="almtoasig">
          <option value="">Selecciona...</option>
          <?php echo $subordinds ?>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addsubordn"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer-admin'); ?>
<script src="<?php echo site_url('assets/js/admin.js'); ?>"></script>
<script>
	$('#asignarmateria').click(function(){
		var mat = $('#mat').val();
		var idencr = $('#idencr').val();
		if(mat!=""){
			$.ajax({
		      url : base_url+'materiasfunc/addmateriastoalumn',
		      type : 'POST',
		      data : 'mat='+encodeURIComponent(mat)+'&idencr='+encodeURIComponent(idencr),
		      beforeSend:function(){
		        $('#AddMatToUser').modal('hide');
		        $('.capaload').fadeIn(600);
		      },
		      success : function(datav) {
		        setTimeout(function(){
			        if(datav=="success"){
			            location.reload();
			        }else{
			            $('.capaload').fadeOut(600);
			            $.dialog({
			              title: '¡Ups! tuvimos problemas dar de alta la materia',
			              content: datav,
			            });
			        }
		        },1000);
		      }
		    });
		}
	});
	$('.addusers').click(function(){
		var v = $(this).attr('v');
		if(v!=""){
			$('#modalgeneral').modal('show');
			$(".viewsgeneral").load("<?php echo base_url().'generalesfunc/modaladduser'; ?>/?v="+encodeURIComponent(v));

		}
	});
	//Graficas
	var avance = <?php echo $avance_g; ?>;
	avance = parseFloat(avance);
	var poravanzar = 100-avance;
	var doughnutData = [
	    {
	        value: avance,
	        color:"#2196f3",
	        highlight: "#3d9de8",
	        label: "Avance"
	    },
	    {
	        value: poravanzar,
	        color: "#6254b2",
	        highlight: "#8176c1",
	        label: "Por avanzar"
	    }
	  ];
	var ctx = $(".doughnut-chart")[0].getContext("2d");
	window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
	      responsive : true,
	      showTooltips: true
	});
</script>