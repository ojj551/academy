<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php $this->load->view('header-left'); ?>
<div id="content" class="profile-v1">
	<div class="col-md-12 col-sm-12 profile-v1-wrapper">
		<div class="panel-body">
		  <canvas class="doughnut-chart" id="myChart" width="1300" height="400"></canvas><br>
		  <strong>Avanzado: </strong><span><?php echo $avance_g; ?>%</span><br>
		  <strong>Por avanzar: </strong><span><?php echo $falt_avance_g ?>%</span><br>
		</div>
	</div>
</div>
<?php $this->load->view('footer-admin'); ?> 
<script>
	var avance = <?php echo $avance_g; ?>;
	  avance = parseFloat(avance);
	  var poravanzar = 100-avance;
	  var data = {
	      datasets: [{
	          data: [
	              avance,
	              poravanzar
	          ],
	          backgroundColor: [
	              "#1ABB9C",
	          "#2A3F54",
	          ],
	          label: 'My dataset' // for legend
	      }],
	      labels: [
	          "Avance logrado "+avance+"%",
	          "Avance por lograr "+poravanzar+"%",
	      ]
	  };
	  var ctx = document.getElementById("myChart");
	    new Chart(ctx, {
	      data: data,
	      type: "doughnut"
	  });
</script>