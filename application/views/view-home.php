<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php $this->load->view('header-left'); ?>
<style>
  .activeAvatar{
    background: #d4ecff;
}
</style>  
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Bienvenid@ <strong><?php echo $this->session->userdata('UserFirstName'); ?></strong></h3>

          </div>
      </div>                    
    </div>
    <div class="col-xs-12 col-sm-12 col-lg-12">
      <div class="col-xs-1 col-sm-12 col-lg-1"></div>
      <div class="col-xs-10 col-sm-12 col-lg-10">
        <div class="col-xs-12 col-sm-12 col-lg-12" >
          <h1 class="tpg-relawey addmat"><strong>Elige una imagen</strong>, que te represente</h1>

          <div class="col-xs-6 col-sm-12 col-lg-6 padding0" >
            <h3 class="tpg-relawey">Sube una imagen</h3><br>
            <div class="col-lg-12">
              <div class="input-group fileupload-v1">
              <?php echo form_open_multipart("generalesfunc/saveimage"); ?>
             
                <input type="file" name="manualfile" class="fileupload-v1-file hidden" required>
                <input type="text" class="form-control fileupload-v1-path" placeholder="Dirección..." disabled="">
                <span class="input-group-btn">
                  <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Buscar imagen</button>
                </span>

                </div><!-- /input-group -->
                <div class="col-lg-12" style="padding-top: 10px;padding-left: 0px;">
                  <input type="submit" class="submit btn btn-danger" id="saveimagen" value="Guardar" />
                </div>
              
              <?php echo form_close();?>
            </div>
          </div>
          <div class="col-xs-6 col-sm-12 col-lg-6 padding0" >
            <h3 class="tpg-relawey">Ó elige un avatar</h3><br>
            <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#myModal">Elegir avatar</button>
          </div>
        </div>
      </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="col-xs-12 col-sm-12 col-lg-12 modal-content">
      <div class="col-xs-12 col-sm-12 col-lg-12 modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title blue">Seleccionar un avatar</h4>
      </div>
      <div class="col-xs-12 col-sm-12 col-lg-12 modal-body">
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="1" src="<?php echo site_url('assets/img/avatar/user1.png'); ?>">
          </div>
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="2" src="<?php echo site_url('assets/img/avatar/user2.png'); ?>">
          </div>
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="3" src="<?php echo site_url('assets/img/avatar/user3.png'); ?>">
          </div>
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="4" src="<?php echo site_url('assets/img/avatar/user4.png'); ?>">
          </div>
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="5" src="<?php echo site_url('assets/img/avatar/user5.png'); ?>">
          </div>
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="6" src="<?php echo site_url('assets/img/avatar/user6.png'); ?>">
          </div>
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="7" src="<?php echo site_url('assets/img/avatar/user7.png'); ?>">
          </div>
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="8" src="<?php echo site_url('assets/img/avatar/user8.png'); ?>">
          </div>
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="9" src="<?php echo site_url('assets/img/avatar/user9.png'); ?>">
          </div>
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="10" src="<?php echo site_url('assets/img/avatar/user10.png'); ?>">
          </div>
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="11" src="<?php echo site_url('assets/img/avatar/user11.png'); ?>">
          </div>
          <div class="col-xs-3 col-sm-12 col-lg-3">
            <img class="myavatar" id="12" src="<?php echo site_url('assets/img/avatar/user12.png'); ?>">
          </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-lg-12 modal-footer">
        <button type="button" class="btn btn-primary" id="save-avatar"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('footer-admin'); ?>
<script>
  //subir avatar
  $('.myavatar').click(function(){
    $('.myavatar').removeClass('activeAvatar');
    $(this).addClass('activeAvatar');
  });
  //guardar avatar
  $('#save-avatar').click(function(){
    var IDavatar = $('.activeAvatar').attr('id');
    var datos = ('IDavatar='+IDavatar);
    $.ajax({
        type:'POST',
        url : 'generalesfunc/myavatar',
        data: datos,
        beforeSend:function(){

        }, 
        success : function(data) {
        
        }
    });
  });

</script>
  