<?php $this->load->view('cabecera'); ?>
<video class="login-video" autoplay loop>
  <source src="<?php echo site_url('assets/video/Spacious.mp4'); ?>" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
<div class="login-conteiner">
	<div class="col-xs-12 col-sm-12 col-lg-12 text-center">
		<!---<img class="login-logo" src="<?php echo site_url('assets/img/logodemo.png'); ?>">-->
		<div class="col-xs-12 col-sm-12 col-lg-12">
			<div class="col-xs-2 col-sm-12 col-lg-2"></div>
			<div class="col-xs-8 col-sm-12 col-lg-8 text-center">
				<?php 
				if($user!=""){
				?>
				<h1 class="activacion-listo tpg-logo blanco">Felicidades</h1>
				<h1 class="tpg-logo blanco activacion-description">has completado el ultimo paso de tu registro</h1>
				
				<div class="activacion-empezar">
					<a>Iniciar</a>
				</div>
				<?php
				}else{ ?>
				<h1 class="activacion-listo tpg-logo blanco">Activación no valida</h1>
				<h1 class="tpg-logo blanco activacion-description">Lo sentimos tu activación tiene problemas</h1>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('footer'); ?>
<script>
	$('.activacion-empezar').click(function(){
		var user = "<?php echo $user; ?>";
		if(user!=""){
			$.ajax({
				type:'POST',
		       	url : base_url+'login/fistlogin',
		       	data: 'user='+encodeURIComponent(user),
				beforeSend:function(){
					$('.activacion-listo').addClass('animated bounceOutUp');
				},
				success : function(data) {
					if(data=="success"){
						setTimeout(function(){ 
							$('.activacion-description').addClass('animated bounceOutUp');
							setTimeout(function(){ 
								$('.activacion-empezar').addClass('animated bounceOutUp');
									$(location).attr('href', base_url+'inicio');
							},600);
						},600);
					}
				}
			});
		}
	});
		/*
		$('.activacion-empezar').click(function(){
			$('.activacion-listo').addClass('animated bounceOutUp');
			setTimeout(function(){ 
				$('.activacion-description').addClass('animated bounceOutUp');
				setTimeout(function(){ 
					$('.activacion-empezar').addClass('animated bounceOutUp');

					
				},600);
			},600);
		});*/
</script>