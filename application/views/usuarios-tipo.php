<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php $this->load->view('header-left'); ?>
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Tipos de usuarios</h3>

          </div>
      </div>                    
    </div> 
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="col-xs-2 col-sm-2 col-lg-2 text-left">
			<h3 class="title_page"><?php echo $this->uri->segment(1); ?></h3>
		</div>
		<div class="col-xs-10 col-sm-10 col-lg-10 text-left div-single-title">
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="col-xs-4 col-sm-12 col-lg-4 divcateg">
			<div class="col-xs-6 col-sm-12 col-lg-6">
				<h3 class="gray">Tipos de usuarios</h3>
			</div>
			<div class="col-xs-6 col-sm-12 col-lg-6 text-right">
				<label>Agregar <button type="button" class="btn btn-primary btng submenu-cust-mem" data-toggle="modal" data-target="#MaddCategoryF"><i class="fa fa-plus" aria-hidden="true"></i></button></label>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12 padding0 margCat">
				<?php echo $listtype; ?>
			</div>
		</div>
		<div class="col-xs-8 col-sm-12 col-lg-8 divcategL">
			<div class="col-xs-12 col-sm-12 col-lg-12 padding0">
				<div class="col-xs-3 col-sm-12 col-lg-3 padding0">
					<h3 class="gray">Modúlos</h3>
				</div>
				<div class="col-xs-9 col-sm-12 col-lg-9 padding0">
					
					<div class="col-xs-2 col-sm-12 col-lg-2 padding0 text-center">
						<h5 class="gray">Editar</h5>
					</div>
					<div class="col-xs-2 col-sm-12 col-lg-2 padding0 text-center">
						<h5 class="gray">Borrar</h5>
					</div>
					<div class="col-xs-2 col-sm-12 col-lg-2 padding0">
						<h5 class="gray">Todos</h5>
					</div>
					<div class="col-xs-1 col-sm-12 col-lg-1 padding0">
						<h5 class="gray"></h5>
					</div>
					<div class="col-xs-1 col-sm-12 col-lg-1 padding0">
						<h5 class="gray"></h5>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12 padding0 modulesnow" style="margin-top: 30px;">

			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12 text-right">
					<label>Agregar Modúlo<button type="button" class="btn btn-primary btng submenu-cust-mem showmodules" ><i class="fa fa-plus" aria-hidden="true"></i></button></label>
			</div>
		</div>	
	</div>
	<div id="MaddCategoryF" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="sending">
			<div class="spinner">
			  <div class="double-bounce1"></div>
			  <div class="double-bounce2"></div>
			</div>
		  </div>
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Agregar nuevo tipo de usuario</h4>
	      </div>
	      <div class="col-xs-12 col-sm-12 col-lg-12 modal-body modaladd blur">
	      	<form id="addexam">
		      	<label>Nombre</label>
		        <input class="form-control inputs-customer" type="text" name="TypeName" id="TypeName" placeholder="Agrega un nombre"><br>
	        </form>
	        <div class="col-xs-12 col-sm-12 col-lg-12 text-right padding0">
				<button type="button" class="btn btn-primary btng" style="display: inline-block;" id="save">Guardar</button>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div id="Modulos" class="modal fade" role="dialog">
		<div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Agregar modúlo</h4>
		      </div>
		      <div class="col-xs-12 col-sm-12 col-lg-12 modal-body modaladd blur">
		      	<div class="text-right">
		      		<button type="button" class="btn btn-primary" id="select_all">Seleccionar todo</button>
		      		<button type="button" class="btn btn-secondary" id="unselect_all">Deseleccionar todo</button>
		      	</div>
		      	<div id="moduleslist">
		      		<?php //echo $modulos; ?>
		      	</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary btng" style="display: inline-block;" id="savemodulos">Guardar</button>
		      </div>
		    </div>
		</div>
	</div>
	<!-- Modal editar -->
	<div id="Edit" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="sending">
			<div class="spinner">
			  <div class="double-bounce1"></div>
			  <div class="double-bounce2"></div>
			</div>
		  </div>
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Editar tipo de usuario</h4>
	      </div>
	      <div class="col-xs-12 col-sm-12 col-lg-12 modal-body modaladd blur">
	      	<form id="edittypeUs">
	        </form>
	        <div class="col-xs-12 col-sm-12 col-lg-12 text-right padding0">
				<button type="button" class="btn btn-primary btng" id="editType">Guardar</button>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<?php $this->load->view('footer-admin'); ?>  
<script>
	//Elegir tipo de usuario
	$('.clickme').click(function(){
		$('.clickme').removeClass('catActive');
		$(this).addClass('catActive');
		//
		var val = $(this).attr('val');
		var dato = ('TypeUserID='+encodeURIComponent(val));
		$.ajax({
	    	type:'POST',
	       	url : '<?php echo base_url() ?>usuarios/showmodules',
	       	data: dato,
			beforeSend:function(){
			},
			success : function(data) {
				$('.modulesnow').html(data);
				//borrar
				$('.dlt').click(function(){
					var t = $(this).attr('t');
					var id = $(this).attr('id');
					var vl = $(this).attr('vl');

					$.confirm({
					    title: 'Eliminar registro',
					    content: '¿Seguro que quieres eliminar el registro?',
					    confirm: function(){
					    	if(t!="" && id!="" && vl!=""){
								$.ajax({
							        url : base_url+'generalesfunc/delete',
							       	type : 'POST',
							       	data : 't='+encodeURIComponent(t)+'&id='+encodeURIComponent(id)+'&vl='+encodeURIComponent(vl),
							       	beforeSend:function(){
										$('.capaload').fadeIn(600);
									},
									success : function(datav) {
										setTimeout(function(){
											$('.capaload').fadeOut(600);
											if(datav=="success"){
												location.reload();
											}else{
												$.dialog({
													title: '¡Ups! tuvimos problemas para actualizar tus datos',
													content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
												});
											}
										},1000);
									}
								});
							}
					    }
					});

					
				});
			}
		});
	});
	//Mostrar modulos por modal
	$('.showmodules').click(function(){
		$('#Modulos').modal('show');
		var catActive = $('.catActive').attr('val');
		var dato = ('catActive='+encodeURIComponent(catActive));
		$.ajax({
	    	type:'POST',
	       	url : base_url+'usuarios/modulesshow',
	       	data: dato,
			beforeSend:function(){
			},
			success : function(vl) {
				$('#moduleslist').html(vl);
				//Seleccionar todos los modulos
				$('#select_all').click(function() {
				    $('input:checkbox').attr('checked',true); 
				});
				//Deseleccionar todo
				$('#unselect_all').click(function() {
				    $('input:checkbox').attr('checked',false); 
				});
				//Agregar modulos a tipo de usuario
				/*
				$('#savemodulos').click(function() {
				    $('input:checkbox').attr('checked',false); 
				});*/
			}
		});
	});
	
	//agregar nuevo tipo de usuario
	$('#save').click(function(){
		var TypeName = $('#TypeName').val();
		if(TypeName!=""){
			var dato = ('TypeName='+TypeName);
			$.ajax({
		    	type:'POST',
		       	url : '<?php echo base_url() ?>usuarios/addtypeuser',
		       	data: dato,
				beforeSend:function(){
					$('.sending').show();
					$('.blur').addClass('addmemClass');
				},
				success : function(data) {
					$('.sending').hide();
					$('.blur').removeClass('addmemClass');
					$('#Madd').modal('hide');
		           	if(data=="exist"){
		           		$('#modalERROR').modal('show');
		           	}else{
		           		//console.log(data);
		           		
			           	$.alert({
			           		theme: 'material',
						    title: 'Tipo de usuario agregado',
						    content: 'El nuevo tipo de usuario se creó correctamente.',
						    confirm: function(){
						        location.reload();
						    }
						});
						
		           }
				}
			});
		}else{
			$.alert({
			    title: '¡Campos incompletos!',
			    content: 'El campo <strong>nombre es necesario</strong> para poder dar de alta un nuevo tipo de usuario',
			});
		}
	});
	//agregar modulo a tipo de usuario
	$('#savemodulos').click(function(){
		var Modulos = [];
		var TipoUsuario = $('#TypUsr').val();
		if(TipoUsuario!='undefined'){
			$('#moduleslist input:checked').each(function() {
				var data = {};
				var Module = $(this).attr('value');
				//
			    data.ModuleID = Module;
	        	data.TypeUserID = TipoUsuario;
	        	Modulos.push(data);

			});
			var ajax = $.ajax({
			    method: "POST",
			    url : base_url+'usuarios/addmoduletypeuser',
			    data : {files: Modulos},
			    dataType: "json",
			    beforeSend:function(){
					$('#Modulos').modal('hide');
       				$('.capaload').fadeIn(600);
				},
				success : function(data) {
					setTimeout(function(){
						$('.capaload').fadeOut(600);
		          		$.alert({
			           		theme: 'material',
						    title: 'Modulo(s) agregado(s)',
						    content: 'El modulo(s) se agrego correctamente al tipo de usuario.',
						    confirm: function(){
						        location.reload();
						    }
						});
		           },1000);
				}
			})
			
		}else{
			$.alert({
           		theme: 'material',
			    title: 'Selecciona un tipo de usuario',
			    content: 'Tienes que seleccionar un tipo de usuario para poder asignar un modulo.',
			    confirm: function(){
			        //location.reload();
			    }
			});
		}
		/*
		if($('.catActive').attr('val')==undefined){
			$.alert({
           		theme: 'material',
			    title: 'Selecciona un tipo de usuario',
			    content: 'Tienes que seleccionar un tipo de usuario para poder asignar un modulo.',
			    confirm: function(){
			        //location.reload();
			    }
			});
		}else{
			var ajax = $.ajax({
			    method: "POST",
			    url : '<?php echo base_url() ?>usuarios/module_typeuser',
			    data : {files: Modulos},
			    dataType: "json",
			    beforeSend:function(){
					$('.blur').addClass('addmemClass');
				},
				success : function(data) {
					$('.blur').removeClass('addmemClass');
					$('#Madd').modal('hide');
		           	if(data=="exist"){
		           		$('#modalERROR').modal('show');
		           	}else{
		           		if(data){
			          		$.alert({
				           		theme: 'material',
							    title: 'Modulo agregado',
							    content: 'El modulo se agrego correctamente al tipo de usuario.',
							    confirm: function(){
							        location.reload();
							    }
							});
			           	}
						
		           }
				}
			})
		}*/
	});
	/*****EDITAR TYPE USER**********/
	/*
	//ver datos en el modal
	$('.editar').click(function(){
		var val = $(this).attr('val');
		var dato = ('val='+val);
		$.ajax({
	    	type:'POST',
	       	url : '<?php echo base_url() ?>usuarios/modalEdit',
	       	data: dato,
			beforeSend:function(){
			},
			success : function(data) {
				$('#edittypeUs').html(data);
				
			}
		});
	});
	//guardar datos
	$('#editType').click(function(){
		var dato = $('#edittypeUs').serialize();
		$.ajax({
	    	type:'POST',
	       	url : '<?php echo base_url() ?>usuarios/EditTypeUser',
	       	data: dato,
			beforeSend:function(){
				$('.sending').show();
				$('.blur').addClass('addmemClass');
			},
			success : function(data) {
				$('.sending').hide();
				$('.blur').removeClass('addmemClass');
				if(data="success"){
					$.alert({
		           		theme: 'material',
					    title: '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Tipo de usuario actualizado',
					    content: 'El nuevo tipo de usuario se actualizo correctamente.',
					    confirm: function(){
					        location.reload();
					    }
					});
				}else{
					$.alert({
		           		theme: 'material',
					    title: '<i class="fa fa-exclamation" aria-hidden="true"></i> Problemas al actualizar',
					    content: 'Surgio un problema al tratar de actualizar el tipo de usuario, intentalo nuevamente y de ser continuo el error acude con el departamento de TI',
					    confirm: function(){
					    }
					});
				}
			}
		});
	});*/
</script>