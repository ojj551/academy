<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-header'); ?>
<div class="col-xs-12 col-sm-12 col-lg-12">
<h3>Materias - profesor</h3> <a href="<?php echo base_url() ?>asignaturas/profesor"><i class="fa fa-plus" aria-hidden="true"></i> Agregar</a>

<table id="example" class="table table-striped table-bordered headtablemodules tablessearch" cellspacing="0" width="100%">
	<thead>
		<th><label>Fecha de asignación</label></th>
		<th><label>Profesor</label></th>
		<th><label>Materia</label></th>
		<th></th>
	</thead>
	<tbody>
		<?php echo $list; ?>
	</tbody>
</table>
</div>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('footer'); ?>