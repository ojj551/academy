<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>      
<?php $this->load->view('header-left'); ?> 
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft"><?php echo $CustomerName; ?></h3>

          </div>
      </div>                    
    </div>  
    <div class="col-md-12">
      <div class="col-md-12">
          
	      <div class="col-md-12 tabs-area">

	        <?php //echo $menu; ?>
	        <div id="tabsDemo6Content" class="tab-content tab-content-v6 col-md-12">
	          <div role="tabpanel" class="tab-pane fade active in" id="tabs-demo7-area1" aria-labelledby="tabs-demo7-area1">
	            <?php echo $content; ?>
	          </div>

	        </div>

	      </div>
      </div>
  </div>
</div>
<div id="addalumnos-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Dar de alta usuarios</h4>
      </div>
      <div class="modal-body">
        <?php echo $modal; ?>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="createalumnos"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>
<div id="asigmat-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Asignar materias</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="ctm" value="<?php echo $ctm; ?>">
        <label>Materia</label>
        <select class="form-control" id="matprof">
          <option value="">Selecciona...</option>
          <?php echo $materias ?>
        </select>
        <label>Profesor</label>
        <select class="form-control" id="profmat">
          <option value="">Selecciona...</option>
          <?php echo $profes ?>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addmattoprof"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>
<div id="asigme-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Asignarme usuarios</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="ctm" value="<?php echo $ctm; ?>">
        <input type="hidden" id="usr" value="<?php echo $usr; ?>">
        <label>Alumnos</label>
        <select class="form-control" id="almtoasig">
          <option value="">Selecciona...</option>
          <?php echo $subordinds ?>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addsubordn"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>
<div id="asigto-form" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Asignar usuarios</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="ctm" value="<?php echo $ctm; ?>">
        <label>Agregar a</label>
        <select class="form-control" id="fromusr">
          <option value="">Seleccionar...</option>
          <?php echo $subordinds; ?>
        </select>
        <label>El siguiente usuario:</label>
        <select class="form-control" id="touser">
          
          
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addsubordntouser"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>
<!--MODALES-->
<?php $this->load->view('clientes/modal-add-carrera'); ?>
<?php $this->load->view('clientes/modal-add-materia'); ?>
<?php $this->load->view('footer-admin'); ?>  
<script src="<?php echo site_url('assets/js/admin.js'); ?>"></script>