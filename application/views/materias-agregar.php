<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('menu-header'); ?>
<div class="conteiner-right  col-xs-12 col-sm-12 col-lg-12">
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<h1 class="tpg-logo title-section">Nueva materia</h1>

	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="col-xs-2 col-sm-12 col-lg-2"></div>
		<div class="col-xs-8 col-sm-12 col-lg-8">
			<div class="col-xs-12 col-sm-12 col-lg-12" >
				<label class="tpg-relawey addmat">Nombre de la materia</label>
				<input class="form-control form-add-mat" type="text" name="nombre" id="nombre">
			</div>
			<!--
			<div class="col-xs-12 col-sm-12 col-lg-12" >
				<div class="col-xs-6 col-sm-12 col-lg-6 padding0">
					<label class="tpg-relawey addmat">Inicia</label>
					<input class="form-control form-add-mat" type="date" name="inicia" id="inicia">
				</div>
				<div class="col-xs-6 col-sm-12 col-lg-6 padding0">
					<label class="tpg-relawey addmat">Termina</label>
					<input class="form-control form-add-mat" type="date" name="finaliza" id="finaliza">
				</div>
			</div>-->
			<div class="col-xs-12 col-sm-12 col-lg-12" >
				<label class="tpg-relawey addmat">Requerimientos</label>
				<textarea class="form-control form-add-mat" name="requerimientos" id="requerimientos" style="min-height: 90px;"></textarea>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12" >
				<label class="tpg-relawey addmat">Objetivos</label>
				<textarea class="form-control form-add-mat" name="objetivos" id="objetivos" style="min-height: 90px;"></textarea>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 30px;">
				<a type="button" class="btn btn-secondary savemateria" id="crear-materia">Crear</a>
			</div>
		</div>

	</div>
</div>


<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer'); ?>
<script>
	//var requerimientos = CKEDITOR.replace( 'requerimientos' );
	//var objetivos = CKEDITOR.replace( 'objetivos' );
	$('#crear-materia').click(function(){
		var nombre = $('#nombre').val();
		var inicia = $('#inicia').val();
		var finaliza = $('#finaliza').val();
		var requerimientos = $('#requerimientos').val();
		var objetivos = $('#objetivos').val();
		///
		if(nombre!="" && requerimientos!="" && objetivos!=""){
			$.ajax({
				type:'POST',
		       	url : base_url+'materiasfunc/add',
		       	data: 'nombre='+nombre+'&inicia='+inicia+'&finaliza='+finaliza+'&requerimientos='+requerimientos+'&objetivos='+objetivos,
				beforeSend:function(){
					$('.capaload').fadeIn(600);
				},
				success : function(data) {
					$('.capaload').fadeOut(600);
					setTimeout(function(){ 
						if(data!="error"){
							$(location).attr('href', base_url+'materias');
							//$(location).attr('href', base_url+'materias/admin/?m='+encodeURIComponent(data));
						}else{
							$.dialog({
								title: 'Ups!',
								content: data,
							});
						}
					},700);
				}
			});
		}
	});	
</script>