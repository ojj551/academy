<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('menu-header'); ?>
<?php $this->load->view('modals'); ?>
<div class="col-xs-12 col-sm-12 col-lg-12 marginfirst">
	<div class="col-xs-2 col-sm-12 col-lg-2 "></div>
	<div class="col-xs-8 col-sm-12 col-lg-8 ">
		<div class="col-xs-4 col-sm-12 col-lg-4">
			<form id="form">
				<label>Idioma</label>
				<select class="form selectpicker" name="idiomtxt" id="idiomtxt" data-live-search="true">
					<option value="">Selecciona...</option>
					<?php echo $options; ?>
				</select><br>
				<label>Tipo de negocio</label>
				<select class="form-control" name="lead" id="lead">
					<option value="">Selecciona...</option>
					<option value="edu">Educacional</option>
					<option value="com">Comercial</option>
				</select>
				<label>Key del texto</label>
				<input type="text" name="keytxt" id="keytxt" class="form-control">
				<label>Texto</label>
				<textarea class="form-control" name="text" id="text" style="min-height: 40px;"></textarea>
				<label>Descripción</label>
				<textarea class="form-control" name="descrption" id="descrption" style="min-height: 70px;"></textarea>
			</form>
			<div class="text-right">
				<button type="button" class="btn btn-primary btng" id="save-txt">Guardar</button>
			</div>
		</div>
		<div class="col-xs-8 col-sm-12 col-lg-8">
			<table id="example" class="table table-striped table-bordered headtablemodules tablessearch" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		            	<th><label>Lenguaje</label></th>
		            	<th><label>Tipo Custom</label></th>
		                <th><label>Key</label></th>
		                <th><label>Texto</label></th>
		                <th><label>Descripción</label></th>
		                <th><label>Ajustes</label></th>
		            </tr>
		        </thead>
		        <tbody>
		        	<?php echo $list; ?>
		        </tbody>
		    </table>
		</div>
	</div>
</div>
<div id="updatetxt" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar</h4>
      </div>
      <div class="modal-body divcontent">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save-txtupdate"><?php echo $FormGuardar; ?></button>
        <a class="" data-dismiss="modal"><?php echo $FormCancel; ?></a>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('footer'); ?>
<script>
	$('#save-txt').click(function(){
		var lead = $('#lead').val();
		var idiomtxt = $('#idiomtxt').val();
		var keytxt = $('#keytxt').val();
		var text = $('#text').val();
		var descrption = $('#descrption').val();

		if(idiomtxt!="" && keytxt!="" && text!="" && descrption!=""){
			$.ajax({
		        type:'POST',
		        url : base_url+'textos/add',
		        data: 'idiomtxt='+idiomtxt+'&keytxt='+encodeURIComponent(keytxt)+'&text='+encodeURIComponent(text)+'&descrption='+encodeURIComponent(descrption)+'&lead='+lead,
		        beforeSend:function(){
		        	$('.capaload').fadeIn(600);
		        },
		        success: function(data){
		        	$('.capaload').fadeOut(600);
		        	if(data=="success"){
		        		location.reload();
		        	}else{
		        		$.alert({
			              title: 'Tuvimos problemas al crear el texto',
			              content: '<strong>Detalles: </strong>'+data,
			              confirm: function () {
			                  //$(location).attr('href', base_url+'grupos');
			              }
			            });
		        	}
		        }
		    });
		}
	});
	//
	$('.edit2').click(function(){
		$('#updatetxt').modal('show');
		//
		var vl = $(this).attr('vl');
		if(vl!=""){
			$.ajax({
				url : base_url+'textos/formtxt',
				type : 'POST',
		       	data : 'vl='+encodeURIComponent(vl),
		       	beforeSend:function(){
		       		console.log('enviando...');
					//$('.capaload').fadeIn(600);
				},
				success : function(datav) {
					$('.divcontent').html(datav);
				}
			});
		}
	});
	//
	$('#save-txtupdate').click(function(){
		var txtid = $('#txtid').val();
		var keytxt = $('#keytxtupdt').val();
		var txt = $('#txtupt').val();
		var lgupdt = $('#lgupdt').val();
		var leadupdt = $('#leadupdt').val();
		if(txtid!="" && keytxt!="" && txt!=""){
			$.ajax({
				url : base_url+'textos/update',
				type : 'POST',
		       	data : 't='+encodeURIComponent(txtid)+'&keytxt='+encodeURIComponent(keytxt)+'&txt='+encodeURIComponent(txt)+'&leadupdt='+leadupdt+'&lgupdt='+lgupdt,
		       	beforeSend:function(){
		       		//console.log('enviando...');
				},
				success : function(datav) {
					if(datav=="success"){
						location.reload();
					}else{
						location.reload();
					}
					//console.log(datav);
				}
			});
		}
	});
</script>