<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('menu-header'); ?>
<div class="conteiner-right  col-xs-12 col-sm-12 col-lg-12">
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<h1 class="tpg-logo title-section">Nuevo grupo</h1>

	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="col-xs-2 col-sm-12 col-lg-2"></div>
		<div class="col-xs-8 col-sm-12 col-lg-8">
			<div class="col-xs-12 col-sm-12 col-lg-12" >
				<label class="tpg-relawey addmat">Carrera</label>
				<select class="form-control" id="carrera" style="border: 1px solid;">
					<option value="">Selecciona carrera</option>
					<?php echo $carreras; ?>
				</select>
				<label class="tpg-relawey addmat">Semestre</label>
				<select class="form-control" id="semestre" style="border: 1px solid;">
					<option value=""></option>
				</select>
				<label class="tpg-relawey addmat">Materia a impartir</label>
				<select class="form-control" id="materias" style="border: 1px solid;">
					<option>Selecciona...</option>
					<?php echo $materias; ?>
				</select>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 30px;">
				<a type="button" class="btn btn-secondary savemateria" id="crear-grupo">Crear</a>
			</div>
		</div>

	</div>
</div>


<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer'); ?>
<script>
	//var requerimientos = CKEDITOR.replace( 'requerimientos' );
	//var objetivos = CKEDITOR.replace( 'objetivos' );
	$( "#carrera" ).change(function() {
		var vl = $(this).val();
		if(vl!=""){
			$.ajax({
				type:'POST',
		       	url : base_url+'grupos/semesters',
		       	data: 'vl='+vl,
				beforeSend:function(){
					$('.capaload').fadeIn(600);
				},
				success : function(data) {
					setTimeout(function(){ 
						$('.capaload').fadeOut(600);
						$('#semestre').html(data);
					},700);
				}
			});
		}
		//console.log(vl);
	});
	$('#crear-grupo').click(function(){
		var carrera = $('#carrera').val();
		var semestre = $('#semestre').val();
		var materias = $('#materias').val();
		///
		if(carrera!="" && semestre!="" && materias!=""){
			$.ajax({
				type:'POST',
		       	url : base_url+'gruposfunc/add',
		       	data: 'carrera='+encodeURIComponent(carrera)+'&semestre='+semestre+'&materias='+encodeURIComponent(materias),
				beforeSend:function(){
					$('.capaload').fadeIn(600);
				},
				success : function(data) {
					setTimeout(function(){ 
						$('.capaload').fadeOut(600);
						if(data=='exist'){
							$.dialog({
								title: 'El grupo ya existe',
								content: 'Al parecer existe un grupo con las mismas caracteristicas',
							});
						}
						if(data=='success'){
							$.dialog({
								title: 'El grupo se creado con exito',
								content: 'Tu grupo se ha creado con exito <a href="'+base_url+'grupos">Ver grupos</a>',
							});
						}
					},700);
				}
			});
		}
	});	
</script>