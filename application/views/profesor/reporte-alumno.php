<?php $this->load->view('profesor/header-left'); ?> 
<div id="content">
	<div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft"><span class="naranja"><?php echo $FullName; ?></span> | <?php echo $CourseName; ?></h3>
              <div class="col-md-6 col-sm-12" style="border-right: 1px solid #dedede;">
              	<h3>Temario,</h3>
              	<?php echo $topics; ?>
              </div>
              <div class="col-md-6 col-sm-12">
              	<h4 class="inlineb"><b class="naranja">Ultima interacción:</b> <?php echo $Last; ?></h4>
              	<div class="col-md-8 col-sm-12">
              		<canvas id="myChart" width="300" height="300"></canvas>
              	</div>
              	<!--<canvas class="doughnut-chart"></canvas>-->
              </div>
          </div>
      </div>                    
    </div> 
</div>
<?php $this->load->view('footer-admin'); ?>
<script>
  	var avance = <?php echo $AvanceAlumno; ?>;
  	console.log(avance);
  	avance = parseFloat(avance);
  	var poravanzar = 100-avance;
  	var doughnutData = [
	    {
	        value: avance,
	        color:"#2196F3",
	        highlight: "#3d9de8",
	        label: "Avance"
	    },
	    {
	        value: poravanzar,
	        color: "#f52a2a",
	        highlight: "#ea4444",
	        label: "Por avanzar"
	    }
	  ];
  	var ctx = document.getElementById("myChart").getContext('2d');

	var myChart = new Chart(ctx, {
	    type: 'doughnut',
	    data: {
	        labels: ["Avance", "Por avanzar"],
	        datasets: [{
	            label: '# of Votes',
	            data: [avance, poravanzar],
	            backgroundColor: [
	                '#ff7800',
	                '#b5b5b5',
	            ]
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }]
	        }
	    }
	});
</script>