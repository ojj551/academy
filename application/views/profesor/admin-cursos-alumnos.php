<?php $this->load->view('profesor/header-left'); ?> 
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Alumnos del curso</h3>

          </div>
      </div>                    
    </div>
    <div class="col-md-12">
      <?php 
      $dato['tp'] = 'alm';
      $this->load->view('profesor/sliderbar',$dato); 
      ?> 
    </div>
    <div class="tab-content">
      <div class="col-md-12 col-sm-12 col-lg-12" style="padding-top: 30px;">
        <a class="tpg-relawey add-curso-directo" data-toggle="modal" data-target="#addsuscript"><i class="fa fa-plus" aria-hidden="true"></i> Agregar un usuario</a>
      </div>
      <div class="col-md-12 col-sm-12 col-lg-12">
      	<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	        <thead>
	            <tr>
	                <th>Nombre</th>
	                <th>Apellido</th>
	                <th>Email</th>
	                <th>Carrera | Nivel</th>
	                <th>-</th>
	            </tr>
	        </thead>
	        <tbody>
	        	<?php echo $alumnos; ?>
	        </tbody>
	    </table>
      </div>
      
    </div> 
    
</div>
<div id="addsuscript" class="modal fade" role="dialog">
	<div class="modal-dialog">
	  <div class="col-md-12 col-sm-12 col-lg-12 modal-content">
	    <div class="col-md-12 col-sm-12 col-lg-12 modal-header">
	      <button type="button" class="close" data-dismiss="modal">&times;</button>
	      <h4 class="modal-title">Agregar usuarios a curso</h4>
	    </div>
	    <div class="col-md-12 col-sm-12 col-lg-12 modal-body">
	    	<div class="col-md-6 col-sm-12 col-lg-6">
	          <label>Buscar <b class="red">*</b></label>
	          <select class="form-control selectpicker" id="user-select" data-show-subtext="true" data-live-search="true" required>
	            <?php echo $listusers; ?>
	          </select>
	          <div class="text-center" style="padding-top: 10px;">
	          	<button type="buttom" class="btn btn-primary" id="add"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
	          </div>
	        </div>
	        <div class="col-md-6 col-sm-12 col-lg-6">
	        	<div class="miform" style="display: none;">
	        		<div class="paint-users">
	        			
	        		</div>
	        		<button id="add-usersbd" class="btn btn-primary">Agregar</button>
	        	</div>
	        </div>
	    </div>
	    <div class="col-md-12 col-sm-12 col-lg-12 modal-footer">
	      <a data-dismiss="modal">Cancelar</a>
	    </div>
	  </div>

	</div>
</div>

<?php $this->load->view('footer-admin'); ?>
<script src="<?php echo site_url('assets/js/cursos/alumnos.js'); ?>"></script>
<script>
	$(document).ready(function() {
    	$('#example').DataTable();
	} );
</script>