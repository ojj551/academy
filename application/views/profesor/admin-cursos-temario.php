<?php $this->load->view('profesor/header-left'); ?> 
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Temario</h3>

          </div>
      </div>                    
    </div>
    <div class="col-md-12">
      <?php 
      $dato['tp'] = 'tem';
      $this->load->view('profesor/sliderbar',$dato); 
      ?> 
    </div>
    <div class="tab-content">
      <div class="col-md-12 col-sm-12 col-lg-12 text-right">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addunidad">Agregar unidad</button>
      </div>
      <div class="col-md-12 col-sm-12 col-lg-12 div-lecciones">
        <?php echo $list; ?>
      </div>
    </div> 
    
</div>

<div id="addunidad" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar unidad</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="MatSt" id="MatSt" value="<?php echo $MateriaIDEncryp; ?>">
        
        <div class="sections section-1">
          <label>Nombre de la unidad</label>
          <input class="form-control" type="text" id="nameunidad">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="create-section">Aceptar</button>
      </div>
    </div>

  </div>
</div>
<div class="capa-white"></div>
<?php $this->load->view('footer-admin'); ?> 
<script src="<?php echo site_url('assets/js/cursos/temario.js'); ?>"></script>