<?php $this->load->view('profesor/header-left'); ?> 
<?php echo link_tag('assets/js/editor-master/css/medium-editor.css'); ?>
<?php echo link_tag('assets/js/editor-master/css/themes/default.css'); ?>
<style>
	.opener-left-menu{display: none;}
	.div-question{
		width: 80%;
	    position: relative;
	    left: 20%;
	    top: 75px;
	}
	.capa-white{display: none;}
</style>

<div class="col-xs-12 col-sm-12 col-lg-12">
    <div class="container">
    	<div class="div-question text-left">
    		<h1 class="bar">Nombre del quiz</h1>
    		<div class="col-xs-10 col-sm-10 col-lg-10">
		     	<div class="form-group form-animate-text" style="margin-top:40px !important;">
		          <div class="form-text editor_edit" id="name-quiz" name="name-quiz" placeholder="Nombre del quiz..."></div>
		    	</div>
	        </div>
	        <div class="col-xs-2 col-sm-2 col-lg-2" style="padding-top: 53px;">
	        	<button type="button" class="btn btn-primary save-quize" c="<?php echo $CourseIDEnCrypt; ?>" tpc="<?php echo $TopicID; ?>" itm="<?php echo $ItemID; ?>"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
	        </div>

        </div>
    </div>
</div>
<div class="capa-white"></div>
<?php $this->load->view('footer-admin'); ?>
<script src="<?php echo site_url('assets/js/editor-master/js/medium-editor.js'); ?>"></script>
<script>
	//Guardar quiz
	$('.save-quize').click(function(){
		var name_quiz = $('#name-quiz').text();
		var c = $(this).attr('c');
		var itm = $(this).attr('itm');
		var tpc = $(this).attr('tpc');
		$.ajax({
	      type:'POST',
	      url : base_url+'materiasfunc/savequize',
	      data : 'c='+encodeURIComponent(c)+'&itm='+encodeURIComponent(itm)+'&l='+encodeURIComponent(tpc)+'&name='+name_quiz,
	      beforeSend:function(){
	      	$('.capa-white').fadeIn(600);
	      },
	      success : function(vl) {
	      	if(vl!='exist'){
	      		$(location).attr('href', base_url+'materias/examen/?ex='+vl);
	      		///$('.sbmenu-items-add').addClass('show-sbmenu');
	      	}
	      }
	    });
	});

	var editor = new MediumEditor('.editor_edit', {
    toolbar: {
        allowMultiParagraphSelection: true,
        //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
        buttons: ['bold', 'italic', 'anchor'],
        diffLeft: 0,
        diffTop: -10,
        firstButtonClass: 'medium-editor-button-first',
        lastButtonClass: 'medium-editor-button-last',
        relativeContainer: null,
        standardizeSelectionStart: false,
        static: false,

        /* options which only apply when static is true */
        align: 'center',
        sticky: false,
        updateOnEmptySelection: false
    }
	});
	
</script>