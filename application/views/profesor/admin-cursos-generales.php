<?php $this->load->view('profesor/header-left'); ?> 
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Generales</h3>

          </div>
      </div>                    
    </div>
    <div class="col-md-12">
      <?php 
      $dato['tp'] = 'gen';
      $this->load->view('profesor/sliderbar',$dato); 
      ?> 
    </div>
    <div class="col-md-12">  
      <div class="col-md-7 col-sm-12 col-lg-7">
        <div class="col-md-12 col-sm-12 col-lg-12">
          <div class="panel box-v7">
            <div class="panel-body">
              <div class="col-md-12 padding-0 box-v7-header">
                  <div class="col-md-12 padding-0">
                      <div class="col-md-12 padding-0 text-right">
                        <button type="button" class="btn btn-primary text-right" data-toggle="modal" data-target="#edit-description-mat">Editar contenido</button>
                      </div>
                      <div class="col-md-12 padding-0">

                        <label class="tpg-relawey  descrip-mat1">Requerimientos</label><BR>
                        <label class="tpg-relawey descrip descrip-mat2" style="line-height: initial;"><?php echo $CourseRequirements; ?></label><br>

                        <label class="tpg-relawey  descrip-mat1">Objetivos: </label> 
                        <label class="tpg-relawey descrip descrip-mat2" style="line-height: initial;"><?php echo $CourseGoals; ?></label>
                      </div>
                      
                  </div>
              </div>

            </div>
          </div>

        </div>
      </div>
      <div class="col-md-5 col-sm-12 col-lg-5">
          <div class="panel box-v7">
            <div class="panel-body">
              <div class="col-md-12 padding-0 box-v7-header">
                  <div class="col-md-12 padding-0">
                      <div class="col-md-10 padding-0">
                        <img src="<?php echo $CourseImage; ?>" class="box-v7-avatar pull-left">
                        <h4>Imagen del curso</h4>
                      </div>
                      <div class="col-md-2 padding-0">
                        <div class="btn-group pull-right">
                        <i class="icon-options-vertical icons box-v7-menu" data-toggle="dropdown"></i>
                        <ul class="dropdown-menu" role="menu">
                          <li><a data-toggle="modal" data-target="#edit-banner">Cambiar imagen</a></li>
                          <li><a href="#">Elimniar</a></li>
                        </ul>
                      </div>
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="panel box-v7">
            <h4>Ajustes de diseño</h4>
            <div class="panel-body">
              <div class="col-md-12 padding-0 box-v7-header">
                  <div class="col-md-12 padding-0">
                      <div class="col-md-6 padding-0">
                        
                        <h4>Elegir tipografia</h4>
                      </div>
                      <div class="col-md-6 padding-0">
                        <div class="">
                          <select class="form-control" id="fontsize">
                            <option value="">Seleccionar</option>
                            <option value="roboto" <?php if($CourseFont=='roboto'){echo 'selected';} ?>>Roboto</option>
                            <option value="nunito" <?php if($CourseFont=='nunito'){echo 'selected';} ?>>Ninito</option>
                            <option value="opensans" <?php if($CourseFont=='opensans'){echo 'selected';} ?>>Open Sans</option>
                            <option value="lato" <?php if($CourseFont=='lato'){echo 'selected';} ?>>Lato</option>
                          </select>
                        </div>
                      </div>
                  </div>
              </div>
            </div>
            <div class="panel-body">
              <div class="col-md-12 padding-0 box-v7-header">
                  <div class="col-md-12 padding-0">
                      <div class="col-md-6 padding-0">
                        
                        <h4>Paletas de colores</h4>
                      </div>
                      <div class="col-md-6 padding-0">
                        <div class="">
                          <label>Elige pricipales tonos de colores</label><br>
                          <input class="form-control" type="color" id="color1" value="<?php echo $ColorPalette1; ?>">
                          <input class="form-control" type="color" id="color2" value="<?php echo $ColorPalette2; ?>">
                          <input class="form-control" type="color" id="color3" value="<?php echo $ColorPalette3; ?>">
                        </div>
                      </div>
                  </div>
              </div>
            </div>
            <div class="text-center" style="padding-bottom: 20px;margin-bottom: 50px;">
              <button type="button" class="btn btn-primary" id="guardar_design">Guardar ajustes de diseño</button>
            </div>
          </div>
      </div>
    </div>
</div>

<div id="edit-banner" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar datos del banner</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idmat" id="idmat" value="<?php echo $MateriaIDEncryp; ?>">
        <label class="tpg-relawey addmat">Banner de la materia</label>
        <input class="form-control" type="file" name="bannermupt" id="bannermupt">
        <label class="tpg-relawey addmat">Color banner</label>
        <input class="form-control" type="color" name="colorbmupt" id="colorbmupt" style="width: 50px;height: 50px;display: none;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="updatebannerdata">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div id="edit-description-mat" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar datos</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idmat" id="idmat" value="<?php echo $MateriaIDEncryp; ?>">
        <label class="tpg-relawey addmat">Requerimientos</label>
        <textarea class="form-control" name="reqmupd" id="reqmupd" style="min-height: 170px;"><?php echo $CourseRequirements; ?></textarea>
        <label class="tpg-relawey addmat">Objetivos</label>
        <textarea class="form-control" name="objmupd" id="objmupd" style="min-height: 170px;"><?php echo $CourseGoals; ?></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="updatedecrpdata">Aceptar</button>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('footer-admin'); ?> 
<script src="<?php echo site_url('assets/js/cursos/generales.js'); ?>"></script>