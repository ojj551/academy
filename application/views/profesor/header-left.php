<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('header-top'); ?>
<div id="left-menu">
  	<div class="sub-left-menu scroll">
  		<img src="<?php echo site_url('assets/img/escuelas/logo_unaq.png'); ?>" style="position: relative;padding-top: 29%; width: 70%;left: 15%;">
	    <ul class="nav nav-list mnlft">
	        <li>
	        	<a href="<?php echo base_url(); ?>profesor"><i class="fa fa-home" aria-hidden="true"></i> Inicio</a>
	        </li>
	        <li>
	        	<a href="<?php echo base_url(); ?>profesor/alumnos"><i class="fa fa-users" aria-hidden="true"></i> Alumnos</a>
	        </li>
	        <li>
	        	<a href="<?php echo base_url(); ?>cursos"><i class="fa fa-list" aria-hidden="true"></i> Cursos</a>
	        </li>
	    </ul>
	    <div class="footer-menu-left text-center" style="border-top:1px solid #d2d2d2;padding-top: 20px;position: absolute;bottom: 0px;">
	    	<img src="<?php echo site_url('assets/img/logo-black.png'); ?>" style="position: relative; width: 40%;">
	    	<div class="">
		    	<a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
		    	<a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
		    	<a href=""><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
		    	<a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
	    	</div>
	    </div>
    </div>
</div>