<?php $this->load->view('cabecera'); ?>
<?php $this->load->view('menu-lateral'); ?>
<?php $this->load->view('menu-header'); ?>
<div class="conteiner-right  col-xs-12 col-sm-12 col-lg-12">
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<h1 class="tpg-logo title-section">Hola <?php echo $this->session->userdata('UserFirstName'); ?>, te damos la bienvenida. </h1>

	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="col-xs-2 col-sm-12 col-lg-2"></div>
		<div class="col-xs-8 col-sm-12 col-lg-8">
			<div class="col-xs-12 col-sm-12 col-lg-12" >
				<h1 class="tpg-relawey addmat"><strong>Paso 1</strong>, elige una imagen que te represente</h1>
				<div class="col-xs-6 col-sm-12 col-lg-6" >
					<label class="tpg-relawey">Sube una imagen</label><br>
					<input type="file" name="imgavatar" id="imfavatar">
				</div>
				<div class="col-xs-6 col-sm-12 col-lg-6" >
					<label class="tpg-relawey">Elige un avatar</label><br>
					<button type="button" class="btn btn-secondary">Elegir</button>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-lg-12 text-right">
	<a href="<?php echo base_url() ?>carreras" type="button" class="btn btn-info">Siguiente</a>
	</div>
</div>
<?php $this->load->view('modals'); ?>
<?php $this->load->view('footer'); ?>