<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('nc/header-top'); ?>      
<?php $this->load->view('nc/header-left'); ?>
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">
            
          </div>
      </div>                    
    </div> 
	<div class="col-xs-12 col-sm-12 col-lg-12">
    <div class="col-sm-12 col-md-6 col-lg-6">
      <h1 class="editor_edit" nm="title">Escribe título de curso aquí...</h1><br><br>
      <label class="editor_edit descp-cours" nm="description">Escribe una descripción para tu curso...</label>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6"></div>
	</div>
</div>
<?php $this->load->view('footer-admin'); ?> 
<script src="<?php echo site_url('assets/js/editor-master/js/medium-editor.js'); ?>"></script>
<script>
    $('.editor_edit').click(function(){
      $(this).html('');
    });
   var editor = new MediumEditor('.editor_edit', {
    toolbar: {
        allowMultiParagraphSelection: true,
        //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
        buttons: ['bold', 'italic', 'anchor'],
        diffLeft: 0,
        diffTop: -10,
        firstButtonClass: 'medium-editor-button-first',
        lastButtonClass: 'medium-editor-button-last',
        relativeContainer: null,
        standardizeSelectionStart: false,
        static: false,
        /* options which only apply when static is true */
        align: 'center',
        sticky: false,
        updateOnEmptySelection: false,
        setFocusOutlineEnabled : false
    }
  });

  //editor.setFocusOutlineEnabled(false);
</script>