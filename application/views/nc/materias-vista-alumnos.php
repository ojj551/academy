<?php $this->load->view('cabecera-admin'); ?>
<?php echo link_tag('assets/css/style-alumn.css'); ?>
<div class="swiper-container swipmenu" style="overflow-x: hidden !important;overflow: initial !important;">
	<div class="swiper-wrapper">
		<div class="swiper-slide swipe-slide-menu menu">
			<div class="menu-left-alumn">
				<?php echo $topics; ?>

			</div>
		</div>
		<div class="swiper-slide content">
			<div class="menu-button" style="z-index: 1;">
				<div class="bar"></div>
				<div class="bar"></div>
				<div class="bar"></div>
			</div>
				<div class="col-xs-12 col-sm-12 col-lg-12 padding0" style="height: 800px;">
					<div class="col-xs-12 col-sm-12 col-lg-12 cursobanner-alumn padding0 text-center" style="background: url(<?php echo site_url('assets/img/escuelas/unaq/'.$Banner) ?>)no-repeat;" data-0="background-position: 100% 100%;" data-466="background-position: 100% 55%;"> 
						<div class="text-right" style="position: relative;z-index: 111;">
							<?php echo $salir; ?>
						</div>
						<div class="capagrp" style="background: rgba(0, 0, 0, 0.65);" data-0="padding-top: 25%;" data-466="padding-top: 5%;">

							<div class="col-xs-3 col-sm-12 col-lg-3"></div>
							<div class="col-xs-6 col-sm-12 col-lg-6">
								<div class="col-xs-12 col-sm-12 col-lg-12">
									<h1 class="blanco tpg-relawey title-curso-alumn"><?php echo $CourseName; ?></h1>
								</div>
								<i class="blanco">By <?php echo $ProfesorName; ?></i>
								
							</div>


							<!--<div class="bounce"><a class="downbounce"><i class="fa fa-angle-double-down"></i></a></div>-->
							<div class="empezar-course-alumn">
								<a class="animateslow" href="<?php echo base_url(); ?>cursosnc/leccion_vista/?l=<?php echo $first_lesson; ?>">Empezar</a>
							</div>
							
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-lg-12 padding0 contenido-course-alm">
						<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 30px;">
							<div class="col-xs-2 col-sm-12 col-lg-2"></div>
							<div class="col-xs-8 col-sm-12 col-lg-8">
								<strong class="tpg-relawey descrip-mat1">Requerimientos: </strong> <p class="tpg-relawey descrip-mat2"><?php echo $CourseRequirements; ?></p><br>
								<strong class="tpg-relawey descrip-mat1">Objetivos: </strong> <p class="tpg-relawey descrip-mat2"><?php echo $CourseGoals; ?></p>
							</div>
						</div>
						
					</div>
				</div>
		</div>
	</div>
</div>
<?php $this->load->view('footer-admin'); ?>