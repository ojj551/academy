<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('nc/header-top'); ?>      
<?php $this->load->view('nc/header-left'); ?>
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">
            <!--<a href="<?php echo base_url().'cursosnc/crear' ?>" class="btn btn-primary animate">Agregar curso</a>-->
            <?php if($creator==1){ ?>
            <button class="btn btn-primary animate" data-toggle="modal" data-target="#maddcurso">Agregar curso</button>
            <?php } ?>
          </div>
      </div>                    
    </div> 
	<div class="col-xs-12 col-sm-12 col-lg-12">
			<?php echo $cursos; ?>
	</div>

</div>

<div id="maddcurso" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Crear nuevo curso</h4>
      </div>
      <div class="modal-body">  
            <h1 class="editor_edit" nm="title" id="title">Escribe título de curso aquí...</h1><br><br>
            <label class="editor_edit descp-cours" nm="description" id="description">Escribe una descripción para tu curso...</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addcurso">Aceptar</button>
        <a data-dismiss="modal">Ó Cancelar</a>
      </div>
    </div>

  </div>
</div>

<?php $this->load->view('footer-admin'); ?>
<script src="<?php echo site_url('assets/js/editor-master/js/medium-editor.js'); ?>"></script>
<script>

    $('.editor_edit').click(function(){
      var contenido = $(this).html();
      if(contenido=='Escribe título de curso aquí...'){
        $(this).html('');
      }
      if(contenido=='Escribe una descripción para tu curso...'){
        $(this).html('');
      }
      /*
      if(contenido==''){
        var nm = $(this).attr('nm');
        if(nm=='title'){
          //$(this).html('Escribe título de curso aquí...');
        }
        if(nm=='description'){
          //$(this).html('Escribe una descripción para tu curso...');
        }
      }*/
    });

    var editor = new MediumEditor('.editor_edit', {
      toolbar: {
          allowMultiParagraphSelection: true,
          //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
          buttons: ['bold', 'italic', 'anchor'],
          diffLeft: 0,
          diffTop: -10,
          firstButtonClass: 'medium-editor-button-first',
          lastButtonClass: 'medium-editor-button-last',
          relativeContainer: null,
          standardizeSelectionStart: false,
          static: false,
          /* options which only apply when static is true */
          align: 'center',
          sticky: false,
          updateOnEmptySelection: false,
          setFocusOutlineEnabled : false
      }
    });
  //Agregar curso
  $('#addcurso').click(function(){
    var title = $('#title').html();
    var description = $('#description').html();
    if(title!=""){
      $.ajax({
        type:'POST',
        url : base_url+'cursosnc/addcorse',
        data : 'title='+title+'&description='+description,
        beforeSend:function(){
          $('.capa-white').fadeIn(600);
          $('#maddcurso').modal('hide');
        },
        success : function(vl) {
          if(vl=='success'){
            location.reload();
          }else{
            $('.capa-white').fadeOut(600);
            $.dialog({
              title: 'Problemas al crear curso',
              content: 'No pudimos agregar tu curso, intentalo nuevamente y de ser continuo el error comunicate con el equipo de TI',
            });
          }
        }
      });
    }else{
      $.dialog({
        title: 'Campos incompletos',
        content: 'Es necesario llenar los campos para poder crear un curso',
      });
    }
    //var title = $('.')
  });
  //editor.setFocusOutlineEnabled(false);
</script>