<?php echo link_tag('assets/css/nc/style.css'); ?>
<?php echo link_tag('assets/js/editor-master/css/medium-editor.css'); ?>
<?php echo link_tag('assets/js/editor-master/css/themes/default.css'); ?>
<!-- start: Header -->
  <nav class="navbar navbar-default header navbar-fixed-top">
    <div class="col-md-12 nav-wrapper">
      <div class="navbar-header" style="width:100%;">
        <div class="opener-left-menu is-open">
          <span class="top"></span>
          <span class="middle"></span>
          <span class="bottom"></span>
        </div>

        
        <ul class="nav navbar-nav navbar-right user-nav text-right" style="padding-right: 20px;">
          <li class="dropdown avatar-dropdown">
            <span class="negro tpg-relawey dropdown-toggle nameuser-m" data-toggle="dropdown"><?php echo $this->session->userdata('FullName'); ?></span>
            <img class="dropdown-toggle avatar" src="<?php echo site_url('assets/img/user-demo.png'); ?>" data-toggle="dropdown">
            <ul class="dropdown-menu user-dropdown">
              <li >
                 
                  <div class="text-center">
                    <a class="tpg-relawey" href="<?php echo base_url().'login/logout' ?>"><span class="fa fa-power-off "> Cerrar sesion</span></a>
                  </div>
              </li>
            </ul>
          </li>
        </ul>
        
      </div>
    </div>
  </nav>
<!-- end: Header -->
<div class="container-fluid mimin-wrapper">