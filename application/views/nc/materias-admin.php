<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('nc/header-top'); ?>      
<?php $this->load->view('nc/header-left'); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
	.lesson,.edit-lesson{
		height: 55px;
	}
	.lessoncreada{
		background: aliceblue;
	    margin-top: 10px;
	    margin-bottom: 10px;
	}
	.ui-state-default{cursor: move;}
	.des-li{
		
		height: 100%;
		width: 100%;
		border: 1px solid #ffffff;
    	background: #f5f5f5;
	}
	ul{padding-left: 0px !important;}

</style>
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">
          	<label class="negro">Curso: </label>
            <h1 class="negro editor_edit title"><?php echo $CourseName; ?></h1>
          </div>
          <div class="col-md-12 col-sm-12">
          	<a class="tpg-relawey" data-toggle="modal" data-target="#edit-banner" style="position: absolute;right: 10px;bottom: 10px;"><i class="fa fa-pencil" aria-hidden="true"></i> Editar Banner</a>
          </div>
      </div>                    
    </div> 
	<div class="col-md-12 col-sm-12 col-lg-12">
		<div class="col-md-12 col-sm-12 col-lg-12">
			<label class="negro">Descripción: </label>
			<h3 class="editor_edit description"><?php echo $CourseDescription; ?></h3>
		</div>
		<div class="col-md-12 col-sm-12 col-lg-12 div-lecciones"><br>
			<h4 class="tpg-relawey negro">Temas:</h4>
			<div class="col-md-12 col-sm-12 col-lg-12 padding0">
				<ul id="sortable">
					<?php echo $temas; ?>
				</ul>
				<ul>
					<li class="col-sm-12 col-md-12 col-lg-12 des-li">
					<input class="form-control lesson enter-lesson" type="text" placeholder="Escribe nombre del tema" autofocus>
					</li>
				</ul>
			</div>
		</div>
	</div>

</div>

<!--
<div class="col-xs-12 col-sm-12 col-lg-12 padding0">
	<div class="col-xs-12 col-sm-12 col-lg-12 materias-banner padding0 text-center" style="background: url(<?php echo site_url('assets/img/escuelas/unaq/'.$Banner) ?>)no-repeat;">
		<div class="capa div-cap-banner" style="background: rgba(117, 117, 117, 0.6);" >
			<div class="col-xs-12 col-sm-12 col-lg-12 padding0 div-name" v="name">
				<h1 class="tpg-logo title-section editables" >
					<?php echo $CourseName; ?>
					<br><span style="font-size: 31px;"><?php echo "(".$Inc." - ".$Fin.")"; ?></span>
				</h1>
			</div>

			<a class="tpg-relawey edits edit-banner blanco" data-toggle="modal" data-target="#edit-banner" style="position: absolute;right: 10px;bottom: 10px;"><i class="fa fa-pencil" aria-hidden="true"></i> Editar Banner</a>

		</div>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12 sub-info-user text-right" style="padding-top: 20px;padding-bottom: 0px;">
	
    <?php echo $btn_preview.$btn_exam; ?>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12" style="margin-top: 30px;">
	<div class="col-xs-2 col-sm-12 col-lg-2"></div>
	<div class="col-xs-8 col-sm-12 col-lg-8">
		<strong class="tpg-relawey descrip-mat1">Requerimientos: </strong> <p class="tpg-relawey descrip-mat2"><?php echo $CourseRequirements; ?></p><br>
		<strong class="tpg-relawey descrip-mat1">Objetivos: </strong> <p class="tpg-relawey descrip-mat2"><?php echo $CourseGoals; ?></p>
		<div class="col-xs-12 col-sm-12 col-lg-12 text-right" style="margin-top: 30px;">
			<a class="tpg-relawey descript-mat" data-toggle="modal" data-target="#edit-description-mat"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
		</div>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12">
	<div class="col-xs-2 col-sm-12 col-lg-2"></div>
	<div class="col-xs-8 col-sm-12 col-lg-8">
		<strong class="tpg-relawey descrip-mat1">Temario: </strong> <br>
		<?php echo $list; ?>

		<div class="col-xs-12 col-sm-12 col-lg-12 text-right" style="margin-top: 30px;">
		
			<a class="tpg-relawey" data-toggle="modal" data-target="#add-topic-unidad"><i class="fa fa-pencil" aria-hidden="true"></i> Agregar tu primer unidad</a>
		</div>
	</div>
</div>
-->
<?php $this->load->view('modals'); ?>  
<?php $this->load->view('footer-admin'); ?>
<div class="capa-white"></div>
<script src="<?php echo site_url('assets/js/editor-master/js/medium-editor.js'); ?>"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	//////drag and drop
	$(function() {
	    $("#sortable").sortable({
	        stop: function( ) {
	        	var items = [];
	        	$.each($('ul#sortable > li'), function( index, value ) {
	        		var data = {};
	        		data.v = $(value).attr('v');
        			data.o = $(value).attr('o');
        			items.push(data);
				  //console.log($(value).attr('v'));
				});
				$.ajax({
			        type:'POST',
			        url : base_url+'cursosnc/reordentopics',
			        data : {arry_item:items},
			        //processData: false,  // tell jQuery not to process the data
			       	//contentType: false,  // tell jQuery not to set contentType
			        beforeSend:function(){
			          $('.capa-white').fadeIn(600);
			        },
			        success : function(vl) {
			        	$('.capa-white').fadeOut(600);
					   
			        }
			    });
	        	//console.log(items);
	        	//alert('cambio');
        	}
	    });
	    $("#sortable").disableSelection();
	});

	/////
	var editor = new MediumEditor('.editor_edit', {
      toolbar: {
          allowMultiParagraphSelection: true,
          //buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
          buttons: ['bold', 'italic', 'anchor'],
          diffLeft: 0,
          diffTop: -10,
          firstButtonClass: 'medium-editor-button-first',
          lastButtonClass: 'medium-editor-button-last',
          relativeContainer: null,
          standardizeSelectionStart: false,
          static: false,
          /* options which only apply when static is true */
          align: 'center',
          sticky: false,
          updateOnEmptySelection: false,
          setFocusOutlineEnabled : false
      }
    });
    //editar curso
    $('.title').focusout(function() {
    	var cid = "<?php echo $MateriaIDEncryp ?>";
        var c = $(this);
	    var name = c.html();
	    if(name!=""){
	    	$.ajax({
		        type:'POST',
		        url : base_url+'cursosnc/editcourse',
		        data : 'c='+encodeURIComponent(cid)+'&title='+name,
		        beforeSend:function(){
		          $('.capa-white').fadeIn(600);
		        },
		        success : function(vl) {
		        	setTimeout(function(){ $('.capa-white').fadeOut(600); }, 300);
		        }
		    });
	    }
    });
    
    $('.description').focusout(function() {
    	var cid = "<?php echo $MateriaIDEncryp ?>";
        var c = $(this);

	    var name = c.html();
	    if(name!=""){

	    	$.ajax({
		        type:'POST',
		        url : base_url+'cursosnc/editcourse',
		        data : 'c='+encodeURIComponent(cid)+'&description='+name,
		        beforeSend:function(){
		          $('.capa-white').fadeIn(600);
		        },
		        success : function(vl) {
		        	setTimeout(function(){ $('.capa-white').fadeOut(600); }, 300);
		        }
		    });
	    }
       
    });

    //Editar leccion
    $('.edit-lesson').change(function() {
        var c = $(this);
        $.when(c.focusout()).then(function() {
            var v = c.attr('v');
		    var name = c.val();
		    if(v!="" && name!=""){
		    	$.ajax({
			        type:'POST',
			        url : base_url+'cursosnc/editname',
			        data : 't='+encodeURIComponent(v)+'&temaname='+name,
			        beforeSend:function(){
			          $('.capa-white').fadeIn(600);
			        },
			        success : function(vl) {
			        	if(vl=='success'){
			        		setTimeout(function(){ $('.capa-white').fadeOut(600); }, 300);
			        		
			        	}
			        }
			    });
		    }
        });
    });
    //Agregar leccion
    $(".div-lecciones").on("keypress", ".lesson", function(e){
    	var input = $(this);
    	if(e.which == 13) {
    		var vlt = $(this).val();
    		if(vlt!=""){
    			var c = "<?php echo $MateriaIDEncryp; ?>";
    			$.ajax({
			        type:'POST',
			        url : base_url+'cursosnc/addlesson',
			        data : 'c='+encodeURIComponent(c)+'&temaname='+vlt,
			        beforeSend:function(){
			          $('.capa-white').fadeIn(600);
			        },
			        success : function(vl) {
			        	$('.capa-white').fadeOut(600);
			        	$('#sortable').html(vl);
			        	$('.enter-lesson').val('');
			        	//editar
			        	$('.edit-lesson').change(function() {
					        var c = $(this);
					        $.when(c.focusout()).then(function() {
					            var v = c.attr('v');
							    var name = c.val();
							    if(v!="" && name!=""){
							    	$.ajax({
								        type:'POST',
								        url : base_url+'cursosnc/editname',
								        data : 't='+encodeURIComponent(v)+'&temaname='+name,
								        beforeSend:function(){
								          $('.capa-white').fadeIn(600);
								        },
								        success : function(vl) {
								        	if(vl=='success'){
								        		setTimeout(function(){ $('.capa-white').fadeOut(600); }, 300);
								        		
								        	}
								        }
								    });
							    }
					        });
					    });
					    //eliminar
					    $('.dlt').click(function(){
						    var t = $(this).attr('t');
						    var id = $(this).attr('id');
						    var vl = $(this).attr('vl');

						    $.confirm({
						        title: 'Eliminar registro',
						        content: '¿Seguro que quieres eliminar el registro?',
						        confirm: function(){
						          if(t!="" && id!="" && vl!=""){
						          $.ajax({
						                url : base_url+'generalesfunc/delete',
						                type : 'POST',
						                data : 't='+encodeURIComponent(t)+'&id='+encodeURIComponent(id)+'&vl='+encodeURIComponent(vl),
						                beforeSend:function(){
						              $('.capaload').fadeIn(600);
						            },
						            success : function(datav) {
						              setTimeout(function(){
						                $('.capaload').fadeOut(600);
						                if(datav=="success"){
						                  location.reload();
						                }else{
						                  $.dialog({
						                    title: '¡Ups! tuvimos problemas para actualizar tus datos',
						                    content: 'Intenta nuevamente y de ser continuo el error comunicate con <strong>contacto@xpertcad.com</strong>',
						                  });
						                }
						              },1000);
						            }
						          });
						        }
						        }
						    });

						    
						});
			        }
			    });

    		}
    	}
    });
    //
</script>