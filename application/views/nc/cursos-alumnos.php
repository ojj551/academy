<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('nc/header-top'); ?>      
<?php $this->load->view('nc/header-left'); ?>
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">

              <h3 class="animated fadeInLeft">Alumnos curso <?php echo $CourseName; ?></h3>

          </div>
      </div>                    
    </div> 
	<div class="col-xs-12 col-sm-12 col-lg-12">
    <button class="btn btn-primary animate" data-toggle="modal" data-target="#maddusers">Agregar usuarios</button><br><br>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Fecha de registro</th>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>Depto</th>
          <th>Avance</th>
          <th>-</th>
        </tr>
      </thead>
      <tbody>
        <?php echo $alumnos; ?>
      </tbody>
			
	</div>

</div>
<div id="maddusers" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar usuarios</h4>
      </div>
      <div class="modal-body">  
          <label>Agregar por departamento</label>
          <select class="form-control" id="depto">
             <option value="">...</option>
             <?php echo $depto; ?>
          </select>
          <!--
          <label>Agregar por departamento</label>
          <select class="form-control selectpicker" id="allusers" data-show-subtext="true" data-live-search="true">
             <option value="">...</option>
             <?php echo $allusers; ?>
          </select>
           <label>Agregados</label><br>
           -->
          <form id="elemnts">
          
          </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addtocurso">Agregar al curso</button>
        <a data-dismiss="modal">Ó Cancelar</a>
      </div>
    </div>

  </div>
</div>
<div class="capa-white"></div>
<?php $this->load->view('footer-admin'); ?> 
<script>
  var c = 1;
  var crs = "<?php echo $MateriaIDEncryp ?>";
  $('#addtocurso').click(function(){
    var users = $('input[name=usr]:checked');
    if(users.length>0){
      var files=[];
      users.each(function(k,r){
        var data = {};
        data.crs = crs;
        data.v = $(this).val();
        files.push(data);
      });
      //console.log(files);
      $.ajax({
        type:'POST',
        url : base_url+'cursosnc/adduserstoc',
        data : {files: files},
        dataType: "json",
        beforeSend:function(){
          $('.capa-white').fadeIn(600);
        },
        error : function(vl) {
          $('.capa-white').fadeOut(600);
          location.reload();
        }
      });
    }
  });
  $("#depto").change(function(){
    var v = $(this).val();
    var tx = $(this).text();
    $.ajax({
      type:'POST',
      url : base_url+'cursosnc/showusers',
      data : 'v='+v,
      beforeSend:function(){
        $('.capa-white').fadeIn(600);
      },
      success : function(vl) {
        $('.capa-white').fadeOut(600);
        $('#elemnts').html(vl);
      }
    });
    /*
    $('#elemnts').append('<div class="items-users" v="'+v+'"><label>'+v+'</label> <a><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>');
    //$('#elemnts').append('<input type="text" name="user'+c+'" id="user'+c+'" value="'+v+'"  />');
    c++;
    //console.log(v);
    */
  });
</script>