<?php $this->load->view('cabecera-admin'); ?>
<?php $this->load->view('nc/header-top'); ?>      
<?php $this->load->view('nc/header-left'); ?>
<div id="content">
    <div class="panel">
      <div class="panel-body">
          <div class="col-md-12 col-sm-12">
            <h3>Mis cursos</h3>
          </div>
      </div>                    
    </div> 
	<div class="col-xs-12 col-sm-12 col-lg-12">
			<?php echo $miscursos; ?>
	</div>

</div>

<div id="maddcurso" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Crear nuevo curso</h4>
      </div>
      <div class="modal-body">  
            <h1 class="editor_edit" nm="title" id="title">Escribe título de curso aquí...</h1><br><br>
            <label class="editor_edit descp-cours" nm="description" id="description">Escribe una descripción para tu curso...</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addcurso">Aceptar</button>
        <a data-dismiss="modal">Ó Cancelar</a>
      </div>
    </div>

  </div>
</div>
<div class="capa-white"></div>
<?php $this->load->view('footer-admin'); ?> 