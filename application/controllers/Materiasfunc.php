<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materiasfunc extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->helper('html');
        $this->load->model('alumnos_model');
        $this->load->model('materias_model');
        $this->load->model('cursos_model');
        if (!$this->session->userdata('UserID')) {
            redirect(base_url());
        }

        $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
        $this->CustomerID = '';
        if ($usr) {
            $this->CustomerID = $usr[0]->CustomerID;
        }
    }

    function updatename()
    {
        $nombre = $this->input->post('nombre');
        $nombre = trim($nombre);
        $idmat = $this->input->post('idmat');
        $idmat = trim($idmat);
        $MateriaID = urlencode($idmat);
        $MateriaID = str_replace("+", "%2B", $MateriaID);
        $MateriaID = urldecode($MateriaID);
        $MateriaID = $this->encrypt->decode($MateriaID);
        $incmupdt = $this->input->post('incmupdt');
        $finmupdt = $this->input->post('finmupdt');

        $SQL = "UPDATE bn_topics SET CourseName='$nombre',CourseStar='$incmupdt',CourseEnd='$finmupdt' WHERE CourseID='$MateriaID'";
        $upt = $this->general_model->QUERYSUPT($SQL);
        echo "success";
    }

    function updatebanner()
    {

        $file = $_FILES["img"];
        $color = $this->input->post('color');
        $id = $this->input->post('id');
        $MateriaID = urlencode($id);
        $MateriaID = str_replace("+", "%2B", $MateriaID);
        $MateriaID = urldecode($MateriaID);
        $MateriaID = $this->encrypt->decode($MateriaID);

        if ($file != "") {
            //$target_dir = "assets/img/escuelas/unaq/";
            $inf = $this->general_model->AboutMateria($MateriaID);
            if ($inf) {

                $CutomerIDEncrypt = md5($this->CustomerID);
                $ProfesorIDEncrypt = md5($inf[0]->CourseProfesorID);
                $MateriaIDEncrypt = md5($MateriaID);

                $target_dir = "../files/Customers/" . $CutomerIDEncrypt . '/prof/' . $ProfesorIDEncrypt . '/' . $MateriaIDEncrypt . '/';
                if (!file_exists($target_dir)) {
                    mkdir($target_dir, 0755, true);
                }
                $namefoto = 'banner_' . md5($MateriaID);
                $target_file = $target_dir . $namefoto . '.' . basename($file["type"]);

                $DocDate = date('Y-m-d H:i:s');
                $data = array(
                    'CourseImage' => $namefoto . '.' . basename($file["type"]),
                    'DocUpdateDate' => $DocDate,
                    'DocUpdateUserID' => $this->session->userdata('UserID'),
                    'CourseColor' => $color
                );


                $upd = $this->materias_model->UpdateBanner($MateriaID, $data);

                if ($upd) {
                    echo $target_file;
                    if (move_uploaded_file($file["tmp_name"], $target_file)) {
                        echo "success";
                    }
                }
            }
        } else {
            $DocDate = date('Y-m-d H:i:s');
            $data = array(
                'DocUpdateDate' => $DocDate,
                'DocUpdateUserID' => $this->session->userdata('UserID'),
                'CourseColor' => $color
            );


            $upd = $this->materias_model->UpdateBanner($MateriaID, $data);
            if ($upd) {
                echo "success";
            }
        }
    }

    function updatedescription()
    {
        $reqmupd = $this->input->post('reqmupd');
        $reqmupd = trim($reqmupd);
        $objmupd = $this->input->post('objmupd');
        $objmupd = trim($objmupd);
        $idmat = $this->input->post('idmat');
        $idmat = trim($idmat);
        $MateriaID = urlencode($idmat);
        $MateriaID = str_replace("+", "%2B", $MateriaID);
        $MateriaID = urldecode($MateriaID);
        $MateriaID = $this->encrypt->decode($MateriaID);
        $incmupdt = $this->input->post('incmupdt');
        $finmupdt = $this->input->post('finmupdt');

        $DocDate = date('Y-m-d H:i:s');
        $data = array(
            'CourseRequirements' => strip_tags($reqmupd),
            'CourseGoals' => strip_tags($objmupd),
            'DocUpdateDate' => $DocDate,
            'DocUpdateUserID' => $this->session->userdata('UserID')
        );


        $upd = $this->materias_model->UpdateBanner($MateriaID, $data);
        if ($upd) {
            echo "success";
        }
    }

    function createsetion()
    {
        $MatSt = $this->input->post('MatSt');
        $CourseID = trim($MatSt);
        $CourseID = urlencode($CourseID);
        $CourseID = str_replace("+", "%2B", $CourseID);
        $CourseID = urldecode($CourseID);
        $CourseID = $this->encrypt->decode($CourseID);

        $NameSection = $this->input->post('name');
        $NameSection = trim($NameSection);

        $cont = $this->materias_model->SectionCourse($CourseID);
        //echo $CourseID."<br>";
        //echo count($cont)+1;
        //$OrderSection = $this->input->post('number');
        //$OrderSection = trim($OrderSection);

        $DocDate = date('Y-m-d H:i:s');
        $data = array(
            'DocDate' => $DocDate,
            'DocUserID' => $this->session->userdata('UserID'),
            'NameSection' => $NameSection,
            'OrderSection' => count($cont) + 1,
            'CourseID' => $CourseID);

        $upd = $this->materias_model->AddSection($CourseID, $OrderSection, $data);
        if ($upd) {
            $SetionID = $this->encrypt->encode($upd);
            echo $SetionID;
        } else {
            echo "error";
        }

    }

    function addtopics()
    {
        $data = json_decode(stripslashes($_POST['data']));
        $i = 1;
        $c = 0;
        $ic = 0;
        if (count($data) > 0) {
            foreach ($data as $row) {
                $texto = $row->texto;
                $NameTopic = trim($texto);

                $section = $row->section;
                $SectionID = trim($section);
                $SectionID = urlencode($SectionID);
                $SectionID = str_replace("+", "%2B", $SectionID);
                $SectionID = urldecode($SectionID);
                $SectionID = $this->encrypt->decode($SectionID);

                $idmt = $row->idmt;
                $CourseID = trim($idmt);
                $CourseID = urlencode($CourseID);
                $CourseID = str_replace("+", "%2B", $CourseID);
                $CourseID = urldecode($CourseID);
                $CourseID = $this->encrypt->decode($CourseID);
                $OrderUp = $this->materias_model->ADDtopics1($SectionID);
                $DocDate = date('Y-m-d H:i:s');
                $data = array(
                    'DocDate' => $DocDate,
                    'DocUserID' => $this->session->userdata('UserID'),
                    'SectionID' => $SectionID,
                    'NameTopic' => $NameTopic,
                    'CourseID' => $CourseID,
                    'OrderTopic' => $OrderUp [0]->Ultimo
                );
                //
                $insert = $this->materias_model->AddTopics($SectionID, $i, $data);
                if ($insert) {
                    $c++;
                } else {
                    $ic++;
                }
                $i++;
                //echo $NameTopic."<br>".$SectionID."<br>";
                //print_r($row);
            }
        }
        if (count($data) != $ic) {
            echo "success";
        } else {
            echo "se crearon correctos" . $c . "<br> Se crearon incorrectos: " . $ic;
        }
    }

    function addsbtopics()
    {

        $data = json_decode(stripslashes($_POST['data']));
        $i = 1;
        $c = 0;
        $ic = 0;
        foreach ($data as $row) {
            $texto = $row->texto;
            $NameTopic = trim($texto);

            $Topic = $row->Topic;
            $TopicID = trim($Topic);
            $TopicID = urlencode($TopicID);
            $TopicID = str_replace("+", "%2B", $TopicID);
            $TopicID = urldecode($TopicID);
            $TopicID = $this->encrypt->decode($TopicID);

            $tc = $this->materias_model->AboutTopic($TopicID);
            if ($tc) {
                $SectionID = $tc[0]->SectionID;
                $Parent = $tc[0]->TopicID;
                $CourseID = $tc[0]->CourseID;
                $OderUpSB = $this->materias_model->ADDSBTopics1($SectionID, $Parent);
                $DocDate = date('Y-m-d H:i:s');
                $data = array(
                    'DocDate' => $DocDate,
                    'DocUserID' => $this->session->userdata('UserID'),
                    'SectionID' => $SectionID,
                    'Parent' => $Parent,
                    'NameTopic' => $NameTopic,
                    'CourseID' => $CourseID,
                    'OrderTopic' => $OderUpSB[0]->Ultimo
                );
            }
            //
            $insert = $this->materias_model->AddTopics($SectionID, $i, $data);
            if ($insert) {
                $c++;
            } else {
                $ic++;
            }
            $i++;
        }
        if (count($data) != $ic) {
            echo "success";
        } else {
            echo "se crearon correctos" . $c . "<br> Se crearon incorrectos: " . $ic;
        }
    }

    function addmateriastoalumn()
    {
        $CourseID = trim($this->input->post('mat'));
        $CourseID = urlencode($CourseID);
        $CourseID = str_replace("+", "%2B", $CourseID);
        $CourseID = urldecode($CourseID);
        $CourseID = $this->encrypt->decode($CourseID);

        $UserID = trim($this->input->post('idencr'));
        $UserID = urlencode($UserID);
        $UserID = str_replace("+", "%2B", $UserID);
        $UserID = urldecode($UserID);
        $UserID = $this->encrypt->decode($UserID);

        //Info de curso
        $SQ1 = "SELECT * FROM bn_courses WHERE CourseID='$CourseID' AND Cancelled=0";
        $rs1 = $this->general_model->QUERYS($SQ1);
        if ($rs1) {
            foreach ($rs1 as $row) {
                //Suscripcion
                $DocDatesbsb = date('Y-m-d H:i:s');
                $datasb = array(
                    'DocDate' => $DocDatesbsb,
                    'UserID' => $UserID,
                    'CourseID' => $row->CourseID,
                );
                //Supervisor del curso
                $datasp = array(
                    'DocDate' => $DocDatesbsb,
                    'DocUserID' => $this->session->userdata('UserID'),
                    'ResponsableID' => $row->CourseProfesorID,
                    'UserID' => $UserID
                );

                $inser = $this->alumnos_model->AddSuscription($row->CourseID, $UserID, $datasb);
                if ($inser) {
                    $if = $this->alumnos_model->InsertAsignation($row->CourseProfesorID, $UserID, $datasp);
                    echo "success";
                } else {
                    echo "Ya tiene asignada esta materia";
                }
            }
        }
    }

    /**********EXÁMENES**************/
    function savequize()
    {
        $QuizName = trim($this->input->post('name'));

        $CourseID = $this->input->post('c');
        $CourseID = urlencode($CourseID);
        $CourseID = str_replace("+", "%2B", $CourseID);
        $CourseID = urldecode($CourseID);
        $CourseID = $this->encrypt->decode($CourseID);

        $TopicID = trim($this->input->post('l'));
        $TopicID = urlencode($TopicID);
        $TopicID = str_replace("+", "%2B", $TopicID);
        $TopicID = urldecode($TopicID);
        $TopicID = $this->encrypt->decode($TopicID);

        $ItemID = trim($this->input->post('itm'));
        $ItemID = urlencode($ItemID);
        $ItemID = str_replace("+", "%2B", $ItemID);
        $ItemID = urldecode($ItemID);
        $ItemID = $this->encrypt->decode($ItemID);

        $DocDate = date('Y-m-d H:i:s');

        //echo "Examen Name: ".$QuizName."<br>CourseID: ".$CourseID."<br>TopicID: ".$TopicID."<br>ItemID: ".$ItemID;
        //Quiz lección
        if ($TopicID != "" && $ItemID != "") {

            $data = array(
                'DocDate' => $DocDate,
                'DocUserID' => $this->session->userdata('UserID'),
                'QuizName' => $QuizName,
                'TopicID' => $TopicID,
                'ItemID' => $ItemID
            );
            $insrt = $this->materias_model->AddQuizLesson($QuizName, $TopicID, $ItemID, $this->session->userdata('UserID'), $data);
            if ($insrt) {
                echo $this->encrypt->encode($insrt);
            } else {
                echo "exist";
            }
        } else {
            //Quiz curso
            if ($CourseID != "") {
                $data = array(
                    'DocDate' => $DocDate,
                    'DocUserID' => $this->session->userdata('UserID'),
                    'QuizName' => $QuizName,
                    'CourseID' => $CourseID,
                );
                $insrt = $this->materias_model->AddQuiz($QuizName, $CourseID, $this->session->userdata('UserID'), $data);
                if ($insrt) {
                    echo $this->encrypt->encode($insrt);
                } else {
                    echo "exist";
                }
            }
        }

    }

    function getquestion()
    {
        $QuizID = trim($this->input->post('quiz'));
        $QuizID = urlencode($QuizID);
        $QuizID = str_replace("+", "%2B", $QuizID);
        $QuizID = urldecode($QuizID);
        $QuizID = $this->encrypt->decode($QuizID);
        $q = $this->encrypt->encode($this->input->post('q'));

        $Number = trim($this->input->post('i'));
        $result = '';

        if (isset($_POST['question'])) {
            $QuestionID = trim($this->input->post('question'));
            $QuestionID = urlencode($QuestionID);
            $QuestionID = str_replace("+", "%2B", $QuestionID);
            $QuestionID = urldecode($QuestionID);
            $QuestionID = $this->encrypt->decode($QuestionID);

            $qs = $this->materias_model->AboutQuestion($QuestionID);
            if ($qs) {
                $opq = $this->materias_model->OptionQuestion($QuestionID);
                $result .= '
				<div class="col-xs-10 col-sm-12 col-lg-10">
					<div class="col-xs-12 col-sm-12 col-lg-12 padding0">
						<label>Escribe pregunta</label>
					</div>
			     	<div class="form-group form-animate-text">
			          <div class="form-text editor_edit" id="title-q" name="title-q" placeholder="Escribe pregunta...">' . $qs[0]->QuestionName . '</div>
			          <span class="bar"></span>
			    	</div>
		        </div>
		        <div class="col-xs-2 col-sm-2 col-lg-2" style="padding-top: 12px;">
		        	<button type="button" class="btn btn-primary save-qstnm" i="' . $qs[0]->Orden . '" q="' . $this->encrypt->encode($QuestionID) . '" v="1"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
		        </div>
				';

                if ($opq) {
                    $result .= '<div class="col-xs-12 col-sm-12 col-lg-12 opciones-q">';
                    foreach ($opq as $row) {
                        if ($qs[0]->QuestionAnswerID == $row->OptionID) {
                            $selected = 'checked="checked"';
                        } else {
                            $selected = '';
                        }
                        $result .= '
							<div class="col-xs-12 col-sm-12 col-lg-12 opcion-op">
								<div class="col-xs-1 col-sm-2 col-lg-1">
									<div class="form-animate-radio">
										<label class="radio">
											<input  type="radio" name="radios" value="' . $this->encrypt->encode($row->OptionID) . '" q="' . $this->encrypt->encode($QuestionID) . '" ' . $selected . '>
											<span class="outer">
												<span class="inner"></span>
											</span>
										</label>
									</div>
								</div>
								<div class="col-xs-10 col-sm-2 col-lg-10">
									<div class="col-xs-11 col-sm-12 col-lg-11">
										<label class="editor_edit resp-opcion" i="' . $row->Orden . '">' . $row->OptionName . '</label>
									</div>
									<div class="col-xs-1 col-sm-12 col-lg-1 text-right">
										<a class="dlt-option animate" v="' . $this->encrypt->encode($row->OptionID) . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									</div>
								</div>
							</div>
							
						';
                    }
                    $result .= '

					</div>
					<div class="col-xs-12 col-sm-12 col-lg-12 text-center padding0"><button type="button" class="btn btn-primary add-opcion"><i class="fa fa-plus" aria-hidden="true"></i></button></div></div>
					';
                } else {
                    $result .= '
						<div class="col-xs-12 col-sm-12 col-lg-12 text-left">
				        	<div class="form-group form-animate-checkbox">
		                          <input type="checkbox" class="checkbox" id="opcion-ml">
		                          <label> Opción multiple</label>
		                    </div>
				        </div>
				        <div class="col-xs-12 col-sm-12 col-lg-12 opciones-q">
				        	
				        </div>
					';
                }

            }
        } else {
            $result .= '
			<div class="col-xs-12 col-sm-12 col-lg-12 padding0">
				<label>Escribe pregunta</label>
			</div>
			<div class="col-xs-10 col-sm-10 col-lg-10">
		     	<div class="form-group form-animate-text">
		          <div class="form-text editor_edit" id="title-q" name="title-q" placeholder="Escribe pregunta..."></div>
		          <span class="bar"></span>
		    	</div>
	        </div>
	        <div class="col-xs-2 col-sm-2 col-lg-2" style="padding-top: 12px;">
		        	<button type="button" class="btn btn-primary save-qst" i="' . $Number . '" q="' . $q . '" v="0"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
		    </div>
	        <div class="col-xs-12 col-sm-12 col-lg-12 addoptions">

	        </div>
			';
        }
        echo $result;

    }

    //
    function savequestion()
    {

        $QuizID = trim($this->input->post('q'));

        $QuizID = urlencode($QuizID);
        $QuizID = str_replace("+", "%2B", $QuizID);
        $QuizID = urldecode($QuizID);
        $QuizID = $this->encrypt->decode($QuizID);/**/


        $QuestionName = trim($this->input->post('name'));
        $Orden = trim($this->input->post('i'));
        $new = trim($this->input->post('new'));
        $DocDate = date('Y-m-d H:i:s');


        if ($new == "0") {

        } else {
            //echo $new;
            $data = array(
                'DocDate' => $DocDate,
                'DocUserID' => $this->session->userdata('UserID'),
                'QuestionName' => $QuestionName,
                'QuizID' => $QuizID,
                'Orden' => $Orden
            );
            $insrt = $this->materias_model->AddQuestion($QuestionName, $QuizID, $this->session->userdata('UserID'), $data);

            if ($insrt) {
                echo $this->encrypt->encode($insrt);
            } else {
                echo "exist";
            }
        }
    }

    function saveoptions()
    {
        $files = $this->input->post('files');
        $QuestionID = trim($files[0]['qst']);
        $QuestionID = urlencode($QuestionID);
        $QuestionID = str_replace("+", "%2B", $QuestionID);
        $QuestionID = urldecode($QuestionID);
        $QuestionID = $this->encrypt->decode($QuestionID);

        $QuestionName = $files[0]['qsnm'];

        $where = array('QuestionID' => $QuestionID);
        $dataupdate = array('QuestionName' => $QuestionName);

        $upt = $this->materias_model->UpdateQuestion($where, $dataupdate);

        foreach ($files as $row) {
            $OptionName = $row['opc'];
            $Orden = $row['ord'];

            $DocDate = date('Y-m-d H:i:s');
            $data = array(
                'DocDate' => $DocDate,
                'DocUserID' => $this->session->userdata('UserID'),
                'OptionName' => $OptionName,
                'QuestionID' => $QuestionID,
                'Orden' => $Orden
            );

            $in = $this->materias_model->AddOptions($Orden, $QuestionID, $this->session->userdata('UserID'), $data);
            if ($in == 'success') {

            } else {
                $dataupdt = array(
                    'OptionName' => $OptionName,
                    'DocUpdateDate' => $DocDate,
                    'DocUpdateUserID' => $this->session->userdata('UserID'),
                );
                $wh = array('OptionID' => $in);

                $upd = $this->materias_model->UpdateOption($dataupdt, $wh);
            }

        }
        echo "success";
    }

    //
    function saveanswer()
    {
        $QuestionID = trim($this->input->post('question'));
        $QuestionID = urlencode($QuestionID);
        $QuestionID = str_replace("+", "%2B", $QuestionID);
        $QuestionID = urldecode($QuestionID);
        $QuestionID = $this->encrypt->decode($QuestionID);

        $QuestionAnswerID = trim($this->input->post('anws'));
        $QuestionAnswerID = urlencode($QuestionAnswerID);
        $QuestionAnswerID = str_replace("+", "%2B", $QuestionAnswerID);
        $QuestionAnswerID = urldecode($QuestionAnswerID);
        $QuestionAnswerID = $this->encrypt->decode($QuestionAnswerID);

        $where = array('QuestionID' => $QuestionID);
        $dataupdate = array('QuestionAnswerID' => $QuestionAnswerID);

        $upt = $this->materias_model->UpdateQuestion($where, $dataupdate);
        echo "success";
        //echo $QuestionID."<br>".$QuestionAnswerID;
    }

    function deleteoption()
    {
        $OptionID = trim($this->input->post('q'));
        $OptionID = urlencode($OptionID);
        $OptionID = str_replace("+", "%2B", $OptionID);
        $OptionID = urldecode($OptionID);
        $OptionID = $this->encrypt->decode($OptionID);

        $SQL = "DELETE FROM bn_questions_options WHERE OptionID='$OptionID'";
        $upt = $this->general_model->QUERYSUPT($SQL);
        echo "success";
    }

    /*new version*/
    function addlesson()
    {
        $NameTopic = trim($this->input->post('temaname'));

        $c = trim($this->input->post('c'));
        $c = urlencode($c);
        $c = str_replace("+", "%2B", $c);
        $c = urldecode($c);
        $c = $this->encrypt->decode($c);

        //
        $u = trim($this->input->post('u'));
        $u = urlencode($u);
        $u = str_replace("+", "%2B", $u);
        $u = urldecode($u);
        $u = $this->encrypt->decode($u);

        $Parent = trim($this->input->post('p'));
        $Parent = urlencode($Parent);
        $Parent = str_replace("+", "%2B", $Parent);
        $Parent = urldecode($Parent);
        $Parent = $this->encrypt->decode($Parent);

        echo "Parent: " . $Parent;


        //Orden del topic
        $or = $this->cursos_model->CountTopicsChild($c, $Parent, $this->session->userdata('UserID'));
        $OrderTopic = 0;
        if ($or) {
            foreach ($or as $ctd) {
                $OrderTopic++;
            }
        }
        $OrderTopic = $OrderTopic + 1;


        $DocDate = date('Y-m-d H:i:s');
        $data = array(
            'DocDate' => $DocDate,
            'DocUserID' => $this->session->userdata('UserID'),
            'SectionID' => $u,
            'Parent' => $Parent,
            'NameTopic' => trim($NameTopic),
            'CourseID' => $c,
            'OrderTopic' => $OrderTopic
        );

        $ins = $this->cursos_model->InsertLesson($data);
        if ($ins) {
            echo "success";
        }
    }

    function editname()
    {
        $NameTopic = trim($this->input->post('temaname'));

        $TopicID = trim($this->input->post('t'));
        $TopicID = urlencode($TopicID);
        $TopicID = str_replace("+", "%2B", $TopicID);
        $TopicID = urldecode($TopicID);
        $TopicID = $this->encrypt->decode($TopicID);

        if ($TopicID != "" && $NameTopic != "") {
            $data = array(
                'NameTopic' => $NameTopic,
                'DocUpdateDate' => date('Y-m-d H:i:s'),
                'DocUpdateUserID' => $this->session->userdata('UserID')
            );
            $where = array('TopicID' => $TopicID);

            $updtpc = $this->cursos_model->UpdateTopic($data, $where);
            echo "success";
        }
    }

    function editcourse()
    {
        $CourseID = trim($this->input->post('c'));
        $CourseID = urlencode($CourseID);
        $CourseID = str_replace("+", "%2B", $CourseID);
        $CourseID = urldecode($CourseID);
        $CourseID = $this->encrypt->decode($CourseID);
        if (isset($_POST['title'])) {

            $CourseName = trim($this->input->post('title'));

            if ($CourseID != "" && $CourseName != "") {
                $data = array(
                    'CourseName' => $CourseName,
                    'DocUpdateDate' => date('Y-m-d H:i:s'),
                    'DocUpdateUserID' => $this->session->userdata('UserID')
                );
                $where = array('CourseID' => $CourseID);

                $updtpc = $this->cursos_model->UpdateCourse($data, $where);
                echo "success";
            }

        }
        if (isset($_POST['description'])) {

            $CourseDescription = trim($this->input->post('description'));

            if ($CourseID != "" && $CourseDescription != "") {
                $data = array(
                    'CourseDescription' => $CourseDescription,
                    'DocUpdateDate' => date('Y-m-d H:i:s'),
                    'DocUpdateUserID' => $this->session->userdata('UserID')
                );
                $where = array('CourseID' => $CourseID);

                $updtpc = $this->cursos_model->UpdateCourse($data, $where);
                echo "success";
            }

        }
    }

    function subtopics()
    {
        $TopicID = trim($this->input->post('v'));
        $TopicID = urlencode($TopicID);
        $TopicID = str_replace("+", "%2B", $TopicID);
        $TopicID = urldecode($TopicID);
        $TopicID = $this->encrypt->decode($TopicID);

        $cld = $this->cursos_model->ChildTopic($TopicID);
        $childs = '';
        if ($cld) {
            $paint = 0;
            $tabla = $this->encrypt->encode('topics');
            $id = $this->encrypt->encode('TopicID');
            $childs .= '<ul>';
            foreach ($cld as $g) {
                //print_r($g);

                $childs .= '
				<li class="col-sm-12 col-md-12 col-lg-12 des-li ui-state-default" v="' . $g->TopicID . '" o="' . $g->OrderTopic . '" padding0>
					<div class="col-sm-12 col-md-12 col-lg-12 padding0" style="margin-top:10px;margin-bottom:10px;">
						<div class="col-sm-12 col-md-12 col-lg-12 padding0">
							<input class="form-control edit-lesson " type="text" placeholder="Escribe nombre del tema" value="' . $g->NameTopic . '" v="' . $this->encrypt->encode($g->TopicID) . '" />
				
							<div class="btn-group right-option-v1" style="top: 1px;font-size: 26px;">
		                        <i class="icon-options-vertical icons box-v7-menu" data-toggle="dropdown"></i>
		                        <ul class="dropdown-menu" role="menu" style="left: initial; right: 0px;">
		                          <li><a href="' . base_url() . 'temas/?t=' . $this->encrypt->encode($g->TopicID) . '"><i class="fa fa-pencil" aria-hidden="true"></i> Administrar contenido</a></li>
		                          <li><a class="dlt" t="' . $tabla . '" id="' . $id . '" vl="' . $this->encrypt->encode($g->TopicID) . '"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a></li>
		                        </ul>
		                    </div>
						</div>
					</div>
					</li>
				';
                $paint++;

            }
            $childs .= '</ul>';
        }

        //Info del topic padre
        $tpp = $this->cursos_model->AboutTopic($TopicID);
        if ($tpp) {
            $CIDEncrypt = $this->encrypt->encode($tpp[0]->CourseID);
            $SIDEncrypt = $this->encrypt->encode($tpp[0]->SectionID);
            $Lss = $this->encrypt->encode($TopicID);
            $childs .= '
			
				<ul>
					<li class="col-sm-12 col-md-12 col-lg-12 ui-state-default">
						<div class="col-sm-12 col-md-12 col-lg-12 padding0" style="margin-top:10px;margin-bottom:10px;">
							<input class="form-control lesson enter-lesson" type="text" placeholder="Escribe nueva sub lección" autofocus v="' . $CIDEncrypt . '" u="' . $SIDEncrypt . '" p="' . $Lss . '">
						</div>
					</li>
				</ul>
			
			';
        }

        echo $childs;
    }

    function reordentopics()
    {
        $arry_item = $_POST['arry_item'];
        $cont = 1;

        //print_r($arry_item);
        //echo "<br> total:".count($arry_item);

        foreach ($arry_item as $row) {
            $valor = $row['v'];
            $order_now = $row['o'];

            $data = array('OrderTopic' => $cont);
            $where = array('TopicID' => $valor);

            $update = $this->cursos_model->UpdateTopic($data, $where);
            $cont++;
        }
        if (count($arry_item) >= $cont) {
            echo "success";
        }

    }

    function addalumnos()
    {
        $arry_item = $_POST['arry_item'];
        foreach ($arry_item as $row) {
            $UserID = $row['v'];
            $UserID = urlencode($UserID);
            $UserID = str_replace("+", "%2B", $UserID);
            $UserID = urldecode($UserID);
            $UserID = $this->encrypt->decode($UserID);

            $CourseID = $row['c'];
            $CourseID = urlencode($CourseID);
            $CourseID = str_replace("+", "%2B", $CourseID);
            $CourseID = urldecode($CourseID);
            $CourseID = $this->encrypt->decode($CourseID);

            $DocDate = date('Y-m-d H:i:s');
            $data = array(
                'DocDate' => $DocDate,
                'DocUserID' => $this->session->userdata('UserID'),
                'UserID' => $UserID,
                'CourseID' => $CourseID
            );
            $isrt = $this->cursos_model->AddUsersToCurso($this->session->userdata('UserID'), $CourseID, $data);
            if ($isrt) {
                echo "success";
            }

        }
    }

    function update_design()
    {
        $CourseID = trim($this->input->post('idmat'));
        $CourseID = urlencode($CourseID);
        $CourseID = str_replace("+", "%2B", $CourseID);
        $CourseID = urldecode($CourseID);
        $CourseID = $this->encrypt->decode($CourseID);

        $fontsize = trim($this->input->post('fontsize'));
        $color1 = trim($this->input->post('color1'));
        $color2 = trim($this->input->post('color2'));
        $color3 = trim($this->input->post('color3'));

        if ($color1 == '#FFFFFF') {
            $color1 = '';
        }
        if ($color2 == '#FFFFFF') {
            $color2 = '';
        }
        if ($color3 == '#FFFFFF') {
            $color3 = '';
        }

        if ($fontsize != "" && $color1 != "" && $color2 != "" && $color3 != "") {
            $data = array(
                'CourseFont' => $fontsize,
                'ColorPalette1' => $color1,
                'ColorPalette2' => $color2,
                'ColorPalette3' => $color3
            );
            //
            $where = array('CourseID' => $CourseID);
            $updt = $this->materias_model->UpdateBanner($CourseID, $data);

        }

    }

}