<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profesor extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('alumnos_model');  
		$this->load->model('cursos_model');
		$this->load->model('carreras_model');
		if (!$this->session->userdata('UserID')){ 
	        redirect(base_url());
	    }
	    $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
	    $this->UserTypeID = '';
	    $this->CustomerID = '';
	    if($usr){
	    	//Tipo de usuario
        	$UserTypeID = $usr[0]->UserTypeID;
        	$typ = $this->general_model->TypeUsers("UserTypeID='$UserTypeID'");
        	if($typ){
        		if($typ[0]->Slug=='teacher'){
        			$this->UserTypeID = $usr[0]->UserTypeID;
					$this->CustomerID = $usr[0]->CustomerID;
        		}else{redirect(base_url().'denied');}
        	}else{redirect(base_url().'denied');}
	    }else{
	    	redirect(base_url().'denied');
	    }

	}
	public function index(){
		$cs = $this->cursos_model->GetCourses($this->session->userdata('UserID'));
		$cursos = array();
		$tpalumnos = array();
		if($cs){
			foreach ($cs as $row) {
				//Porcentaje de curso
				$av = $this->cursos_model->AvanceCourse($row->CourseID);
				if($av!=""){$av=$av;}else{$av=0;}
                $cursos[] = array(
                    'CourseID' => $this->encrypt->encode($row->CourseID),
                    'CourseName' => $row->CourseName,
                    'av' => $av
                );
			}
		}
		$dato['cursos'] = $cursos;

		//Usuarios top
		$al = $this->alumnos_model->TopAlumnos($this->session->userdata('UserID'));
		if($al){
			foreach ($al as $alm) {
				$gtus = $this->general_model->AboutUser($alm->UserID);
				if($gtus){
					$UserFirstName = $gtus[0]->UserFirstName;
					$UserLastName = $gtus[0]->UserLastName;
					$progress = $alm->Suma/$alm->Cantidad;
                    $tpalumnos[] = array(
                        'UserID' => $this->encrypt->encode($alm->UserID),
                        'UserFirstName' => $UserFirstName,
                        'UserLastName' => $UserLastName,
                        'progress' => $progress
                    );
				}
			}
		}
		$dato['tpalumnos'] = $tpalumnos;

		$dato['header_left'] = $this->load->view('profesor/header-left','',True);
        $dato['footer']      = $this->load->view('footer-admin', '', true);
		$this->twig->display('profesor/home',$dato);
	}

	function alumnos(){
		$MyID = $this->session->userdata('UserID');

		$words = '';
		if(isset($_GET['s'])){
			$words = $_GET['s'];
			$total_rows = $this->alumnos_model->GetAllUsersSearch($MyID,$words,1);
		}else{
			$total_rows = $this->alumnos_model->GetAllUsers($MyID,1);
		}
		$dato['words'] = $words;
		$config['reuse_query_string'] = TRUE;
		$config['base_url'] = base_url().'profesor/alumnos';
		$config['uri_segment'] = 3;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = 10;
		$config['num_links'] = 5;
		$config['first_link'] = "Primero";
		$config['last_link'] = "Ultimo";
		$config['next_link'] = "Siguiente";
		$config['prev_link'] = "Anterior";

		$config['cur_tag_open'] = "<b class='actual'>";
		$config['cur_tag_close'] = "</b>";

		$config['full_tag_open'] = "<div id='paginacion'>";
		$config['full_tag_close'] = "</div>";
		$this->pagination->initialize($config);
		$pagination = $this->pagination->create_links();

		if(isset($_GET['s'])){
			$words = $_GET['s'];
			$query = "SELECT *,date_format(T1.DocDate,'%d-%m-%Y') AS FRegister FROM bn_responsable_user AS T1, bn_users AS T2 WHERE T2.UserFirstName LIKE '%$words%' AND T1.ResponsableID='$MyID' AND T1.UserID=T2.UserID AND T1.Cancelled=0 AND T2.Cancelled=0 OR T2.UserLastName LIKE '%$words%' AND T1.ResponsableID='$MyID' AND T1.UserID=T2.UserID AND T1.Cancelled=0 AND T2.Cancelled=0 OR T2.UserEmail LIKE '%$words%' AND T1.ResponsableID='$MyID' AND T1.UserID=T2.UserID AND T1.Cancelled=0 AND T2.Cancelled=0"; 
		}else{
			$query = "SELECT *,date_format(T1.DocDate,'%d-%m-%Y') AS FRegister FROM bn_responsable_user AS T1, bn_users AS T2 WHERE T1.ResponsableID='$MyID' AND T1.UserID=T2.UserID AND T1.Cancelled=0 AND T2.Cancelled=0";
		}
		$gt = $this->alumnos_model->GetUsers($query,$config['per_page']);
		$usuarios = '';
		if($gt){
			foreach ($gt as $ky => $row) {
				$UserIDEncrypt = $this->encrypt->encode($row->UserID);

				//
				$ver = '';
				$interacciones_rg = '';
				if($row->ConfirmationID!="" && $row->ConfirmationDetaills!=""){
					$ver = '
					<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver</a>
					';
				}else{
					
					if($row->ConfirmationID!=""){
						$interacciones_rg = 'Agrego sus datos, como nombre contraseña, etc. Pero no accedio a la liga que se envio a su correo para poder activar el paso final.';
					}else{
						$interacciones_rg = 'No ha tenido ninguna interacción con el sistema.';
					}
					$ver = '
					<a class="details_inactive" data-toggle="modal" data-target="#myModal" int="'.$interacciones_rg.'">Ver</a>
					';
				}
				//Carrera
				$crr = $this->carreras_model->AboutCareer($row->CareerID);
				$carrera = '';
				if($crr){
					$carrera = $crr[0]->CareerName;
				}
				$cm = $this->general_model->AboutCustomerByID($this->CustomerID);
				if($cm){
					if($cm[0]->TypeNivelID==1){
						$type_nivel = 'Semestre';
					}else{
						$type_nivel = 'Cuatrimestre';
					}
				}
				if($row->UserSemester!=0){
					$dsc = '<b style="font-size:9px;">'.$row->UserSemester.' '.$type_nivel.'</b>';
				}else{
					$dsc = '';
				}

				$usuarios .= '
					<tr>
						<td>'.$row->FRegister.'</td>
						<td>'.$row->UserFirstName.'</td>
						<td>'.$row->UserLastName.'</td>
						<td>'.$row->UserEmail.'</td>
						<td>'.$carrera.'<br>'.$dsc.'</td>
						<td>'.$ver.'</td>
					</tr>
					';
			}
		}
		$dato['usuarios'] = $usuarios;
		$dato['pagination'] = $pagination;

		//Vista de alumnos dependiendo del tipo de usuario
		$dato['listtype'] = '';
		$sql = "UserTypeID!='4' AND Parent='$this->UserTypeID' AND Cancelled=0";
		$ty = $this->general_model->TypeUsers($sql);
		
		$listtype = '';
		if($ty){
			foreach ($ty as $row) {
				//print_r($row);
				$UserTypeIDEncryp = $this->encrypt->encode($row->UserTypeID);
				$listtype .= 
						'
						<option value="'.$UserTypeIDEncryp.'">'.$row->UserTypeName.'</option>
						';

				$sql2 = "UserTypeID!='4' AND Parent='$row->UserTypeID' AND Cancelled=0";
				$ty2 = $this->general_model->TypeUsers($sql2);
				if($ty2){
					foreach ($ty2 as $vl) {
						$UserTypeIDEncryp2 = $this->encrypt->encode($vl->UserTypeID);
						$listtype .= 
						'
						<option value="'.$UserTypeIDEncryp2.'">'.$vl->UserTypeName.'</option>
						';
						$sql3 = "UserTypeID!='4' AND Parent='$vl->UserTypeID' AND Cancelled=0";
						$ty3 = $this->general_model->TypeUsers($sql3);
						if($ty3){
							foreach ($ty3 as $file) {
								$UserTypeIDEncryp3 = $this->encrypt->encode($file->UserTypeID);
								$listtype .= 
								'
								<option value="'.$UserTypeIDEncryp3.'">'.$file->UserTypeName.'</option>
								';
							}
						}
					}
				}
			}
				
		}
		$dato['listtype'] = $listtype;

        $dato['header_left']    = $this->load->view('profesor/header-left','',True);
        $dato['footer']         = $this->load->view('footer-admin', '', true);
        $dato['admin_js']       = site_url('assets/js/admin.js');
        $dato['actions_js']     = site_url('assets/js/sup/actions.js');
		$this->twig->display('Alumnos/profesor-lista-alumnos',$dato);
	}

}
