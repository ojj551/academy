<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Activacion extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	    $this->load->helper('html');
		$this->load->library('pagination');
		$this->load->model('login_model');
	}
	public function index(){
		$ConfirmationIDE = $this->input->get('c');
		$ConfirmationID = urlencode($ConfirmationIDE);
		$ConfirmationID = str_replace("+", "%2B",$ConfirmationID);
		$ConfirmationID = urldecode($ConfirmationID);
		$ConfirmationID = $this->encrypt->decode($ConfirmationID);

		$VCID = $this->login_model->ValidateConfimationID($ConfirmationID);
		if($VCID){
			$UserID = $VCID[0]->UserID;
			$UserReference = $VCID[0]->UserReference;
			$ConfirmationDetaills = "Date: " . date("Y-m-d H:i:s") . " From: ".$_SERVER['REMOTE_ADDR'];
			$SQUP = "UPDATE bn_users SET ConfirmationDetaills='$ConfirmationDetaills',VisibleStatus=0 WHERE UserID='$UserID'";
			$squp = $this->general_model->QUERYSUPT($SQUP);
			//upd2
			//$SQUP2 = "UPDATE bn_user_reference SET UserID='$UserID' WHERE Reference='$UserReference'";
			//$squp2 = $this->general_model->QUERYSUPT($SQUP2);
			$dato['user'] = $this->encrypt->encode($UserID);

			//$usr = $this->login_model->AboutReference($UserID);
			//if($usr){

				$UserSemester = $usr[0]->UserSemester;
				$CareerID = $usr[0]->CareerID;
				$ProfesorID = $usr[0]->SupervisorID;
				if($CareerID!="" && $Level!="" && $ProfesorID !=""){
					$SQL ="SELECT * FROM bn_groups WHERE ProfesorID='$ProfesorID' AND CareerID='$CareerID' AND LevelCareer='$UserSemester'";
					$gp = $this->general_model->QUERYS($SQL);
					if($gp){
						foreach ($gp as $vl) {
							$DocDate = date('Y-m-d H:i:s');
							$data = array(
								'DocDate' => $DocDate,
								'UserID' => $UserID,
								'CourseID' => $vl->CourseID,
							);

							$insert = $this->login_model->AddSuscription($vl->CourseID,$UserID,$data);
						}
					}
				}
			//}
		}else{
			$dato['user'] = "";
		}

		$this->load->view('activacion',$dato);
		//echo $ConfirmationID;
	}

}

?>