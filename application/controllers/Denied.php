<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Denied extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('alumnos_model');
		$this->load->model('carreras_model');
		$this->load->model('materias_model');
		$this->load->model('institucion_model');
		if (!$this->session->userdata('UserID')){ 
	        redirect(base_url());
	    }
	    $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
	    $this->UserTypeID = '';
	    $this->CustomerID = '';
	    if($usr){
	    	//Tipo de usuario
        	$this->CustomerID = $usr[0]->CustomerID;
	    }else{
	    	redirect(base_url().'login');
	    }

	}
	public function index(){
		$this->load->view('denied');

	}

}
