<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materiasfnc extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
        $this->load->library('pagination');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model('alumnos_model');
        $this->load->model('materias_model');
        $this->load->model('cursos_model');
        if (!$this->session->userdata('UserID')) {
            redirect(base_url());
        }

        $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
        $this->CustomerID = '';
        if ($usr) {
            $this->CustomerID = $usr[0]->CustomerID;
        }
    }

    function updatename()
    {
        $nombre = $this->input->post('nombre');
        $nombre = trim($nombre);
        $idmat = $this->input->post('idmat');
        $idmat = trim($idmat);
        $MateriaID = urlencode($idmat);
        $MateriaID = str_replace("+", "%2B", $MateriaID);
        $MateriaID = urldecode($MateriaID);
        $MateriaID = $this->encrypt->decode($MateriaID);
        $incmupdt = $this->input->post('incmupdt');
        $finmupdt = $this->input->post('finmupdt');

        $SQL = "UPDATE bn_topics SET CourseName='$nombre',CourseStar='$incmupdt',CourseEnd='$finmupdt' WHERE CourseID='$MateriaID'";
        $upt = $this->general_model->QUERYSUPT($SQL);
        echo "success";
    }

    function updatebanner()
    {

        $file = $_FILES["img"];
        $color = $this->input->post('color');
        $id = $this->input->post('id');
        $MateriaID = urlencode($id);
        $MateriaID = str_replace("+", "%2B", $MateriaID);
        $MateriaID = urldecode($MateriaID);
        $MateriaID = $this->encrypt->decode($MateriaID);

        if ($file != "") {
            //$target_dir = "assets/img/escuelas/unaq/";
            $inf = $this->general_model->AboutMateria($MateriaID);
            if ($inf) {

                $CutomerIDEncrypt = md5($this->CustomerID);
                $ProfesorIDEncrypt = md5($inf[0]->CourseProfesorID);
                $MateriaIDEncrypt = md5($MateriaID);

                $target_dir = "../files/Customers/" . $CutomerIDEncrypt . '/prof/' . $ProfesorIDEncrypt . '/' . $MateriaIDEncrypt . '/';
                if (!file_exists($target_dir)) {
                    mkdir($target_dir, 0755, true);
                }
                $namefoto = 'banner_' . md5($MateriaID);
                $target_file = $target_dir . $namefoto . '.' . basename($file["type"]);

                $DocDate = date('Y-m-d H:i:s');
                $data = array(
                    'CourseImage' => $namefoto . '.' . basename($file["type"]),
                    'DocUpdateDate' => $DocDate,
                    'DocUpdateUserID' => $this->session->userdata('UserID'),
                    'CourseColor' => $color
                );


                $upd = $this->materias_model->UpdateBanner($MateriaID, $data);

                if ($upd) {
                    if (move_uploaded_file($file["tmp_name"], $target_file)) {
                        echo "success";
                    }
                }
            }
        } else {
            $DocDate = date('Y-m-d H:i:s');
            $data = array(
                'DocUpdateDate' => $DocDate,
                'DocUpdateUserID' => $this->session->userdata('UserID'),
                'CourseColor' => $color
            );


            $upd = $this->materias_model->UpdateBanner($MateriaID, $data);
            if ($upd) {
                echo "success";
            }
        }
    }

    function updatedescription()
    {
        $reqmupd = $this->input->post('reqmupd');
        $reqmupd = trim($reqmupd);
        $objmupd = $this->input->post('objmupd');
        $objmupd = trim($objmupd);
        $idmat = $this->input->post('idmat');
        $idmat = trim($idmat);
        $MateriaID = urlencode($idmat);
        $MateriaID = str_replace("+", "%2B", $MateriaID);
        $MateriaID = urldecode($MateriaID);
        $MateriaID = $this->encrypt->decode($MateriaID);
        $incmupdt = $this->input->post('incmupdt');
        $finmupdt = $this->input->post('finmupdt');

        $DocDate = date('Y-m-d H:i:s');
        $data = array(
            'CourseRequirements' => strip_tags($reqmupd),
            'CourseGoals' => strip_tags($objmupd),
            'DocUpdateDate' => $DocDate,
            'DocUpdateUserID' => $this->session->userdata('UserID')
        );


        $upd = $this->materias_model->UpdateBanner($MateriaID, $data);
        if ($upd) {
            echo "success";
        }
    }

    function createsetion()
    {
        $MatSt = $this->input->post('MatSt');
        $CourseID = trim($MatSt);
        $CourseID = urlencode($CourseID);
        $CourseID = str_replace("+", "%2B", $CourseID);
        $CourseID = urldecode($CourseID);
        $CourseID = $this->encrypt->decode($CourseID);

        $NameSection = $this->input->post('name');
        $NameSection = trim($NameSection);

        $cont = $this->materias_model->SectionCourse($CourseID);
        //echo $CourseID."<br>";
        //echo count($cont)+1;
        //$OrderSection = $this->input->post('number');
        //$OrderSection = trim($OrderSection);

        $DocDate = date('Y-m-d H:i:s');
        $data = array(
            'DocDate' => $DocDate,
            'DocUserID' => $this->session->userdata('UserID'),
            'NameSection' => $NameSection,
            'OrderSection' => count($cont) + 1,
            'CourseID' => $CourseID);

        $upd = $this->materias_model->AddSection($CourseID, $OrderSection, $data);
        if ($upd) {
            $SetionID = $this->encrypt->encode($upd);
            echo $SetionID;
        } else {
            echo "error";
        }

    }

    function addtopics()
    {
        $data = json_decode(stripslashes($_POST['data']));
        $i = 1;
        $c = 0;
        $ic = 0;
        if (count($data) > 0) {
            foreach ($data as $row) {
                $texto = $row->texto;
                $NameTopic = trim($texto);

                $section = $row->section;
                $SectionID = trim($section);
                $SectionID = urlencode($SectionID);
                $SectionID = str_replace("+", "%2B", $SectionID);
                $SectionID = urldecode($SectionID);
                $SectionID = $this->encrypt->decode($SectionID);

                $idmt = $row->idmt;
                $CourseID = trim($idmt);
                $CourseID = urlencode($CourseID);
                $CourseID = str_replace("+", "%2B", $CourseID);
                $CourseID = urldecode($CourseID);
                $CourseID = $this->encrypt->decode($CourseID);
                $OrderUp = $this->materias_model->ADDtopics1($SectionID);
                $DocDate = date('Y-m-d H:i:s');
                $data = array(
                    'DocDate' => $DocDate,
                    'DocUserID' => $this->session->userdata('UserID'),
                    'SectionID' => $SectionID,
                    'NameTopic' => $NameTopic,
                    'CourseID' => $CourseID,
                    'OrderTopic' => $OrderUp [0]->Ultimo
                );
                //
                $insert = $this->materias_model->AddTopics($SectionID, $i, $data);
                if ($insert) {
                    $c++;
                } else {
                    $ic++;
                }
                $i++;
                //echo $NameTopic."<br>".$SectionID."<br>";
                //print_r($row);
            }
        }
        if (count($data) != $ic) {
            echo "success";
        } else {
            echo "se crearon correctos" . $c . "<br> Se crearon incorrectos: " . $ic;
        }
    }

    function addsbtopics()
    {

        $data = json_decode(stripslashes($_POST['data']));
        $i = 1;
        $c = 0;
        $ic = 0;
        foreach ($data as $row) {
            $texto = $row->texto;
            $NameTopic = trim($texto);

            $Topic = $row->Topic;
            $TopicID = trim($Topic);
            $TopicID = urlencode($TopicID);
            $TopicID = str_replace("+", "%2B", $TopicID);
            $TopicID = urldecode($TopicID);
            $TopicID = $this->encrypt->decode($TopicID);

            $tc = $this->materias_model->AboutTopic($TopicID);
            if ($tc) {
                $SectionID = $tc[0]->SectionID;
                $Parent = $tc[0]->TopicID;
                $CourseID = $tc[0]->CourseID;
                $OderUpSB = $this->materias_model->ADDSBTopics1($SectionID, $Parent);
                $DocDate = date('Y-m-d H:i:s');
                $data = array(
                    'DocDate' => $DocDate,
                    'DocUserID' => $this->session->userdata('UserID'),
                    'SectionID' => $SectionID,
                    'Parent' => $Parent,
                    'NameTopic' => $NameTopic,
                    'CourseID' => $CourseID,
                    'OrderTopic' => $OderUpSB[0]->Ultimo
                );
            }
            //
            $insert = $this->materias_model->AddTopics($SectionID, $i, $data);
            if ($insert) {
                $c++;
            } else {
                $ic++;
            }
            $i++;
        }
        if (count($data) != $ic) {
            echo "success";
        } else {
            echo "se crearon correctos" . $c . "<br> Se crearon incorrectos: " . $ic;
        }
    }

    function addmateriastoalumn()
    {
        $CourseID = trim($this->input->post('mat'));
        $CourseID = urlencode($CourseID);
        $CourseID = str_replace("+", "%2B", $CourseID);
        $CourseID = urldecode($CourseID);
        $CourseID = $this->encrypt->decode($CourseID);

        $UserID = trim($this->input->post('idencr'));
        $UserID = urlencode($UserID);
        $UserID = str_replace("+", "%2B", $UserID);
        $UserID = urldecode($UserID);
        $UserID = $this->encrypt->decode($UserID);

        //Info de curso
        $SQ1 = "SELECT * FROM bn_courses WHERE CourseID='$CourseID' AND Cancelled=0";
        $rs1 = $this->general_model->QUERYS($SQ1);
        if ($rs1) {
            foreach ($rs1 as $row) {
                //Suscripcion
                $DocDatesbsb = date('Y-m-d H:i:s');
                $datasb = array(
                    'DocDate' => $DocDatesbsb,
                    'UserID' => $UserID,
                    'CourseID' => $row->CourseID,
                );
                //Supervisor del curso
                $datasp = array(
                    'DocDate' => $DocDatesbsb,
                    'DocUserID' => $this->session->userdata('UserID'),
                    'ResponsableID' => $row->CourseProfesorID,
                    'UserID' => $UserID
                );

                $inser = $this->alumnos_model->AddSuscription($row->CourseID, $UserID, $datasb);
                if ($inser) {
                    $if = $this->alumnos_model->InsertAsignation($row->CourseProfesorID, $UserID, $datasp);
                    echo "success";
                } else {
                    echo "Ya tiene asignada esta materia";
                }
            }
        }
    }

    /**********EXÁMENES**************/
    public function savequize()
    {
        $QuizName = trim($this->input->post('name'));

        $CourseID = $this->input->post('c');
        $CourseID = urlencode($CourseID);
        $CourseID = str_replace("+", "%2B", $CourseID);
        $CourseID = urldecode($CourseID);
        $CourseID = $this->encrypt->decode($CourseID);

        $TopicID = trim($this->input->post('l'));
        $TopicID = urlencode($TopicID);
        $TopicID = str_replace("+", "%2B", $TopicID);
        $TopicID = urldecode($TopicID);
        $TopicID = $this->encrypt->decode($TopicID);

        $ItemID = trim($this->input->post('itm'));
        $ItemID = urlencode($ItemID);
        $ItemID = str_replace("+", "%2B", $ItemID);
        $ItemID = urldecode($ItemID);
        $ItemID = $this->encrypt->decode($ItemID);

        $DocDate = date('Y-m-d H:i:s');

        //echo "Examen Name: ".$QuizName."<br>CourseID: ".$CourseID."<br>TopicID: ".$TopicID."<br>ItemID: ".$ItemID;
        //Quiz lección
        if ($TopicID != "" && $ItemID != "") {

            $data = array(
                'DocDate' => $DocDate,
                'DocUserID' => $this->session->userdata('UserID'),
                'QuizName' => $QuizName,
                'TopicID' => $TopicID,
                'ItemID' => $ItemID
            );
            $insrt = $this->materias_model->AddQuizLesson($QuizName, $TopicID, $ItemID, $this->session->userdata('UserID'), $data);
            if ($insrt) {
                echo $this->encrypt->encode($insrt);
            } else {
                echo "exist";
            }
        } else {
            //Quiz curso
            if ($CourseID != "") {
                $data = array(
                    'DocDate' => $DocDate,
                    'DocUserID' => $this->session->userdata('UserID'),
                    'QuizName' => $QuizName,
                    'CourseID' => $CourseID,
                );
                $insrt = $this->materias_model->AddQuiz($QuizName, $CourseID, $this->session->userdata('UserID'), $data);
                if ($insrt) {
                    echo $this->encrypt->encode($insrt);
                } else {
                    echo "exist";
                }
            }
        }

    }

    public function getquestion()
    {
        $QuizID = trim($this->input->post('quiz'));
        $QuizID = urlencode($QuizID);
        $QuizID = str_replace("+", "%2B", $QuizID);
        $QuizID = urldecode($QuizID);
        $QuizID = $this->encrypt->decode($QuizID);
        $q = $this->encrypt->encode($this->input->post('q'));
        $FormID = 'Qst_' . random_string('alnum', 5);
        $FormIDOps = 'Qst_' . random_string('alnum', 5);
        $QuizID = $this->encrypt->encode($QuizID);
        $Options = '';

        $Number = trim($this->input->post('i'));
        $datos = array(
            'QuestionName' => '',
            'QuestionType' => '',
            'FeedBack' => '',
            'FormID' => $FormID,
            'FormIDOps' => $FormIDOps,
            'Orden' => '',
            'QuestionID' => '',
            'QuizID' => $QuizID,
            'New' => 0,
            'q' => $q,
            'tabla' => $this->encrypt->encode('questions'),
            'id' => $this->encrypt->encode('QuestionID'),
            'Number' => $Number,
            'opq' => array()
        );
        if (isset($_POST['question'])) {
            if ($_POST['question'] == '') {
                $datos['New'] = 1;
            } else {
                $QuestionID = trim($this->input->post('question'));
                $QuestionID = urlencode($QuestionID);
                $QuestionID = str_replace("+", "%2B", $QuestionID);
                $QuestionID = urldecode($QuestionID);
                $QuestionID = $this->encrypt->decode($QuestionID);


                $qs = $this->materias_model->AboutQuestion($QuestionID);
                if ($qs) {
                    if ($qs[0]->QuestionType != 'rE') {
                        $datos['QuestionName'] = $qs[0]->QuestionName;
                        $datos['QuestionType'] = $qs[0]->QuestionType;
                        $datos['FeedBack'] = $qs[0]->Feedback;
                        $datos['Orden'] = $qs[0]->Orden;
                        $datos['QuestionID'] = $this->encrypt->encode($QuestionID);

                        $opq = $this->materias_model->OptionQuestion($QuestionID);
                        if ($opq) {
                            foreach ($opq as $row) {
                                if ($qs[0]->QuestionAnswerID == $row->OptionID) {
                                    $selected = 'checked="checked"';
                                } else {
                                    $selected = '';
                                }
                                $datos['opq'][] = array(
                                    'selected' => $selected,
                                    'OptionID' => $this->encrypt->encode($row->OptionID),
                                    'radioID' => $this->encrypt->encode($row->OptionID),
                                    'QuestionID' => $this->encrypt->encode($QuestionID),
                                    'Orden' => $row->Orden,
                                    'OptionName' => $row->OptionName
                                );
                            }
                        }
                    } else {
                        $datos['QuestionName'] = $qs[0]->QuestionName;
                        $datos['QuestionType'] = $qs[0]->QuestionType;
                        $datos['FeedBack'] = $qs[0]->Feedback;
                        $datos['Orden'] = $qs[0]->Orden;
                        $datos['QuestionID'] = $this->encrypt->encode($QuestionID);

                        $opq = $this->materias_model->OptionQuestion($QuestionID);
                        if ($opq) {
                            foreach ($opq as $row) {
                                $from = '/' . preg_quote('___', '/') . '/';
                                $to = '<a iq="' . $Number . '" qz="' . $QuizID . '" qst="' . $this->encrypt->encode($QuestionID) . '" op="' . $this->encrypt->encode($row->OptionID) . '" class="get-options">__</a>';
                                $datos['QuestionName'] = preg_replace($from, $to, $datos['QuestionName'], 1);
                            }
                        }
                    }
                }
            }
        } else {
            $datos['New'] = 1;
        }
        $datos['form_close'] = form_close();
        $attributes = array('id' => 'Qs_Form');
        $datos['form_open'] = form_open('materiasfnc/savequestion', $attributes);
        $Questions = $this->twig->render('Quiz/Question', $datos);


        if ($datos['QuestionType'] != 'rE' && $datos['QuestionType'] != '') {
            $datos['form_open'] = form_open('materiasfnc/saveoptions');
            $Options = $this->twig->render('Quiz/Options', $datos);
        }

        $this->twig->display('Quiz/Question_Edit', array('Questions' => $Questions, 'Options' => $Options));
    }

    public function savequestion()
    {

        $QuestionName = $this->input->post('name');
        $QuestionRE = $this->input->post('rellenarE');
        if ($QuestionName != "") {
            $QuestionName = trim($QuestionName);
        } else if ($QuestionRE != "") {
            $QuestionName = trim($QuestionRE);
        }

        $QuestionID = $this->input->post('QstnID');
        $QuestionID = $this->encrypt->decode($QuestionID);
        $QuizID = trim($this->input->post('Qz'));
        $QuizID = urlencode($QuizID);
        $QuizID = str_replace("+", "%2B", $QuizID);
        $QuizID = urldecode($QuizID);
        $QuizID = $this->encrypt->decode(trim($QuizID));

        $QuestionType = trim($this->input->post('selType'));
        if (!$QuestionType) {
            $QuestionType = trim($this->input->post('QType'));
        }
        $FeedBack = trim($this->input->post('fdbck'));
        $Orden = trim($this->input->post('i'));
        $DocDate = date('Y-m-d H:i:s');


        if ($QuestionID == '') {
            $data = array(
                'DocDate' => $DocDate,
                'DocUserID' => $this->session->userdata('UserID'),
                'QuestionName' => $QuestionName,
                'QuestionType' => $QuestionType,
                'Feedback' => $FeedBack,
                'QuizID' => $QuizID,
                'Orden' => $Orden
            );
            $insrt = $this->materias_model->AddQuestion($data);
            if ($QuestionType == 'opMul') {
                for ($i = 1; $i < 4; $i++) {
                    $data = array(
                        'DocDate' => $DocDate,
                        'DocUserID' => $this->session->userdata('UserID'),
                        'OptionName' => 'Escribe Opción ' . $i,
                        'QuestionID' => $insrt,
                        'Orden' => $i
                    );
                    $this->materias_model->AddOptions($i, $insrt, $this->session->userdata('UserID'), $data);
                }
            } elseif ($QuestionType == 'vF') {
                $vf[0] = 'Falso';
                $vf[1] = 'Verdadero';
                for ($i = 1; $i < 3; $i++) {
                    $data = array(
                        'DocDate' => $DocDate,
                        'DocUserID' => $this->session->userdata('UserID'),
                        'OptionName' => $vf[$i % 2],
                        'QuestionID' => $insrt,
                        'Orden' => $i
                    );
                    $this->materias_model->AddOptions($i, $insrt, $this->session->userdata('UserID'), $data);
                }
            } elseif ($QuestionType == 'rE') {
                $lastPos = 0;
                $positions = array();
                while (($lastPos = strpos($QuestionName, '___', $lastPos)) !== false) {
                    $lastPos = $lastPos + strlen('___');
                    $positions[] = array(
                        'Pos' => $lastPos,
                        'IDName' => 'rEspacios' . $lastPos
                    );
                }

                // Displays 3 and 10
                foreach ($positions as $value) {
                    $OptionExtra = array();
                    $OptionExtra[] = array(
                        'Name' => "Escribe la Opción 1",
                        'Orden' => "0"
                    );
                    $OptionExtra[] = array(
                        'Name' => "Escribe la Opción 2",
                        'Orden' => "1"
                    );
                    $OptionExtra[] = array(
                        'Name' => "Escribe la Opción 3",
                        'Orden' => "2"
                    );
                    $data = array(
                        'DocDate' => $DocDate,
                        'DocUserID' => $this->session->userdata('UserID'),
                        'OptionName' => $value['IDName'],
                        'QuestionID' => $insrt,
                        'Orden' => $value['Pos'],
                        'OptionExtra' => json_encode($OptionExtra)
                    );
                    $this->materias_model->AddOptions(0, $insrt, $this->session->userdata('UserID'), $data);
                }
            }

            if ($insrt) {
                echo $this->encrypt->encode($insrt);
            } else {
                echo "exist";
            }
        } else {
            $data = array(
                'DocUpdateDate' => $DocDate,
                'DocUserID' => $this->session->userdata('UserID'),
                'Feedback' => htmlentities($FeedBack),
                'Orden' => $Orden
            );
            if ($QuestionType != 'rE') {
                $data['QuestionName'] = htmlentities($QuestionName);
            }
            $insrt = $this->materias_model->UpdateQuestion($QuestionID, $data);
            echo $insrt;
        }


        redirect(base_url() . 'materias/examen/?ex=' . urlencode($this->encrypt->encode($QuizID)) . '&qstn=' . urlencode(base64_encode($Orden)));
    }

    public function saveoptions()
    {
        $QuizID = trim($this->input->post('Qz'));
        $QuizID = $this->encrypt->decode(trim($QuizID));
        $Options = $this->input->post('options');
        $QuestionID = trim($this->encrypt->decode($this->input->post('q')));
        $dataupdt = array();
        $Right_Answer = $this->input->post('radios');
        $Orden_Question = $this->input->post('i');

        foreach ($Options['opc'] as $key => $value) {
            $OptionName = $value;
            if ($OptionName == '') {
                continue;
            }
            $Orden = $Options['ord'][$key];

            $DocDate = date('Y-m-d H:i:s');
            $data = array(
                'DocDate' => $DocDate,
                'DocUserID' => $this->session->userdata('UserID'),
                'OptionName' => $OptionName,
                'QuestionID' => $QuestionID,
                'Orden' => $Orden
            );

            if (!isset($Options['opI'][$key])) {
                $in = $this->materias_model->AddOptions($Orden, $QuestionID, $this->session->userdata('UserID'), $data);
                if (isset($Right_Answer)) {
                    if ($Right_Answer == $Orden) {
                        $Right_Answer = $this->encrypt->encode($in);
                    }
                }
            } else {
                $OptionID = $this->encrypt->decode($Options['opI'][$key]);
                $dataupdt[] = array(
                    'OptionID' => $OptionID,
                    'OptionName' => $OptionName,
                    'DocUpdateDate' => $DocDate,
                    'DocUpdateUserID' => $this->session->userdata('UserID'),
                );
            }

        }
        if (count($dataupdt) > 0) {
            $this->materias_model->UpdateOption($dataupdt, 'OptionID');
        }
        if (isset($Right_Answer)) {
            $Right_Answer = trim($this->encrypt->decode($Right_Answer));
            $where = $QuestionID;
            $dataupdate = array('QuestionAnswerID' => $Right_Answer);

            $this->materias_model->UpdateQuestion($where, $dataupdate);
        }
        echo "success";
        redirect(base_url() . 'materias/examen/?ex=' . urlencode($this->encrypt->encode($QuizID)) . '&qstn=' . urlencode(base64_encode($Orden_Question)));
    }

    public function saveReOptions()
    {
        $QuizID = trim($this->input->post('Qz'));
        $QuizID = $this->encrypt->decode(trim($QuizID));
        $Options = $this->input->post('options');
        $QuestionID = trim($this->encrypt->decode($this->input->post('q')));
        $Right_Answer = $this->input->post('radios');
        $Right_Answer1 = array();
        $Orden_Question = $this->input->post('i');

        foreach ($Options['opc'] as $key => $value) {
            $OptionName = $value;
            if ($OptionName == '') {
                continue;
            }
            $OptionExtra[] = array(
                'Name' => $OptionName,
                'Orden' => $Options['ord'][$key]
            );
            if (isset($Options['opI'][$key])) {
                $OptionID = $this->encrypt->decode($Options['opI'][$key]);
            }
        }

        $DocDate = date('Y-m-d H:i:s');

        $dataupdt[] = array(
            'OptionID' => $OptionID,
            'OptionExtra' => json_encode($OptionExtra),
            'DocUpdateDate' => $DocDate,
            'DocUpdateUserID' => $this->session->userdata('UserID'),
        );

        if (isset($Right_Answer)) {
            $Answrs = $this->materias_model->AboutQuestion($QuestionID);
            $Answrs = json_decode($Answrs['0']->QuestionAnswerID, 0);
            $Right_Answer1[] = array('opID' => $OptionID, 'correctA' => trim($this->encrypt->decode($Right_Answer)));
            if ($Answrs != 0) {
                foreach ($Answrs as $ans) {
                    if ($ans->opID != $OptionID) {
                        $Right_Answer1[] = array('opID' => $ans->opID, 'correctA' => $ans->correctA);
                    }
                }
            }

            $where = $QuestionID;
            $dataupdate = array('QuestionAnswerID' => json_encode($Right_Answer1));

            $this->materias_model->UpdateQuestion($where, $dataupdate);
        }
        if (count($dataupdt) > 0) {
            $this->materias_model->UpdateOption($dataupdt, 'OptionID');
        }
        echo "success";
        redirect(base_url() . 'materias/examen/?ex=' . urlencode($this->encrypt->encode($QuizID)) . '&qstn=' . urlencode(base64_encode($Orden_Question)));
    }

    public function getReOptions()
    {
        $QuizID = $this->encrypt->decode(trim($this->input->post('quiz')));
        $QuestionID = $this->encrypt->decode(trim($this->input->post('qst')));
        $OptionID = $this->encrypt->decode(trim($this->input->post('op')));
        $opq = $this->materias_model->OptionQuestion($QuestionID);
        $Number = trim($this->input->post('i'));
        $correctA = null;
        if ($opq) {
            $qs = $this->materias_model->AboutQuestion($QuestionID);
            $Answrs = json_decode($qs['0']->QuestionAnswerID, 0);
            if ($Answrs != 0) {
                foreach ($Answrs as $ans) {
                    if ($ans->opID == $OptionID) {
                        $correctA = $ans->correctA;
                    }
                }
            }
            foreach ($opq as $row) {
                if ($OptionID == $row->OptionID) {
                    $datosRe = json_decode($row->OptionExtra, true);
                    foreach ($datosRe as $ReOption) {
                        if ($correctA == $ReOption['Orden']) {
                            $selected = 'checked="checked"';
                        } else {
                            $selected = '';
                        }

                        $datos['opq'][] = array(
                            'selected' => $selected,
                            'OptionOrd' => $this->encrypt->encode($ReOption['Orden']),
                            'OptionID' => $this->encrypt->encode($row->OptionID),
                            'radioID' => $this->encrypt->encode($ReOption['Orden']),
                            'QuestionID' => $this->encrypt->encode($QuestionID),
                            'Orden' => $ReOption['Orden'],
                            'OptionName' => $ReOption['Name']
                        );
                    }
                }
            }
            $datos['form_open'] = form_open('materiasfnc/saveReOptions');
            $datos['form_close'] = form_close();
            $datos['QuestionType'] = 'rE';
            $datos['QuizID'] = $this->encrypt->encode($QuizID);
            $datos['QuestionID'] = $this->encrypt->encode($QuestionID);
            $datos['Number'] = $Number;
            $this->twig->display('Quiz/Options', $datos);
        }
    }// obtener opciones del tipo de pregunta 'Rellenar espacios'


    public function saveanswer()
    {
        $QuestionID = trim($this->input->post('question'));
        $QuestionID = urlencode($QuestionID);
        $QuestionID = str_replace("+", "%2B", $QuestionID);
        $QuestionID = urldecode($QuestionID);
        $QuestionID = $this->encrypt->decode($QuestionID);

        $QuestionAnswerID = trim($this->input->post('anws'));
        $QuestionAnswerID = urlencode($QuestionAnswerID);
        $QuestionAnswerID = str_replace("+", "%2B", $QuestionAnswerID);
        $QuestionAnswerID = urldecode($QuestionAnswerID);
        $QuestionAnswerID = $this->encrypt->decode($QuestionAnswerID);

        $where = array('QuestionID' => $QuestionID);
        $dataupdate = array('QuestionAnswerID' => $QuestionAnswerID);

        $upt = $this->materias_model->UpdateQuestion($where, $dataupdate);
        echo "success";
        //echo $QuestionID."<br>".$QuestionAnswerID;
    }

    public function deleteoption()
    {
        $OptionID = trim($this->input->post('q'));
        $OptionID = $this->encrypt->decode($OptionID);
        $Orden = trim($this->input->post('or'));

        $OptionType = trim($this->input->post('sT'));
        if ($OptionType == 'rE') {
            $Optns = $this->materias_model->getOption($OptionID);
            $Optns = json_decode($Optns['0']->OptionExtra, 0);

            if ($Optns) {
                foreach ($Optns as $ops) {
                    if ($ops->Orden != $Orden) {
                        $OptionExtra[] = array(
                            'Name' => $ops->Name,
                            'Orden' => $ops->Orden
                        );
                    }
                }
            }

            $DocDate = date('Y-m-d H:i:s');
            $dataupdt[] = array(
                'OptionID' => $OptionID,
                'OptionExtra' => json_encode($OptionExtra),
                'DocUpdateDate' => $DocDate,
                'DocUpdateUserID' => $this->session->userdata('UserID'),
            );
            if (count($dataupdt) > 0) {
                $this->materias_model->UpdateOption($dataupdt, 'OptionID');
            }
        } else {
            $upt = $this->materias_model->DeleteOption($OptionID);
        }
        echo "success";
    }

    /*new version*/
    function addlesson()
    {
        $NameTopic = trim($this->input->post('temaname'));

        $c = trim($this->input->post('c'));
        $c = urlencode($c);
        $c = str_replace("+", "%2B", $c);
        $c = urldecode($c);
        $c = $this->encrypt->decode($c);

        //
        $u = trim($this->input->post('u'));
        $u = urlencode($u);
        $u = str_replace("+", "%2B", $u);
        $u = urldecode($u);
        $u = $this->encrypt->decode($u);

        $Parent = trim($this->input->post('p'));
        $Parent = urlencode($Parent);
        $Parent = str_replace("+", "%2B", $Parent);
        $Parent = urldecode($Parent);
        $Parent = $this->encrypt->decode($Parent);

        echo "Parent: " . $Parent;


        //Orden del topic
        $or = $this->cursos_model->CountTopicsChild($c, $Parent, $this->session->userdata('UserID'));
        $OrderTopic = 0;
        if ($or) {
            foreach ($or as $ctd) {
                $OrderTopic++;
            }
        }
        $OrderTopic = $OrderTopic + 1;


        $DocDate = date('Y-m-d H:i:s');
        $data = array(
            'DocDate' => $DocDate,
            'DocUserID' => $this->session->userdata('UserID'),
            'SectionID' => $u,
            'Parent' => $Parent,
            'NameTopic' => trim($NameTopic),
            'CourseID' => $c,
            'OrderTopic' => $OrderTopic
        );

        $ins = $this->cursos_model->InsertLesson($data);
        if ($ins) {
            echo "success";
        }
    }

    function editname()
    {
        $NameTopic = trim($this->input->post('temaname'));

        $TopicID = trim($this->input->post('t'));
        $TopicID = urlencode($TopicID);
        $TopicID = str_replace("+", "%2B", $TopicID);
        $TopicID = urldecode($TopicID);
        $TopicID = $this->encrypt->decode($TopicID);

        if ($TopicID != "" && $NameTopic != "") {
            $data = array(
                'NameTopic' => $NameTopic,
                'DocUpdateDate' => date('Y-m-d H:i:s'),
                'DocUpdateUserID' => $this->session->userdata('UserID')
            );
            $where = array('TopicID' => $TopicID);

            $updtpc = $this->cursos_model->UpdateTopic($data, $where);
            echo "success";
        }
    }

    function editcourse()
    {
        $CourseID = trim($this->input->post('c'));
        $CourseID = urlencode($CourseID);
        $CourseID = str_replace("+", "%2B", $CourseID);
        $CourseID = urldecode($CourseID);
        $CourseID = $this->encrypt->decode($CourseID);
        if (isset($_POST['title'])) {

            $CourseName = trim($this->input->post('title'));

            if ($CourseID != "" && $CourseName != "") {
                $data = array(
                    'CourseName' => $CourseName,
                    'DocUpdateDate' => date('Y-m-d H:i:s'),
                    'DocUpdateUserID' => $this->session->userdata('UserID')
                );
                $where = array('CourseID' => $CourseID);

                $updtpc = $this->cursos_model->UpdateCourse($data, $where);
                echo "success";
            }

        }
        if (isset($_POST['description'])) {

            $CourseDescription = trim($this->input->post('description'));

            if ($CourseID != "" && $CourseDescription != "") {
                $data = array(
                    'CourseDescription' => $CourseDescription,
                    'DocUpdateDate' => date('Y-m-d H:i:s'),
                    'DocUpdateUserID' => $this->session->userdata('UserID')
                );
                $where = array('CourseID' => $CourseID);

                $updtpc = $this->cursos_model->UpdateCourse($data, $where);
                echo "success";
            }

        }
    }

    function subtopics()
    {
        $TopicID = trim($this->input->post('v'));
        $TopicID = urlencode($TopicID);
        $TopicID = str_replace("+", "%2B", $TopicID);
        $TopicID = urldecode($TopicID);
        $TopicID = $this->encrypt->decode($TopicID);

        $cld = $this->cursos_model->ChildTopic($TopicID);
        $childs = '';
        if ($cld) {
            $paint = 0;
            $tabla = $this->encrypt->encode('topics');
            $id = $this->encrypt->encode('TopicID');
            $childs .= '<ul>';
            foreach ($cld as $g) {
                //print_r($g);

                $childs .= '
				<li class="col-sm-12 col-md-12 col-lg-12 des-li ui-state-default" v="' . $g->TopicID . '" o="' . $g->OrderTopic . '" padding0>
					<div class="col-sm-12 col-md-12 col-lg-12 padding0" style="margin-top:10px;margin-bottom:10px;">
						<div class="col-sm-12 col-md-12 col-lg-12 padding0">
							<input class="form-control edit-lesson " type="text" placeholder="Escribe nombre del tema" value="' . $g->NameTopic . '" v="' . $this->encrypt->encode($g->TopicID) . '" />
				
							<div class="btn-group right-option-v1" style="top: 1px;font-size: 26px;">
		                        <i class="icon-options-vertical icons box-v7-menu" data-toggle="dropdown"></i>
		                        <ul class="dropdown-menu" role="menu" style="left: initial; right: 0px;">
		                          <li><a href="' . base_url() . 'temas/?t=' . $this->encrypt->encode($g->TopicID) . '"><i class="fa fa-pencil" aria-hidden="true"></i> Administrar contenido</a></li>
		                          <li><a class="dlt" t="' . $tabla . '" id="' . $id . '" vl="' . $this->encrypt->encode($g->TopicID) . '"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a></li>
		                        </ul>
		                    </div>
						</div>
					</div>
					</li>
				';
                $paint++;

            }
            $childs .= '</ul>';
        }

        //Info del topic padre
        $tpp = $this->cursos_model->AboutTopic($TopicID);
        if ($tpp) {
            $CIDEncrypt = $this->encrypt->encode($tpp[0]->CourseID);
            $SIDEncrypt = $this->encrypt->encode($tpp[0]->SectionID);
            $Lss = $this->encrypt->encode($TopicID);
            $childs .= '
			
				<ul>
					<li class="col-sm-12 col-md-12 col-lg-12 ui-state-default">
						<div class="col-sm-12 col-md-12 col-lg-12 padding0" style="margin-top:10px;margin-bottom:10px;">
							<input class="form-control lesson enter-lesson" type="text" placeholder="Escribe nueva sub lección" autofocus v="' . $CIDEncrypt . '" u="' . $SIDEncrypt . '" p="' . $Lss . '">
						</div>
					</li>
				</ul>
			
			';
        }

        echo $childs;
    }

    function reordentopics()
    {
        $arry_item = $_POST['arry_item'];
        $cont = 1;

        //print_r($arry_item);
        //echo "<br> total:".count($arry_item);

        foreach ($arry_item as $row) {
            $valor = $row['v'];
            $order_now = $row['o'];

            $data = array('OrderTopic' => $cont);
            $where = array('TopicID' => $valor);

            $update = $this->cursos_model->UpdateTopic($data, $where);
            $cont++;
        }
        if (count($arry_item) >= $cont) {
            echo "success";
        }

    }

    function addalumnos()
    {
        $arry_item = $_POST['arry_item'];
        foreach ($arry_item as $row) {
            $UserID = $row['v'];
            $UserID = urlencode($UserID);
            $UserID = str_replace("+", "%2B", $UserID);
            $UserID = urldecode($UserID);
            $UserID = $this->encrypt->decode($UserID);

            $CourseID = $row['c'];
            $CourseID = urlencode($CourseID);
            $CourseID = str_replace("+", "%2B", $CourseID);
            $CourseID = urldecode($CourseID);
            $CourseID = $this->encrypt->decode($CourseID);

            $DocDate = date('Y-m-d H:i:s');
            $data = array(
                'DocDate' => $DocDate,
                'DocUserID' => $this->session->userdata('UserID'),
                'UserID' => $UserID,
                'CourseID' => $CourseID
            );
            $isrt = $this->cursos_model->AddUsersToCurso($this->session->userdata('UserID'), $CourseID, $data);
            if ($isrt) {
                echo "success";
            }

        }
    }

    function update_design()
    {
        $CourseID = trim($this->input->post('idmat'));
        $CourseID = urlencode($CourseID);
        $CourseID = str_replace("+", "%2B", $CourseID);
        $CourseID = urldecode($CourseID);
        $CourseID = $this->encrypt->decode($CourseID);

        $fontsize = trim($this->input->post('fontsize'));
        $color1 = trim($this->input->post('color1'));
        $color2 = trim($this->input->post('color2'));
        $color3 = trim($this->input->post('color3'));

        if ($color1 == '#FFFFFF') {
            $color1 = '';
        }
        if ($color2 == '#FFFFFF') {
            $color2 = '';
        }
        if ($color3 == '#FFFFFF') {
            $color3 = '';
        }

        if ($fontsize != "" && $color1 != "" && $color2 != "" && $color3 != "") {
            $data = array(
                'CourseFont' => $fontsize,
                'ColorPalette1' => $color1,
                'ColorPalette2' => $color2,
                'ColorPalette3' => $color3
            );
            //
            $where = array('CourseID' => $CourseID);
            $updt = $this->materias_model->UpdateBanner($CourseID, $data);

        }

    }
}