<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('materias_model'); 
		$this->load->model('temas_model');


		if ($this->session->userdata('UserID')){ 

			$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
	        if($usr){ 
	        	$UserTypeID = $usr[0]->UserTypeID;
	        	$typ = $this->general_model->TypeUsers("UserTypeID='$UserTypeID'");
	        	if($typ){
	        		if($typ[0]->UserTypeSlug=='admin'){
	        			
		        	}else{ redirect(base_url().'inicio'); }
	        	}else{ redirect(base_url().'inicio'); }

	        }else{
	        	redirect(base_url());
	        }
	        
	    }else{
	    	redirect(base_url());
	    }
	}
	public function index(){
		$this->load->view('admin/templates');
	}
}