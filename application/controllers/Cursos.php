<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('cursos_model'); 
		$this->load->model('materias_model');
		if ( ! $this->session->userdata('UserID')){ 
	        redirect(base_url());
	    }
	    $this->actual_link = actual_link();
	    $this->UserTypeID = '';
		$this->CustomerID = '';
		$this->Slug = '';
	    $usrnc = $this->general_model->UserNC($this->session->userdata('UserID'));
	    $this->UserNC = 0;
	    if($usrnc){
	    	$this->UserNC = 1; 
	    }else{
		    $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		    
			if($usr){
				$CustomerIDP = $usr[0]->CustomerID;
				$this->UserTypeID = $usr[0]->UserTypeID;
				$this->CustomerID = $usr[0]->CustomerID;
			}
			$sllgusertype = $this->general_model->GetSlugUserType($usr[0]->UserTypeID);
			$this->UserTypeSlug = '';
			if($sllgusertype){
				$UserTypeSlug = $sllgusertype[0]->UserTypeSlug;
				$this->UserTypeSlug = $sllgusertype[0]->UserTypeSlug;
				$this->Slug = $sllgusertype[0]->Slug;
			}
			$this->Lang = 'es';
			$cm = $this->general_model->AboutCustomerByID($CustomerIDP);
			$this->LeadType = '';
			if($cm){
				$this->LeadType = $cm[0]->LeadType;
			}
			//Datos para formulario
			$this->FormCancel = $this->general_model->GetText($this->Lang,'','form.cancel');
			//
			$this->FormGuardar = $this->general_model->GetText($this->Lang,'','form.guardar');
			//
			$this->ModalAdd = $this->general_model->GetText($this->Lang,'','modal.add');

			$this->Depto = $this->general_model->GetText($this->Lang,$this->LeadType,'depto');
			$this->FormName = $this->general_model->GetText($this->Lang,'','form.nombre');
		}
	}

	public function index(){
		$gtc = $this->cursos_model->GetCourses($this->session->userdata('UserID'));
		$cursos = '';
		if($gtc){
			foreach ($gtc as $row) {
				$avgc = $this->cursos_model->AvanceCourse($row->CourseID);
				if($avgc){
					$avgc = $avgc;
				}else{
					$avgc = 0;
				}
				$encode = $this->encrypt->encode($row->CourseID);
				if($row->CourseImage!=""){
					$CutomerIDEncrypt = md5($this->CustomerID);
					$ProfesorIDEncrypt = md5($row->CourseProfesorID);
					$MateriaIDEncrypt = md5($row->CourseID);
					$Imgbg = $this->actual_link.'/files/Customers/'.$CutomerIDEncrypt.'/prof/'.$ProfesorIDEncrypt.'/'.$MateriaIDEncrypt.'/'.$row->CourseImage;
					//$Imgbg = site_url('assets/img/escuelas/unaq/'.$row->CourseImage);
				}else{
					$Imgbg = site_url('assets/img/no_imagen.jpg');
				}
				$cursos .= '
					<div class="col-md-4 " style="position:relative;min-height:400px;">
			          <div class="panel course-on-list animateslow">
			               <div class="panel-heading-white panel-heading text-center">
			                  <h4>'.$row->CourseName.'</h4>
			                </div>
			                <div class="panel-body text-center" style="padding: 0px;">
			                	<div style="background: url('.$Imgbg.')no-repeat;height:150px;    background-size: cover;"></div>
			                  <div class="col-md-12">
			                  	<h2>'.$avgc.'%</h2><span> de avance</span>
			                  </div>
			                  <div class="col-md-12">
			                  	<a href="'.base_url().'materias/admin/?m='.$encode.'">Ver más</a>
			                  </div>

			                </div>
			          </div>
			        </div>
				';
			}
		}
		$dato['cursos'] = $cursos;
		$dato['FormCancel'] = $this->FormCancel;

		if($this->Slug=='teacher'){
			$this->load->view('profesor/header-left'); 
		}

		$this->load->view('cursos-lista',$dato);
	}
	public function alumnos(){
		$MateriaIDE = $this->input->get('m');

		$MateriaID = urlencode($MateriaIDE);
		$MateriaID = str_replace("+", "%2B",$MateriaID);
		$MateriaID = urldecode($MateriaID);
		$MateriaID = $this->encrypt->decode($MateriaID);

		$dato['MateriaIDEncryp'] = $this->encrypt->encode($MateriaID);

		$inf = $this->general_model->AboutMateria($MateriaID);
		$CourseName = ''; $alumnos = '';
		if($inf){
			$alumc = $this->cursos_model->AlumnosByCourse($MateriaID);
			if($alumc){
				foreach ($alumc as $row) {
					$UserIDEncrypt = $this->encrypt->encode($row->UserID);
					$alumnos .= '
					<tr>
						<td>'.$row->DocDate.'</td>
						<td>'.$row->UserFirstName.'</td>
						<td>'.$row->UserLastName.'</td>
						<td>'.$row->UserEmail.'</td>
						 <td>'.$ver = '<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver alumno</a>'.'</td>
					</tr>
					';
				}
			}
			$CourseName = $inf[0]->CourseName;
		}
		$dato['alumnos'] = $alumnos;
		$dato['CourseName'] = $CourseName;
		if($this->UserNC==1){
			//all users
			$als = $this->cursos_model->AllUsers();
			$allusers = '';
			if($als){
				foreach ($als as $row) {
					$allusers .= '
						<option value="'.$row->Depto.'">'.$row->FullName.'</option>
					';
				}
			}
			//all deptos
			$al = $this->cursos_model->Deptos();
			$depto = '';
			if($al){
				foreach ($al as $row) {
					$depto .= '
						<option value="'.$row->Depto.'">'.$row->departmentName.'</option>
					';
				}
			}

			$dato['depto'] = $depto;
			$dato['allusers'] = $allusers;
			$this->load->view('nc/cursos-alumnos',$dato);
		}else{
			$this->load->view('cursos-alumnos',$dato);
		}
	}
	/*
	public function prevista(){
		$MateriaIDE = $this->input->get('m');

		$MateriaID = urlencode($MateriaIDE);
		$MateriaID = str_replace("+", "%2B",$MateriaID);
		$MateriaID = urldecode($MateriaID);
		$MateriaID = $this->encrypt->decode($MateriaID);

		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$inf = $this->general_model->AboutMateria($MateriaID);
			if($inf){
				$CourseName = $inf[0]->CourseName;
				$MateriaIDEncryp = $this->encrypt->encode($MateriaID);
				$dato['CourseName'] = $CourseName;
				$dato['MateriaIDEncryp'] = $MateriaIDEncryp;
				$dato['UserTypeID'] = $usr[0]->UserTypeID;
				$dato['Inc'] = $inf[0]->CourseStar;
				$dato['Fin'] = $inf[0]->CourseEnd;
				$dato['Banner'] = $inf[0]->CourseImage;
				$dato['CourseRequirements'] = $inf[0]->CourseRequirements;
				$dato['CourseGoals'] = $inf[0]->CourseGoals;

				//Obtener las unidades y los topics
				$gsc = $this->materias_model->GetSection($this->session->userdata('UserID'),$MateriaID);
				$list = '';
				if($gsc){
					$tabla = $this->encrypt->encode('topics');
					$id = $this->encrypt->encode('TopicID');
					foreach ($gsc as $vl) {
						$tablasc = $this->encrypt->encode('section');
						$idsc = $this->encrypt->encode('SectionID');

						$TopicIDEncryptUnidad = $this->encrypt->encode($vl->SectionID);
						$head = '
							<div class="col-xs-12 col-sm-12 col-lg-12">
								<h4 class="tpg-relawey"><strong>Unidad '.$vl->OrderSection.',</strong> <a href="'.base_url().'unidad/?u='.$TopicIDEncryptUnidad.'">'.$vl->NameSection.'</a> <a class="addtm" v="'.$TopicIDEncryptUnidad.'" style="font-size: 9px;">Agregar tema</a> <a class="dlt" t="'.$tablasc.'" id="'.$idsc.'" vl="'.$TopicIDEncryptUnidad.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a></h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-lg-12">
								<div class="col-xs-12 col-sm-12 col-lg-12">
							';
						$gut = $this->materias_model->GetSectionTopics($vl->SectionID,false);
						$gutsb = $this->materias_model->GetSectionTopics($vl->SectionID,true);
						$topics = '';
						//Cascada 1
						if($gut){
							
							foreach ($gut as $row) {
								$fc = $this->Get_childrens($row->TopicID);
								foreach ($fc as $k => $vl) {
									$abt = $this->materias_model->AboutTopic($k);
									//print_r($abt);
								}
								//echo count($fc)."<br>";
								
		
								
								$childtopics = '<div class="col-xs-12 col-sm-12 col-lg-12">';
								//Cascada 2
								if($gutsb){
									foreach ($gutsb as $sb) {
										if($sb->Parent==$row->TopicID){
											$sbchildtopics = '<div class="col-xs-12 col-sm-12 col-lg-12">';
											//Cascada 3
											if($gutsb){
												foreach ($gutsb as $sbsb) {

													$sbsbchildtopics = '<div class="col-xs-12 col-sm-12 col-lg-12">';
													//cascada 4

													if($gutsb){
														foreach ($gutsb as $sbsbsb) {
															if($sbsbsb->Parent==$sbsb->TopicID){
																$TopicIDEncryptSbSbSb = $this->encrypt->encode($sbsbsb->TopicID);
																$sbchildtopics .= '
																<a href="'.base_url().'temas/?t='.$TopicIDEncryptSbSbSb.'" style="font-size: 20px;"><strong>'.$sbsbsb->OrderTopic.'</strong>.- '.$sbsbsb->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncryptSbSbSb.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncryptSbSbSb.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>';

															}
															
														}
													}
													$sbsbchildtopics .= '</div>';
													if($sbsb->Parent==$sb->TopicID){
														$TopicIDEncryptSbSb = $this->encrypt->encode($sbsb->TopicID);
														$sbchildtopics .= '
														<a href="'.base_url().'temas/?t='.$TopicIDEncryptSbSb.'" style="font-size: 20px;"><strong>'.$sbsb->OrderTopic.'</strong>.- '.$sbsb->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncryptSbSb.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncryptSbSb.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>'.$sbsbchildtopics;

													}
												}
											}

											$sbchildtopics .= '</div>';
											$TopicIDEncryptSb = $this->encrypt->encode($sb->TopicID);
											$childtopics .= '
											<a href="'.base_url().'temas/?t='.$TopicIDEncryptSb.'" style="font-size: 20px;"><strong>'.$sb->OrderTopic.'</strong>.- '.$sb->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncryptSb.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncryptSb.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>'.$sbchildtopics;

										}
									}
								}
								
								$childtopics .= '</div>';
								$TopicIDEncrypt = $this->encrypt->encode($row->TopicID);
								
								$topics .= '
									<a href="'.base_url().'temas/?t='.$TopicIDEncrypt.'" style="font-size: 20px;"><strong>'.$row->OrderTopic.'</strong>.- '.$row->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncrypt.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncrypt.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>
									'.$childtopics.'
								';
							}
						}
						$footer = '</div></div>';
						$list .= $head.$topics.$footer;
					}
				}
				
				$dato['list'] = $list;
				//if($usr[0]->UserTypeID=='1'){
					$this->load->view('materias-admin',$dato);
				//}else{
				//	redirect(base_url().'inicio');
				//}
			}
		}else{
			$this->session->sess_destroy();
	    	redirect(base_url());
		}

	}*/
}