<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('modules_model'); 
		if ( ! $this->session->userdata('UserEmail')){ 
	        redirect(base_url());
	    }
	    $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if(!$usr){
			$this->session->sess_destroy();
	    	redirect(base_url());
		}
	}
	public function tiposusuarios(){

		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));

		$sqSbC = $this->modules_model->get_typeuser();
		if($sqSbC){
			$bodyArr = array();
			$header='
					<div class="col-xs-12 col-sm-12 col-lg-12 headSBcat">
						<div class="col-xs-6 col-sm-12 col-lg-6">
							<strong>Nombre</strong>
						</div>
						<div class="col-xs-3 col-sm-12 col-lg-6 text-right">
							<strong>Ajustes</strong>
						</div>
					</div>
				';
			foreach ($sqSbC as $key => $row) {
				$encryUserTypeID = $this->encrypt->encode($row->UserTypeID);
				$sub_body = '
					<div class="col-xs-12 col-sm-12 col-lg-12 diCatf clickme" val="'.$encryUserTypeID.'">
						<div class="col-xs-6 col-sm-12 col-lg-6">
							<a>'.$row->UserTypeName.'</a>
						</div>
						<div class="col-xs-6 col-sm-12 col-lg-6 text-right">
							<div class="dropdown">
								<a class="dropdown-toggle togglemembers" data-toggle="dropdown"><i class="fa fa-cogs" aria-hidden="true"></i></a>
							  	<ul class="dropdown-menu menudrop-cat">
							    	<li><a  val="'.$encryUserTypeID.'" ><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a></li>
							    	<li><a  idtable="UserTypeID"  table="bn_typeuser" idregistro="'.$encryUserTypeID.'"><i class="fa fa-trash-o" aria-hidden="true"></i> Borrar</a></li>
							  	</ul>
							</div>
						</div>
					</div>
				';
				array_push($bodyArr, $sub_body);
			}
			$elemn = implode(",", $bodyArr);
            $catF=str_replace(',','',$elemn);
            $SBcat = $header.$catF;
            $dato['listtype'] = $SBcat;
		}else{
			$dato['listtype'] = "";
		}
		$dato['UserTypeID'] = $usr[0]->UserTypeID;
		$this->load->view('usuarios-tipo',$dato); 
	}

	function showmodules(){
		$TypeUserIDE = $this->input->post('TypeUserID');
		$TypeUserIDE = trim($TypeUserIDE);
		$TypeUserID = urlencode($TypeUserIDE);
		$TypeUserID = str_replace("+", "%2B",$TypeUserID);
		$TypeUserID = urldecode($TypeUserID);
		$TypeUserID = $this->encrypt->decode($TypeUserID);

		$sql = $this->modules_model->GetModulesByTypeUser($TypeUserID,0);

		$tabla = $this->encrypt->encode('usertype_module');
		$id = $this->encrypt->encode('UserModuleID');
		$md = '';
		if($sql){
			foreach ($sql as $row) {
				$encrypt1 = $this->encrypt->encode($row->UserModuleID);
				$md .= '<div class="col-xs-12 col-sm-12 col-lg-12" style="padding: 3px;" ><label>'.$row->ModuleName.'</label> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$encrypt1.'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a><div class="col-xs-12 col-sm-12 col-lg-12" style="padding: 3px;">';
				$chl = $this->modules_model->GetModulesByTypeUser($TypeUserID,$row->ModuleID);
				if($chl){
					foreach ($chl as $vl) {
						$encrypt2 = $this->encrypt->encode($vl->UserModuleID);
						$md .= '<div style="padding: 3px;"><label>'.$vl->ModuleName.'</label> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$encrypt2.'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a></div>';
					}
				}
				$md .= '</div></div>';
			}
			
		}
		echo $md;
	}
	function modulesshow(){
		$TypeUserIDE = $this->input->post('catActive');
		$TypeUserIDE = trim($TypeUserIDE);
		$TypeUserID = urlencode($TypeUserIDE);
		$TypeUserID = str_replace("+", "%2B",$TypeUserID);
		$TypeUserID = urldecode($TypeUserID);
		$TypeUserID = $this->encrypt->decode($TypeUserID);
		


		$mdl = $this->modules_model->get_modules('0');
		$modulos = '<input type="hidden" id="TypUsr" value="'.$TypeUserIDE.'" />';
		if($mdl){
			foreach ($mdl as $key => $row) {
				//Saber si ya tiene asignada este modulo
				$mut = $this->modules_model->ModuleUserType($TypeUserID,$row->ModuleID);
				$checked = ''; 
				if($mut){
					$checked = 'checked'; 
				}
				$encryid1 = $this->encrypt->encode($row->ModuleID);
				$modulos .= '<div class="col-xs-12 col-sm-12 col-lg-12" style="padding: 3px;" >'.$row->ModuleName.' <input type="checkbox" name="mdl" id="mdl" value="'.$encryid1.'" '.$checked.' /><br><div class="col-xs-12 col-sm-12 col-lg-12" style="padding: 3px;">';
				$mdlchl = $this->modules_model->get_modules($row->ModuleID);
				if($mdlchl){
					foreach ($mdlchl as $vl) {
						//Saber si ya tiene asignada este modulo
						$mut2 = $this->modules_model->ModuleUserType($TypeUserID,$vl->ModuleID);
						$checked2 = ''; 
						if($mut2){
							$checked2 = 'checked'; 
						}

						$encryid2 = $this->encrypt->encode($vl->ModuleID);
						$modulos .= '<div style="padding: 3px;">'.$vl->ModuleName.' <input type="checkbox" name="mdl" id="mdl" value="'.$encryid2.'" '.$checked2.' />';

						$mdlchl2 = $this->modules_model->get_modules($vl->ModuleID);
						if($mdlchl2){
							/*
							foreach ($mdlchl2 as $vl2) {
								$mut3 = $this->modules_model->ModuleUserType($TypeUserID,$vl2->ModuleID);
								$checked3 = ''; 
								if($mut3){
									$checked3 = 'checked'; 
								}
								$encryid3 = $this->encrypt->encode($vl2->ModuleID);
								$modulos .= '<div style="padding: 3px;">'.$vl2->ModuleName.' <input type="checkbox" name="mdl" id="mdl" value="'.$encryid3.'" '.$checked3.' />';
							}*/
						}
						$modulos .= '</div>';
					}
				}
				$modulos .= '</div></div>';
			}
		}
		echo $modulos;
	}
	function addmoduletypeuser(){
		$files = $this->input->post('files');
		
		$cont = 0;
		$TypeUserIDE = $files[0]['TypeUserID'];
		$TypeUserIDE = trim($TypeUserIDE);
		$TypeUserID = urlencode($TypeUserIDE);
		$TypeUserID = str_replace("+", "%2B",$TypeUserID);
		$TypeUserID = urldecode($TypeUserID);
		$TypeUserID = $this->encrypt->decode($TypeUserID);
		$dlt = $this->modules_model->DeleteModuleTypeUser($TypeUserID);
		foreach ($files as $key => $value) {
			$ModuleIDE = $value['ModuleID'];
			$ModuleIDE = trim($ModuleIDE);
			$ModuleID = urlencode($ModuleIDE);
			$ModuleID = str_replace("+", "%2B",$ModuleID);
			$ModuleID = urldecode($ModuleID);
			$ModuleID = $this->encrypt->decode($ModuleID);

			$DocDate = date('Y-m-d H:i:s');
			$DocUserID = $this->session->userdata('UserID');

			$SQL = "SELECT * FROM bn_module WHERE ModuleID='$ModuleID' AND Parent!=0 AND Cancelled=0";
			$cl = $this->general_model->QUERYS($SQL);
			if($cl){
				$Parent = $cl[0]->Parent;

				$data1 = array(
					'DocDate' => $DocDate,
					'DocUserID' => $DocUserID,
					'UserTypeID' => $TypeUserID,
					'ModuleID' => $Parent
				);
				
				$insert1 = $this->modules_model->InsertModuleTypeUser($TypeUserID,$Parent,$data1); 
			}

			$data2 = array(
				'DocDate' => $DocDate,
				'DocUserID' => $DocUserID,
				'UserTypeID' => $TypeUserID,
				'ModuleID' => $ModuleID
			);
			
			$insert2 = $this->modules_model->InsertModuleTypeUser($TypeUserID,$ModuleID,$data2);
			$cont++;
			
		}

		if($cont==count($files)){
			echo "true";
		}else{
			echo "false";
		}
		
		
	}
}
?>