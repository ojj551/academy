<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vista extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model('alumnos_model');
        $this->load->model('institucion_model');
        $this->load->model('cursos_model');
        $this->load->model('materias_model');
        $this->load->model('temas_model');
        if (!$this->session->userdata('UserID')) {
            redirect(base_url());
        }
        $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
        $this->UserTypeID = '';
        $this->CustomerID = '';
        $this->actual_link = actual_link();
        if ($usr) {
            //Tipo de usuario
            $UserTypeID = $usr[0]->UserTypeID;
            $typ = $this->general_model->TypeUsers("UserTypeID='$UserTypeID'");
            if ($typ) {
                if ($typ[0]->Slug == 'student' || $typ[0]->Slug == 'teacher') {
                    $this->UserTypeID = $usr[0]->UserTypeID;
                    $this->CustomerID = $usr[0]->CustomerID;
                } else {
                    redirect(base_url() . 'denied');
                }
            } else {
                redirect(base_url() . 'denied');
            }
        } else {
            redirect(base_url() . 'denied');
        }

    }

    public function index()
    {
        $TopicID = trim($this->input->get('l'));
        $TopicID = urlencode($TopicID);
        $TopicID = str_replace("+", "%2B", $TopicID);
        $TopicID = urldecode($TopicID);
        $TopicID = $this->encrypt->decode($TopicID);

        $tpc = $this->materias_model->AboutTopic($TopicID);
        $CourseProfesorID = '';
        $dato['imgcurso'] = '';
        $dato['CourseName'] = '';
        $dato['CourseEncryp'] = '';
        $dato['topics'] = array();

        if ($tpc) {
            $CourseID = $tpc[0]->CourseID;
            $NameTopic = $tpc[0]->NameTopic;
            $inf = $this->general_model->AboutMateria($CourseID);
            if ($inf) {
                $CourseProfesorID = $inf[0]->CourseProfesorID;
                //img curso
                $CutomerIDEncrypt = md5($this->CustomerID);
                $ProfesorIDEncrypt = md5($inf[0]->CourseProfesorID);
                $MateriaIDEncrypt = md5($inf[0]->CourseID);
                $imgcurso = $this->actual_link . '/files_academy/Customers/' . $CutomerIDEncrypt . '/prof/' . $ProfesorIDEncrypt . '/' . $MateriaIDEncrypt . '/' . $inf[0]->CourseImage;
                $dato['imgcurso'] = $imgcurso;
                $dato['CourseName'] = $inf[0]->CourseName;
                $dato['CourseEncryp'] = '';
            }

            /* Obtener unidades y topics para el sidebar */
            $gsc = $this->materias_model->GetSection($CourseID);
            if ($gsc) {
                $dato['topics'][] = $this->sidebar($gsc);
            }

            /************************** *********************/
            //Obtener templates
            $templ = $this->temas_model->GetTemplatesById($TopicID);
            $loads = $this->Get_Templates($TopicID, $templ);
            /************************** *********************/

            $dato['loads'] = $loads;
            $havecontent = 0;
            if ($loads != "") {
                $havecontent = 1;
            }
            $dato['havecontent'] = $havecontent;
            $dato['TopicIDEn'] = $this->encrypt->encode($TopicID);
            $dato['TopicID'] = $this->encrypt->encode($TopicID);
            //Salir de vista previa
            $salir = '';
            if ($CourseProfesorID == $this->session->userdata('UserID')) {
                $salir = '<a href="' . base_url() . 'temas/?t=' . $this->input->get('l') . '" class="btn btn-primary">Salir de vista previa</a>';
            }
            $dato['salir'] = $salir;
            $dato['NameTopic'] = $NameTopic;
            $dato['typ_user'] = $this->input->get('l');

            //Estilos
            $stl = $this->materias_model->GetCoursesByID($CourseID);
            $CourseFont = '';
            $ColorPalette1 = '';
            $ColorPalette2 = '';
            $ColorPalette3 = '';
            if ($stl) {
                $font = $stl[0]->CourseFont;
                if ($font != "") {
                    if ($font == 'roboto') {
                        $CourseFont = "'Roboto', sans-serif";
                    }
                    if ($font == 'nunito') {
                        $CourseFont = "'Nunito', sans-serif";
                    }
                    if ($font == 'opensans') {
                        $CourseFont = "'Open Sans', sans-serif";
                    }
                    if ($font == 'lato') {
                        $CourseFont = "'Lato', sans-serif";
                    }

                } else {
                    $CourseFont = "'Open Sans', sans-serif";
                }

                $clr1 = $stl[0]->ColorPalette1;
                if ($clr1 != "") {
                    $ColorPalette1 = $clr1;
                } else {
                    $ColorPalette1 = '#ed8721';
                }
                $clr2 = $stl[0]->ColorPalette2;
                if ($clr2 != "") {
                    $ColorPalette2 = $clr2;
                } else {
                    $ColorPalette2 = '#e5e5e5';
                }
                $clr3 = $stl[0]->ColorPalette3;
                if ($clr3 != "") {
                    $ColorPalette3 = $clr3;
                } else {
                    $ColorPalette3 = '#111111';
                }

            }
            $dato['CourseFont'] = $CourseFont;
            $dato['ColorPalette1'] = $ColorPalette1;
            $dato['ColorPalette2'] = $ColorPalette2;
            $dato['ColorPalette3'] = $ColorPalette3;


            $dato['header'] = $this->load->view('cabecera-admin', '', true);
            $dato['header'] .= $this->load->view('header-top', '', true);
            $dato['header'] .= link_tag('assets/css/style-alumn.css');
            $dato['header'] .= link_tag('assets/css/templates.css');
            $dato['footer'] = $this->load->view('footer-admin', '', true);
            $this->twig->display('estudiante/leccion-vista-alumnos', $dato);
        }


    }

    Private function sidebar($gsc)
    {
        //$tabla = $this->encrypt->encode('topics');
        //$id = $this->encrypt->encode('TopicID');
        $ord = 1;
        $topics1 = array();
        foreach ($gsc as $vl) {
            //$TopicIDEncryptUnidad = $this->encrypt->encode($vl->SectionID);
            $topics1 = array();
            $topics1['ord'] = $ord;
            $topics1['NameSection'] = $vl->NameSection;
            $topics1['Topic'] = array();
            $ord++;
            $gut = $this->materias_model->GetSectionTopics($vl->SectionID, false);

            if ($gut) {
                $ort = 1;
                foreach ($gut as $g) {
                    $gutsb = $this->materias_model->GetSubSectionTopics($vl->SectionID, $g->TopicID);
                    if ($gutsb) {
                        $sbcont = 1;
                        foreach ($gutsb as $stp) {
                            $topics1['Topic'][$ort]['sub'][] = array(
                                'TopicID' => $this->encrypt->encode($stp->TopicID),
                                'sbcont' => $sbcont,
                                'NameTopic' => $stp->NameTopic
                            );
                            $sbcont++;
                        }
                    }

                    $topics1['Topic'][$ort]['TopicID'] = $this->encrypt->encode($g->TopicID);
                    $topics1['Topic'][$ort]['ort'] = $ort;
                    $topics1['Topic'][$ort]['NameTopic'] = $g->NameTopic;
                    $ort++;
                }
            }
        }
        return $topics1;
    }

    Private function Get_Templates($TopicID, $templ)
    {
        $loads = "";
        if ($templ) {
            $loads = '';
            $i = 1;
            $p = 0;
            foreach ($templ as $row) {
                $CategorySlug = $row->CategoryTemplateID;
                //Los items

                $Items = '';
                if ($row->Items != "") {
                    $Items .= $row->Items;
                } else {
                    $UrlFunctionTemplate = $row->UrlFunctionTemplate;
                    $CategoryTemplateID = $row->CategoryTemplateID;
                    $ItemID = $row->ItemID;
                    $Settings = $row->Settings;

                    //Acera de ItemID
                    $gat = $this->temas_model->GetItemByID($ItemID);
                    if ($gat) {
                        $stg_item = $gat[0]->Settings;
                        if ($stg_item != "") {
                            $stgs = $stg_item;
                        } else {
                            $stgs = $Settings;
                        }

                        $ctg = $this->temas_model->AboutCategory($CategoryTemplateID);
                        if ($ctg) {
                            if ($ctg[0]->CategorySlug != "actividades") {
                                $csbit = $this->temas_model->GetSubItemByItemID($ItemID);
                                if ($csbit) {
                                    $arr = array();
                                    foreach ($csbit as $vl) {
                                        $sub_arr = array(
                                            'DocEntry' => $this->encrypt->encode($vl->DocEntry),
                                            'Type' => $vl->Type,
                                            'Valor' => $vl->Valor,
                                            'OrdenSection' => $row->Orden,
                                            'Orden' => $vl->Orden,
                                            'Setting' => $stgs

                                        );
                                        array_push($arr, $sub_arr);
                                    }
                                    $agrupar = groupArray($arr, 'Orden');
                                    $Items .= $UrlFunctionTemplate($agrupar);

                                } else {
                                    $if_Helper = ($CategorySlug != 'texto' && $CategorySlug != 'divisiones');
                                    if ($if_Helper == false) {
                                        $Items .= '<div class="">' . $row->TemplateItems . '</div>';
                                    }
                                }
                            } else {
                                if ($row->TemplateID != "49") {
                                    $img_file = site_url('assets/img/ejercicio.png');

                                    //Para las actividades
                                    //1. verificar si el usuario ya contesto esta actividad
                                    //$aws = $this->temas_model->AnswerActivities($ItemID, $ctg[0]->CategorySlug);

                                    //Si aun no a contestado nada, se pinta desde los valores del template hecho por el profesor
                                    $CategorySlug = $ctg[0]->CategorySlug;
                                    $ordenar = array();
                                    $ordenar['img_file'] = $img_file;
                                    //if(!$aws){
                                    if ($ctg[0]->CategorySlug == 'actividades') {
                                        $tpv = $this->temas_model->TemplateActivities($ItemID, $CategorySlug);
                                        if ($tpv) {
                                            foreach ($tpv as $stm) {
                                                $ordenar['valor'][] = $stm->Valor;
                                            }
                                        }
                                        $ordenar['ItemID'] = $this->encrypt->encode($ItemID);
                                        $Items .= $this->twig->render('Activities/ordenar_correctamente', $ordenar);
                                    }
                                    /*
                                    }else{
                                        $Items .= '
                                        <b>Tus respuestas fueron: </b><br>
                                        <ul>
                                            ';
                                        $Values = $aws[0]->Values;
                                        $Values = json_decode($Values);
                                        $ctv = 1;
                                        foreach ($Values as $stm) {
                                            $Items .= '
                                            <li class="col-sm-12 col-md-12 col-lg-12">
                                                '.$ctv.'.- '.$stm.'
                                            </li>
                                            ';
                                            $ctv++;
                                        }



                                        $Items .= '</ul>';
                                        //print_r($aws);
                                    }*/
                                } else {
                                    $dato = array();
                                    //$aws = $this->temas_model->AnswerActivities($ItemID, $ctg[0]->CategorySlug);
                                    $check = $this->materias_model->ExistExam($TopicID, $ItemID);
                                    $dato['QuizName'] = $check[0]->QuizName;
                                    $QuizArr = array();
                                    if ($check) {
                                        $QuizID = $check[0]->QuizID;
                                        $qs = $this->materias_model->GetQuestions($QuizID);

                                        $i = 0;
                                        if ($qs) {
                                            foreach ($qs as $key => $q) {
                                                $dato['swiperNum'] = $i;

                                                //consultar opciones
                                                $gtop = $this->materias_model->OptionQuestion($q->QuestionID);
                                                if ($gtop) {
                                                    //Obtener nombre solo si existen opciones
                                                    $QuizArr[$i]['QuestionName'] = $q->QuestionName;
                                                    $QuizArr[$i]['QuestionType'] = $q->QuestionType;

                                                    if ($q->QuestionType != 'rE') {
                                                        $arrColSize[5] = array(2, 3, 2, 3, 2);
                                                        $arrColSize[4] = array(3, 3, 3, 3);
                                                        $ColDist[2] = array(0, 1, 0, 1, 0);
                                                        $ColDist[3] = array(0, 1, 1, 1, 0);
                                                        $ColDist[4] = array(1, 1, 1, 1);
                                                        $ColDist[5] = array(1, 1, 1, 1, 1);
                                                        $QuizArr[$i]['opCount'] = count($gtop);
                                                        $QuizArr[$i]['ColDist'] = $ColDist;
                                                        $QuizArr[$i]['key'] = $key;
                                                        $QuizArr[$i]['QuestionID'] = $this->encrypt->encode($q->QuestionID);
                                                        foreach ($gtop as $optns) {
                                                            $QuizArr[$i]['options'][] = array(
                                                                'OptionID' => $this->encrypt->encode($optns->OptionID),
                                                                'OptionName' => $optns->OptionName
                                                            );
                                                        }
                                                        if (count($gtop) == 5 || count($gtop) == 3 || count($gtop) == 2) {
                                                            $QuizArr[$i]['arrColSize'] = $arrColSize[5];
                                                        } elseif (count($gtop) == 4) {
                                                            $QuizArr[$i]['arrColSize'] = $arrColSize[4];
                                                        }
                                                    } else {
                                                        if ($gtop) {
                                                            foreach ($gtop as $row) {

                                                                $options = json_decode($row->OptionExtra, True);
                                                                $options1 = array();
                                                                $options1[''] = '--';
                                                                foreach ($options as $ops) {
                                                                    $options1[$ops['Orden']] = $ops['Name'];
                                                                }

                                                                //Se manda el valor de la opcion para que vaya directamente el ID, en el model se hace comprobacion con la question y el usuario
                                                                $js = array(
                                                                    'class' => 'selectOp rE',
                                                                    'dt' => $this->encrypt->encode(json_encode(array(
                                                                        'QuestionID' => $this->encrypt->encode($q->QuestionID),
                                                                        'OptionID' => $this->encrypt->encode($row->OptionID)
                                                                    )))
                                                                );

                                                                $from = '/' . preg_quote('___', '/') . '/';
                                                                $to = form_dropdown('rE', $options1, 'large', $js);
                                                                $QuizArr[$i]['QuestionName'] = preg_replace($from, $to, $QuizArr[$i]['QuestionName'], 1);
                                                            }
                                                        }
                                                    }
                                                }

                                                $i++;
                                            }

                                            $Items .= $this->twig->render('Activities/Quiz_Tpl', array('QuizArr' => $QuizArr, 'datos' => $dato));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $Items = stripslashes($Items);

                $loads .=
                    '
					<div class="col-xs-12 col-sm-12 col-lg-12 margin-template">
						' . $Items . '
					</div>
					';

                $i++;
                $p++;
            }
        }
        return $loads;
    }
}