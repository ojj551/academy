<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supervisor extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('carreras_model');
		$this->load->model('materias_model');
		$this->load->model('institucion_model');
		if (!$this->session->userdata('UserID')){ 
	        redirect(base_url());
	    }

	    $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
	    $this->UserTypeID = '';
	    $this->CustomerID = '';

	   
	    if($usr){
	    	//Tipo de usuario
        	$UserTypeID = $usr[0]->UserTypeID;
        	$typ = $this->general_model->TypeUsers("UserTypeID='$UserTypeID'");
        	if($typ){
        		if($typ[0]->Slug=='supervisor'){
        			$this->UserTypeID = $usr[0]->UserTypeID;
					$this->CustomerID = $usr[0]->CustomerID;
        		}else{redirect(base_url().'denied');}
        	}else{redirect(base_url().'denied');}
			
	    	

	    }else{
	    	redirect(base_url().'denied');
	    }
	    
	}
	public function index(){
		
		$cm = $this->general_model->AboutCustomer($this->CustomerID);
		$CustomerName = '';
		if($cm){
			$CustomerName = $cm[0]->CustomerName;
			$CustomerID = $cm[0]->CustomerID;
		}
		$dato['CustomerName'] = $CustomerName;

		//Avance global
		$avancglobal = $this->institucion_model->AvanceInstitucionGlobal($this->CustomerID);
		$dato['avanceglobal'] = $avancglobal;
		$dato['poravanzarglobal'] = 100-$avancglobal;

		
		//Avance por carreras
		$crr = $this->institucion_model->AllCarreras($this->CustomerID);
		$NamesCarreras = array();
		$AVGCarreras = array();
		if($crr){
			foreach ($crr as $row) {

				array_push($NamesCarreras, $row->CareerName);
				$avgc = $this->institucion_model->AvgCareer($row->CareerID);
				if($avgc){
					if($avgc[0]->Total!=""){
						array_push($AVGCarreras, $avgc[0]->Total);
					}else{
						array_push($AVGCarreras, 0);
					}
				}else{
					array_push($AVGCarreras, 0);
				}
			}
		}
		
		if(count($crr)>12){
			$dato['grig'] = '12';
		}else{
			$dato['grig'] = '6';
		}
		$dato['NamesCarreras'] = $NamesCarreras;
		$dato['AVGCarreras'] = $AVGCarreras;

		//Total de materias en la institución
		$dato['TotalMaterias'] = $this->institucion_model->AllMaterias($this->CustomerID,1);
		//Total de profesores en la institución
		$dato['TotalProfes'] = $this->institucion_model->AllProfesInst($this->CustomerID,'supervisor',1);
		//Total de alumnos en la institución
		$dato['TotalAlumnos'] = $this->institucion_model->AllProfesInst($this->CustomerID,'student',1);

		$this->load->view('supervisor/home',$dato);
		//echo $ConfirmationID;
	}

	public function usuarios(){
		$words = '';
		if(isset($_GET['s'])){
			$words = $_GET['s'];
			$total_rows = $this->institucion_model->GetAllUsersSearch($this->CustomerID,$words,1);
		}else{
			$total_rows = $this->institucion_model->GetAllUsers($this->CustomerID,1);
		}
		$config['reuse_query_string'] = TRUE;
		$config['base_url'] = base_url().'supervisor/usuarios';
		$config['uri_segment'] = 3;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = 10;
		$config['num_links'] = 5;
		$config['first_link'] = "Primero";
		$config['last_link'] = "Ultimo";
		$config['next_link'] = "Siguiente";
		$config['prev_link'] = "Anterior";

		$config['cur_tag_open'] = "<b class='actual'>";
		$config['cur_tag_close'] = "</b>";

		$config['full_tag_open'] = "<div id='paginacion'>";
		$config['full_tag_close'] = "</div>";
		$this->pagination->initialize($config);
		$pagination = $this->pagination->create_links();

		//$gt = $this->institucion_model->GetAllUsers($this->CustomerID,0);
        //echo "avance: ".$per_page."<br>to: ".$uri;
		$CustomerID = $this->CustomerID;
		if(isset($_GET['s'])){
			
			$words = $_GET['s'];
			$query = "SELECT *,date_format(DocDate,'%d-%m-%Y') AS FRegister FROM bn_users WHERE UserFirstName LIKE '%$words%' AND CustomerID='$CustomerID' AND Cancelled=0 OR UserLastName LIKE '%$words%' AND CustomerID='$CustomerID' AND Cancelled=0 OR UserEmail LIKE '%$words%' AND CustomerID='$CustomerID' AND Cancelled=0"; 
		}else{
			$query = "SELECT *,date_format(DocDate,'%d-%m-%Y') AS FRegister FROM bn_users WHERE CustomerID='$CustomerID' AND Cancelled=0";
		}
		$gt = $this->institucion_model->GetUsers($query,$config['per_page']);
		$usuarios = '';
		
		if($gt){
			foreach ($gt as $ky => $row) {
				//tipo de usuario
				$UserTypeID = $row->UserTypeID;
				$t = $this->general_model->TypeUsers("UserTypeID='$UserTypeID'");
				$TUser = '';
				$SlugUser = '';
				if($t){
					$TUser = $t[0]->UserTypeName;
					$SlugUser = $t[0]->Slug;
				}
				//Responsable de usuario
				$gtu = $this->institucion_model->HaveThisUser($row->UserID,$this->session->userdata('UserID'));
				$responsable = '';
				if($gtu){
					$responsable = '<i class="fa fa-map" aria-hidden="true"></i>';
				}
				//
				$ctr = $this->general_model->AboutUser($row->DocUserID);
				$UserName = '';
				if($ctr){
					$UserName = $ctr[0]->UserFirstName.' '.$ctr[0]->UserLastName;
				}
				//Status
				$UserIDEncrypt = $this->encrypt->encode($row->UserID);
				$status = '';
				$ver = '';
				$interacciones_rg = '';
				if($row->ConfirmationID!="" && $row->ConfirmationDetaills!=""){
					$status = '<span class="green">Activado</span>';
					$ver = '
					<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver</a>
					';
				}else{
					$status = '<span classs="blue">Pendiente</span>';
					
					if($row->ConfirmationID!=""){
						$interacciones_rg = 'Agrego sus datos, como nombre contraseña, etc. Pero no accedio a la liga que se envio a su correo para poder activar el paso final.';
					}else{
						$interacciones_rg = 'No ha tenido ninguna interacción con el sistema.';
					}
					$ver = '
					<a class="details_inactive" data-toggle="modal" data-target="#myModal" int="'.$interacciones_rg.'">Ver</a>
					';
				}
				if($SlugUser=="teacher" || $SlugUser=="student"){
					$ver = $ver;
				}else{$ver = '';}
				//if($SlugUser!="admin"){
					$usuarios .= '
					<tr>
						<td>'.$row->FRegister.'</td>
						<td>'.$row->UserFirstName.'</td>
						<td>'.$row->UserLastName.'</td>
						<td>'.$row->UserEmail.'</td>
						<td><i style="font-size:8px;">'.$responsable.'</i> '.$TUser.'</td>
						<td>'.$status.'</td>
						<td>'.$ver.'</td>
						
					</tr>
					';
				//}
			}
			
		}
		
		$dato['usuarios'] = $usuarios;
		$dato['pagination'] = $pagination;

		//Vista de alumnos dependiendo del tipo de usuario
		$dato['listtype'] = '';
		$sql = "UserTypeID!='4' AND Parent='$this->UserTypeID' AND Cancelled=0";
		$ty = $this->general_model->TypeUsers($sql);
		
		$listtype = '<option value="">Seleccionar...</option>';
		if($ty){
			foreach ($ty as $row) {
				//print_r($row);
				$UserTypeIDEncryp = $this->encrypt->encode($row->UserTypeID);
				$listtype .= 
						'
						<option value="'.$UserTypeIDEncryp.'">'.$row->UserTypeName.'</option>
						';

				$sql2 = "UserTypeID!='4' AND Parent='$row->UserTypeID' AND Cancelled=0";
				$ty2 = $this->general_model->TypeUsers($sql2);
				if($ty2){
					foreach ($ty2 as $vl) {
						$UserTypeIDEncryp2 = $this->encrypt->encode($vl->UserTypeID);
						$listtype .= 
						'
						<option value="'.$UserTypeIDEncryp2.'">'.$vl->UserTypeName.'</option>
						';
						$sql3 = "UserTypeID!='4' AND Parent='$vl->UserTypeID' AND Cancelled=0";
						$ty3 = $this->general_model->TypeUsers($sql3);
						if($ty3){
							foreach ($ty3 as $file) {
								$UserTypeIDEncryp3 = $this->encrypt->encode($file->UserTypeID);
								$listtype .= 
								'
								<option value="'.$UserTypeIDEncryp3.'">'.$file->UserTypeName.'</option>
								';
							}
						}
					}
				}
			}
				
		}
		$dato['listtype'] = $listtype;
		$this->load->view('supervisor/usuarios',$dato);
	}
	function carreras(){
		$cm = $this->general_model->AboutCustomerByID($this->CustomerID);
		if($cm){
			$dato['CustomerName'] = $cm[0]->CustomerName;

			$gc = $this->carreras_model->GetCareer($this->CustomerID);
			$content = '';
			if($gc){
				$tabla = $this->encrypt->encode('career');
				$id = $this->encrypt->encode('CareerID');
				foreach ($gc as $row) {
					$encrypt = $this->encrypt->encode($row->CareerID);
					//Fechas
					if($row->DocUpdateDate!="0000-00-00 00:00:00"){
						$date = $row->DocUpdateDate;
					}else{
						$date = $row->DocDate;
					}
					//Total de usuarios por carrera
					$TotalAlmCar = $this->institucion_model->UsuariosDeCarreras($row->CareerID,1);
					if($TotalAlmCar!=""){
						$TotalAlmCar = $TotalAlmCar;
					}else{
						$TotalAlmCar = 0;
					}

					$content .= '
					<tr>
						<td>'.$date.'</td>
						<td><a href="'.base_url().'supervisor/carrera/?c='.$encrypt.'">'.$row->CareerName.'</a></td>
						<td>'.$row->CareerTotalNivels.'</td>
						<td>'.$TotalAlmCar.'</td>
						<td>
							<div class="dropdown">
								<a class="dropdown-toggle togglemembers" data-toggle="dropdown"><i class="fa fa-cogs" aria-hidden="true"></i></a>
								<ul class="dropdown-menu menudrop-course">
									<li><a class="edit" data-toggle="modal" data-target="#updatealumnos-form" val="'.$encrypt.'"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a></li>
									<li>
										<a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$encrypt.'"><i class="fa fa-trash-o" aria-hidden="true"></i> Borrar</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
					';
				}
			}
			$dato['content'] = $content;

			$dato['ctm'] = $this->encrypt->encode($this->CustomerID);

			$this->load->view('supervisor/carreras',$dato);
		}
	}
	function carrera(){
		$CareerID = $this->input->get('c');
		$CareerID = urlencode($CareerID);
		$CareerID = str_replace("+", "%2B",$CareerID);
		$CareerID = urldecode($CareerID);
		$CareerID = $this->encrypt->decode($CareerID);

		$cr = $this->carreras_model->AboutCareer($CareerID);
		if($cr){
			$data['namecarr'] = $cr[0]->CareerName;

			$uc = $this->carreras_model->UserCareer($this->CustomerID,$CareerID);
			$progress = 0;
			$totalAlumn = 0;
			$users = '';
			if($uc){
				foreach ($uc as $row) {
					$users .= '
					<tr>
						<td>'.$row->UserFirstName.' '.$row->UserLastName.'</td>
						<td>'.$row->UserEmail.'</td>
						<td>'.$row->UserSemester.'</td>
						<td>
							<div style="display:inline-block;width:100%;height:5px;background:gray;">
								<div style="background:orange;width:'.$row->Progress.'%;height:100%;"></div>
							</div>
							'.$row->Progress.' %
						</td>
						<td></td>
					</tr>
					';
				}
				$totalAlumn++;
			}
			$data['avance_g'] = $progress;
			$data['falt_avance_g'] = 100-$progress;

			$data['users'] = $users;

			$this->load->view('supervisor/carreras-single',$data);
		}

	}
	function materias(){

		$gc = $this->materias_model->GetMaterias($this->CustomerID);
		$content = '';
		if($gc){
			$tabla = $this->encrypt->encode('asigntaturas');
			$id = $this->encrypt->encode('AsignaturaID');
			foreach ($gc as $row) {
				$encrypt = $this->encrypt->encode($row->AsignaturaID);
				//Fechas
				if($row->DocUpdateDate!="0000-00-00 00:00:00"){
					$date = $row->DocUpdateDate;
				}else{
					$date = $row->DocDate;
				}
				//Total de usuarios por carrera
				$TotalAlumnos = 0;
				$cr = $this->materias_model->GetCoursesByAsig($row->AsignaturaID);
				if($cr){
					foreach ($cr as $c) {
						$alm = $this->materias_model->UsersSuscription($c->CourseID,1);
						$TotalAlumnos = $TotalAlumnos+$alm;
					}
				}
				$content .= '
				<tr>
					<td>'.$date.'</td>
					<td><a href="'.base_url().'supervisor/materia/?c='.$encrypt.'">'.$row->AsignaturaName.'</a></td>
					<td>'.$TotalAlumnos.'</td>
					<td>
						<div class="dropdown">
							<a class="dropdown-toggle togglemembers" data-toggle="dropdown"><i class="fa fa-cogs" aria-hidden="true"></i></a>
							<ul class="dropdown-menu menudrop-course">
								<li><a class="edit-asig" data-toggle="modal" data-target="#update" val="'.$encrypt.'"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a></li>
								<li>
									<a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$encrypt.'"><i class="fa fa-trash-o" aria-hidden="true"></i> Borrar</a>
								</li>
							</ul>
						</div>
					</td>
				</tr>
				';
			}
		}
		$dato['content'] = $content;

		$dato['ctm'] = $this->encrypt->encode($this->CustomerID);

		$this->load->view('supervisor/materias',$dato);
	
	}
	
	function materia(){
		$AsignaturaID = $this->input->get('c');
		$AsignaturaID = urlencode($AsignaturaID);
		$AsignaturaID = str_replace("+", "%2B",$AsignaturaID);
		$AsignaturaID = urldecode($AsignaturaID);
		$AsignaturaID = $this->encrypt->decode($AsignaturaID);

		$cr = $this->materias_model->UsersIntoAsignature($AsignaturaID);
		$data['nameasig'] = '';
		$users = '';
		if($cr){
			$data['nameasig'] = $cr[0]->AsignaturaName;
			$users = '';
			foreach ($cr as $row) {
				$usrc = $this->general_model->AboutUser($row->UserID);
				if($usrc){
					$users .= '
					<tr>
						<td>'.$usrc[0]->UserFirstName.' '.$usrc[0]->UserLastName.'</td>
						<td>'.$usrc[0]->UserEmail.'</td>
						<td>'.$usrc[0]->UserSemester.'</td>
						<td>
							<div style="display:inline-block;width:100%;height:5px;background:gray;">
								<div style="background:orange;width:'.$row->Progress.'%;height:100%;"></div>
							</div>
							'.$row->Progress.' %
						</td>
						<td></td>
					</tr>
					';
				}
				
			}
			
			
		}
		$data['users'] = $users;
		$this->load->view('supervisor/materia-single',$data);
	}
}

?>