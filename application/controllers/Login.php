<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->helper('html');
        $this->load->model('login_model');
    }

    public function index()
    {
        $error = $this->session->userdata('formError');
        if (!$this->session->userdata('UserEmail')) {
            $this->load->view('login', array('err' => $error));
            $this->session->unset_userdata('formError');
        } else {
            //Tipo de usuario
            $c = $this->general_model->AboutUser($this->session->userdata('UserID'));
            $UserTypeID = $c[0]->UserTypeID;
            $typ = $this->general_model->TypeUsers("UserTypeID='$UserTypeID'");
            if ($typ) {
                if ($typ[0]->Slug == 'supervisor') {
                    redirect('supervisor');
                }
                if ($typ[0]->Slug == 'teacher') {
                    redirect('profesor');
                }
                if ($typ[0]->Slug == 'student') {
                    redirect('estudiante');
                }
            } else {
                redirect('login');
            }
        }
    }

    function login()
    {
        $email = $this->input->post('email');
        $email = trim($email);
        $password = $this->input->post('password');
        $password = trim($password);

        $c = $this->login_model->login($email, $password);

        if ($c) {
            $this->session->set_userdata(array(
                'UserID' => $c[0]->UserID,
                'UserFirstName' => $c[0]->UserFirstName,
                'UserLastName' => $c[0]->UserLastName,
                'UserEmail' => $c[0]->UserEmail,
                'CustomerID' => $c[0]->CustomerID
            ));
            //Tipo de usuario
            $UserTypeID = $c[0]->UserTypeID;
            $typ = $this->general_model->TypeUsers("UserTypeID='$UserTypeID'");
            if ($typ) {
                if ($typ[0]->Slug == 'supervisor') {
                    redirect('supervisor');
                }
                if ($typ[0]->Slug == 'teacher') {
                    redirect('profesor');
                }
                if ($typ[0]->Slug == 'student') {
                    redirect('estudiante');
                }
            } else {
                $this->session->set_userdata('formError', 1);
                redirect('/login');
            }
            //redirect('clientes/actividades/?c='.$this->encrypt->encode($ClienteID));
            //echo "success";
        } else {
            $this->session->set_userdata('formError', 1);
            redirect('/');
        }

    }

    function createaccount()
    {
        $matriculac = $this->input->post('matriculac');
        $matriculac = trim($matriculac);
        $nombrec = $this->input->post('nombrec');
        $nombrec = trim($nombrec);
        $apellidosc = $this->input->post('apellidosc');
        $apellidosc = trim($apellidosc);
        $emailc = $this->input->post('emailc');
        $emailc = trim($emailc);
        $passwordc = $this->input->post('passwordc');
        $passwordc = trim($passwordc);

        $valPwdTxt = $passwordc;
        $vPassword = md5($passwordc);
        $ende = new BN_EnDe('zabetuBaStuZejapreweGa8tu6rajacemeTHEb2udrUstex2neThU7edEx6fraza');
        $vPasswordCrypt = "[V2]_" . base64_encode($ende->encrypt($valPwdTxt));

        $GETM = $this->login_model->Matricula($matriculac);

        if ($GETM) {
            if (count($GETM) > 1) {
                echo "Al parecer el ID Referencia que has ingresado existe más de una vez";
            } else {
                //$EM = $this->login_model->ExitsEmail($emailc);
                $ConfirmationID = BN_guid();
                //if(!$EM){
                $DocDate = date('Y-m-d H:i:s');

                $UserSemester = $GETM[0]->UserSemester;
                $CareerID = $GETM[0]->CareerID;

                $data = array(
                    'DocDate' => $DocDate,
                    'CustomerID' => $GETM[0]->CustomerID,
                    'UserFirstName' => $nombrec,
                    'UserLastName' => $apellidosc,
                    'UserEmail' => $emailc,
                    'UserReference' => $matriculac,
                    'UserPasswd' => $vPassword,
                    'UserPasswdCrypt' => $vPasswordCrypt,
                    'UserTypeID' => $GETM[0]->UserTypeID,
                    'UserSemester' => $UserSemester,
                    'CareerID' => $CareerID,
                    'ConfirmationID' => $ConfirmationID,
                    'VisibleStatus' => 1
                );
                $insert = $this->login_model->NewUser($data);
                if ($insert) {
                    //Actualizar reference
                    /*
                    $SQL = "UPDATE bn_user_reference SET UserID='$insert' WHERE Reference='$matriculac'";
                    $upt = $this->general_model->QUERYSUPT($SQL);
                    */

                    //Agregar a user_grupos
                    $CareerID = $GETM[0]->CareerID;
                    $Level = $GETM[0]->UserSemester;
                    $ProfesorID = $GETM[0]->SupervisorID;
                    if ($CareerID != "" && $Level != "" && $ProfesorID != "") {
                        $SQLC = "SELECT * FROM bn_groups WHERE CareerID='$CareerID' AND LevelCareer='$Level' AND ProfesorID='$ProfesorID' AND Cancelled=0";
                        $rsl = $this->general_model->QUERYS($SQLC);
                        if ($rsl) {
                            foreach ($rsl as $vl) {
                                $DocDatesb = date('Y-m-d H:i:s');
                                $data = array(
                                    'DocDate' => $DocDatesb,
                                    'DocUserID' => $ProfesorID,
                                    'UserID' => $insert,
                                    'GroupID' => $vl->GroupID
                                );
                                $ist = $this->login_model->AddUserGroup($insert, $vl->GroupID, $data);
                            }
                        }
                    }

                    $ConfirmationID = $this->encrypt->encode($ConfirmationID);
                    echo base_url() . 'activacion/?c=' . $ConfirmationID;
                }
                /*
                }else{
                    echo "El correo que has ingresado ya existe.";
                }*/
            }
        }
    }

    function fistlogin()
    {
        $userE = $this->input->post('user');
        $user = urlencode($userE);
        $user = str_replace("+", "%2B", $user);
        $user = urldecode($user);
        $user = $this->encrypt->decode($user);


        $SQL = "SELECT * FROM bn_users WHERE UserID='$user' AND Cancelled=0 AND VisibleStatus=0";
        $c = $this->general_model->QUERYS($SQL);
        if ($c) {
            $this->session->set_userdata(array(
                'UserID' => $c[0]->UserID,
                'UserFirstName' => $c[0]->UserFirstName,
                'UserLastName' => $c[0]->UserLastName,
                'UserEmail' => $c[0]->UserEmail,
                'CustomerID' => $c[0]->CustomerID
            ));
            echo "success";
        }

    }

    function recovery()
    {
        $email = $this->input->post('email');

        $SQL = "SELECT * FROM bn_users WHERE UserEmail='$email' AND Cancelled=0";
        $c = $this->general_model->QUERYS($SQL);
        if ($c) {
            $UserFirstName = $c[0]->UserFirstName;
            $UserEncrypt = $this->encrypt->encode($c[0]->UserID);
            //crear codigo de cambio de contraseña
            $UserRecoveryPassword = strtoupper(random_code());
            $DocDate = date('Y-m-d H:i:s');
            $data = array(
                'DocUpdateDate' => $DocDate,
                'UserRecoveryPassword' => $UserRecoveryPassword
            );
            $where = array('UserEmail' => $email, 'Cancelled' => 0);
            $update = $this->general_model->UpdateUser($data, $where);
            //
            if ($update) {
                $recode = $this->encrypt->encode($UserRecoveryPassword);
                $sd = send_recovery_password($UserFirstName, $email, 'c=' . $recode . '&e=' . $UserEncrypt);
                if ($sd) {
                    echo $sd;
                }
            }
        } else {
            echo "error";
        }
    }

    public function recoverypassword()
    {
        $UserRecoveryPassword = trim($this->input->get('c'));
        $UserRecoveryPassword = urlencode($UserRecoveryPassword);
        $UserRecoveryPassword = str_replace("+", "%2B", $UserRecoveryPassword);
        $UserRecoveryPassword = urldecode($UserRecoveryPassword);
        $UserRecoveryPassword = $this->encrypt->decode($UserRecoveryPassword);

        $UserID = trim($this->input->get('e'));
        $UserID = urlencode($UserID);
        $UserID = str_replace("+", "%2B", $UserID);
        $UserID = urldecode($UserID);
        $UserID = $this->encrypt->decode($UserID);

        $cd = $this->login_model->AboutCode($UserID, $UserRecoveryPassword);
        if ($cd) {
            $dato['continue'] = true;
        } else {
            $dato['continue'] = false;
        }
        $dato['U'] = $this->encrypt->encode($UserID);
        $this->load->view('login-recovery', $dato);
    }

    function changepassword()
    {
        $pssw = trim($this->input->post('pssw'));
        $psswr = trim($this->input->post('psswr'));

        $UserID = trim($this->input->post('u'));
        $UserID = urlencode($UserID);
        $UserID = str_replace("+", "%2B", $UserID);
        $UserID = urldecode($UserID);
        $UserID = $this->encrypt->decode($UserID);

        $valPwdTxt = $pssw;
        $vPassword = md5($pssw);
        $ende = new BN_EnDe('zabetuBaStuZejapreweGa8tu6rajacemeTHEb2udrUstex2neThU7edEx6fraza');
        $vPasswordCrypt = "[V2]_" . base64_encode($ende->encrypt($valPwdTxt));

        $usr = $this->general_model->AboutUser($UserID);
        if ($usr) {
            if ($pssw == $psswr) {
                $data = array(
                    'DocUpdateDate' => $DocDate,
                    'DocUpdateUserID' => $UserID,
                    'UserPasswd' => $vPassword,
                    'UserPasswdCrypt' => $vPasswordCrypt,
                    'UserRecoveryPassword' => ''
                );
                $where = array('UserID' => $UserID);

                $upd = $this->general_model->UpdateUser($data, $where);
                if ($upd) {
                    $this->session->set_userdata(array(
                        'UserID' => $usr[0]->UserID,
                        'UserFirstName' => $usr[0]->UserFirstName,
                        'UserLastName' => $usr[0]->UserLastName,
                        'UserEmail' => $usr[0]->UserEmail,
                        'CustomerID' => $usr[0]->CustomerID
                    ));
                    echo "success";
                } else {
                    echo "Tenemos problemas al cambiar tu contraseña, contactanos a <b>contacto@xpertcad.com</b>";
                }
            } else {
                echo "Tus contraseñas no coinciden";
            }

        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }

}