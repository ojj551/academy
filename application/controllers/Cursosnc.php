<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursosnc extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('cursos_model'); 
		$this->load->model('materias_model');
		$this->load->model('temas_model');

		if ( ! $this->session->userdata('UserID')){ 
	        redirect(base_url().'loginnc');
	    }

	    $usr = $this->general_model->UserNC($this->session->userdata('UserID'));
	    $this->CustomerID = '';
	    $this->DocEntry = '';
	    $this->Creator = '';

	    if($usr){
	    	$this->CustomerID = $usr[0]->CustomerID;
	    	$this->DocEntry = $usr[0]->DocEntry;
	    	$this->Creator = $usr[0]->Creator;
	    }
		
	}

	public function index(){
		$gtc = $this->cursos_model->GetCourses($this->DocEntry);
		//echo $this->session->userdata('UserID');
		echo $this->DocEntry."<br>";
		$cursos = '';
		if($gtc){
			$tabla = $this->encrypt->encode('courses');
			$id = $this->encrypt->encode('CourseID');
			foreach ($gtc as $row) {
				$avgc = $this->cursos_model->AvanceCourse($row->CourseID);
				if($avgc){
					$avgc = $avgc;
				}else{
					$avgc = 0;
				}
				$encode = $this->encrypt->encode($row->CourseID);
				if($row->CourseImage!=""){
					$Imgbg = site_url('assets/img/escuelas/unaq/'.$row->CourseImage);
				}else{
					$Imgbg = site_url('assets/img/no_imagen.jpg');
				}
				$encryptc = $this->encrypt->encode($row->CourseID);
				$cursos .= '
					<div class="col-md-4 " style="position:relative">
			          <div class="panel course-on-list animateslow">
			               <div class="panel-heading-white panel-heading text-center">
			                  <h4 class="ttl-crs"><a href="'.base_url().'cursosnc/vista/?c='.$encryptc.'">'.$row->CourseName.'</a></h4>
			                  <a class="dlt dlt-crs" t="'.$tabla.'" id="'.$id.'" vl="'.$encryptc.'"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
			                </div>
			                <div class="panel-body text-center" style="padding: 0px;">
			                	<div style="background: url('.$Imgbg.')no-repeat;height:150px;    background-size: cover;"></div>
			                  
			                  <div class="col-md-12">
			                  <br>
			                  	<a href="'.base_url().'materias/prevista/?m='.$encode.'">Generar o editar contenido</a>

			                  </div>
			                  <div class="col-md-12">
			                  	<a href="'.base_url().'cursosnc/alumnos/?m='.$encode.'">Ver usuarios</a>
			                  	<br>
			                  </div>
			                </div>
			          </div>
			        </div>
				';
			}
		}else{
			$cursos .= '<h1>Aún no tienes cursos</h1>';
		}
		$dato['creator'] = $this->Creator;
		$dato['cursos'] = $cursos;
		$this->load->view('nc/cursos-lista',$dato);


	}
	public function crear(){
		$dato['cursos'] = '';
		$this->load->view('nc/cursos-crear',$dato);
	}
	function addcorse(){
		$title = $this->input->post('title');
		$description = $this->input->post('description');

		$DocDate = date('Y-m-d H:i:s');
		$data = array(
			'DocDate' => $DocDate,
			'DocUserID' => $this->session->userdata('UserID'),
			'CourseName' => trim($title),
			'CourseDescription' => trim($description),
			'CourseProfesorID' => $this->DocEntry,
			'CustomerID' => $this->CustomerID
		);

		$add = $this->cursos_model->AddCurso($data);
		if($add){
			echo "success";
		}
	}
	function addlesson(){
		$NameTopic = trim($this->input->post('temaname'));

		$c = trim($this->input->post('c'));
		$c = urlencode($c);
		$c = str_replace("+", "%2B",$c);
		$c = urldecode($c);
		$c = $this->encrypt->decode($c);

		$or = $this->cursos_model->CountTopics($c,$this->session->userdata('DocEntry'));
		$OrderTopic = 0;
		if($or){
			foreach ($or as $ctd) {
				$OrderTopic++;
			}
		}
		$OrderTopic = $OrderTopic+1;

		
		$DocDate = date('Y-m-d H:i:s');
		$data = array(
			'DocDate' => $DocDate,
			'DocUserID' => $this->session->userdata('UserID'),
			'SectionID' => 0,
			'Parent' => 0,
			'NameTopic' => trim($NameTopic),
			'CourseID' => $c,
			'OrderTopic' => $OrderTopic
		);
		//echo '<input class="form-control lesson" type="text" placeholder="Escribe nombre del tema" autofocus>';
		$ins = $this->cursos_model->InsertLesson($data);
		if($ins){
			$get_l = $this->cursos_model->CountTopics($c,$this->session->userdata('DocEntry'));
			$temas = '';
			if($get_l){
				$tabla = $this->encrypt->encode('topics');
				$id = $this->encrypt->encode('TopicID');
				foreach ($get_l as $row) {
					$temas .= '
					<li class="col-sm-12 col-md-12 col-lg-12 ui-state-default" v="'.$row->TopicID.'" o="'.$row->OrderTopic.'" style="border:1px solid #ffffff;">
						<div class="col-sm-12 col-md-12 col-lg-12 padding0" style="margin-top:10px;margin-bottom:10px;">
							<div class="col-sm-12 col-md-12 col-lg-12 padding0">
								<input class="form-control lesson " type="text" placeholder="Escribe nombre del tema" value="'.$row->NameTopic.'" v="'.$this->encrypt->encode($row->TopicID).'"  />
					
								<div class="btn-group right-option-v1" style="top: 9px;font-size: 33px;">
			                        <i class="icon-options-vertical icons box-v7-menu" data-toggle="dropdown"></i>
			                        <ul class="dropdown-menu" role="menu" style="left: initial; right: 0px;">
			                          <li><a href="'.base_url().'temas/?t='.$this->encrypt->encode($row->TopicID).'"><i class="fa fa-pencil" aria-hidden="true"></i> Administrar contenido</a></li>
			                          <li><a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$this->encrypt->encode($row->TopicID).'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a></li>
			                        </ul>
			                    </div>
							</div>
						</div>
					</li>
					';
				}
				
			}
			$temas .= '';
			echo $temas;
			//echo "success";
		}
	}
	function editname(){
		$NameTopic = trim($this->input->post('temaname'));

		$TopicID = trim($this->input->post('t'));
		$TopicID = urlencode($TopicID);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);

		if($TopicID!="" && $NameTopic!=""){
			$data = array(
				'NameTopic' => $NameTopic,
				'DocUpdateDate' => date('Y-m-d H:i:s'),
				'DocUpdateUserID' => $this->session->userdata('UserID')
			);
			$where = array('TopicID' => $TopicID);

			$updtpc = $this->cursos_model->UpdateTopic($data,$where);
			echo "success";
		}
	}
	function editcourse(){
		$CourseID = trim($this->input->post('c'));
		$CourseID = urlencode($CourseID);
		$CourseID = str_replace("+", "%2B",$CourseID);
		$CourseID = urldecode($CourseID);
		$CourseID = $this->encrypt->decode($CourseID);
		if(isset($_POST['title'])){
			
			$CourseName = trim($this->input->post('title'));

			if($CourseID!="" && $CourseName!=""){
				$data = array(
					'CourseName' => $CourseName,
					'DocUpdateDate' => date('Y-m-d H:i:s'),
					'DocUpdateUserID' => $this->session->userdata('UserID')
				);
				$where = array('CourseID' => $CourseID);

				$updtpc = $this->cursos_model->UpdateCourse($data,$where);
				echo "success";
			}
			
		}
		if(isset($_POST['description'])){
			
			$CourseDescription = trim($this->input->post('description'));

			if($CourseID!="" && $CourseDescription!=""){
				$data = array(
					'CourseDescription' => $CourseDescription,
					'DocUpdateDate' => date('Y-m-d H:i:s'),
					'DocUpdateUserID' => $this->session->userdata('UserID')
				);
				$where = array('CourseID' => $CourseID);

				$updtpc = $this->cursos_model->UpdateCourse($data,$where);
				echo "success";
			}
			
		}
	}
	function showusers(){
		$Depto = trim($this->input->post('v'));
		$query = "SELECT * FROM bn_nc WHERE Depto='$Depto' AND CancelStatus=0";
		$sql = $this->general_model->QUERYS($query);
		if($sql){
			$alumnos = '';
			foreach ($sql as $row) {
				$alumnos .= '
				<div class="form-group form-animate-checkbox">
                  <input type="checkbox" name="usr" value="'.$row->DocEntry.'" class="checkbox">
                  <label> '.$row->FullName.'</label>
                </div>
				';
			}
			echo $alumnos;
		}
	}
	function adduserstoc(){
		$files = $this->input->post('files');
		$DocDate = date('Y-m-d H:i:s');
		$DocUserID = $this->session->userdata('UserID');
		foreach ($files as $value) {
			$MateriaID = trim($value['crs']);
			$MateriaID = urlencode($MateriaID);
			$MateriaID = str_replace("+", "%2B",$MateriaID);
			$MateriaID = urldecode($MateriaID);
			$MateriaID = $this->encrypt->decode($MateriaID);

			$UserID = trim($value['v']);

			$data = array(
				'DocDate' => $DocDate,
				'DocUserID' => $DocUserID,
				'UserID' => $UserID,
				'CourseID' => $MateriaID
			);
			$add = $this->cursos_model->AddUsersToCurso($UserID,$MateriaID,$data);

		}
		echo "success";

	}
	public function alumnos(){
		$MateriaIDE = $this->input->get('m');

		$MateriaID = urlencode($MateriaIDE);
		$MateriaID = str_replace("+", "%2B",$MateriaID);
		$MateriaID = urldecode($MateriaID);
		$MateriaID = $this->encrypt->decode($MateriaID);
		$inf = $this->general_model->AboutMateria($MateriaID);
		$CourseName = ''; $alumnos = '';
		if($inf){
			$alumc = $this->cursos_model->AlumnosNCByCourse($MateriaID);
			if($alumc){
				$tabla = $this->encrypt->encode('user_suscriptions');
				$id = $this->encrypt->encode('SuscriptionID');
				foreach ($alumc as $row) {
					$UserIDEncrypt = $this->encrypt->encode($row->UserID);
					$alumnos .= '
					<tr>
						<td>'.$row->DocDate.'</td>
						<td>'.$row->FirstName.'</td>
						<td>'.$row->LastName.'</td>
						<td>'.$row->Depto.'</td>
						<td>
							<a href="'.base_url().'cursosnc/reporte/?u='.$UserIDEncrypt.'&c='.$this->encrypt->encode($MateriaID).'"><i class="fa fa-file-text-o" aria-hidden="true"></i>
						</a></td>
						<td><a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$this->encrypt->encode($row->SuscriptionID).'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a></td>
					</tr>
					';
				}
			}
			$CourseName = $inf[0]->CourseName;
		}
		//all users
		$als = $this->cursos_model->AllUsers();
		$allusers = '';
		if($als){
			foreach ($als as $row) {
				$allusers .= '
					<option value="'.$row->Depto.'">'.$row->FullName.'</option>
				';
			}
		}
		//all deptos
		$al = $this->cursos_model->Deptos();
		$depto = '';
		if($al){
			foreach ($al as $row) {
				$depto .= '
					<option value="'.$row->Depto.'">'.$row->departmentName.'</option>
				';
			}
		}

		$dato['MateriaIDEncryp'] = $this->encrypt->encode($MateriaID);

		$dato['depto'] = $depto;
		$dato['allusers'] = $allusers;

		$dato['alumnos'] = $alumnos;
		$dato['CourseName'] = $CourseName;
		$this->load->view('nc/cursos-alumnos',$dato);
	}

	public function miscursos(){

		$getmc = $this->cursos_model->GetMyCourses($this->DocEntry);
		$miscursos = '';
		if($getmc){
			foreach ($getmc as $row) {
				$avgc = $this->cursos_model->AvanceCourse($row->CourseID);
				if($avgc){
					$avgc = $avgc;
				}else{
					$avgc = 0;
				}
				$encode = $this->encrypt->encode($row->SuscriptionID);
				if($row->CourseImage!=""){
					$Imgbg = site_url('assets/img/escuelas/unaq/'.$row->CourseImage);
				}else{
					$Imgbg = site_url('assets/img/no_imagen.jpg');
				}
				//Ver avance de curso
				//1. Obtener topics
				$tpcs = $this->cursos_model->TopicsOnCourse($row->CourseID);
				$TotalTopics = 0;
				$OKFinished = 0;
				if($tpcs){
					foreach ($tpcs as $rowtp) {
						$avan = $this->temas_model->TopicLoad($rowtp->TopicID,$this->session->userdata('DocEntry'));
						if($avan){
							foreach ($avan as $vl) {
								if($vl->DateFinished!="0000-00-00 00:00:00"){
									$OKFinished++;
								}
							}
						}
						$TotalTopics++;
					}
				}
				$AvanceAlumno = $OKFinished*100;
				$AvanceAlumno = $AvanceAlumno/$TotalTopics;
				$AvanceAlumno = number_format($AvanceAlumno,2);
				//echo '<h2>'.$AvanceAlumno.'</h2><hr>';
				//echo "<br>Total temas: ".$TotalTopics."<br>Total de temas termindados".$OKFinished."<br><hr>";
				$encryptc = $this->encrypt->encode($row->CourseID);
				$miscursos .= '
					<div class="col-md-4 " style="position:relative">
			          <div class="panel course-on-list animateslow">
			               <div class="panel-heading-white panel-heading text-center">
			                  <h4 class="ttl-crs">'.$row->CourseName.'</h4>
			                </div>
			                <div class="panel-body text-center" style="padding: 0px;">
			                	<div style="background: url('.$Imgbg.')no-repeat;height:150px;    background-size: cover;"></div>
			                  <div class="col-md-12">
			                  	<h2>'.$AvanceAlumno.'%</h2><span> de avance</span>
			                  </div>
			                  <div class="col-md-12">
			                  	<a href="'.base_url().'cursosnc/vista/?c='.$encryptc.'">Ir a curso</a>
			                  </div>
			                </div>
			          </div>
			        </div>
				';
			}
		}else{
			$miscursos = '<h1>Aún no tienes cursos</h1>';
		}
		$dato['miscursos'] = $miscursos;
		$this->load->view('nc/mis-cursos',$dato);
	}
	public function vista(){
		$CourseID = $this->input->get('c');
		$CourseID = urlencode($CourseID);
		$CourseID = str_replace("+", "%2B",$CourseID);
		$CourseID = urldecode($CourseID);
		$CourseID = $this->encrypt->decode($CourseID);

		$inf = $this->general_model->AboutMateria($CourseID);
		if($inf){
			$CourseName = $inf[0]->CourseName;
			$MateriaIDEncryp = $this->encrypt->encode($CourseID);
			$CourseProfesorID = $inf[0]->CourseProfesorID;
			$prf = $this->general_model->UserNCDocEntry($CourseProfesorID);
			$ProfesorName = '';
			if($prf){
				$ProfesorName = $prf[0]->FirstName.' '.$prf[0]->LastName;
			}


			$dato['CourseName'] = $CourseName;
			$dato['MateriaIDEncryp'] = $MateriaIDEncryp;
			$dato['ProfesorName'] = $ProfesorName;
			$dato['Inc'] = $inf[0]->CourseStar;
			$dato['Fin'] = $inf[0]->CourseEnd;
			$dato['Banner'] = $inf[0]->CourseImage;
			$dato['CourseRequirements'] = $inf[0]->CourseRequirements;
			$dato['CourseGoals'] = $inf[0]->CourseGoals;

			$list = '';
			$TopicIDEncrypt = '';
			$gsc = $this->cursos_model->TopicsOnCourse($CourseID);
			//echo "aquí: ".$CourseID.'<br>DocEntry: '.$this->session->userdata('DocEntry');
			$topics = '';
			if($gsc){
				$topics .= '
					<div class="col-xs-12 col-sm-12 col-lg-12">
						<h4 class="tpg-relawey"><strong class="numberunidad-alumn">1,</strong> <label class="titleunidad-alumn"><i>Lecciones</i></label> </h4>
					</div>
					<div class="col-xs-12 col-sm-12 col-lg-12">

						<ul class="mini-timeline">
					';
				foreach ($gsc as $row) {
					$TopicIDEncrypt = $this->encrypt->encode($row->TopicID);
							
					$topics .= '
						<li class="mini-timeline-highlight">
							<div class="mini-timeline-panel">
								<a href="'.base_url().'cursosnc/leccion_vista/?l='.$TopicIDEncrypt.'" style="font-size: 15px;"><strong>'.$row->OrderTopic.'</strong>.- '.$row->NameTopic.'</a> 
							</div>
						</li>
					';
				}
				$topics .= '</ul></div>';
			}
			$dato['topics'] = $topics;
			//Obtener las unidades y los topics
			
			//Salir de vista previa
			
			$salir = '<a href="'.base_url().'cursosnc/miscursos" class="btn btn-primary">Ir a inicio</a>';
			
			$dato['salir'] = $salir;
			

			$dato['first_lesson'] = $this->encrypt->encode($gsc[0]->TopicID);

			$this->load->view('nc/materias-vista-alumnos',$dato);
		}

		
	}
	function leccion_vista(){
		$TopicID = trim($this->input->get('l'));
		$TopicID = urlencode($TopicID);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);

		$tpc = $this->materias_model->AboutTopic($TopicID);

		$CourseProfesorID = '';
		$NameTopic = '';
		if($tpc){
			$CourseID = $tpc[0]->CourseID;
			$cmb = $this->cursos_model->UserIntoCurso($this->session->userdata('DocEntry'),$CourseID);
			//echo $this->session->userdata('DocEntry')."<br>".$CourseID;
			if($cmb){
				$NameTopic = $tpc[0]->NameTopic;
				$inf = $this->general_model->AboutMateria($CourseID);
				if($inf){
					$CourseProfesorID = $inf[0]->CourseProfesorID;
				}
				//Obtener las unidades y los topics
				$gsc = $this->cursos_model->TopicsOnCourse($CourseID);
				$list = '';
				if($gsc){

					$list .= '
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<h4 class="tpg-relawey"><strong class="numberunidad-alumn"></strong> <label class="titleunidad-alumn">Lecciones</label> </h4>
						</div>
						<div class="">

							<ul class="mini-timeline">
						';
					$count_t = 1;
					foreach ($gsc as $row) {
						$TopicIDEncrypt = $this->encrypt->encode($row->TopicID);
								
						$list .= '
							<li class="mini-timeline-highlight">
								<div class="mini-timeline-panel">
									<a href="'.base_url().'cursosnc/leccion_vista/?l='.$TopicIDEncrypt.'" style="font-size: 15px;"><strong class="negro">'.$count_t.'</strong>.- '.$row->NameTopic.'</a> 
								</div>
							</li>
						';
						$count_t++;
					}
					$list .= '</ul></div>';
				}

				
				$dato['list'] = $list;

				/************************** *********************/
				//Obtener templates
				$templ = $this->temas_model->GetTemplatesById($TopicID);
				if($templ){
					$tablati = $this->encrypt->encode('topics_templates_items');
					$idti = $this->encrypt->encode('ItemID');
					$loads = '';
					$i = 1;
					$p = 0;
					foreach ($templ as $row) {

						//Para editar
						$EncryptItemID = $this->encrypt->encode($row->ItemID);

						$CategoryTemplateID = $row->CategoryTemplateID;
						$ctg = $this->temas_model->AboutCategory($CategoryTemplateID);

						$edt_item = '';
						//Los items
						$Items = '';
						if($row->Items!=""){
							$Items .= $row->Items;
						}else{
							$UrlFunctionTemplate = $row->UrlFunctionTemplate;
							$CategoryTemplateID = $row->CategoryTemplateID;
							$ItemID = $row->ItemID;

							$ctg = $this->temas_model->AboutCategory($CategoryTemplateID);
							if($ctg){
								
								$csbit = $this->temas_model->GetSubItemByItemID($ItemID);
								if($csbit){
									$arr = array();
									foreach ($csbit as $vl) {

										$sub_arr = array(
											'DocEntry' => $this->encrypt->encode($vl->DocEntry),
											'Type' => $vl->Type,
											'Valor' => $vl->Valor,
											'OrdenSection' => $row->Orden,
											'Orden' => $vl->Orden,
										);
										array_push($arr, $sub_arr);
									}
									$agrupar = groupArray($arr,'Orden');
									$Items .= $UrlFunctionTemplate($agrupar);

								}else{

									if($CategorySlug!='texto' && $CategorySlug!='divisiones'){
										
										$Items .= '

										';
									}else{
										$Items .= '<div class="">'.$row->TemplateItems.'</div>';
									}
								}
								
							}

						}
						$Items = stripslashes($Items);
						
						$loads .= 
						'
						<div class="col-xs-12 col-sm-12 col-lg-12 padding0">
							'.$Items.'
						</div>
						';

						$i++;
						$p++;
					}
				}else{

					$loads = '';
				}
				if($loads!=""){
					$dato['havecontent'] = 1;
				}else{
					$dato['havecontent'] = 0;
				}
				$dato['loads'] = $loads;

				//Salir de vista previa
				$salir = '';
				if($CourseProfesorID==$this->session->userdata('UserID')){
					$salir = '<a href="'.base_url().'temas/?t='.$this->input->get('l').'" class="btn btn-primary">Salir de vista previa</a>';
				}
				$dato['salir'] = $salir;
				$dato['NameTopic'] = $NameTopic;
				$dato['TopicID'] = $this->encrypt->encode($TopicID);

				$this->load->view('leccion-vista-alumnos',$dato);
			}
		}
	}
	function reporte(){
		$UserID = $this->input->get('u');
		$UserID = urlencode($UserID);
		$UserID = str_replace("+", "%2B",$UserID);
		$UserID = urldecode($UserID);
		$UserID = $this->encrypt->decode($UserID);

		$CourseID = $this->input->get('c');
		$CourseID = urlencode($CourseID);
		$CourseID = str_replace("+", "%2B",$CourseID);
		$CourseID = urldecode($CourseID);
		$CourseID = $this->encrypt->decode($CourseID);

		$us = $this->general_model->UserNC($UserID);
		if($us){
			$dato['FullName'] = $us[0]->FirstName.' '.$us[0]->LastName;

			$inf = $this->general_model->AboutMateria($CourseID);
			//print_r($inf);
			if($inf){
				$dato['CourseName'] = $inf[0]->CourseName;

				$gsc = $this->cursos_model->TopicsOnCourse($CourseID);
				$topics = '';

				$TotalTopics = 0;
				$OKFinished = 0;
				if($gsc){
					foreach ($gsc as $row) {
						$TopicID = $row->TopicID;
						//echo $us[0]->DocEntry."--".$TopicID."<br>";
						$avn = $this->cursos_model->AvanceByUser($us[0]->DocEntry,$TopicID);

						if($avn){
							//print_r($avn);
							if($avn[0]->DateFinished!="0000-00-00 00:00:00"){
								$topics .= '
								<h4 class="negro inlineb">'.$row->OrderTopic.'.- '.$row->NameTopic.'</h4> <h4 class="green inlineb"><i class="fa fa-check" aria-hidden="true"></i></h4><span> Completado</span>
								<hr>
								';
								$OKFinished++;
							}else{
								$topics .= '
								<h4 class="negro inlineb">'.$row->OrderTopic.'.- '.$row->NameTopic.'</h4> <h4 class="azul inlineb"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> '.$avn[0]->Porcent.'%</h4><span> </span>
								<hr>
								';
							}
						}else{
							$topics .= '
								<h4 class="negro inlineb">'.$row->OrderTopic.'.- '.$row->NameTopic.'</h4> <h4 class="gris inlineb"><i class="fa fa-clock-o" aria-hidden="true"></i></h4><span> Sin tomar</span>
								<hr>
								';
						}
						$TotalTopics++;
					}
					//
					$LastIteraction = $this->cursos_model->LastInteractionUser($us[0]->DocEntry);
					if($LastIteraction){
						$dato['Last'] = $LastIteraction;
					}else{
						$dato['Last'] = 'Sin interacción con el curso';
					}
					$AvanceAlumno = $OKFinished*100;
					$AvanceAlumno = $AvanceAlumno/$TotalTopics;
					$AvanceAlumno = number_format($AvanceAlumno,2);

					$dato['AvanceAlumno'] = $AvanceAlumno;
				}
				$dato['topics'] = $topics;
				
			}
			$this->load->view('nc/reporte-alumno',$dato);
			


		}
		
	}
	function reordentopics(){
		$arry_item = $_POST['arry_item'];
		$cont = 1;

		//print_r($arry_item);
		//echo "<br> total:".count($arry_item);
		
		foreach ($arry_item as $row) {
			$valor = $row['v'];
			$order_now = $row['o'];

			$data = array( 'OrderTopic' => $cont );
			$where = array('TopicID' => $valor );

			$update = $this->cursos_model->UpdateTopic($data,$where);
			$cont++;
		}
		if(count($arry_item)>=$cont){
			echo "success";
		}
		
	}
}