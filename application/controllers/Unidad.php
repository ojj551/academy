<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unidad extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('materias_model');
		$this->load->model('unidad_model');
		if ( ! $this->session->userdata('UserEmail')){ 
	        redirect(base_url());
	    }
	}
	public function index(){
		$SectionIDE = $this->input->get('u');

		$SectionID = urlencode($SectionIDE);
		$SectionID = str_replace("+", "%2B",$SectionID);
		$SectionID = urldecode($SectionID);
		$SectionID = $this->encrypt->decode($SectionID);

		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$dato['tabla'] = $this->encrypt->encode('section');
			$dato['id'] = $this->encrypt->encode('SectionID');

			$dato['UserTypeID'] = $usr[0]->UserTypeID;
			$dato['SectionID'] = $SectionIDE;
			$priv = $this->general_model->GetModuleByTypeUser($usr[0]->UserTypeID,'unidad',false);
			if($priv){
				$c = $this->unidad_model->AboutUnidad($SectionID);
				if($c){
					$dato['NameSection'] = $c[0]->NameSection;
					$this->load->view('section-single',$dato);
				}
			}else{
				redirect(base_url().'inicio');
			}
			
		}else{
			$this->session->sess_destroy();
	    	redirect(base_url());
		}
	}

}