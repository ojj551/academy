<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supervisorfunc extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('carreras_model');
		$this->load->model('materias_model');
		if (!$this->session->userdata('UserID')){ 
	        redirect(base_url());
	    }
	}
	//***************Carreras********************////
	function add(){
		$Lang = $this->Lang;

		$ctm = trim($this->input->post('ctm'));
		$CustomerID = urlencode($ctm);
		$CustomerID = str_replace("+", "%2B",$CustomerID);
		$CustomerID = urldecode($CustomerID);
		$CustomerID = $this->encrypt->decode($CustomerID);


		$CareerName = $this->input->post('name');
		$CareerTotalNivels = $this->input->post('leveldpto');


		$DocDate = date('Y-m-d H:i:s');
		$data = array(
			'DocDate' => $DocDate,
			'DocUserID' => $this->session->userdata('UserID'),
			'CareerName' => $CareerName,
			'CareerTotalNivels' => $CareerTotalNivels,
			'CustomerID' => $CustomerID
		);
		$insert = $this->carreras_model->AddDepto($CareerName,$CustomerID,$data);
		redirect(base_url().'supervisor/carreras');
	}
	//devolver datos de carrera
	function getcarr(){
		$CareerID = trim($this->input->post('cr'));
		$CareerID = urlencode($CareerID);
		$CareerID = str_replace("+", "%2B",$CareerID);
		$CareerID = urldecode($CareerID);
		$CareerID = $this->encrypt->decode($CareerID);

		//echo 'here: '.$CareerID;
		$uc = $this->carreras_model->AboutCareer($CareerID);
		if($uc){
			echo '
			<input type="hidden" name="cr" value="'.$this->encrypt->encode($CareerID).'">
	        <label>Nombre de la carrera</label>
	        <input class="form-control" type="text" name="name" value="'.$uc[0]->CareerName.'" requiered>
	        <label>Total niveles</label>
	        <input class="form-control" type="number" name="level" value="'.$uc[0]->CareerTotalNivels.'" requiered>
			';
		}

	}
	function update(){
		$CareerID = trim($this->input->post('cr'));
		$CareerID = urlencode($CareerID);
		$CareerID = str_replace("+", "%2B",$CareerID);
		$CareerID = urldecode($CareerID);
		$CareerID = $this->encrypt->decode($CareerID);

		$CareerName = trim($this->input->post('name'));
		$CareerTotalNivels = trim($this->input->post('level'));

		$data = array(
			'CareerName' => $CareerName,
			'CareerTotalNivels' => $CareerTotalNivels
		);
		$where = array('CareerID' => $CareerID);

		$upd = $this->carreras_model->UpdateCareer($where,$data);
		redirect(base_url().'supervisor/carreras');
	}
	/////////////////////MATERIAS/////////////////////
	public function addmat(){
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$ctm = trim($this->input->post('ctm'));
			$CustomerID = urlencode($ctm);
			$CustomerID = str_replace("+", "%2B",$CustomerID);
			$CustomerID = urldecode($CustomerID);
			$CustomerID = $this->encrypt->decode($CustomerID);
		
			$nombre = $this->input->post('nombre');
			$nombre = trim($nombre);
			$requerimientos = $this->input->post('requerimientos');
			$requerimientos = trim($requerimientos);
			$objetivos = $this->input->post('objetivos');
			$objetivos = trim($objetivos);

			$DocDate = date('Y-m-d H:i:s');
			$data = array(
				'DocDate' => $DocDate,
				'DocUserID' => $this->session->userdata('UserID'),
				'AsignaturaName' => $nombre,
				'AsignaturaRequirements' => $requerimientos,
				'AsignaturaGoals' => $objetivos,
				'CustomerID' => $CustomerID
			);
			//print_r($data);
			$insert = $this->materias_model->Add($nombre,$data);
			redirect(base_url().'supervisor/materias');
		}else{
			echo "error";
		}
	}
	function getasig(){
		$AsignaturaID = trim($this->input->post('asig'));
		$AsignaturaID = urlencode($AsignaturaID);
		$AsignaturaID = str_replace("+", "%2B",$AsignaturaID);
		$AsignaturaID = urldecode($AsignaturaID);
		$AsignaturaID = $this->encrypt->decode($AsignaturaID);

		//echo 'here: '.$CareerID;
		$uc = $this->materias_model->AsignaturaBYID($AsignaturaID);
		if($uc){
			echo '
			<input type="hidden" name="cr" value="'.$this->encrypt->encode($AsignaturaID).'">
	        <label class="tpg-relawey addmat">Nombre de la materia</label>
	        <input class="form-control" type="text" name="nombre" id="nombre" value="'.$uc[0]->AsignaturaName.'" required>

	        <label class="tpg-relawey addmat">Requerimientos</label>
	        <textarea class="form-control" name="requerimientos" id="requerimientos" style="min-height: 90px;">'.$uc[0]->AsignaturaRequirements.'</textarea>

	        <label class="tpg-relawey addmat">Objetivos</label>
	        <textarea class="form-control" name="objetivos" id="objetivos" style="min-height: 90px;">'.$uc[0]->AsignaturaGoals.'</textarea>
			';
		}

	}
	function updatemat(){
		$AsignaturaID = trim($this->input->post('cr'));
		$AsignaturaID = urlencode($AsignaturaID);
		$AsignaturaID = str_replace("+", "%2B",$AsignaturaID);
		$AsignaturaID = urldecode($AsignaturaID);
		$AsignaturaID = $this->encrypt->decode($AsignaturaID);

		$nombre = $this->input->post('nombre');
		$nombre = trim($nombre);
		$requerimientos = $this->input->post('requerimientos');
		$requerimientos = trim($requerimientos);
		$objetivos = $this->input->post('objetivos');
		$objetivos = trim($objetivos);

		$DocDate = date('Y-m-d H:i:s');
		$data = array(
			'AsignaturaName' => $nombre,
			'AsignaturaRequirements' => $requerimientos,
			'AsignaturaGoals' => $objetivos
		);


		$where = array('AsignaturaID' => $AsignaturaID);

		$upd = $this->materias_model->UpdateAsignatura($where,$data);
		redirect(base_url().'supervisor/materias');
	}
}

?>