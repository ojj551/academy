<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temasfunc extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('materias_model');
		$this->load->model('temas_model'); 
		$this->load->model('actividades_model');
		if ( ! $this->session->userdata('UserID')){ 
	        redirect(base_url());
	    }
	    $this->UserNC = 0; 
	    $usrnc = $this->general_model->UserNC($this->session->userdata('UserID'));
	    if($usrnc){
	    	$this->UserNC = 1; 
	    }
	}
	public function bytema(){
		$TopicE = $this->input->post('v');

		$TopicID = urlencode($TopicE);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);

		$c = $this->materias_model->AboutTopic($TopicID);
		if($c){
			//$dato['NameTopic'] = $c[0]->NameTopic;
			/*echo '
			<input class="form-control" type="text" id="nametopic-upd" value="'.$c[0]->NameTopic.'" />
			';*/
			echo $c[0]->NameTopic;
		}

		
	}
	function updatename(){
		$TopicE = $this->input->post('idtc');

		$TopicID = urlencode($TopicE);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);

		$NameTopic = $this->input->post('nombre');
		$NameTopic = trim($NameTopic);

		$SQL = "UPDATE bn_topics SET NameTopic='$NameTopic' WHERE TopicID='$TopicID'";
		$upt = $this->general_model->QUERYSUPT($SQL);
		echo "success";
	}
	function categorys(){
		$CategoryTemplateID = trim($this->input->post('ct'));
		$CategoryTemplateID = urlencode($CategoryTemplateID);
		$CategoryTemplateID = str_replace("+", "%2B",$CategoryTemplateID);
		$CategoryTemplateID = urldecode($CategoryTemplateID);
		$CategoryTemplateID = $this->encrypt->decode($CategoryTemplateID);

		if($CategoryTemplateID!=""){
			$templates = '';
			$tmc = $this->temas_model->GetTemplatesByCategory($CategoryTemplateID);
			if($tmc){
				foreach ($tmc as $row) {
					$encrypt = $this->encrypt->encode($row->TemplateID);
					$templates .= '
						<div style="padding-left:23px;padding-right:23px;padding-top: 10px;">
							<label style="font-family: sans-serif;">'.$row->TemplateName.'</label>
						</div>
						<img class="imgtemplate"  v="'.$encrypt.'" src="'.site_url('assets/'.$row->Thumbnail.'').'">
					';
				}
			}
			echo $templates;
		}
	}
	function template(){
		$TemplateID = trim($this->input->post('t'));
		$TemplateID = urlencode($TemplateID);
		$TemplateID = str_replace("+", "%2B",$TemplateID);
		$TemplateID = urldecode($TemplateID);
		$TemplateID = $this->encrypt->decode($TemplateID);

		$gt = $this->temas_model->GetTemplateByID($TemplateID);
		if($gt){
			echo $gt[0]->UrlFile;
		}
	}

	function savetext(){
		//Primer versión
		$files = $this->input->post('files');
		
		foreach ($files as $row) {
			$txt = $row['txt'];
			$vlr = $row['vlr'];

			$DocEntry = trim($vlr);
			$DocEntry = urlencode($DocEntry);
			$DocEntry = str_replace("+", "%2B",$DocEntry);
			$DocEntry = urldecode($DocEntry);
			$DocEntry = $this->encrypt->decode($DocEntry);

			$DocDate = date('Y-m-d H:i:s');
			//Update
			$dataupd = array(
				'Valor' => $txt,
				'DocUpdateDate' => $DocDate
			);
			//where
			$where = array(
				'DocEntry' => $DocEntry
			);
			
			$upd = $this->temas_model->UpdateSubItemsTemplate($where,$dataupd);

		}
		
	}
	function byitemid(){
		$files = $this->input->post('files');
		$ItemID = trim($files[0]['v']);
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);
		//Obtener info del usuario por template
		$tmc = $this->temas_model->GetItemByID($ItemID);
		$Form = '';
		if($tmc){
			//Obtener datos de template
			$tmp = $this->temas_model->GetTemplate($tmc[0]->TemplateID);
			if($tmp){
				$TemplateItems = $tmp[0]->TemplateItems;
			}
			$obj = json_decode($TemplateItems);
			$ItemsTemplate = array();
			foreach($obj as $entry){
			   $ItemsTemplate += array_keys(get_object_vars($entry));
			}
			
			//Recorrer array obtenido de post
			foreach ($files as $row ){
				
				//Recorrer items del template
				foreach($ItemsTemplate as $v){
					//print_r($row['etiqueta']);
					
					if($v==$row['etiqueta']){
						$Form .= '
							<label name="'.$v.'">'.$v.'</label>
							<textarea class="form-control" name="'.$v.'">'.$row['contenido'].'</textarea>
						';
					}
				}
			}
			
		}
		echo $Form;
	}
	//Agregar video
	function addvideo(){
		$url = trim($this->input->post('url'));

		$TopicID = trim($this->input->post('tpc'));
		$TopicID = urlencode($TopicID);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);

		$TemplateID = trim($this->input->post('tmpl'));
		$Orden = trim($this->input->post('ord'));
		$opc = $this->input->post('opc');

		$tam = trim($this->input->post('tam'));
		if($tam=='c'){
			$style = "position: relative;width:50%;height:300px;left:25%;";
			$width = '50%';
			$height = '300px';
		}
		if($tam=='m'){
			$style = "position: relative;width:70%;height:500px;left:15%;";
			$width = '75%';
			$height = '500px';
		}
		if($tam=='g'){
			$style = "position: relative;width:100%;height:900px;";
			$width = '100%';
			$height = '900px';
		}

		//Tipo de video
		$type = trim($this->input->post('type'));
		if($type==2){
			$expld = explode("?v=", $url);
			if(count($expld)>1){
				$embe = "https://www.youtube.com/embed/".$expld[1];
				$Items = '
				<iframe style="'.$style.'"
				src="'.$embe.'">
				</iframe>
				';
			}else{
				$Items = '
				<iframe style="'.$style.'"
				src="'.$url.'">
				</iframe>
				';
			}
		}
		if($type==3){
			$expld = explode("https://vimeo.com/", $url);
			if(count($expld)>1){
				$embe = "https://player.vimeo.com/video/".$expld[1];
				$Items = '
				<iframe style="'.$style.'"
				src="'.$embe.'">
				</iframe>
				';
			}else{
				$Items = '
				<iframe style="'.$style.'"
				src="'.$url.'">
				</iframe>
				';
			}
		}
		$Items = addslashes($Items);

		$DocDate = date('Y-m-d H:i:s');
		$data = array(
			'DocDate' => $DocDate,
			'DocUserID' => $this->session->userdata('UserID'),
			'TopicID' => $TopicID,
			'TemplateID' => $TemplateID,
			'Items' => $Items,
			'Orden' => $Orden
		);

		$isrt = $this->temas_model->InsertItemsTemplate($TopicID,$TemplateID,$Orden,$data);
		if($isrt){
			echo "success";
		}else{
			
			$DocDate = date('Y-m-d H:i:s');
			//Update
			$dataupd = array(
				'Items' => $Items,
				'DocUpdateDate' => $DocDate
			);
			//where
			$where = array(
				'TopicID' => $TopicID,
				'TemplateID' => $TemplateID,
				'Orden' => $Orden
			);
			$upd = $this->temas_model->UpdateItemsTemplate($where,$dataupd);
			echo "success";
		}
		
	}
	//Cambiar orden de items
	function change_orden(){
		$ItemID = trim($this->input->post('v'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		$CItemID = trim($this->input->post('oth'));
		$CItemID = urlencode($CItemID);
		$CItemID = str_replace("+", "%2B",$CItemID);
		$CItemID = urldecode($CItemID);
		$CItemID = $this->encrypt->decode($CItemID);

		$i = trim($this->input->post('i'));
		$typ = trim($this->input->post('typ'));

		$gt = $this->temas_model->GetItemByID($ItemID);
		if($gt){
			$gtchg = $this->temas_model->GetItemByID($CItemID);
			if($gtchg){
				$i_c = $gtchg[0]->Orden;
				/*
				echo "Cambiar 1. ".$ItemID." a posición: ".$i_c;
				echo "<br>";
				echo "Cambiar 2. ".$CItemID." a posición: ".$i;*/
				//Cambiar uno por el otro
				$SQLC1 = "UPDATE bn_topics_templates_items SET Orden=$i_c WHERE ItemID=$ItemID";
				$UPD1 = $this->general_model->QUERYSUPT($SQLC1);
				$SQLC2 = "UPDATE bn_topics_templates_items SET Orden=$i WHERE ItemID=$CItemID";
				$UPD2 = $this->general_model->QUERYSUPT($SQLC2);
			}
			
		}
		echo "success";
	}

	//Agregar template a mi lienzo
	function addtemplatelienzo(){
		$CItemID = trim($this->input->post('nxt'));
		$CItemID = urlencode($CItemID);
		$CItemID = str_replace("+", "%2B",$CItemID);
		$CItemID = urldecode($CItemID);
		$CItemID = $this->encrypt->decode($CItemID);

		$TemplateID = trim($this->input->post('tp'));
		$TemplateID = urlencode($TemplateID);
		$TemplateID = str_replace("+", "%2B",$TemplateID);
		$TemplateID = urldecode($TemplateID);
		$TemplateID = $this->encrypt->decode($TemplateID);

		$TopicID = trim($this->input->post('tpc'));
		$TopicID = urlencode($TopicID);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);

		
		//Total de sections en TopicID
		$SQL = "SELECT * FROM bn_topics_templates_items WHERE TopicID=$TopicID AND Cancelled=0";
		$tt = $this->general_model->QUERYS($SQL);
		if($tt){
			$or = count($tt)+1;
		}else{
			$or = 1;
		}
		//Datos del siguiente TemeID
		$gtchg = $this->temas_model->GetItemByID($CItemID);
		if($gtchg){
			$Orden = $gtchg[0]->Orden;
	
			$allnext = "SELECT * FROM `bn_topics_templates_items` WHERE TopicID=$TopicID AND Orden>=$Orden AND Cancelled=0";
			$sqlall = $this->general_model->QUERYS($allnext);
			if($sqlall){
				foreach ($sqlall as $row) {
					$MYItemID = $row->ItemID;
					$MYOrden = $row->Orden;
					$MYOrden = $MYOrden+1;

					$SQLC2 = "UPDATE bn_topics_templates_items SET Orden=$MYOrden WHERE ItemID=$MYItemID AND Cancelled=0";
					$UPD2 = $this->general_model->QUERYSUPT($SQLC2);
				}
			}
		}else{
			$Orden = $or;
		}

		$DocDate = date('Y-m-d H:i:s');
		$data = array(
			'DocDate' => $DocDate,
			'DocUserID' => $this->session->userdata('UserID'),
			'TopicID' => $TopicID,
			'TemplateID' => $TemplateID,
			'Items' => '',
			'Orden' => $Orden
		);
		
		$isrt = $this->temas_model->InsertItemsTemplate($TopicID,$TemplateID,$Orden,$data);
		echo "success";
	}
	//Salvar altualización de item
	function save_item(){
		$txt = trim($this->input->post('txt'));

		$ItemID = trim($this->input->post('v'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		$Items = addslashes($txt);
		$DocDate = date('Y-m-d H:i:s');
		//Update
		$dataupd = array(
			'Items' => $Items,
			'DocUpdateDate' => $DocDate
		);
		//where
		$where = array(
			'ItemID' => $ItemID
		);
		$upd = $this->temas_model->UpdateItemsTemplate($where,$dataupd);
		echo "success";

		//echo "TemplateID: ".$TemplateID."<br>".$txt;
	}
	//Regresar indo de edit
	function backinfoedit(){
		$ItemID = trim($this->input->post('v'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		$i = trim($this->input->post('i'));

		$CategoryTemplateID = trim($this->input->post('ct'));
		$CategoryTemplateID = urlencode($CategoryTemplateID);
		$CategoryTemplateID = str_replace("+", "%2B",$CategoryTemplateID);
		$CategoryTemplateID = urldecode($CategoryTemplateID);
		$CategoryTemplateID = $this->encrypt->decode($CategoryTemplateID);

		//Obtener info del template 
		$ifitms = $this->temas_model->GetItemByID($ItemID);
		if($ifitms){

			$TemplateID = $ifitms[0]->TemplateID;
			
			$gt = $this->temas_model->GetTemplate($TemplateID);
			if($gt){
				$UrlFile = $gt[0]->UrlFile;
				//
				if($gt[0]->TemplateID!="49"){
					//Obtener Categoria info
					$ctg = $this->temas_model->AboutCategory($CategoryTemplateID);
					if($ctg){
						$CategoryItems = $ctg[0]->CategoryItems;
						$CategorySlug = $ctg[0]->CategorySlug;

						$obj = json_decode($CategoryItems);
						
						$ItemsTemplate = array();
						foreach($obj as $entry){
						   $ItemsTemplate += array_keys(get_object_vars($entry));
						}

						
						$csbit = $this->temas_model->GetSubItemByItemID($ItemID);
						if($csbit){
							$nowi = $csbit[count($csbit)-1]->Orden;
						}else{
							$nowi = $i;
						}
						if($CategorySlug=='complementos'){
							if($TemplateID==24){
								$form = '
								<div class="divsave text-center">
									<a class="close-edit button-saveitems save-item-embed" v="'.$this->encrypt->encode($ItemID).'" i="'.$nowi.'" ct="'.$this->encrypt->encode($CategoryTemplateID).'"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
								</div>
								
								';
							}
						}
						
						if($CategorySlug=='interactivos'){
							if($TemplateID==42){
								$form = '
								<div class="text-center divsave">
									<a class="close-edit button-saveitems save-item-flash animate" v="'.$this->encrypt->encode($ItemID).'" i="'.$nowi.'" ct="'.$this->encrypt->encode($CategoryTemplateID).'" fr="true"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
								</div>
							';
							}else{
								$form = '

									<div class="text-center divsave">
										<a class="close-edit button-saveitems save-item-sg animate" v="'.$this->encrypt->encode($ItemID).'" i="'.$nowi.'" ct="'.$this->encrypt->encode($CategoryTemplateID).'" fr="false"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
									</div>
								';
							}
						}
						if($CategorySlug!='complementos' && $CategorySlug!='interactivos'){
							$form = '
								<div class="text-center divsave">
									<a class="close-edit button-saveitems save-item-sg animate" v="'.$this->encrypt->encode($ItemID).'" i="'.$nowi.'" ct="'.$this->encrypt->encode($CategoryTemplateID).'" fr="false"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
								</div>
							';
						}
						
						$form .= '
							<form id="itemseditados">';
						
						/*
						$form .= '
							<form id="itemseditados">
								<a class="getsettings" v="'.$this->encrypt->encode($ItemID).'" ct="'.$this->encrypt->encode($CategoryTemplateID).'">Ajustes <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>';
						*/
						if($csbit){
							$dlt = '';
							$arr = array();
							foreach ($csbit as $vl) {


								$subarray = array(
									'DocEntry' => $this->encrypt->encode($vl->DocEntry),
									'ItemID' => $vl->ItemID,
									'CategorySlug' => $vl->CategorySlug,
									'Type' => $vl->Type,
									'Valor' => $vl->Valor,
									'ValorType' => $vl->ValorType,
									'Orden' => $vl->Orden
								);
								array_push($arr, $subarray);
							}
							$agrupar = groupArray($arr,'Orden');
							$p = $UrlFile($agrupar);
							//print_r($p);
							$form .= $UrlFile($agrupar);
						}else{
							$dlt = '';
							$arr = array();
							$i = 1;
							foreach ($ItemsTemplate as $key => $row) {
								$subarray = array(
									'DocEntry' => '',
									'ItemID' => $ItemID,
									'CategorySlug' => $CategorySlug,
									'Type' => $row,
									'Valor' => '',
									'ValorType' => $obj[0]->$row,
									'Orden' => $i
								);
								array_push($arr, $subarray);
							}
							$agrupar = groupArray($arr,'Orden');
							$p = $UrlFile($agrupar);
							//print_r($p);
							$form .= $UrlFile($agrupar);
							
						}
						if($CategorySlug=='progreso' || $CategorySlug=='lineadeltiempo' || $CategorySlug=='listas' || $CategorySlug=='galerias' || $CategorySlug=='interactivos' || $CategorySlug=='texto_columns' || $CategorySlug=='actividades'){
							
							$form .= '
								<div class="col-xs-12 col-sm-12 col-lg-12 padding0 moreitems">

								</div>
							</form>
							<div class="col-xs-12 col-sm-12 col-lg-12 text-center" style="margin-top: 30px;">
								<button type="button" class="btn btn-info addotheritem" i="'.$nowi.'" style="background: #2196f3 !important;"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>
							</div>
							';
						}
						echo $form;
					}
				}else{
					$itm = $this->temas_model->GetItemByID($ItemID);
					if($itm){
						$TopicID = $itm[0]->TopicID;
						redirect(base_url().'materias/addexamen/?l='.$this->encrypt->encode($TopicID));
					}
					//echo "string";
					//print_r($itm);
					/*
					//consultar si ya hay un cuestionario
					$cv =  $this->actividades_model->GetQuizByItem($ItemID);
					$question_name = '';
					if($cv){
						$question_name = $cv[0]->QuestionName;
					}else{
						echo '
						<div class="col-xs-12 col-sm-12 col-lg-12 padding0">
							<input type="hidden" id="itm" value="'.$this->encrypt->encode($ItemID).'" /> 
		                    <label>Pregunta</label>
		                    <textarea class="form-control" name="pregunta" id="pregunta" style="border-left: 0px;border-right: 0px;border-top: 0px;">'.$question_name.'</textarea><br>
		                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0">
		                    	<div class="col-xs-10 col-sm-12 col-lg-10 padding0">
		                    		<label>Opción</label>
		                    		<input class="form-control options-actv" num="1" type="text" placeholder="Escribe opción" /><br>
		                    	</div>
		                    	<div class="col-xs-2 col-sm-12 col-lg-2 text-center" style="padding-top:25px;">
		                    		<input type="radio" name="awnswer" value="1" />
		                    	</div>
		                    </div>
		                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0">
		                    	<div class="col-xs-10 col-sm-12 col-lg-10 padding0" >
			                    	<label>Opción</label>
			                    	<input class="form-control options-actv" num="2" type="text" placeholder="Escribe opción" /><br>
			                    </div>
			                    <div class="col-xs-2 col-sm-12 col-lg-2 text-center" style="padding-top:25px;">
		                    		<input type="radio" name="awnswer" value="2" />
		                    	</div>
			                </div>
		                    <div class="paint-more-options"></div>
		                    <div class="text-center">
		                    	<a class="add-more-options">Agregar otra opción <i class="fa fa-plus" aria-hidden="true"></i></a>
		                    </div>
		                    
		                   
		                </div>
		                <div class="col-xs-12 col-sm-12 col-lg-12 padding0 moreitems">

						</div>

						<div class="text-right">
		                    	<div class="divsave text-center">
									<a class="close-edit button-saveitems save-questions" v="'.$this->encrypt->encode($ItemID).'" ct="'.$this->encrypt->encode($CategoryTemplateID).'"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
								</div>
		                    </div>
						';
					}
					*/
				}
			}
		}

		

	}
	function backadditems(){
		$ItemID = trim($this->input->post('v'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		$i = trim($this->input->post('i'));

		$CategoryTemplateID = trim($this->input->post('ct'));
		$CategoryTemplateID = urlencode($CategoryTemplateID);
		$CategoryTemplateID = str_replace("+", "%2B",$CategoryTemplateID);
		$CategoryTemplateID = urldecode($CategoryTemplateID);
		$CategoryTemplateID = $this->encrypt->decode($CategoryTemplateID);

		//Obtener info del template 
		$ifitms = $this->temas_model->GetItemByID($ItemID);
		if($ifitms){

			$TemplateID = $ifitms[0]->TemplateID;
			$gt = $this->temas_model->GetTemplate($TemplateID);
			if($gt){
				$UrlFile = $gt[0]->UrlFile;
				if($gt[0]->TemplateID!="49"){
					//
					$ctg = $this->temas_model->AboutCategory($CategoryTemplateID);
					if($ctg){
						$CategoryItems = $ctg[0]->CategoryItems;
						$CategorySlug = $ctg[0]->CategorySlug;

						$obj = json_decode($CategoryItems);
						
						$ItemsTemplate = array();
						foreach($obj as $entry){
						   $ItemsTemplate += array_keys(get_object_vars($entry));
						}

						$form = '<div class="col-xs-12 col-sm-12 col-lg-12 dvcat'.$i.'"><a class="dlt-item-print" v="'.$i.'"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';

						/*
						foreach ($ItemsTemplate as $row) {
							$input = $obj[0]->$row;
							$form .= $UrlFile($row,$input,$CategorySlug,$i,'');
							
						}*/
						$dlt = '';
						$arr = array();
		
						foreach ($ItemsTemplate as $key => $row) {
							$subarray = array(
								'DocEntry' => '',
								'ItemID' => $ItemID,
								'CategorySlug' => $CategorySlug,
								'Type' => $row,
								'Valor' => '',
								'ValorType' => $obj[0]->$row,
								'Orden' => $i
							);
							array_push($arr, $subarray);
						}
						$agrupar = groupArray($arr,'Orden');
						$p = $UrlFile($agrupar);
						//print_r($p);
						$form .= $UrlFile($agrupar);
						
						echo $form.'</div>';
					}
				}else{
					//consultar si ya hay un cuestionario
					$cv =  $this->actividades_model->GetQuizByItem($ItemID);
					$question_name = '';
					if($cv){
						$question_name = $cv[0]->QuestionName;
					}
					echo '
					<div class="col-xs-12 col-sm-12 col-lg-12 padding0">
						<input type="hidden" 
	                    <label>Pregunta</label>
	                    <textarea class="form-control" name="pregunta" id="pregunta" style="border-left: 0px;border-right: 0px;border-top: 0px;">'.$question_name.'</textarea><br>
	                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0">
	                    	<div class="col-xs-10 col-sm-12 col-lg-10 padding0">
	                    		<label>Opción</label>
	                    		<input class="form-control options-actv" num="1" type="text" placeholder="Escribe opción" /><br>
	                    	</div>
	                    	<div class="col-xs-2 col-sm-12 col-lg-2 text-center" style="padding-top:25px;">
	                    		<input type="radio" name="awnswer" value="1" />
	                    	</div>
	                    </div>
	                    <div class="col-xs-12 col-sm-12 col-lg-12 padding0">
	                    	<div class="col-xs-10 col-sm-12 col-lg-10 padding0" >
		                    	<label>Opción</label>
		                    	<input class="form-control options-actv" num="2" type="text" placeholder="Escribe opción" /><br>
		                    </div>
		                    <div class="col-xs-2 col-sm-12 col-lg-2 text-center" style="padding-top:25px;">
	                    		<input type="radio" name="awnswer" value="2" />
	                    	</div>
		                </div>
	                    <div class="paint-more-options"></div>
	                    <div class="text-center">
	                    	<a class="add-more-options">Agregar otra opción <i class="fa fa-plus" aria-hidden="true"></i></a>
	                    </div>
	                   
	                </div>
					';
				}
			}
		}
	}
	
	//Delete items
	function deletefatheritem(){
		$ItemID = trim($this->input->post('vl'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		$sql = $this->temas_model->GetSubItemByItemID($ItemID);
		if($sql){
			$c = 1;
			foreach ($sql as $row) {
				$DocEntry = $row->DocEntry;
				$SQL = "DELETE FROM bn_topics_templates_sub_items WHERE DocEntry='$DocEntry'";
				$upt = $this->general_model->QUERYSUPT($SQL);
				$c++;
			}
			
			$SQL2 = "DELETE FROM bn_topics_templates_items WHERE ItemID='$ItemID'";
			$upt2 = $this->general_model->QUERYSUPT($SQL2);
			
		}else{
			$SQL2 = "DELETE FROM bn_topics_templates_items WHERE ItemID='$ItemID'";
			$upt2 = $this->general_model->QUERYSUPT($SQL2);
		}
	}
	//Delete sub items
	function deleteitems(){
		$result = $_POST['result'];
		$datos = json_decode($result);
		foreach ($datos as $row) {
			if($row){
				$DocEntry = urlencode($row);
				$DocEntry = str_replace("+", "%2B",$DocEntry);
				$DocEntry = urldecode($DocEntry);
				$DocEntry = $this->encrypt->decode($DocEntry);
				if($DocEntry){
					//
					$abtm = $this->temas_model->AboutSubItem($DocEntry);
					if($abtm){
						//seleccionar direccion
						$ValorType = $abtm[0]->ValorType;
						if($ValorType=='file'){
							$Valor = $abtm[0]->Valor;

							$carpeta="../files/";
							unlink($carpeta.$Valor); 
						}
						$SQL = "DELETE FROM bn_topics_templates_sub_items WHERE DocEntry='$DocEntry'";
						$upt = $this->general_model->QUERYSUPT($SQL);
					}
				}
			}
		}

	}
	//save questions
	function save_question(){
		$files = $_POST['files'];

		foreach ($files as $row) {
			$QuestionName = $row['preg'];
			$QuestionAnswerID = $row['awns'];
			$QuestionName = $row['preg'];
		}
		//print_r($files);
	}
	//Save options files
	function save_item_options_file(){
		$tam = trim($this->input->post('tam'));
		$i = trim($this->input->post('i'));
		$fr = trim($this->input->post('fr'));

		$ItemID = trim($this->input->post('v'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		$CustomerID = $this->session->userdata('CustomerID');
		
		$CategoryTemplateID = trim($this->input->post('ct'));
		$CategoryTemplateID = urlencode($CategoryTemplateID);
		$CategoryTemplateID = str_replace("+", "%2B",$CategoryTemplateID);
		$CategoryTemplateID = urldecode($CategoryTemplateID);
		$CategoryTemplateID = $this->encrypt->decode($CategoryTemplateID);

		$ctg = $this->temas_model->AboutCategory($CategoryTemplateID);

		if($ctg){
		    $CustomerData = $this->general_model->AboutCustomerByID($CustomerID);
			$CategoryItems = $ctg[0]->CategoryItems;
			$CategorySlug = $ctg[0]->CategorySlug;

			$obj = json_decode($CategoryItems);

			$ItemsTemplate = array();
			foreach($obj as $entry){
			   $ItemsTemplate += array_keys(get_object_vars($entry));
			}
			
			$DocDate = date('Y-m-d H:i:s');
			$CategoryIDEncrypt = $this->encrypt->encode($CategoryTemplateID);
			$ItemIDEncrypt = $this->encrypt->encode($ItemID);
			for ($i=1; $i <= $tam; $i++) {
				$content = '';
				//print_r($ItemsTemplate);
				//Sin flash card
				foreach ($ItemsTemplate as $row) {
					$txt = '';
					if($row=='media' || $row=='imagen' || $row=='video' || $row=='scorm'){
						if(isset($_FILES[''.$row.$i.''])){
							$file = $_FILES[''.$row.$i.''];

							$DocUserID = $this->session->userdata('UserID');
							$target_dir = "../files/".md5($CustomerID.$CustomerData[0]->DocDateTimestamp)."/".mktime(1, 0, 0, 1, 1, date("Y"))/100;//el numero generado por mktime se multiplica por 100 y asi se obtiene la fecha
							//Crear
							if (!file_exists($target_dir)) {
							    mkdir($target_dir, 0777, true);
							}
							if(basename($file["type"])!='x-zip-compressed'){
								$target_file = $target_dir.'/'.md5($CustomerID.gmdate('U')).'.'.basename($file["type"]);
							}else{
								$target_file = $target_dir.'/'.md5($CustomerID.gmdate('U')).'.zip';
							}
							
							if (move_uploaded_file($file["tmp_name"], $target_file)){ 
								//para descomprimir archivos scorms
								if($CategorySlug=='scorms'){
									$zip = new ZipArchive;
									if ($zip->open($target_file) === TRUE) {
									    $zip->extractTo($target_dir.'/loads'.md5($CustomerID.gmdate('U')).'/');
									    $zip->close();
									    echo 'ok';
									} else {
									    echo 'failed';
									}
								}
							}
							//condiciones para mostrar archivo descomprimido
							if(basename($file["type"])!='x-zip-compressed'){
								$txt = md5($CustomerID.$CustomerData[0]->DocDateTimestamp)."/".(mktime(1, 0, 0, 1, 1, date("Y"))/100).'/'.md5($CustomerID.gmdate('U')).'.'.basename($file["type"]);
							}else{
								$txt = md5($CustomerID.$CustomerData[0]->DocDateTimestamp)."/".(mktime(1, 0, 0, 1, 1, date("Y"))/100).'/loads'.md5($CustomerID.gmdate('U')).'/';
							}
							
						}
					}else{
						$txt = trim($this->input->post(''.$row.$i.''));
					}

					if($txt!=""){
						$input = $obj[0]->$row;
						$Items = addslashes($txt);
						$data = array(
							'DocDate' => $DocDate,
							'DocUserID' => $this->session->userdata('UserID'),
							'ItemID' => $ItemID,
							'CategorySlug' => $CategorySlug,
							'Type' => $row,
							'Valor' => $Items,
							'ValorType' => $input,
							'Orden' => $i
						);
						
						$insrt = $this->temas_model->AddSubItems($ItemID,$CategorySlug,$row,$i,$data);
						if($insrt=='success'){
							echo "success";
						}else{
							if($row=='media'){
								if(!empty($_FILES[''.$row.$i.''])){
									//Actualizar
									$dataupd = array(
										'Valor' => $Items,
										'DocUpdateDate' => $DocDate
									);
									//where
									$where = array(
										'DocEntry' => $insrt
									);
									$upd = $this->temas_model->UpdateSubItemsTemplate($where,$dataupd);
								}
							}else{
								//Actualizar
								$dataupd = array(
									'Valor' => $Items,
									'DocUpdateDate' => $DocDate
								);
								//where
								$where = array(
									'DocEntry' => $insrt
								);
								$upd = $this->temas_model->UpdateSubItemsTemplate($where,$dataupd);
							}
						}
					}
				}
			}
		}
	}
	function save_item_flashcard(){
		$tam = trim($this->input->post('tam'));
		$ipd = trim($this->input->post('i'));
		$fr = trim($this->input->post('fr'));

		$ItemID = trim($this->input->post('v'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		
		$CategoryTemplateID = trim($this->input->post('ct'));
		$CategoryTemplateID = urlencode($CategoryTemplateID);
		$CategoryTemplateID = str_replace("+", "%2B",$CategoryTemplateID);
		$CategoryTemplateID = urldecode($CategoryTemplateID);
		$CategoryTemplateID = $this->encrypt->decode($CategoryTemplateID);


		$ctg = $this->temas_model->AboutCategory($CategoryTemplateID);

		if($ctg){

			$CategoryItems = $ctg[0]->CategoryItems;
			$CategorySlug = $ctg[0]->CategorySlug;

			$obj = json_decode($CategoryItems);

			$ItemsTemplate = array();
			foreach($obj as $entry){
			   $ItemsTemplate += array_keys(get_object_vars($entry));
			}
			
			$DocDate = date('Y-m-d H:i:s');
			$CategoryIDEncrypt = $this->encrypt->encode($CategoryTemplateID);
			$ItemIDEncrypt = $this->encrypt->encode($ItemID);
			$arryinput = array();
			for ($i=1; $i <= $tam; $i++) {

				$content = '';

				for ($ict=0; $ict < 2; $ict++) {

					
					if($ict==0){
		                $abrv = 'f';
		            }else{
		                $abrv = 'b';
		            }
		            $txt = '';
		            
		            //Textos
					if(isset($_POST[''.$abrv.'_t_'.$i.''])){
						array_push($arryinput, $abrv.'_t_'.$i);
					}
					//Multimedia
					if(isset($_FILES[''.$abrv.'_m_'.$i.''])){
						array_push($arryinput, $abrv.'_m_'.$i);
					}
				}
			}
			$arrtodas = array();
			foreach ($arryinput as $fila) {
				$expl = explode('_', $fila);
				$formar_multimedia = $expl[0].'_m_'.$expl[2];
				if($fila!=$formar_multimedia){
					array_push($arrtodas, $fila);
					array_push($arrtodas, $formar_multimedia);
				}
			}
			foreach ($arrtodas as $fila) {
				$expl = explode('_', $fila);
				//print_r($expl);
				if($expl[1]=='m'){
					$ValorType = 'multimedia';
					if(isset($_FILES[$fila])){
						$file = $_FILES[$fila];

						$DocUserID = $this->session->userdata('UserID');
						$rand = rand(1, 5); 
						$target_dir = "../files/".md5($CategoryTemplateID)."/".md5($ItemID);
						//Crear
						if (!file_exists($target_dir)) {
						    mkdir($target_dir, 0777, true);
						}
						$target_file = $target_dir.'/'.md5($DocUserID.$rand).'.'.basename($file["type"]);
						if (move_uploaded_file($file["tmp_name"], $target_file)){ 
							
						}
						$txt = md5($CategoryTemplateID)."/".md5($ItemID).'/'.md5($DocUserID.$rand).'.'.basename($file["type"]);

						
					}else{
						$txt = '';
					}
				}else{
					$ValorType = 'textarea';
					if(isset($_POST[$fila])){
						$txt = trim($this->input->post($fila));
					}
				}
				$data = array(
					'DocDate' => $DocDate,
					'DocUserID' => $this->session->userdata('UserID'),
					'ItemID' => $ItemID,
					'CategorySlug' => $CategorySlug,
					'Type' => $fila,
					'Valor' => $txt,
					'ValorType' => $ValorType,
					'Orden' => $tam
				);

				$insrt = $this->temas_model->AddSubItems($ItemID,$CategorySlug,$row,$i,$data);
				if($insrt=='success'){
					echo "success";
				}else{
					echo $insrt."-".$Items."<br>";
					if($row=='media'){
						if(!empty($_FILES[''.$row.$ict.$i.''])){
							//Actualizar
							$dataupd = array(
								'Valor' => $Items,
								'DocUpdateDate' => $DocDate
							);
							//where
							$where = array(
								'DocEntry' => $insrt
							);
							$upd = $this->temas_model->UpdateSubItemsTemplate($where,$dataupd);
						}
					}else{
						//Actualizar
						$dataupd = array(
							'Valor' => $Items,
							'DocUpdateDate' => $DocDate
						);
						//where
						$where = array(
							'DocEntry' => $insrt
						);
						$upd = $this->temas_model->UpdateSubItemsTemplate($where,$dataupd);
					}
				}
				
			}

		}
	}
	function update_item_flashcard(){

	}
	//Settings
	function settings(){
		$ItemID = trim($this->input->post('v'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		
		$CategoryTemplateID = trim($this->input->post('ct'));
		$CategoryTemplateID = urlencode($CategoryTemplateID);
		$CategoryTemplateID = str_replace("+", "%2B",$CategoryTemplateID);
		$CategoryTemplateID = urldecode($CategoryTemplateID);
		$CategoryTemplateID = $this->encrypt->decode($CategoryTemplateID);

		$git = $this->temas_model->GetItemByID($ItemID);

		if($git){

			$Settings = $git[0]->Settings;

			if($Settings==""){
				$TemplateID = $git[0]->TemplateID;
				$gt = $this->temas_model->AboutTemplate($TemplateID);
				if($gt){
					$Settings = $gt[0]->Settings;
				}
			}
			

			if($Settings!=""){
				//
				$stg = json_decode($Settings);
				$bg = $stg[0]->background;
	            $pdg = $stg[0]->padding;
	            $alg = $stg[0]->aling;
	            $clr = $stg[0]->color;
	            
	            $slc_aln = '';
	            if($alg=='left'){
	            	$slc_aln .= '<option value="left" selected>Izquierda</option>';
	            }else{$slc_aln .= '<option value="left">Izquierda</option>'; }
	            if($alg=='center'){
	            	$slc_aln .= '<option value="center" selected>Centrado</option>';
	            }else{$slc_aln .= '<option value="center">Centrado</option>'; }
	            if($alg=='right'){
	            	$slc_aln .= '<option value="right" selected>Derecha</option>';
	            }else{$slc_aln .= '<option value="right">Derecha</option>'; }
	            

	            //Pintar settings
	            echo '
	            <br>
	            <input type="hidden" value="'.$this->encrypt->encode($ItemID).'" name="itm" />
	            <label>Background</label><br>
	            <input class="form-control" type="color"  value="'.$bg.'" name="background" /><br>
	            
	            <label>Aling</label><br>
	            <select name="aling">
	            	'.$slc_aln.'
	            </select><br><br>
	            <label>Color de letra</label><br>
	            <input class="form-control" type="color"  value="'.$clr.'" name="color" /><br>
	            ';
			}

		}

		//echo "Item: ".$ItemID."<br>CategoriId: ".$CategoryTemplateID;
	}
	function addsettings(){
		$ItemID = trim($this->input->post('itm'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		$git = $this->temas_model->GetItemByID($ItemID);
		if($git){
			$TopicID = $git[0]->TopicID;

			$background = trim($this->input->post('background'));
			$padding = trim($this->input->post('padding'));
			$aling = trim($this->input->post('aling'));
			$color = trim($this->input->post('color'));

			if($background=='#000000'){$background='transparent';}

			$arry = array(
				'background' => $background,
				'padding' => $padding,
				'aling' => $aling,
				'color' => $color
			);
			$arr = array($arry);
			$encode = json_encode($arr);

			$data = array(
				'Settings' => $encode, 
			);
			$where = array('ItemID' => $ItemID);

			$update_tm = $this->temas_model->UpdateItemsTemplate($where,$data);


			redirect(base_url().'temas/?t='.$this->encrypt->encode($TopicID));
		}
	}
	//Save embed
	function save_embed(){
		$i = trim($this->input->post('i'));

		$ItemID = trim($this->input->post('v'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		
		$CategoryTemplateID = trim($this->input->post('ct'));
		$CategoryTemplateID = urlencode($CategoryTemplateID);
		$CategoryTemplateID = str_replace("+", "%2B",$CategoryTemplateID);
		$CategoryTemplateID = urldecode($CategoryTemplateID);
		$CategoryTemplateID = $this->encrypt->decode($CategoryTemplateID);

		$ctg = $this->temas_model->AboutCategory($CategoryTemplateID);
		if($ctg){
			$CategoryItems = $ctg[0]->CategoryItems;
			$CategorySlug = $ctg[0]->CategorySlug;

		}
		$iframe = trim($this->input->post('iframe'));
		$urlsave = trim($this->input->post('urlsave'));

		if($iframe!=""){
			$DocDate = date('Y-m-d H:i:s');
			$data = array(
				'DocDate' => $DocDate,
				'DocUserID' => $this->session->userdata('UserID'),
				'ItemID' => $ItemID,
				'CategorySlug' => $CategorySlug,
				'Type' => 'url',
				'Valor' => $urlsave,
				'ValorType' => 'text',
				'Orden' => $i
			);
			$insrt = $this->temas_model->AddSubItems($ItemID,$CategorySlug,'url',$i,$data);
			if($insrt=='success'){
				echo "success";
			}else{
				//Actualizar
				$dataupd = array(
					'Valor' => $urlsave,
					'DocUpdateDate' => $DocDate
				);
				//where
				$where = array(
					'DocEntry' => $insrt
				);
				$upd = $this->temas_model->UpdateSubItemsTemplate($where,$dataupd);
			}
			
			//Agregar codigo embebido
			$Items = addslashes($iframe);
			$DocDate = date('Y-m-d H:i:s');
			//Update
			$dataupd = array(
				'Items' => $Items,
				'DocUpdateDate' => $DocDate
			);
			//where
			$where = array(
				'ItemID' => $ItemID
			);
			$upd = $this->temas_model->UpdateItemsTemplate($where,$dataupd);
			echo "success";
		}
	}

	/*************ITEMS CON TRATO ESPECIAL***************/
	//puntos sensibles
	function puntos(){
		$ItemID = trim($this->input->post('v'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		$i = trim($this->input->post('i'));

		$form = '';

		$csbit = $this->temas_model->GetSubItemByItemID($ItemID);
		$array = array();
		if(!$csbit){
			$sbarray = array(
				'ItemID' => $this->input->post('v'),
				'Orden' => $i,
				'Position' => 'left:45%;top:10%;',
				'Title' => '',
				'Description' => '',
				'File' => ''
			);
			array_push($array, $sbarray);
			$agrupar = groupArray($array,'Orden');

			$fc = puntoschck('',$agrupar);

			$form .=$fc;
			
			$nowi = $i;
		}else{
			$nowi = $csbit[count($csbit)-1]->Orden;
		}

		$form .= '
			<div class="col-xs-12 col-sm-12 col-lg-12 padding0 moreitems">

			</div>
		</form>
		<div class="col-xs-12 col-sm-12 col-lg-12 text-center" style="margin-top: 30px;">
			<button type="button" class="btn btn-info addotheritem" i="'.$nowi.'" style="background: #2196f3 !important;"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>
		</div>
		';

		echo $form;
		
	}
	//Punto por punto
	function onlypunto(){

		$ItemID = trim($this->input->post('v'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);
		$Orden = trim($this->input->post('i'));

		$itm = $this->temas_model->GetSubItemByItemIDOrden($ItemID,$Orden);
		//echo $ItemID;
		if($itm){

			$title = ''; 
			$description = '';
			$video = '';
			$audio = '';
			$DocEntry = '';
			foreach ($itm as $row) {
				$DocEntry = $row->DocEntry;
				if($row->Type=='title'){
					$title = $row->Valor;
				}
				if($row->Type=='description'){
					$description = $row->Valor;
				}
				if($row->Type=='video'){
					$video = $row->Valor;
				}
				if($row->Type=='audio'){
					$audio = $row->Valor;
				}

			}
			//eliminar item completo
			$vn_ = $this->encrypt->encode($ItemID);
			$tabla_ = $this->encrypt->encode('topics_templates_sub_items');
			$id_ = $this->encrypt->encode('ItemID');

			//eliminar sub items
			$vn = $this->encrypt->encode($row->DocEntry);
			$tabla = $this->encrypt->encode('topics_templates_sub_items');
			$id = $this->encrypt->encode('DocEntry');
			
			//saber si multimedia tiene valores
			if($video!=""){
				$hv_video = '
				<b>Con video</b><br>
				<a class="dlt dlt-ml-tpl" t="'.$tabla.'" id="'.$id.'" vl="'.$vn.'"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				';
			}else{$hv_video = '';}
			if($audio!=""){
				$hv_audio = '
				<b>Con audio</b><br>
				<a class="dlt dlt-ml-tpl" t="'.$tabla.'" id="'.$id.'" vl="'.$vn.'"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				';
			}else{$hv_audio = '';}


			echo '
			<div class="col-lg-12 col-md-12 col-sm-12 text-right">
				<a class="dlt-punto" vl="'.$vn_.'" ord="'.$itm[0]->Orden.'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar punto</a>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<label>Título</label>
	       		<input type="text" class="form-control" id="titlep" name="title" value="'.$title.'" >
	        </div>
	        <div class="col-lg-12 col-md-12 col-sm-12">
	        	<label>Descripción</label>
	        	<textarea class="animate form-construnctor editor_edit" id="descriptionp" name="description" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px;" >'.$description.'</textarea>
	        </div>
	        <div class="col-lg-12 col-md-12 col-sm-12">
		        <div class="col-lg-10 col-md-10 col-sm-10 padding0">
		        	<label>Video</label>
		        	<input class="form-control" type="file" name="video" id="video"  />
		        </div>
		        <div class="col-lg-2 col-md-2 col-sm-2 text-center padding0 dv-dlt-ml-tpl">
		        	'.$hv_video.'
		        </div>
	        </div>
	        <div class="col-lg-12 col-md-12 col-sm-12">
		        <div class="col-lg-10 col-md-10 col-sm-10 padding0">
		        	<label>Audio</label>
		        	<input class="form-control" type="file" name="audio" id="audio"  />
		        </div>
		        <div class="col-lg-2 col-md-2 col-sm-2 text-center padding0 dv-dlt-ml-tpl">
		        	'.$hv_audio.'
		        </div>
	        </div>
	    
	        <div class="col-lg-12 col-md-12 col-sm-12 text-center dv-bttn-save">
	        	<button type="button" class="btn btn-primary update-point" v="'.$this->input->post('v').'">Guardar</button>
	        	
	        </div>
			';
		}else{
			echo '
			<form id="form-points">
				<label>Título</label>
		        <input type="text" class="form-control" id="titlep" name="title">
		        <label>Descripción</label>
		        <textarea class="animate form-construnctor editor_edit" id="descriptionp" name="description" style="min-height:120px;border-left: 0px;border-right: 0px;border-top: 0px;"></textarea><br>
		        <label>Video</label>
		        <input class="form-control" type="file" name="video" id="video" /><br>
		        <label>Audio</label>
	        	<input class="form-control" type="file" name="audio" id="audio"  /><br>
		        <div class="text-center">

		        	<button type="button" class="btn btn-primary save-point" v="'.$this->input->post('v').'">Guardar</button>
		        	
		        </div>
	        </form>
			';
		}
	}
	//save point
	function savepoint(){
		
		$DocEntry = trim($this->input->post('vl'));
		$DocEntry = urlencode($DocEntry);
		$DocEntry = str_replace("+", "%2B",$DocEntry);
		$DocEntry = urldecode($DocEntry);
		$DocEntry = $this->encrypt->decode($DocEntry);

		$ItemID = trim($this->input->post('itm'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		$title = trim($this->input->post('title'));
		$description = trim($this->input->post('description'));
		$pos = trim($this->input->post('pos'));
		//$tp = trim($this->input->post('tp'));
		$i = trim($this->input->post('i'));


		
		$DocDate = date('Y-m-d H:i:s');

		//Texto
		$datat = array(
			'DocDate' => $DocDate,
			'DocUserID' => $this->session->userdata('UserID'),
			'ItemID' => $ItemID,
			'CategorySlug' => 'complementos',
			'Type' => 'title',
			'Valor' => $title,
			'ValorType' => 'text',
			'Orden' => $i
		);
		$it = $this->temas_model->AddSubItems($ItemID,'complementos','title',$i,$datat);

		//Descripcion
		$datad = array(
			'DocDate' => $DocDate,
			'DocUserID' => $this->session->userdata('UserID'),
			'ItemID' => $ItemID,
			'CategorySlug' => 'complementos',
			'Type' => 'description',
			'Valor' => $description,
			'ValorType' => 'textarea',
			'Orden' => $i
		);
		$idsc = $this->temas_model->AddSubItems($ItemID,'complementos','description',$i,$datad);

		//Position
		$datap = array(
			'DocDate' => $DocDate,
			'DocUserID' => $this->session->userdata('UserID'),
			'ItemID' => $ItemID,
			'CategorySlug' => 'complementos',
			'Type' => 'position',
			'Valor' => $pos,
			'ValorType' => 'style',
			'Orden' => $i
		);
		$ips = $this->temas_model->AddSubItems($ItemID,'complementos','position',$i,$datap);

		
		///////////////////TIPO FILES
		//acerca del topic
		$abtm = $this->temas_model->GetItemByID($ItemID);
		if($abtm){
			$TemplateID = $abtm[0]->TemplateID;

			//acerca del template
			$abtp = $this->temas_model->AboutTemplate($TemplateID);
			if($abtp){
				$CategoryTemplateID = $abtp[0]->CategoryTemplateID;

				$DocUserID = $this->session->userdata('UserID');
				$target_dir = "../files/".md5($CategoryTemplateID)."/".md5($ItemID);
				//Crear
				if (!file_exists($target_dir)) {
				    mkdir($target_dir, 0777, true);
				}
				$file = '';
				if(isset($_FILES['video'])){
					$file = $_FILES['video'];
				}
				if(isset($_FILES['audio'])){
					$file = $_FILES['audio'];
				}
				$target_file = $target_dir.'/'.md5($DocUserID.$i).'.'.basename($file["type"]);
				
				
				if (move_uploaded_file($file["tmp_name"], $target_file)){
					$txt = md5($CategoryTemplateID)."/".md5($ItemID).'/'.md5($DocUserID.$i).'.'.basename($file["type"]);

					//Video
					if(isset($_FILES['video'])){
						$datavd = array(
							'DocDate' => $DocDate,
							'DocUserID' => $this->session->userdata('UserID'),
							'ItemID' => $ItemID,
							'CategorySlug' => 'complementos',
							'Type' => 'video',
							'Valor' => $txt,
							'ValorType' => 'file',
							'Orden' => $i
						);
						$ivd = $this->temas_model->AddSubItems($ItemID,'complementos','video',$i,$datavd);
					}
					if(isset($_FILES['audio'])){
						$datavd = array(
							'DocDate' => $DocDate,
							'DocUserID' => $this->session->userdata('UserID'),
							'ItemID' => $ItemID,
							'CategorySlug' => 'complementos',
							'Type' => 'audio',
							'Valor' => $txt,
							'ValorType' => 'file',
							'Orden' => $i
						);
						$ivd = $this->temas_model->AddSubItems($ItemID,'complementos','audio',$i,$datavd);
					}
					
				}
			}

		}
		
		
		


		
	}
	function updatepoint(){
		$ItemID = trim($this->input->post('itm'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		$Orden = trim($this->input->post('i'));
		$title = trim($this->input->post('title'));
		$description = trim($this->input->post('description'));
		$pos = trim($this->input->post('pos'));

		

		//Actualizar
		
		$DocDate = date('Y-m-d H:i:s');

		//Title
		$where1 = array(
			'ItemID' => $ItemID,
			'Orden' => $Orden,
			'Type' => 'title'
		);
		$updtitle = array(
			'Valor' => $title,
			'DocUpdateDate' => $DocDate
		);
		$upd1 = $this->temas_model->UpdateSubItemsTemplate($where1,$updtitle);

		//Description
		$where2 = array(
			'ItemID' => $ItemID,
			'Orden' => $Orden,
			'Type' => 'description'
		);
		$upddescription = array(
			'Valor' => $description,
			'DocUpdateDate' => $DocDate
		);
		$upd2 = $this->temas_model->UpdateSubItemsTemplate($where2,$upddescription);

		//Position
		$where3 = array(
			'ItemID' => $ItemID,
			'Orden' => $Orden,
			'Type' => 'position'
		);
		$updpostion = array(
			'Valor' => $pos,
			'DocUpdateDate' => $DocDate
		);
		$upd3 = $this->temas_model->UpdateSubItemsTemplate($where3,$updpostion);

		
		//////////////Tipos files
		//acerca del topic
		$abtm = $this->temas_model->GetItemByID($ItemID);
		if($abtm){
			$TemplateID = $abtm[0]->TemplateID;

			//acerca del template
			$abtp = $this->temas_model->AboutTemplate($TemplateID);
			if($abtp){
				$CategoryTemplateID = $abtp[0]->CategoryTemplateID;

				$DocUserID = $this->session->userdata('UserID');
				$target_dir = "../files/".md5($CategoryTemplateID)."/".md5($ItemID);
				//Crear
				if (!file_exists($target_dir)) {
				    mkdir($target_dir, 0777, true);
				}
				//file
				$file = '';
				if(isset($_FILES['video'])){
					$file = $_FILES['video'];
				}
				if(isset($_FILES['audio'])){
					$file = $_FILES['audio'];
				}
			
				$target_file = $target_dir.'/'.md5($DocUserID.$Orden).'.'.basename($file["type"]);
				
				
				if (move_uploaded_file($file["tmp_name"], $target_file)){
					$txt = md5($CategoryTemplateID)."/".md5($ItemID).'/'.md5($DocUserID.$Orden).'.'.basename($file["type"]);

					////////////////////Video///////////////////////

					//Para actualizar 
					if(isset($_FILES['video'])){
						$type = 'video';
					}
					if(isset($_FILES['audio'])){
						$type = 'audio';
					}
					$where4 = array(
						'ItemID' => $ItemID,
						'Orden' => $Orden,
						'Type' => $type
					);
					$updvdo = array(
						'Valor' => $txt,
						'DocUpdateDate' => $DocDate
					);
					//Para agregar si aún no esta
					$datavd = array(
						'DocDate' => $DocDate,
						'DocUserID' => $this->session->userdata('UserID'),
						'ItemID' => $ItemID,
						'CategorySlug' => 'complementos',
						'Type' => $type,
						'Valor' => $txt,
						'ValorType' => 'file',
						'Orden' => $Orden
					);

					$ivd = $this->temas_model->AddSubItems($ItemID,'complementos',$type,$Orden,$datavd);
					if($ivd!="success"){
						$upd4 = $this->temas_model->UpdateSubItemsTemplate($where4,$updvdo);
					}
					
				}
			}

		}
	}
	//SAVE imagen point
	function savebgpoint(){
		$ItemID = trim($this->input->post('vl'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		echo "hola";

		if(isset($_FILES['file'])){

			$file = $_FILES['file'];

			$DocUserID = $this->session->userdata('UserID');
			$rand = rand(1, 5); 
			$target_dir = "../files/".md5($CategoryTemplateID)."/".md5($ItemID);
			//Crear
			if (!file_exists($target_dir)) {
			    mkdir($target_dir, 0777, true);
			}
			$target_file = $target_dir.'/'.md5($DocUserID.$rand).'.'.basename($file["type"]);
			if (move_uploaded_file($file["tmp_name"], $target_file)){ 
				
			}
			$txt = md5($CategoryTemplateID)."/".md5($ItemID).'/'.md5($DocUserID.$rand).'.'.basename($file["type"]);



			$DocDate = date('Y-m-d H:i:s');

			//Texto
			$data = array(
				'DocDate' => $DocDate,
				'DocUserID' => $this->session->userdata('UserID'),
				'ItemID' => $ItemID,
				'CategorySlug' => 'complementos',
				'Type' => 'background',
				'Valor' => $txt,
				'ValorType' => 'text',
				'Orden' => 0
			);
			$it = $this->temas_model->AddSubItemsBg($ItemID,'complementos','background',$data);	
			if($it){
				//Title
				$where1 = array(
					'ItemID' => $ItemID,
					'Type' => 'background'
				);
				$updtitle = array(
					'Valor' => $txt,
					'DocUpdateDate' => $DocDate
				);
				$upd1 = $this->temas_model->UpdateSubItemsTemplate($where1,$updtitle);
			}	

		}
		
	}
	//eliminar point
	function delete_point(){
		$ItemID = trim($this->input->post('vl'));
		$ItemID = urlencode($ItemID);
		$ItemID = str_replace("+", "%2B",$ItemID);
		$ItemID = urldecode($ItemID);
		$ItemID = $this->encrypt->decode($ItemID);

		$Orden = trim($this->input->post('ord'));


		//if($ItemID=!"" && $Orden=!""){
			//echo "ItemID: ".$ItemID."<br>Orden: ".$Orden;
		$SQL1 = "UPDATE bn_topics_templates_sub_items SET Cancelled=1 WHERE ItemID='$ItemID' AND Orden='$Orden'";
		$upt1 = $this->general_model->QUERYSUPT($SQL1);
		echo "success";
			
		//}
	}
	/***PRUEBAS PORS MIENTRAS SUBIR TEMPLATES*/
	public function addtemplates(){
		$TemplateItems = '
		<div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
            	<h1>Slider 1</h1>
            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="swiper-slide">
            	<h1>Slider 2</h1>
            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="swiper-slide">
            	<h1>Slider 3</h1>
            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="swiper-slide">
            	<h1>Slider 4</h1>
            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
   
		';
		$TemplateItems = addslashes($TemplateItems);

		$DocDate = date('Y-m-d H:i:s');
		$data = array(
			'DocDate' => $DocDate,
			'DocUserID' => $this->session->userdata('UserID'),
			'CategoryTemplateID' => '3',
			'TemplateName' => 'Progreso versión 1',
			'UrlFile' => 'progress/?t=1',
			'TemplateItems' => $TemplateItems,
			'Orden' => '1',
			'Thumbnail' => 'templates/progress/progress_1.png',
			'UrlView' => 'progress_1'
		);

		$isrt = $this->temas_model->CreateTemplate($data);
		echo "success";
	}

	function avancetopic(){
		if($this->UserNC==1){
			$UserID = $this->session->userdata('DocEntry');
		}else{
			$UserID = $this->session->userdata('UserID');
		}
		$Porcent = trim($this->input->post('p'));

		$TopicID = trim($this->input->post('tpc'));
		$TopicID = urlencode($TopicID);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);

		$Porcent = round($Porcent, 0, PHP_ROUND_HALF_UP);

		$vsrt = $this->temas_model->TopicLoad($TopicID,$UserID);
		$DocDate = date('Y-m-d H:i:s');

		if($vsrt){
			echo "Esta BD";
			$AvanceID = $vsrt[0]->AvanceID;
			$DateFinished = $vsrt[0]->DateFinished;
			if($DateFinished!="0000-00-00 00:00:00"){
				echo "<br>Con fecha fin";
				$data = array(
					'DateLastInteraction' => $DocDate
				); 
			}else{
				echo "<br>Sin fecha fin";
				if($Porcent>90){
					echo "<br>Mayor a 90";
					$data = array(
						'Porcent' => $Porcent,
						'DateFinished' => $DocDate,
						'DateLastInteraction' => $DocDate
					);
				}else{
					echo "<br>Menor a 90";
					$data = array(
						'Porcent' => $Porcent,
						'DateLastInteraction' => $DocDate
					);
				}
			}
			$where = array('AvanceID' => $AvanceID);

			$updt = $this->temas_model->UpdateAvance($where,$data);
		}else{
			echo "NO ESTA EN BD";
			if($Porcent>90){
				echo "<br>Mayor a 90";
				$data = array(
					'DocDate' => $DocDate,
					'TopicID' => $TopicID,
					'UserID' => $UserID,
					'Porcent' => $Porcent,
					'DateFinished' => $DocDate,
					'DateLastInteraction' => $DocDate
				);
			}else{
				echo "<br>Menor a 90";
				$data = array(
					'DocDate' => $DocDate,
					'TopicID' => $TopicID,
					'UserID' => $UserID,
					'Porcent' => $Porcent,
					'DateLastInteraction' => $DocDate
				);
			}
			$add = $this->temas_model->InsertAvance($data);
		}
		//agregar porcentaje a suscripciones
		//1. consulta curso
		$cr = $this->temas_model->GetMyCourse($TopicID);
		if($cr){
			$sum = 0;
			$total = 0;
			$CourseID = $cr[0]->CourseID;
			$alltpcs = $this->temas_model->TopicsIntoCourse($CourseID);
			if($alltpcs){
				foreach ($alltpcs as $row) {
					$cns = $this->temas_model->TopicLoad($row->TopicID,$UserID);
					$sum = $sum+$cns[0]->Porcent; 
					$total++;
				}
			}
			$Porcentaje_curso = $sum/$total;
			$dtaupd = array('Progress' => $Porcentaje_curso );
			$whr = array('UserID' => $UserID, 'CourseID' => $CourseID);
			$upd_curso = $this->temas_model->UpdateAvanceCourse($dtaupd,$whr);
		}

		//print_r($data);
	}
}