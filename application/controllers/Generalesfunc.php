<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generalesfunc extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		if ( ! $this->session->userdata('UserID')){ 
	        redirect(base_url());
	    }
		
	}

	function delete(){
		$tE = $this->input->post('t');
		$tE = trim($tE);
		$tabla = urlencode($tE);
		$tabla = str_replace("+", "%2B",$tabla);
		$tabla = urldecode($tabla);
		$tabla = $this->encrypt->decode($tabla);

		$idE = $this->input->post('id');
		$idE = trim($idE);
		$id = urlencode($idE);
		$id = str_replace("+", "%2B",$id);
		$id = urldecode($id);
		$id = $this->encrypt->decode($id);

		$vlE = $this->input->post('vl');
		$vlE = trim($vlE);
		$vl = urlencode($vlE);
		$vl = str_replace("+", "%2B",$vl);
		$vl = urldecode($vl);
		$vl = $this->encrypt->decode($vl);

		$table = 'bn_'.$tabla;

		if($tabla=='responsable_user'){
		    $cnsl = "SELECT * FROM $table WHERE $id='$vl'";
			$cn = $this->general_model->QUERYS($cnsl);
			if($cn){
				$ResponsableID = $cn[0]->ResponsableID;
				$UserID = $cn[0]->UserID;

				$cnsl2 = "SELECT * FROM bn_courses AS T1,bn_user_suscriptions AS T2 WHERE T1.CourseProfesorID='$ResponsableID' AND T1.Cancelled=0 AND T2.UserID='$UserID' AND T2.Cancelled=0 AND T1.CourseID=T2.CourseID";
				$cn2 = $this->general_model->QUERYS($cnsl2);
				if($cn2){
					foreach ($cn2 as $row) {
						$SuscriptionID = $row->SuscriptionID;
						$SQL1 = "UPDATE bn_user_suscriptions SET Cancelled=1 WHERE SuscriptionID=$SuscriptionID";
						$upt1 = $this->general_model->QUERYSUPT($SQL1);
					}
				}
				/*
				$SQL1 = "UPDATE bn_user_suscriptions SET Cancelled=1 WHERE $id='$vl'";
				$upt1 = $this->general_model->QUERYSUPT($SQL1);
				//*/
				$SQL = "UPDATE $table SET Cancelled=1 WHERE $id='$vl'";
				$upt = $this->general_model->QUERYSUPT($SQL);
				echo "success";
				
			}
		}else{
            $SQL = "UPDATE $table SET Cancelled=1 WHERE $id='$vl'";
            $upt = $this->general_model->QUERYSUPT($SQL);
            echo "success";
        }
	}
	function updateform(){
		$tE = $this->input->post('t');
		$tE = trim($tE);
		$tabla = urlencode($tE);
		$tabla = str_replace("+", "%2B",$tabla);
		$tabla = urldecode($tabla);
		$tabla = $this->encrypt->decode($tabla);

		$idE = $this->input->post('id');
		$idE = trim($idE);
		$id = urlencode($idE);
		$id = str_replace("+", "%2B",$id);
		$id = urldecode($id);
		$id = $this->encrypt->decode($id);

		$vlE = $this->input->post('vl');
		$vlE = trim($vlE);
		$vl = urlencode($vlE);
		$vl = str_replace("+", "%2B",$vl);
		$vl = urldecode($vl);
		$vl = $this->encrypt->decode($vl);

		$table = 'bn_'.$tabla;

		$SQL = "SELECT * FROM $table WHERE $id='$vl' AND Cancelled=0";
		$gt = $this->general_model->QUERYS($SQL);
		
		//print_r($gt->object_property);
		foreach ($gt as $row) {
			print_r($row);
			/*
			$array = get_object_vars($row);
			print_r($array);*/
			/*
			echo '
			<input type="text" name="'.$.'"
			';*/
		}
	}
	function modaladduser(){
		$UserID = trim($this->input->get('v'));
		$UserID = urlencode($UserID);
		$UserID = str_replace("+", "%2B",$UserID);
		$UserID = urldecode($UserID);
		$UserID = $this->encrypt->decode($UserID);

		$thisuser = $this->general_model->AboutUser($UserID);
		if($thisuser){
			$UserTypeID = $thisuser[0]->UserTypeID;
			$sllgusertype = $this->general_model->GetSlugUserType($UserTypeID);
			if($sllgusertype){
				
			}
		}

		//echo $UserID."<br>".$UserTypeID;
	}
	//
	function saveimage(){
		$file = $_FILES['manualfile'];
		
		if($file["name"]!=""){
			$DocUserID = $this->session->userdata('UserID');
			$target_dir = "../file_academy/us/".md5($DocUserID)."/";
			if (!file_exists($target_dir)) {
			    mkdir($target_dir, 0777, true);
			}
			$target_file = $target_dir.'/'.basename($file["name"]);
			$DocDate = date('Y-m-d H:i:s');
			if (move_uploaded_file($file["tmp_name"], $target_file)){ 
				$data = array(
					'DocUpdateDate' => $DocDate,
					'UserAvatarIcon' => '',
					'UserAvatarImage' => basename($file["name"]),
					'FirstTime' => 1
				);
				$where = array('UserID' => $this->session->userdata('UserID'));

				$update = $this->general_model->UpdateUser($data,$where);
				if($update){
					redirect(base_url().'inicio');
				}
			}
		}
	}
	function myavatar(){
		$IDavatar = $this->input->post('IDavatar');

		$DocDate = date('Y-m-d H:i:s');
		$data = array(
			'DocUpdateDate' => $DocDate,
			'UserAvatarIcon' => $IDavatar,
			'UserAvatarImage' => '',
			'FirstTime' => 1
		);

		$where = array('UserID' => $this->session->userdata('UserID'));

		$update = $this->general_model->UpdateUser($data,$where);
		if($update){
			//$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
			//if($usr[0]->FirstTime)
			redirect(base_url().'inicio');
		}
	}
}