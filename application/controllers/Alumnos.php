<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumnos extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('alumnos_model'); 
		$this->load->model('institucion_model');
		$this->load->model('cursos_model');
		if ( ! $this->session->userdata('UserEmail')){ 
	        redirect(base_url());
	    }
	    $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$CustomerIDP = $usr[0]->CustomerID;
			$this->UserTypeID = $usr[0]->UserTypeID;
			$this->CustomerID = $usr[0]->CustomerID;
		}else{
			$this->session->sess_destroy();
	    	redirect(base_url());
		}
		$sllgusertype = $this->general_model->GetSlugUserType($usr[0]->UserTypeID);
		$this->UserTypeSlug = '';
		if($sllgusertype){
			$UserTypeSlug = $sllgusertype[0]->UserTypeSlug;
			$this->UserTypeSlug = $sllgusertype[0]->UserTypeSlug;
		}
		$this->Lang = 'es';
		$cm = $this->general_model->AboutCustomerByID($CustomerIDP);
		$this->LeadType = '';
		if($cm){
			$this->LeadType = $cm[0]->LeadType;
		}
		//Datos para formulario
		$this->FormCancel = $this->general_model->GetText($this->Lang,'','form.cancel');
		//
		$this->FormGuardar = $this->general_model->GetText($this->Lang,'','form.guardar');
		//
		$this->ModalAdd = $this->general_model->GetText($this->Lang,'','modal.add');

		$this->Depto = $this->general_model->GetText($this->Lang,$this->LeadType,'depto');
		$this->FormName = $this->general_model->GetText($this->Lang,'','form.nombre');
	}
	public function admin(){
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$dato['UserTypeID'] = $usr[0]->UserTypeID;
			$priv = $this->general_model->GetModuleByTypeUser($usr[0]->UserTypeID,'alumnos/admin',true);
			if($priv){
				//Vista de alumnos dependiendo del tipo de usuario
				//admin
				$mlns = $this->alumnos_model->MisAlumnos('all');

				$list = '';
				if($mlns){
					$tabla = $this->encrypt->encode('user_reference');
					$id = $this->encrypt->encode('DocEntry');
					foreach ($mlns as $row) {
						$DocEntryEncrypt = $this->encrypt->encode($row->DocEntry);
						if($row->UserID!=0){
							$udd = $this->general_model->AboutUser($row->UserID);
							if($udd){
								$nombre = $udd[0]->UserFirstName;
								$apellido = $udd[0]->UserLastName;

								$UserIDEncrypt = $this->encrypt->encode($udd[0]->UserID);

								if($udd[0]->ConfirmationDetaills!=""){
									$status = 'Registrado';
									$ver = '<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver alumno</a>';
								}
								if($udd[0]->ConfirmationDetaills==""){
									$status = 'Pendiente de activación';
									$ver = '<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver alumno</a>';
								}
							}else{
								$UserID = $udd[0]->UserID;
								$vs = "SELECT * FROM bn_users WHERE UserID='$UserID' AND Cancelled=1";
								$vw = $this->general_model->QUERYS($vs);
								if($vw){
									$status = 'Alumno eliminado';
								}
								$nombre = '';
								$apellido = '';
								//$status = '';
								$ver = '';
							}
						}else{
							$nombre = '-';
							$apellido = '-';
							$status = 'Pendiente por registrar';
							$ver = '<a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$DocEntryEncrypt.'" ><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a>';
						}
						$CustomerName = '';
						if($row->CustomerID!=""){
							$cn = $this->general_model->AboutCustomer($row->CustomerID);
							if($cn){

								$CustomerName = $cn[0]->CustomerName;
							}
						}
						$list .= '
						<tr>
							<td>'.$row->DocDate.'</td>
							<td>'.$row->Reference.'</td>
					        <td>'.$row->EmailReference.'</td>
					        <td>'.$nombre.'</td>
					        <td>'.$apellido.'</td>
					        <td>'.$CustomerName.'</td>
					        <td>'.$status.'</td>
					        <td>'.$ver.'</td>
					    </tr>
						';
					}
				}

				//Vista de alumnos dependiendo del tipo de usuario
				$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));

				$UserTypeID = '';
				$dato['listtype'] = '';
				if($usr){
					$UserTypeID = $usr[0]->UserTypeID;

					$sql = "Cancelled=0";
					$ty = $this->general_model->TypeUsers($sql);
					
					$listtype = '';
					if($ty){
						foreach ($ty as $row) {
							$UserTypeIDEncryp = $this->encrypt->encode($row->UserTypeID);
							$listtype .= 
							'
							<option value="'.$UserTypeIDEncryp.'">'.$row->UserTypeName.'</option>
							';
						}
					}
				}
				$dato['listtype'] = $listtype;

				//Vista de customers
				$ctm = $this->general_model->AllCustomer();
				$listclients = '';
				if($ctm){
					foreach ($ctm as $row) {
						$CustomerIDEncrypt = $this->encrypt->encode($row->CustomerID);
						$listclients .= '
						<option value="'.$CustomerIDEncrypt.'">'.$row->CustomerName.'</option>
						';
					}
				}
				$dato['listclients'] = $listclients;

				$dato['FormCancel'] = $this->FormCancel;
				$dato['FormGuardar'] = $this->FormGuardar;
				$dato['ModalAdd'] = $this->ModalAdd;

				$dato['list'] = $list;
				$this->load->view('alumnos-lista-admin',$dato);
			}else{
				redirect(base_url().'inicio');
			}
		}else{
			$this->session->sess_destroy();
	    	redirect(base_url());
		}
	}
	public function lista(){
		$t = trim($this->input->get('t'));
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		$UserTypeID = $usr[0]->UserTypeID;
		$sllgusertype = $this->general_model->GetSlugUserType($UserTypeID);
		if($sllgusertype){
			$UserTypeSlug = $sllgusertype[0]->UserTypeSlug;
			/////////////////////////////Supervisor///////////////////////////////////////
			//if($UserTypeSlug=='super'){
				$sqlparent = "Parent='$UserTypeID' AND Cancelled=0";
				$usrt = $this->general_model->TypeUsers($sqlparent);
				$alumnos = '';
				$dato['adduser'] = '';
				if($usrt){
					foreach ($usrt as $vl) {
						$UserTypeIDC = $vl->UserTypeID;
						//$alm = $this->alumnos_model->MisUsuarios($UserTypeIDC,$usr[0]->CustomerID);
						if($t==1){
							$alm = $this->alumnos_model->MyUsersCreates($usr[0]->UserID);
							$dato['adduser'] = '<a class="tpg-relawey add-curso-directo" data-toggle="modal" data-target="#addalumnos-form"><i class="fa fa-plus" aria-hidden="true"></i> Dar de alta un usuario</a>';
						}
						if($t==2){
							$UserTypeIDEncrypt = $this->encrypt->encode($UserTypeID);
							$UserEncrypPD = $this->encrypt->encode($this->session->userdata('UserID'));
							$alm = $this->alumnos_model->MyUsers($usr[0]->UserID);
							$dato['adduser'] = '<a class="tpg-relawey asiguser" typ="'.$UserTypeIDEncrypt.'" usrid="'.$UserEncrypPD.'"><i class="fa fa-plus" aria-hidden="true"></i> Asignarme un usuario</a>';
						}
						if($t!=1 && $t!=2){
							$alm = $this->alumnos_model->MyUsers('');
						}
						if($alm){
							foreach ($alm as $row) {
								$vluser = $this->general_model->AboutUser($row->UserID);
								if($t==1){
									if($vluser){
										if($vluser[0]->ConfirmationDetaills!=""){
											$stts = "Registrado y Activado";
										}else{
											$stts = "Registrado pero sin activar";
										}
										$UserIDEncrypt = $this->encrypt->encode($row->UserID);
										$alumnos .= '
										<tr>
									      	<td>'.$row->DocDate.'</td>
									      	<td>'.$row->Reference.'</td>
									        <td>'.$vluser[0]->UserFirstName.'</td>
											<td>'.$vluser[0]->UserLastName.'</td>
											<td>'.$vluser[0]->UserEmail.'</td>
									        <td></td>
									        <td>'.$stts.'</td>
									        <td>'.$ver = '<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver alumno</a>'.'</td>
									     </tr>
										';
										
									}else{
										$tabla = $this->encrypt->encode('user_reference');
										$id = $this->encrypt->encode('DocEntry');
										$DocEntryEncrypt = $this->encrypt->encode($row->DocEntry);
										$alumnos .= '
										<tr>
									      	<td>'.$row->DocDate.'</td>
									      	<td>'.$row->Reference.'</td>
									        <td></td>
									        <td></td>
									        <td></td>
									        <td></td>
									        <td>Sin registro por parte del usuario</td>
									        <td><a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$DocEntryEncrypt.'" ><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a></td>
									     </tr>
										';
									}
								}
								if($t==2){
									if($vluser){
										$UserIDEncrypt = $this->encrypt->encode($row->UserID);
										$ver = '<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver</a>';
										$alumnos .= '
										<tr>
											<td>'.$vluser[0]->DocDate.'</td>
											<td>'.$vluser[0]->UserFirstName.'</td>
											<td>'.$vluser[0]->UserLastName.'</td>
											<td>'.$vluser[0]->UserEmail.'</td>
											<td></td>
											<td>'.$ver.'</td>
										</tr>
										';
									}
								}
							}
						}
					}
					$dato['t'] = $t;
					$dato['alumnos'] = $alumnos;
					//Vista de alumnos dependiendo del tipo de usuario
					$dato['listtype'] = '';
					$sql = "UserTypeID!='4' AND UserTypeID!='$UserTypeID' AND Cancelled=0";
					$ty = $this->general_model->TypeUsers($sql);
					
					$listtype = '<option value="">Seleccionar...</option>';
					if($ty){
						foreach ($ty as $row) {
							$sbUserTypeID = $row->UserTypeID;
							$sqlparent = "UserTypeID='$sbUserTypeID' AND Cancelled=0";
							$usrt = $this->general_model->TypeUsers($sqlparent);
							if($usrt){
								$Parent = $usrt[0]->Parent;
								$sqlparent2 = "UserTypeID='$Parent' AND Cancelled=0";
								$usrt2 = $this->general_model->TypeUsers($sqlparent2);
								if($usrt2){
									$UserTypeIDEncryp = $this->encrypt->encode($row->UserTypeID);
									$listtype .= 
									'
									<option value="'.$UserTypeIDEncryp.'">'.$row->UserTypeName.'</option>
									';
								}
							}
						}
					}
					$dato['listtype'] = $listtype;

					$this->load->view('alumnos-lista',$dato);
				}
			//}
			/////////////////////////////Profesor///////////////////////////////////////
			/*
			if($UserTypeSlug=='profe'){
				echo "hey puto";
			}*/
		}
			
		
	}
	public function profesor(){
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$dato['UserTypeID'] = $usr[0]->UserTypeID;
			$priv = $this->general_model->GetModuleByTypeUser($usr[0]->UserTypeID,'profesor',true);
			if($priv){
				//Vista de alumnos dependiendo del tipo de usuario
				$mlns = $this->alumnos_model->MisAlumnos($this->session->userdata('UserID'));
				
				$list = '';
				if($mlns){
					foreach ($mlns as $row) {
						$DocEntryEncrypt = $this->encrypt->encode($row->DocEntry);
						if($row->UserID!=0){
							$udd = $this->general_model->AboutUser($row->UserID);
							if($udd){
								$nombre = $udd[0]->UserFirstName;
								$apellido = $udd[0]->UserLastName;

								$UserIDEncrypt = $this->encrypt->encode($udd[0]->UserID);

								if($udd[0]->ConfirmationDetaills!=""){
									$status = 'Registrado';
									$ver = '<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver alumno</a>';
								}
								if($udd[0]->ConfirmationDetaills==""){
									$status = 'Pendiente de activación';
									$ver = '<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver alumno</a>';
								}
							}else{
								$UserID = $udd[0]->UserID;
								$vs = "SELECT * FROM bn_users WHERE UserID='$UserID' AND Cancelled=1";
								$vw = $this->general_model->QUERYS($vs);
								if($vw){
									$status = 'Alumno eliminado';
								}
								$nombre = '';
								$apellido = '';
								//$status = '';
								$ver = '';
							}
						}else{
							$nombre = '-';
							$apellido = '-';
							$status = 'Pendiente por registrar';
							$ver = '<a class="dlt_reference" v="'.$DocEntryEncrypt.'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a>';
						}
						$list .= '
						<tr>
							<td>'.$row->DocDate.'</td>
							<td>'.$row->Reference.'</td>
					        <td>'.$row->EmailReference.'</td>
					        <td>'.$nombre.'</td>
					        <td>'.$apellido.'</td>
					        <td>'.$status.'</td>
					        <td>'.$ver.'</td>
					    </tr>
						';
					}
				}
				$dato['list'] = $list;
				//Vista de alumnos dependiendo del tipo de usuario
				$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));

				$UserTypeID = '';
				$dato['listtype'] = '';
				if($usr){
					$UserTypeID = $usr[0]->UserTypeID;

					$sql = "UserTypeName='Alumno' AND Cancelled=0";
					$ty = $this->general_model->TypeUsers($sql);
					
					$listtype = '';
					if($ty){
						foreach ($ty as $row) {
							$UserTypeIDEncryp = $this->encrypt->encode($row->UserTypeID);
							$listtype .= 
							'
							<option value="'.$UserTypeIDEncryp.'">'.$row->UserTypeName.'</option>
							';
						}
					}
				}
				$dato['listtype'] = $listtype;

				//Vista de carreras del prof
				$CustomerID = $usr[0]->CustomerID;
				$UserID = $this->session->userdata('UserID');
				$SQL = "SELECT * FROM bn_career AS T1, bn_groups AS T2 WHERE T1.CareerID=T2.CareerID AND T1.CustomerID='$CustomerID' AND T2.ProfesorID='$UserID' AND T2.Cancelled=0 GROUP BY T1.CareerID";
				//$SQL = "SELECT * FROM bn_career WHERE CustomerID='$CustomerID' AND Cancelled=0";
				$c = $this->general_model->QUERYS($SQL);

				$carr = '<option value="">Selecciona...</option>';
				if($c){
					foreach ($c as $row) {
						$CareerIDEncryp = $this->encrypt->encode($row->CareerID);
						$carr .= '
						<option value="'.$CareerIDEncryp.'">'.$row->CareerName.'</option>
						';
					}
				}
				$dato['carr'] = $carr;

				$this->load->view('alumnos-lista-profesor',$dato);
			}else{
				redirect(base_url().'inicio');
			}
		}else{
			$this->session->sess_destroy();
	    	redirect(base_url());
		}
	}
	public function supervisor(){
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		$UserTypeID = $usr[0]->UserTypeID;
		$sqlparent = "Parent='$UserTypeID' AND Cancelled=0";
		$usrt = $this->general_model->TypeUsers($sqlparent);
		$alumnos = '';
		if($usrt){
			foreach ($usrt as $vl) {
				$UserTypeIDC = $vl->UserTypeID;
				//echo $UserTypeID." ".$usr[0]->CustomerID;
				$alm = $this->alumnos_model->MisUsuarios($UserTypeIDC,$usr[0]->CustomerID);
				if($alm){
					foreach ($alm as $row) {
						$UserIDEncrypt = $this->encrypt->encode($row->UserID);
						$ver = '<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver</a>';
						$alumnos .= '
						<tr>
							<td>'.$row->DocDate.'</td>
							<td>'.$row->UserFirstName.'</td>
							<td>'.$row->UserLastName.'</td>
							<td>'.$row->UserEmail.'</td>
							<td></td>
							<td>'.$ver.'</td>
						</tr>
						';
					}
				}
				//print_r($alm);
			}
			$dato['alumnos'] = $alumnos;

			//Vista de alumnos dependiendo del tipo de usuario
			$dato['listtype'] = '';
				
			$sql = "UserTypeID!='4' AND UserTypeID!='$UserTypeID' AND Cancelled=0";
			$ty = $this->general_model->TypeUsers($sql);
			
			$listtype = '<option value="">Seleccionar...</option>';
			if($ty){
				foreach ($ty as $row) {
					$sbUserTypeID = $row->UserTypeID;
					$sqlparent = "UserTypeID='$sbUserTypeID' AND Cancelled=0";
					$usrt = $this->general_model->TypeUsers($sqlparent);
					if($usrt){
						$Parent = $usrt[0]->Parent;
						$sqlparent2 = "UserTypeID='$Parent' AND Cancelled=0";
						$usrt2 = $this->general_model->TypeUsers($sqlparent2);
						if($usrt2){
							$UserTypeIDEncryp = $this->encrypt->encode($row->UserTypeID);
							$listtype .= 
							'
							<option value="'.$UserTypeIDEncryp.'">'.$row->UserTypeName.'</option>
							';
						}
					}
				}
			}
			
			$dato['listtype'] = $listtype;

			$this->load->view('alumnos-lista',$dato);
		}
	}
	/*
	public function index(){
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$dato['UserTypeID'] = $usr[0]->UserTypeID;
			$priv = $this->general_model->GetModuleByTypeUser($usr[0]->UserTypeID,'alumnos',false);
			if($priv){
				//Vista de alumnos dependiendo del tipo de usuario
				//admin
				if($usr[0]->UserTypeID=="4"){
					$mlns = $this->alumnos_model->MisAlumnos('all');
				}
				//profersor
				if($usr[0]->UserTypeID=="1"){
					$mlns = $this->alumnos_model->MisAlumnos($this->session->userdata('UserID'));
				}

				$list = '';
				if($mlns){
					foreach ($mlns as $row) {
						$DocEntryEncrypt = $this->encrypt->encode($row->DocEntry);
						if($row->UserID!=0){
							$udd = $this->general_model->AboutUser($row->UserID);
							if($udd){
								$nombre = $udd[0]->UserFirstName;
								$apellido = $udd[0]->UserLastName;

								$UserIDEncrypt = $this->encrypt->encode($udd[0]->UserID);

								if($udd[0]->ConfirmationDetaills!=""){
									$status = 'Registrado';
									$ver = '<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver alumno</a>';
								}
								if($udd[0]->ConfirmationDetaills==""){
									$status = 'Pendiente de activación';
									$ver = '<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver alumno</a>';
								}
							}else{
								$UserID = $udd[0]->UserID;
								$vs = "SELECT * FROM bn_users WHERE UserID='$UserID' AND Cancelled=1";
								$vw = $this->general_model->QUERYS($vs);
								if($vw){
									$status = 'Alumno eliminado';
								}
								$nombre = '';
								$apellido = '';
								//$status = '';
								$ver = '';
							}
						}else{
							$nombre = '-';
							$apellido = '-';
							$status = 'Pendiente por registrar';
							$ver = '<a class="dlt_reference" v="'.$DocEntryEncrypt.'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a>';
						}
						$list .= '
						<tr>
							<td>'.$row->Reference.'</td>
					        <td>'.$row->EmailReference.'</td>
					        <td>'.$nombre.'</td>
					        <td>'.$apellido.'</td>
					        <td>'.$status.'</td>
					        <td>'.$ver.'</td>
					    </tr>
						';
					}
				}
				$dato['list'] = $list;
				$this->load->view('alumnos-lista',$dato);
			}else{
				redirect(base_url().'inicio');
			}
		}else{
			$this->session->sess_destroy();
	    	redirect(base_url());
		}
	}*/
	public function info(){
		$UserIDE = $this->input->get('a');
		$UserID = urlencode($UserIDE);
		$UserID = str_replace("+", "%2B",$UserID);
		$UserID = urldecode($UserID);
		$UserID = $this->encrypt->decode($UserID);
		$UserIDPadre = $UserID;

		$usrinf = $this->general_model->AboutUser($UserID);
		$UserTypeIDInf = '';
		if($usrinf){
			$UserTypeIDInf = $usrinf[0]->UserTypeID;
		}
		$UserEncryp = $this->encrypt->encode($UserID);
		$usr = $this->general_model->AboutUser($UserID);
		if($usr){
			$dato['UserTypeID'] = $usr[0]->UserTypeID;
			$alm = $this->general_model->AboutUser($UserID);
			if($alm){
				
				$priv = $this->general_model->GetModuleByTypeUser($usr[0]->UserTypeID,'info',true);
				//if($priv){
					$sllgusertype = $this->general_model->GetSlugUserType($UserTypeIDInf);
					if($sllgusertype){
						$UserTypeSlug = $sllgusertype[0]->UserTypeSlug;
						//echo $UserTypeSlug."<br>";

						if($UserTypeSlug=="profe"){
							$gsu = $this->alumnos_model->GetSuscriptionUserByProfesor($UserID,$this->session->userdata('UserID'));
						}else{
							$gsu = $this->alumnos_model->GetSuscriptionUserInfo($UserID,$usr[0]->CustomerID);
						}
						$cursos = '';
					
						if($gsu){
							foreach ($gsu as $row) {
								$cursos .= '
								<div>
									<a><i class="fa fa-circle-o" aria-hidden="true"></i> '.$row->CourseName.'</a> <i>Progreso <strong>'.$row->Progress.'</strong></i>
								</div>
								';
							}
						}
						//Materias para agregar
						//print_r($this->UserTypeSlug);
						if($this->UserTypeSlug=="profe"){
							$mp = $this->general_model->MisMateriasAdmin($this->session->userdata('UserID'));
						}else{
							$mp = $this->general_model->AllMaterias($usr[0]->CustomerID);
						}
						$materias = '';
						if($mp){
							foreach ($mp as $row) {
								$prf = "";
								if($usr[0]->UserTypeID!="1"){
									$ProfesorID = $row->CourseProfesorID;
									$sqprf = $this->general_model->AboutUser($ProfesorID);
									if($sqprf){
										$prf = $sqprf[0]->UserFirstName." ".$sqprf[0]->UserLastName;
									}
								}
								$CourseEncryp = $this->encrypt->encode($row->CourseID);
								$materias .= '
									<option value="'.$CourseEncryp.'">'.$row->CourseName.'</option>
								';
							}
						}
						$TableEncrypt = $this->encrypt->encode('users');
						$IDEncrypt = $this->encrypt->encode('UserID');
						
						$dato['TableEncrypt'] = $TableEncrypt;
						$dato['IDEncrypt'] = $IDEncrypt;
						$dato['ValueEncrypt'] = $UserEncryp;

						$dato['UserEncryp'] = $UserEncryp;
						$dato['materias'] = $materias;
						$dato['cursos'] = $cursos;
						$dato['Nombre'] = $alm[0]->UserFirstName;
						$dato['Apellido'] = $alm[0]->UserLastName;
						$dato['UserEmail'] = $alm[0]->UserEmail;


						/*********************Usuarios de Usuarios****************************/
						$asignar = '';
						//Obtener grupos a cargo
						//echo $UserID;
						$gp = $this->alumnos_model->MyUsers($UserID);
						$listmyuser = '';
						$total_alumnos = 0;
						if($gp){
							foreach ($gp as $row) {
								$UserIDSb = $row->UserID;
								$tablasub = $this->encrypt->encode('responsable_user');
								$idsub = $this->encrypt->encode('UnionID');
								$vlsub = $this->encrypt->encode($row->UnionID);
								$iur = $this->general_model->AboutUser($UserIDSb);
								if($iur){
									foreach ($iur as $vl) {
										$UserIDEncryptCH = $this->encrypt->encode($vl->UserID);

										if($UserTypeSlug=='super' || $UserTypeSlug=='profe'){
											if($UserTypeSlug=='profe'){
												if($this->UserTypeSlug=='profe'){
													$liga = '<a class="dlt" t="'.$tablasub.'" id="'.$idsub.'" vl="'.$vlsub.'">Quitar relación</a>';
												}else{
													$liga = '';
												}
											}else{
												$liga = '<a class="dlt" t="'.$tablasub.'" id="'.$idsub.'" vl="'.$vlsub.'">Quitar relación</a>';
											}
											$listmyuser .= '
											<tr>
												<td>
													'.$vl->DocDate.'
												</td>
												<td>
													'.$vl->UserFirstName.'
												</td>
												<td>
													'.$vl->UserLastName.'
												</td>
												<td>
													'.$vl->UserEmail.'
												</td>
												
												<td>'.$liga.'</td>
												
												<td>
													<a href="'.base_url().'alumnos/info/?a='.$UserIDEncryptCH.'">Ver</a>
												</td>
											</tr>
											';
										}
										if($UserTypeSlug=='alumn'){
											$listmyuser .= '
											<tr>
												<td>
													'.$vl->DocDate.'
												</td>
												<td>
													'.$vl->UserFirstName.' '.$vl->UserLastName.'
												</td>
												<td>
													'.$vl->UserEmail.'
												</td>
												<td>
													'.$vl->CareerID.'
												</td>
												<td>
													'.$vl->UserSemester.'
												</td>
												<td>
													<a href="'.base_url().'alumnos/info/?a='.$UserIDEncryptCH.'">Ver</a>
												</td>
											</tr>
											';
										}
										$total_alumnos++;
									}
								}
								
							}
						}
						$dato['listmyuser'] = $listmyuser;

						if($UserTypeSlug=='direc'){
							$asignar = 'Supervisor/Jefe de carrera';
						}
						$avance_g = 0;
						if($UserTypeSlug=='super'){
							$slgf = $this->alumnos_model->ProfesorAdvance($UserIDPadre);
							if($slgf){
								foreach ($slgf as $cr) {
									$crs = $this->alumnos_model->CourseAdvance($cr->CourseID);
									$avance_g = $avance_g+$crs[0]->Total;
								}
								$avance_g = $avance_g/count($slgf);
							}
							$asignar = 'Profesor';
						}
						$materias_list = "";
						$total_materias = 0;
						if($UserTypeSlug=='profe'){
							$mat = $this->alumnos_model->MatByProfesor($UserID,$usr[0]->CustomerID);
							if($mat){
								$tabla = $this->encrypt->encode('courses');
								$id = $this->encrypt->encode('CourseID');
								foreach ($mat as $m) {
									$crs = $this->alumnos_model->CourseAdvance($m->CourseID);
									$AVG = 0;
									if($crs){
										if($crs[0]->Total!=""){
											$AVG = $crs[0]->Total;
										}
									}
									$CourseEncrypt = $this->encrypt->encode($m->CourseID);
								
									$avance_g = $avance_g+$AVG;
									$materias_list .= '
									<div class="media">
			                            <div class="media-left">
			                            	<span class="icons" style="font-size:2em;"><i class="fa fa-book" aria-hidden="true" ></i></span>
			                            </div>
			                            <div class="media-body">
			                            	<div class="col-md-9" >
				                              	<h5 class="media-heading">'.$m->CourseName.' '.$AVG.'%</h5>
				                                <div class="progress progress-mini dline-b">
				                                  <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: '.$AVG.'%;">
				                                    <span class="sr-only">60% Complete</span>
				                                  </div>
				                                </div>
				                            </div>
				                            <div class="col-md-3" >
			                                	<a class="dlt dlcurso" t="'.$tabla.'" id="'.$id.'" vl="'.$CourseEncrypt.'" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
			                                </div>
			                            </div>
			                          </div>
									';
									$total_materias++;
								}
								$avance_g = $avance_g/count($mat);
							}else{
								$materias_list = "No cuenta con asignaturas este ciclo escolar";
							}
							
							$asignar = 'Alumnos';
						}
						if($UserTypeSlug=='alumn'){
							if($this->UserTypeSlug=='profe'){
								$mat = $this->alumnos_model->MatByUserAndProfe($UserID,$this->session->userdata('UserID'));
							}else{
								$mat = $this->alumnos_model->MatByUser($UserID);
							}
							if($mat){
								foreach ($mat as $m) {
									$CourseEncrypt = $this->encrypt->encode($m->CourseID);
									$avance_g = $avance_g+$m->Progress;
									$materias_list .= '
									<div class="media">
			                            <div class="media-left">
			                            	<span class="icons" style="font-size:2em;"><i class="fa fa-book" aria-hidden="true" ></i></span>
			                            </div>
			                            <div class="media-body">
			                              <h5 class="media-heading"><a href="'.base_url().'alumnos/reporte/?u='.$UserEncryp.'&c='.$CourseEncrypt.'">'.$m->CourseName.'</a> '.$m->Progress.'%</h5>
			                                <div class="progress progress-mini">
			                                  <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: '.$m->Progress.'%;">
			                                    <span class="sr-only">60% Complete</span>
			                                  </div>
			                                </div>
			                            </div>
			                          </div>
			                        ';
			                        $total_materias++;
								}
								$avance_g = $avance_g/count($mat);
							}else{
								$materias_list .= '<i>Sin cursos asignados al presente ciclo escolar</i>'; 
							}
						}

						$dato['materias_list'] = $materias_list;
						$dato['total_materias'] = $total_materias;
						//Buscar materias de la insitución
						$asignaturas = '';
						$crall = $this->institucion_model->AllAsignaturas($usr[0]->CustomerID);
						if($crall){
							foreach ($crall as $row) {
								$encrypt = $this->encrypt->encode($row->AsignaturaID);
								$asignaturas .= '
								<option value="'.$encrypt.'">'.$row->AsignaturaName.'</option>
								';
							}
						}
						$dato['asignaturas'] = $asignaturas;

						$dato['addmore'] = '
							<a class="tpg-relawey" data-toggle="modal" data-target="#asigme-form"><i class="fa fa-plus" aria-hidden="true"></i> Asignar '.$asignar.'</a>
							';
						$dato['UserTypeSlug'] = $UserTypeSlug;
						$dato['UserTypeSlugPadre'] = $this->UserTypeSlug;
						//Asignerme usuarios
						$people = '';
						
						$sl = $this->alumnos_model->UserUnder($UserTypeIDInf,$this->CustomerID);

						if($sl){
							foreach ($sl as $row) {
								$encrypt = $this->encrypt->encode($row->UserID);
								$people .= '
									<option value="'.$encrypt.'">'.$row->UserFirstName.' '.$row->UserLastName.'</option>
								';
							}
						}
						$dato['ctm']            = $this->encrypt->encode($this->CustomerID);
						$dato['subordinds']     = $people;
						$dato['usr']            = $UserEncryp;
						//Total de alumnos a su cargo
						$dato['total_alumnos']  = $total_alumnos;
						$dato['avance_g']       = $avance_g;
						$dato['falt_avance_g']  = 100-$avance_g;

						//Modal
						$dato['FormCancel']     = $this->FormCancel;
						$dato['FormGuardar']    = $this->FormGuardar;

						//Para borrar usuario
						$dato['tabla']  = $this->encrypt->encode('users');
						$dato['id']     = $this->encrypt->encode('UserID');

						if($this->UserTypeSlug=='super'){
							$this->load->view('supervisor/header-left'); 
						}
						if($this->UserTypeSlug=='profe'){
							$this->load->view('profesor/header-left'); 
						}

						if($UserTypeSlug=='super'){
							$this->load->view('alumnos-info-supervisor',$dato); 
						}
						if($UserTypeSlug=='admin'){
							echo "Aún no hay vista para esto";
						}
						if($UserTypeSlug=='profe'){
							$this->load->view('alumnos-info-profesor',$dato);
						}
						if($UserTypeSlug=='alumn'){
							$this->load->view('alumnos-info-alumno',$dato);
						}
					}
				//}
			}
		}else{
			redirect('/profesor/alumnos');
		}
		//echo $UserID;
	}

	function reporte(){
		$UserID = $this->input->get('u');
		$UserID = urlencode($UserID);
		$UserID = str_replace("+", "%2B",$UserID);
		$UserID = urldecode($UserID);
		$UserID = $this->encrypt->decode($UserID);

		$CourseID = $this->input->get('c');
		$CourseID = urlencode($CourseID);
		$CourseID = str_replace("+", "%2B",$CourseID);
		$CourseID = urldecode($CourseID);
		$CourseID = $this->encrypt->decode($CourseID);

		$us = $this->general_model->AboutUser($UserID);
		if($us){
			$dato['Last'] = '';
			$dato['FullName'] = $us[0]->UserFirstName.' '.$us[0]->UserLastName;

			$inf = $this->general_model->AboutMateria($CourseID);
			//print_r($inf);
			if($inf){
				$dato['CourseName'] = $inf[0]->CourseName;

				$gsc = $this->cursos_model->TopicsOnCourse($CourseID);
				$topics = '';

				$TotalTopics = 0;
				$OKFinished = 0;
				if($gsc){
					foreach ($gsc as $row) {
						$TopicID = $row->TopicID;
						//echo $us[0]->DocEntry."--".$TopicID."<br>";
						$avn = $this->cursos_model->AvanceByUser($us[0]->UserID,$TopicID);

						if($avn){
							//print_r($avn);
							if($avn[0]->DateFinished!="0000-00-00 00:00:00"){
								$topics .= '
								<h4 class="negro inlineb">'.$row->OrderTopic.'.- '.$row->NameTopic.'</h4> <h4 class="green inlineb"><i class="fa fa-check" aria-hidden="true"></i></h4><span> Completado</span>
								<hr>
								';
								$OKFinished++;
							}else{
								$topics .= '
								<h4 class="negro inlineb">'.$row->OrderTopic.'.- '.$row->NameTopic.'</h4> <h4 class="azul inlineb"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> '.$avn[0]->Porcent.'%</h4><span> </span>
								<hr>
								';
							}
						}else{
							$topics .= '
								<h4 class="negro inlineb">'.$row->OrderTopic.'.- '.$row->NameTopic.'</h4> <h4 class="gris inlineb"><i class="fa fa-clock-o" aria-hidden="true"></i></h4><span> Sin tomar</span>
								<hr>
								';
						}
						$TotalTopics++;
					}
					//
					$LastIteraction = $this->cursos_model->LastInteractionUser($us[0]->UserID);
					if($LastIteraction){
						$dato['Last'] = $LastIteraction;
					}else{
						$dato['Last'] = 'Sin interacción con el curso';
					}
					$AvanceAlumno = $OKFinished*100;
					$AvanceAlumno = $AvanceAlumno/$TotalTopics;
					$AvanceAlumno = number_format($AvanceAlumno,2);

					$dato['AvanceAlumno'] = $AvanceAlumno;
				}
				$dato['topics'] = $topics;
				
			}
			$this->load->view('profesor/reporte-alumno',$dato);
			


		}
	}

}
?>