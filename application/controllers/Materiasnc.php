<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materiasnc extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('materias_model');
		$this->load->model('temas_model');
		if ( ! $this->session->userdata('UserEmail')){ 
	        redirect(base_url());
	    }
	    $usr = $this->general_model->UserNC($this->session->userdata('UserID'));
	    $this->UserThings = $usr;
	    $this->CustomerID = '';
		if($usr){
			$CustomerIDP = $usr[0]->CustomerID;
			$this->CustomerID = $usr[0]->CustomerID;
		}

	}

	public function adminp1(){
		$MateriaIDE = $this->input->get('m');

		$MateriaID = urlencode($MateriaIDE);
		$MateriaID = str_replace("+", "%2B",$MateriaID);
		$MateriaID = urldecode($MateriaID);
		$MateriaID = $this->encrypt->decode($MateriaID);

		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$inf = $this->general_model->AboutMateria($MateriaID);
			if($inf){
				$CourseName = $inf[0]->CourseName;
				$dato['CourseName'] = $CourseName;
				$dato['AsignaturaRequirements'] = $inf[0]->CourseRequirements;
				$dato['AsignaturaGoals'] = $inf[0]->CourseGoals;
				$dato['UserTypeID'] = $usr[0]->UserTypeID;
				$dato['MateriaIDEncryp'] =$this->encrypt->encode($MateriaID);
				$priv = $this->general_model->GetModuleByTypeUser($usr[0]->UserTypeID,'materias',false);
				if($priv){

					$this->load->view('materias-steps1',$dato);
				}else{
					redirect(base_url().'inicio');
				}
			}
		}else{
			$this->session->sess_destroy();
	    	redirect(base_url());
		}

	}
	public function adminp2(){
		$MateriaIDE = $this->input->get('m');

		$MateriaID = urlencode($MateriaIDE);
		$MateriaID = str_replace("+", "%2B",$MateriaID);
		$MateriaID = urldecode($MateriaID);
		$MateriaID = $this->encrypt->decode($MateriaID);

		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$inf = $this->general_model->AboutMateria($MateriaID);
			if($inf){
				$CourseName = $inf[0]->CourseName;
				$dato['CourseName'] = $CourseName;
				$dato['UserTypeID'] = $usr[0]->UserTypeID;
				$dato['MateriaIDEncryp'] =$this->encrypt->encode($MateriaID);
				$priv = $this->general_model->GetModuleByTypeUser($usr[0]->UserTypeID,'materias',false);
				if($priv){

					$this->load->view('materias-steps',$dato);
				}else{
					redirect(base_url().'inicio');
				}
			}
		}else{
			$this->session->sess_destroy();
	    	redirect(base_url());
		}

	}

	public function prevista(){
		$MateriaIDE = $this->input->get('m');

		$MateriaID = urlencode($MateriaIDE);
		$MateriaID = str_replace("+", "%2B",$MateriaID);
		$MateriaID = urldecode($MateriaID);
		$MateriaID = $this->encrypt->decode($MateriaID);

		$usr = $this->general_model->UserNC($this->session->userdata('UserID'));
		$dato['MateriaIDEncryp'] = '';
		
		$inf = $this->general_model->AboutMateria($MateriaID);
		if($inf){
			$CourseName = $inf[0]->CourseName;
			$MateriaIDEncryp = $this->encrypt->encode($MateriaID);
			$dato['CourseName'] = $CourseName;
			$dato['MateriaIDEncryp'] = $MateriaIDEncryp;
			$dato['UserTypeID'] = $usr[0]->UserTypeID;
			$dato['Inc'] = $inf[0]->CourseStar;
			$dato['Fin'] = $inf[0]->CourseEnd;
			$dato['Banner'] = $inf[0]->CourseImage;
			$dato['CourseRequirements'] = $inf[0]->CourseRequirements;
			$dato['CourseGoals'] = $inf[0]->CourseGoals;

			//Obtener las unidades y los topics
			$gsc = $this->materias_model->GetSection($this->session->userdata('UserID'),$MateriaID);
			$list = '';
			if($gsc){
				$tabla = $this->encrypt->encode('topics');
				$id = $this->encrypt->encode('TopicID');
				foreach ($gsc as $vl) {
					$tablasc = $this->encrypt->encode('section');
					$idsc = $this->encrypt->encode('SectionID');

					$TopicIDEncryptUnidad = $this->encrypt->encode($vl->SectionID);
					$head = '
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<h4 class="tpg-relawey"><strong>Unidad '.$vl->OrderSection.',</strong> <a href="'.base_url().'unidad/?u='.$TopicIDEncryptUnidad.'">'.$vl->NameSection.'</a> <a class="addtm" v="'.$TopicIDEncryptUnidad.'" style="font-size: 9px;">Agregar tema</a> <a class="dlt" t="'.$tablasc.'" id="'.$idsc.'" vl="'.$TopicIDEncryptUnidad.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a></h4>
						</div>
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="col-xs-12 col-sm-12 col-lg-12">
						';
					/*
					$gut = $this->materias_model->GetSectionTopics($vl->SectionID,false);
					if($gut){
						foreach ($gut as $row) {
							//echo $row->TopicID."<br>";
							$fc = $this->Get_childrens($row->TopicID);
							foreach ($fc as $vl) {
								$tpc = $this->materias_model->AboutTopic($vl['Padre']);
								if($tpc){

								}
								print_r($vl['Padre']);
							}
							//print_r($fc);
						}
					}*/
					$gut = $this->materias_model->GetSectionTopics($vl->SectionID,false);
					$gutsb = $this->materias_model->GetSectionTopics($vl->SectionID,true);
					$topics = '';
					//Cascada 1
					if($gut){
						
						foreach ($gut as $row) {
							$fc = $this->Get_childrens($row->TopicID);
							foreach ($fc as $k => $vl) {
								$abt = $this->materias_model->AboutTopic($k);
								//print_r($abt);
							}
							//echo count($fc)."<br>";
							
							/*
							if(count($fc)>1){

								foreach ($fc as $key => $value) {
									echo "Si tengo hijos :) :".$value[$key]."<br>";
								}
							}else{
								foreach ($fc as $key => $value) {
									echo "No tengo hijos :( :".$key."<br>";
								}
								//print_r($fc);
								
							}*/
							
							$childtopics = '<div class="col-xs-12 col-sm-12 col-lg-12">';
							//Cascada 2
							if($gutsb){
								foreach ($gutsb as $sb) {
									if($sb->Parent==$row->TopicID){
										$sbchildtopics = '<div class="col-xs-12 col-sm-12 col-lg-12">';
										//Cascada 3
										if($gutsb){
											foreach ($gutsb as $sbsb) {

												$sbsbchildtopics = '<div class="col-xs-12 col-sm-12 col-lg-12">';
												//cascada 4

												if($gutsb){
													foreach ($gutsb as $sbsbsb) {
														if($sbsbsb->Parent==$sbsb->TopicID){
															$TopicIDEncryptSbSbSb = $this->encrypt->encode($sbsbsb->TopicID);
															$sbchildtopics .= '
															<a href="'.base_url().'temas/?t='.$TopicIDEncryptSbSbSb.'" style="font-size: 20px;"><strong>'.$sbsbsb->OrderTopic.'</strong>.- '.$sbsbsb->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncryptSbSbSb.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncryptSbSbSb.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>';

														}
														
													}
												}
												$sbsbchildtopics .= '</div>';
												if($sbsb->Parent==$sb->TopicID){
													$TopicIDEncryptSbSb = $this->encrypt->encode($sbsb->TopicID);
													$sbchildtopics .= '
													<a href="'.base_url().'temas/?t='.$TopicIDEncryptSbSb.'" style="font-size: 20px;"><strong>'.$sbsb->OrderTopic.'</strong>.- '.$sbsb->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncryptSbSb.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncryptSbSb.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>'.$sbsbchildtopics;

												}
											}
										}

										$sbchildtopics .= '</div>';
										$TopicIDEncryptSb = $this->encrypt->encode($sb->TopicID);
										$childtopics .= '
										<a href="'.base_url().'temas/?t='.$TopicIDEncryptSb.'" style="font-size: 20px;"><strong>'.$sb->OrderTopic.'</strong>.- '.$sb->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncryptSb.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncryptSb.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>'.$sbchildtopics;

									}
								}
							}
							
							$childtopics .= '</div>';
							$TopicIDEncrypt = $this->encrypt->encode($row->TopicID);
							
							$topics .= '
								<a href="'.base_url().'temas/?t='.$TopicIDEncrypt.'" style="font-size: 20px;"><strong>'.$row->OrderTopic.'</strong>.- '.$row->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncrypt.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncrypt.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>
								'.$childtopics.'
							';
						}
					}
					$footer = '</div></div>';
					$list .= $head.$topics.$footer;
				}
			}
			
			$dato['list'] = $list;
			$btn_exam = ''; $btn_preview = '';
			//Tiene ya examen?
			if($gsc){
				$exm = $this->materias_model->CourseExam($MateriaID);
				if($exm){
					$ExamEncryt = $this->encrypt->encode($exm[0]->QuizID);
					$btn_exam = '<a class="btn btn-primary" href="'.base_url().'materias/examen/?ex='.$ExamEncryt.'">Ver exámen</a>';
				}else{
					$btn_exam = '<a class="btn btn-primary" href="'.base_url().'materias/examen/?c='.$MateriaIDEncryp.'">Crear exámen del curso</a>';
				}
				//Vista previa
				$btn_preview = '
				<a href="'.base_url().'materias/vista/?c='.$MateriaIDEncryp.'">
					<button class="btn ripple-infinite btn-gradient btn-info">
				        <div>
				          Vista previa
				        </div>
				    </button>
			    </a><br><br>
				';
			}
			$dato['btn_exam'] = $btn_exam;
			$dato['btn_preview'] = $btn_preview;
			//if($usr[0]->UserTypeID=='1'){
				$this->load->view('materias-admin',$dato);
			//}else{
			//	redirect(base_url().'inicio');
			//}
		}
		

	}
	function Get_childrens($pattern){
		$hijos = $this->materias_model->BD_gethijos($pattern);
		$array_ch = array();
		if($hijos) { //Existen hijos
		
			foreach($hijos as $h){
				$rh = $this->Get_childrens($h->TopicID);
				$array_sb = array();
				if ($rh){ //NO tiene hijos

					$array_sb[$pattern]['chld'] = $rh;
					//$res[$pattern][$h->TopicID] = $rh;
					//$res = 'con hijo';
				}else{
					$array_sb[$pattern][$h->TopicID] = '';
					//$res = $hijos;
					//$res[$pattern][$h->TopicID] = $rh;
				}
				array_push($array_ch, $array_sb);
				//$res = $rh;
			}
		}else { //NO existen
			$array_ch[$pattern][0] = '';
		}
		return $array_ch;
		/*
		$res = array();
		$hijos = $this->materias_model->BD_gethijos($pattern);
		$array_ch = array();
		$array_child = array();
		if($hijos) { //Existen hijos
			
			foreach($hijos as $h){
				$rh = $this->Get_childrens($h->TopicID);
				if ($rh){ //NO tiene hijos
					$arrayName = array(
						'Padre' => $rh[0]->TopicID, 
						'Hijo' => $rh
					);
					
					//$res[$pattern][$h->TopicID] = $rh;
					//$res = 'con hijo';
				}else{
					$arrayName = array(
						'Padre' => $h->TopicID, 
						'Hijo' => ''
					);
					//$res = $hijos;
					//$res[$pattern][$h->TopicID] = $rh;
				}
				array_push($array_child, $arrayName);
				//$res = $rh;
			}
			$array_p = array(
				'Padre' => $pattern,
				'Hijo' => $array_child

			);
			
		}else { //NO existen
			$array_p = array(
				'Padre' => $pattern,
				'Hijo' => ''

			);
		}
		array_push($array_ch, $array_p);
		return $array_ch;*/
	}
	function leccion(){
		$TopicE = $this->input->get('l');

		$TopicID = urlencode($TopicE);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);

		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$priv = $this->general_model->GetModuleByTypeUser($usr[0]->UserTypeID,'materias',false);
			$dato['UserTypeID'] = $usr[0]->UserTypeID;
			if($priv){
				$c = $this->materias_model->AboutTopic($TopicID);
				if($c){
					$dato['NameTopic'] = $c[0]->NameTopic;
					$this->load->view('leccion-single',$dato);
				}
			}else{
				redirect(base_url().'inicio');
			}
		}else{
			$this->session->sess_destroy();
	    	redirect(base_url());
		}
	}

	//VISTA DE CURSO POR ALUMNOS//
	public function vista(){
		$CourseID = $this->input->get('c');
		$CourseID = urlencode($CourseID);
		$CourseID = str_replace("+", "%2B",$CourseID);
		$CourseID = urldecode($CourseID);
		$CourseID = $this->encrypt->decode($CourseID);

		$inf = $this->general_model->AboutMateria($CourseID);
		if($inf){
			$CourseName = $inf[0]->CourseName;
			$MateriaIDEncryp = $this->encrypt->encode($CourseID);
			$CourseProfesorID = $inf[0]->CourseProfesorID;
			$prf = $this->general_model->AboutUser($CourseProfesorID);
			$ProfesorName = '';
			if($prf){
				$ProfesorName = $prf[0]->UserFirstName.' '.$prf[0]->UserLastName;
			}


			$dato['CourseName'] = $CourseName;
			$dato['MateriaIDEncryp'] = $MateriaIDEncryp;
			$dato['ProfesorName'] = $ProfesorName;
			$dato['Inc'] = $inf[0]->CourseStar;
			$dato['Fin'] = $inf[0]->CourseEnd;
			$dato['Banner'] = $inf[0]->CourseImage;
			$dato['CourseRequirements'] = $inf[0]->CourseRequirements;
			$dato['CourseGoals'] = $inf[0]->CourseGoals;

			//Obtener las unidades y los topics
			$gsc = $this->materias_model->GetSection($this->session->userdata('UserID'),$CourseID);
			$list = '';
			if($gsc){
				$tabla = $this->encrypt->encode('topics');
				$id = $this->encrypt->encode('TopicID');
				foreach ($gsc as $vl) {
					$tablasc = $this->encrypt->encode('section');
					$idsc = $this->encrypt->encode('SectionID');

					$TopicIDEncryptUnidad = $this->encrypt->encode($vl->SectionID);
					$head = '
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<h4 class="tpg-relawey"><strong class="numberunidad-alumn">Unidad '.$vl->OrderSection.',</strong> <label class="titleunidad-alumn"><i>'.$vl->NameSection.'</i></label> </h4>
						</div>
						<div class="col-xs-12 col-sm-12 col-lg-12">

							<ul class="mini-timeline">
						';

					$gut = $this->materias_model->GetSectionTopics($vl->SectionID,false);
					$gutsb = $this->materias_model->GetSectionTopics($vl->SectionID,true);
					$topics = '';
					//Cascada 1
					if($gut){
						
						foreach ($gut as $row) {
							$fc = $this->Get_childrens($row->TopicID);
							foreach ($fc as $k => $vl) {
								$abt = $this->materias_model->AboutTopic($k);

							}

							
							$childtopics = '';
							/*
							//Cascada 2
							if($gutsb){
								foreach ($gutsb as $sb) {
									if($sb->Parent==$row->TopicID){
										$sbchildtopics = '';
										
										
										//Cascada 3
										if($gutsb){
											foreach ($gutsb as $sbsb) {

												$sbsbchildtopics = '<div class="col-xs-12 col-sm-12 col-lg-12">';
												//cascada 4

												if($gutsb){
													foreach ($gutsb as $sbsbsb) {
														if($sbsbsb->Parent==$sbsb->TopicID){
															$TopicIDEncryptSbSbSb = $this->encrypt->encode($sbsbsb->TopicID);
															$sbchildtopics .= '
															<a href="'.base_url().'temas/?t='.$TopicIDEncryptSbSbSb.'" style="font-size: 20px;"><strong>'.$sbsbsb->OrderTopic.'</strong>.- '.$sbsbsb->NameTopic.'</a> <br>';

														}
														
													}
												}
												$sbsbchildtopics .= '</div>';
												if($sbsb->Parent==$sb->TopicID){
													$TopicIDEncryptSbSb = $this->encrypt->encode($sbsb->TopicID);
													$sbchildtopics .= '
													<a href="'.base_url().'temas/?t='.$TopicIDEncryptSbSb.'" style="font-size: 20px;"><strong>'.$sbsb->OrderTopic.'</strong>.- '.$sbsb->NameTopic.'</a> <br>'.$sbsbchildtopics;

												}
											}
										}

										$sbchildtopics .= '</div>';
										
										
										
										$TopicIDEncryptSb = $this->encrypt->encode($sb->TopicID);
										$childtopics .= '
										<li>
											
											<a href="'.base_url().'temas/?t='.$TopicIDEncryptSb.'" style="font-size: 20px;"><strong>'.$sb->OrderTopic.'</strong>.- '.$sb->NameTopic.'</a>
											
											<ul>'.$sbchildtopics.'</ul>
										</li>
										';
									}
								}
							}
							*/
							
							$TopicIDEncrypt = $this->encrypt->encode($row->TopicID);
							
							$topics .= '
								<li class="mini-timeline-highlight">
								<div class="mini-timeline-panel">
								<a href="'.base_url().'materias/leccion_vista/?l='.$TopicIDEncrypt.'" style="font-size: 15px;"><strong>'.$row->OrderTopic.'</strong>.- '.$row->NameTopic.'</a> </div>
								<ul>'.$childtopics.'</ul></li>
							';
						}
					}
					$footer = '</ul></div>';
					$list .= $head.$topics.$footer;
				}
			}

			//Salir de vista previa
			$salir = '';
			if($inf[0]->CourseProfesorID==$this->session->userdata('UserID')){
				$salir = '<a href="'.base_url().'materias/prevista/?m='.$MateriaIDEncryp.'" class="btn btn-primary">Salir de vista previa</a>';
			}
			$dato['salir'] = $salir;
			
			$dato['list'] = $list;

			$dato['first_lesson'] = $TopicIDEncrypt;

			$this->load->view('materias-vista-alumnos',$dato);
		}

		
	}
	function leccion_vista(){
		$TopicID = trim($this->input->get('l'));
		$TopicID = urlencode($TopicID);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);

		$tpc = $this->materias_model->AboutTopic($TopicID);
		$CourseProfesorID = '';
		if($tpc){
			$CourseID = $tpc[0]->CourseID;
			$inf = $this->general_model->AboutMateria($CourseID);
			if($inf){
				$CourseProfesorID = $inf[0]->CourseProfesorID;
			}
			//Obtener las unidades y los topics
			$gsc = $this->materias_model->GetSection($this->session->userdata('UserID'),$CourseID);
			$list = '';
			if($gsc){
				$tabla = $this->encrypt->encode('topics');
				$id = $this->encrypt->encode('TopicID');
				foreach ($gsc as $vl) {
					$tablasc = $this->encrypt->encode('section');
					$idsc = $this->encrypt->encode('SectionID');

					$TopicIDEncryptUnidad = $this->encrypt->encode($vl->SectionID);
					$head = '
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<h4 class="tpg-relawey"><strong class="numberunidad-alumn">Unidad '.$vl->OrderSection.',</strong> <label class="titleunidad-alumn"><i>'.$vl->NameSection.'</i></label> </h4>
						</div>
						<div class="col-xs-12 col-sm-12 col-lg-12">

							<ul class="mini-timeline">
						';

					$gut = $this->materias_model->GetSectionTopics($vl->SectionID,false);
					$gutsb = $this->materias_model->GetSectionTopics($vl->SectionID,true);
					$topics = '';
					//Cascada 1
					if($gut){
						
						foreach ($gut as $row) {
							$fc = $this->Get_childrens($row->TopicID);
							foreach ($fc as $k => $vl) {
								$abt = $this->materias_model->AboutTopic($k);

							}

							
							$childtopics = '';

							$TopicIDEncrypt = $this->encrypt->encode($row->TopicID);
							
							$topics .= '
								<li class="mini-timeline-highlight">
								<div class="mini-timeline-panel">
								<a href="'.base_url().'materias/leccion_vista/?l='.$TopicIDEncrypt.'" style="font-size: 15px;"><strong>'.$row->OrderTopic.'</strong>.- '.$row->NameTopic.'</a> </div>
								<ul>'.$childtopics.'</ul></li>
							';
						}
					}
					$footer = '</ul></div>';
					$list .= $head.$topics.$footer;
				}
			}

			
			$dato['list'] = $list;

			/************************** *********************/
			//Obtener templates
			$templ = $this->temas_model->GetTemplatesById($TopicID);
			if($templ){
				$tablati = $this->encrypt->encode('topics_templates_items');
				$idti = $this->encrypt->encode('ItemID');
				$loads = '';
				$i = 1;
				$p = 0;
				foreach ($templ as $row) {

					//Para editar
					$EncryptItemID = $this->encrypt->encode($row->ItemID);

					$CategoryTemplateID = $row->CategoryTemplateID;
					$ctg = $this->temas_model->AboutCategory($CategoryTemplateID);

					$edt_item = '';
					//Los items
					$Items = '';
					if($row->Items!=""){
						$Items .= $row->Items;
					}else{
						$UrlFunctionTemplate = $row->UrlFunctionTemplate;
						$CategoryTemplateID = $row->CategoryTemplateID;
						$ItemID = $row->ItemID;

						$ctg = $this->temas_model->AboutCategory($CategoryTemplateID);
						if($ctg){
							
							$csbit = $this->temas_model->GetSubItemByItemID($ItemID);
							if($csbit){
								$arr = array();
								foreach ($csbit as $vl) {

									$sub_arr = array(
										'DocEntry' => $this->encrypt->encode($vl->DocEntry),
										'Type' => $vl->Type,
										'Valor' => $vl->Valor,
										'OrdenSection' => $row->Orden,
										'Orden' => $vl->Orden,
									);
									array_push($arr, $sub_arr);
								}
								$agrupar = groupArray($arr,'Orden');
								$Items .= $UrlFunctionTemplate($agrupar);

							}else{

								if($CategorySlug!='texto' && $CategorySlug!='divisiones'){
									
									$Items .= '

									';
								}else{
									$Items .= '<div class="">'.$row->TemplateItems.'</div>';
								}
							}
							
						}

					}
					$Items = stripslashes($Items);
					
					$loads .= 
					'
					<div class="col-xs-12 col-sm-12 col-lg-12 padding0">
						'.$Items.'
					</div>
					';

					$i++;
					$p++;
				}
			}else{

				$loads = '';
			}
			$dato['loads'] = $loads;

			//Salir de vista previa
			$salir = '';
			if($CourseProfesorID==$this->session->userdata('UserID')){
				$salir = '<a href="'.base_url().'temas/?t='.$this->input->get('l').'" class="btn btn-primary">Salir de vista previa</a>';
			}
			$dato['salir'] = $salir;

			$this->load->view('leccion-vista-alumnos',$dato);
		}

		
	}
	//Quiz
	function addexamen(){
		$CourseID = trim($this->input->get('c'));
		$CourseID = urlencode($CourseID);
		$CourseID = str_replace("+", "%2B",$CourseID);
		$CourseID = urldecode($CourseID);
		$CourseID = $this->encrypt->decode($CourseID);

		$TopicID = trim($this->input->get('l'));
		$TopicID = urlencode($TopicID);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);


		//Quiz curso
		if($CourseID!=""){
			$CourseIDEnCrypt = $this->encrypt->encode($CourseID);

			$data['CourseIDEnCrypt'] = $CourseIDEnCrypt;
			$this->load->view('examen-crear',$data);
		}
		//Quiz lección
		if($TopicID!=""){

		}

	}
	function examen(){
		$QuizID = trim($this->input->get('ex'));
		$QuizID = urlencode($QuizID);
		$QuizID = str_replace("+", "%2B",$QuizID);
		$QuizID = urldecode($QuizID);
		$QuizID = $this->encrypt->decode($QuizID);

		$gq = $this->materias_model->GetQuiz($QuizID);
		if($gq){
			$CourseIDEnCrypt = $this->encrypt->encode($gq[0]->CourseID);
			$TopicIDEnCrypt = $this->encrypt->encode($gq[0]->TopicID);
			$data['CourseIDEnCrypt'] = $CourseIDEnCrypt;
			$data['TopicIDEnCrypt'] = $TopicIDEnCrypt;
			$data['QuizIDEnCryp'] = $this->encrypt->encode($QuizID);
			//Tiene preguntas?
			$qs = $this->materias_model->GetQuestions($QuizID);
			$questions = '';
			if($qs){
				$tabla = $this->encrypt->encode('questions');
				$id = $this->encrypt->encode('QuestionID');
				foreach ($qs as $row) {
					$questions .= '
					<div class="col-xs-12 col-sm-12 col-lg-12 " >
						<div class="col-xs-10 col-sm-10 col-lg-10 div-pregunts" v="'.$this->encrypt->encode($QuizID).'" i="'.$row->Orden.'" q="'.$this->encrypt->encode($row->QuestionID).'">
							<a>Pregunta '.$row->Orden.'</a>
						</div>
						<div class="col-xs-1 col-sm-1 col-lg-1" style="padding-top:15px;">
							<a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$this->encrypt->encode($row->QuestionID).'"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
						</div>
					</div>
					';
				}
			}
			$data['questions'] = $questions;

			$this->load->view('examen-editar',$data);
		}
	}
}