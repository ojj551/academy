<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actividades extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->helper('html');
        $this->load->model('alumnos_model');
        $this->load->model('materias_model');
        $this->load->model('cursos_model');
        $this->load->model('actividades_model');
        if (!$this->session->userdata('UserID')) {
            redirect(base_url());
        }

        $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
        $this->CustomerID = '';
        if ($usr) {
            $this->CustomerID = $usr[0]->CustomerID;

        }
    }

    //ordenar
    function ordenar()
    {
        $arry_item = $_POST['arry_item'];

        $ItemID = trim($arry_item[0]['itm']);
        $ItemID = urlencode($ItemID);
        $ItemID = str_replace("+", "%2B", $ItemID);
        $ItemID = urldecode($ItemID);
        $ItemID = $this->encrypt->decode($ItemID);

        $CategorySlug = 'actividades';
        $hvws = $this->actividades_model->ExistAnswer($this->session->userdata('UserID'), $ItemID, $CategorySlug);

        if (!$hvws) {
            //consultar orden de sub items
            $sbt = $this->actividades_model->SubItemsOrden($this->session->userdata('UserID'), $ItemID, $CategorySlug, 'title');

            $arr_sbitems = array();
            $show_anws = '';
            if ($sbt) {
                $ct = 1;
                foreach ($sbt as $vl) {
                    array_push($arr_sbitems, $vl->Valor);
                    $show_anws .= '
					<li><b>' . $ct . '.-</b> ' . $vl->Valor . '</li>
					';
                    $ct++;
                }
            }

            $arr_anws = array();
            foreach ($arry_item as $row) {
                $v = trim($row['v']);
                array_push($arr_anws, $v);

            }

            $Values = json_encode($arr_anws);


            $count = 0;
            for ($i = 0; $i < count($arr_sbitems); $i++) {

                if ($arr_sbitems[$i] == $arr_anws[$i]) {

                    $count++;
                }

            }

            if ($count == count($arr_sbitems)) {
                $RightAnswer = 1;
            } else {
                $RightAnswer = 0;
            }


            $DocDate = date('Y-m-d H:i:s');
            $data = array(
                'DocDate' => $DocDate,
                'UserID' => $this->session->userdata('UserID'),
                'CategorySlug' => $CategorySlug,
                'ItemID' => $ItemID,
                'Values' => $Values,
                'RightAnswer' => $RightAnswer
            );

            $addaws = $this->actividades_model->AddAnswer($data);
            if ($addaws) {
                if ($RightAnswer == 1) {
                    echo '
					<div style="padding: 20px;">
						<h3 style="color:green;"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Excelente, has ordenado correctamente tus items</h3>
					</div>
					';
                } else {
                    echo '
						<div style="padding: 20px;">
							<h3 style="color:red;"><i class="fa fa-times" aria-hidden="true"></i> Tu respuesta ha sido incorrecta, el orden correcto es el siguiente: </h3>
							<ul>
								' . $show_anws . '
							<ul>
						</div>
					';
                }
            }
        } else {
            echo '
			<div style="padding: 20px;">
				<h3 style="color:green;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> No te preocupes. Ya has contestado correctamente esta actividad</h3>
			</div>
			';
            //echo "No te preocupes. Ya has contestado correctamente esta actividad";
        }


    }

    function saveaws()
    {
        $incorrectAnswer = 0;
        $rcv = trim($this->input->post('datos'));
        $rcv = str_replace("+", "%2B", $rcv);
        $rcv = str_replace("\\", "", $rcv);
        $rcv = json_decode($rcv, True);
        $html = '';
        $nAwr = 0;
        $CategorySlug = 'actividades';
        $Score_negativo = 0;

        foreach ($rcv as $eachRcv) {
            if (!isset($eachRcv['type'])) {
                continue;
            }
            $RightAnswer = 0;
            if ($eachRcv['type'] == 'oM') {
                if(!isset($eachRcv['resp'])){
                    $Resp = '';
                }else{
                    $Resp = trim($eachRcv['resp']);
                    $Resp = str_replace("+", "%2B", $Resp);
                    $Resp = urldecode($Resp);
                    $Resp = $this->encrypt->decode($Resp);
                }

                $QuestionID = trim($eachRcv['qt']);
                $QuestionID = str_replace("+", "%2B", $QuestionID);
                $QuestionID = urldecode($QuestionID);
                $QuestionID = $this->encrypt->decode($QuestionID);

                $rs = $this->actividades_model->GetAwns($QuestionID, $Resp);
                $awr = '';

                if ($Resp === '') {
                    $nAwr++;
                    $incorrectAnswer++;
                    $datos['respuestas'][] = array('name' => '<strong style="color: red"> <i class="fa fa-times" aria-hidden="true"></i>' . $rs[0]->QuestionName . '</strong>', 'feedback' => $rs[0]->Feedback);
                } else {
                    if ($rs[0]->QuestionAnswerID == $Resp) {
                        /* $html	.= '
                        <div style="padding: 20px;">
                            <h5 style="color:green;"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Excelente, tu respuesta ha sido correcta</h5>
                        </div>
                        '; */
                        $RightAnswer = 1;
                    } else {
                        $datos['respuestas'][] = array('name' => '<strong style="color: red"> <i class="fa fa-times" aria-hidden="true"></i>' . $rs[0]->QuestionName . '</strong>', 'feedback' => $rs[0]->Feedback);
                        $RightAnswer = 0;
                        $incorrectAnswer++;
                    }
                    $awr = $rs[0]->OptionName;
                }
                if ($RightAnswer == 0) {
                    $Score_negativo += (100 / count($rcv));
                }
                $Values[] = array('QuestionID' => $QuestionID, 'AnswerID' => $Resp, 'Answer' => $awr, 'RightAnswer' => $RightAnswer);
            } elseif ($eachRcv['type'] == 'rE') {
                $Resps = $eachRcv['resps'];
                if(isset($Resps[0])) {
                    $jsonDt = $Resps[0]['qt'];
                    $jsonDt = str_replace("+", "%2B", $jsonDt);
                    $jsonDt = urldecode($jsonDt);
                    $jsonDt = $this->encrypt->decode(trim($jsonDt));
                    $jsonDt = json_decode($jsonDt, TRUE);

                    $QuestionID = $this->encrypt->decode($jsonDt['QuestionID']);

                    $QName = '';
                    $espacio_blanco = [];
                    foreach ($Resps as $rsp) {
                        $Resp = $rsp['resp'];
                        $OptionID = str_replace("+", "%2B", $rsp['qt']);
                        $OptionID = urldecode($OptionID);
                        $OptionID = $this->encrypt->decode(trim($OptionID));
                        $OptionID = json_decode($OptionID, TRUE);
                        $OptionID = $this->encrypt->decode($OptionID['OptionID']);

                        $rs = $this->actividades_model->GetAwns($QuestionID, $OptionID);
                        $QAnswerID = json_decode($rs[0]->QuestionAnswerID, TRUE);
                        $dbCorrectAws = [];
                        $RightAnswer = 0;
                        if ($QAnswerID != 0) {
                            foreach ($QAnswerID as $eQAnswerID) {
                                $dbCorrectAws[$eQAnswerID['opID']] = $eQAnswerID['correctA'];
                            }
                        }

                        $opExtra = json_decode($rs[0]->OptionExtra, TRUE);

                        $dbAwsOps = [];
                        if ($opExtra) {
                            $dbAwsOps[''] = '--';
                            foreach ($opExtra as $opcs) {
                                $dbAwsOps[$opcs['Orden']] = $opcs['Name'];
                            }
                        }

                        if ($Resp === '' or $QAnswerID == 0) {
                            $RightAnswer = 0;
                        } else {
                            if ($dbCorrectAws[$OptionID] == $Resp) {
                                $RightAnswer = 1;
                            }
                        }
                        $awr = $dbAwsOps[$Resp];
                        if ($QName == '') {
                            $QName = $rs[0]->QuestionName;
                        }
                        $from = '/' . preg_quote('___', '/') . '/';
                        if ($RightAnswer == 0) {
                            $to = '<u><strong style="color:red;"><i class="fa fa-times" aria-hidden="true"></i>' . $dbAwsOps[$Resp] . '</strong></u>';
                            $Score_negativo += ((100 / count($rcv)) / count($Resps));
                        } else {
                            $to = '<u><strong style="color:green;"><i class="fa fa-check" aria-hidden="true"></i>' . $dbAwsOps[$Resp] . '</strong></u>';
                        }
                        $QName = preg_replace($from, $to, $QName, 1);
                        $espacio_blanco[] = array();
                        $Values['re_Question'][$QuestionID][] = array('OptionID' => $OptionID, 'AnswerID' => $Resp, 'Answer' => $awr, 'RightAnswer' => $RightAnswer);
                    }
                    foreach ($Values['re_Question'][$QuestionID] as $qstn) {
                        if (!$qstn['RightAnswer']) {
                            $incorrectAnswer++;
                            $datos['respuestas'][] = array('name' => $QName, 'feedback' => $rs[0]->Feedback);
                            break;
                        }
                    }
                    foreach ($Values['re_Question'][$QuestionID] as $qstn) {
                        if ($qstn['AnswerID'] === '' || $qstn['AnswerID'] === '--') {
                            $nAwr++;
                            break;
                        }
                    }
                }else{
                    $nAwr++;
                    $incorrectAnswer++;
                    $Score_negativo += ((100 / count($rcv)));
                }
            }

        }

        $DocDate = date('Y-m-d H:i:s');
        $Score = 100 - $Score_negativo;

        if ($Score <= 60) {
            $colorScore = 'red';
        } else if ($Score <= 80) {
            $colorScore = 'Yellow';
        } else {
            $colorScore = 'Green';
        }

        $datos['QuizName'] = $rs[0]->QuizName;
        $datos['colorScore'] = $colorScore;
        $datos['Score'] = (($Score == 0) ? '-' : round($Score, 1, PHP_ROUND_HALF_UP));
        $datos['Correctas'] = (count($rcv) - $incorrectAnswer) . '/' . count($rcv);
        $datos['Inconclusas'] = $nAwr;
        $datos['html'] = $html;


        $Values = json_encode($Values);

        $data = array(
            'DocDate' => $DocDate,
            'UserID' => $this->session->userdata('UserID'),
            'CategorySlug' => $CategorySlug,
            'ItemID' => $rs[0]->ItemID1,
            'Values' => $Values,
            'RightAnswer' => round($Score, 2, PHP_ROUND_HALF_UP)
        );
//        echo $html;
        $addaws = $this->actividades_model->AddAnswer($data);
        $this->twig->display('Activities/retroalimentacion_quiz', $datos);


    }

}

?>