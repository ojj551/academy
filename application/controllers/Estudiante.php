<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estudiante extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('alumnos_model'); 
		$this->load->model('institucion_model');
		$this->load->model('cursos_model');
		$this->load->model('materias_model');
		$this->load->model('temas_model'); 
		if ( ! $this->session->userdata('UserID')){ 
	        redirect(base_url());
	    }
	    $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
	    $this->UserTypeID = '';
	    $this->CustomerID = '';
	    $this->actual_link = actual_link();
	    if($usr){
	    	//Tipo de usuario
        	$UserTypeID = $usr[0]->UserTypeID;
        	$typ = $this->general_model->TypeUsers("UserTypeID='$UserTypeID'");
        	if($typ){
        		if($typ[0]->Slug=='student'){
        			$this->UserTypeID = $usr[0]->UserTypeID;
					$this->CustomerID = $usr[0]->CustomerID;
        		}else{redirect(base_url().'denied');}
        	}else{redirect(base_url().'denied');}
	    }else{
	    	redirect(base_url().'denied');
	    }

	}
	public function index(){
		$this->load->view('estudiante/home');
	}
	public function materias(){
		$getmc = $this->cursos_model->GetMyCoursesList($this->session->userdata('UserID'));
		$miscursos = '';
		if($getmc){
			foreach ($getmc as $row) {

				$avgc = $this->cursos_model->AvanceCourse($row->CourseID);
				if($avgc){
					$avgc = $avgc;
				}else{
					$avgc = 0;
				}
				$encode = $this->encrypt->encode($row->SuscriptionID);
				if($row->CourseImage!=""){
					$CutomerIDEncrypt = md5($this->CustomerID);
					$ProfesorIDEncrypt = md5($row->CourseProfesorID);
					$MateriaIDEncrypt = md5($row->CourseID);
					$Imgbg = $this->actual_link.'/files_academy/Customers/'.$CutomerIDEncrypt.'/prof/'.$ProfesorIDEncrypt.'/'.$MateriaIDEncrypt.'/'.$row->CourseImage;
				}else{
					$Imgbg = site_url('assets/img/no_imagen.jpg');
				}
				//Ver avance de curso
				
				$encryptc = $this->encrypt->encode($row->CourseID);
				$miscursos .= '
					<div class="col-md-4 " style="position:relative">
			          <div class="panel course-on-list animateslow">
			               <div class="panel-heading-white panel-heading text-center">
			                  <h4 class="ttl-crs">'.$row->CourseName.'</h4>
			                </div>
			                <div class="panel-body text-center" style="padding: 0px;">
			                	<div style="background: url('.$Imgbg.')no-repeat;height:150px;    background-size: cover;"></div>
			                  <div class="col-md-12">
			                  	<h2>'.number_format($row->Progress,2).'%</h2><span> de avance</span>
			                  </div>
			                  <div class="col-md-12">
			                  	<a href="'.base_url().'estudiante/vista/?c='.$encryptc.'">Ir a curso</a>
			                  </div>
			                </div>
			          </div>
			        </div>
				';
			}
		}else{
			$miscursos = '<h1>Aún no tienes cursos</h1>';
		}
		$dato['miscursos'] = $miscursos;
		$this->load->view('estudiante/mis-cursos',$dato);
	}
	public function vista(){
		$CourseID = $this->input->get('c');
		$CourseID = urlencode($CourseID);
		$CourseID = str_replace("+", "%2B",$CourseID);
		$CourseID = urldecode($CourseID);
		$CourseID = $this->encrypt->decode($CourseID);

		$inf = $this->general_model->AboutMateria($CourseID);
		if($inf){
			$CourseName = $inf[0]->CourseName;
			$MateriaIDEncryp = $this->encrypt->encode($CourseID);
			$CourseProfesorID = $inf[0]->CourseProfesorID;
			$prf = $this->general_model->AboutUser($CourseProfesorID);
			$ProfesorName = '';
			if($prf){
				$ProfesorName = $prf[0]->UserFirstName.' '.$prf[0]->UserLastName;
			}


			$dato['CourseName'] = $CourseName;
			$dato['MateriaIDEncryp'] = $MateriaIDEncryp;
			$dato['ProfesorName'] = $ProfesorName;
			$dato['Inc'] = $inf[0]->CourseStar;
			$dato['Fin'] = $inf[0]->CourseEnd;
			$dato['Banner'] = $inf[0]->CourseImage;
			$dato['CourseRequirements'] = $inf[0]->CourseRequirements;
			$dato['CourseGoals'] = $inf[0]->CourseGoals;


			//img curso
			$CutomerIDEncrypt = md5($this->CustomerID);
			$ProfesorIDEncrypt = md5($inf[0]->CourseProfesorID);
			$MateriaIDEncrypt = md5($inf[0]->CourseID);
			$imgcurso = $this->actual_link.'/files_academy/Customers/'.$CutomerIDEncrypt.'/prof/'.$ProfesorIDEncrypt.'/'.$MateriaIDEncrypt.'/'.$inf[0]->CourseImage;
			$dato['imgcurso'] = $imgcurso;

			$list = '';
			$TopicIDEncrypt = '';
			//Optener unidades y topics
			$topics = '';
			$gsc = $this->materias_model->GetSection($CourseID);
			if($gsc){
				$tabla = $this->encrypt->encode('topics');
				$id = $this->encrypt->encode('TopicID');
				$ord = 1;
				foreach ($gsc as $vl) {
					$TopicIDEncryptUnidad = $this->encrypt->encode($vl->SectionID);
					$topics .= '
					<div class="col-xs-12 col-sm-12 col-lg-12">
						<h4 class="tpg-relawey"><strong class="numberunidad-alumn">'.$ord.',</strong> <label class="titleunidad-alumn"><i>'.$vl->NameSection.'</i></label> </h4>
					</div>
					<div class="col-xs-12 col-sm-12 col-lg-12">
						<ul class="mini-timeline">
					';
					$ord++;

					$gut = $this->materias_model->GetSectionTopics($vl->SectionID,false);
					
					if($gut){
						$ort = 1;
						foreach ($gut as $g) {
							$gutsb = $this->materias_model->GetSubSectionTopics($vl->SectionID,$g->TopicID);
							$sub = '';
							if($gutsb){
								$sbcont = 1;
								foreach ($gutsb as $stp) {
									$sub .= '
									<li class="" style="line-height: 20px;height: auto;padding-top: 22px;">
										<div class="mini-timeline-panel">
										<a href="'.base_url().'vista/?l='.$this->encrypt->encode($stp->TopicID).'" style="font-size: 15px;line-height: 18px;"><strong>'.$sbcont.'</strong>.- '.$stp->NameTopic.'</a>
										</div>
									</li>
									';
									$sbcont++;
								}
							}
							$topics .= '
							<li class="mini-timeline-highlight" style="line-height: 18px;height: auto;">
								<div class="mini-timeline-panel">
									<a href="'.base_url().'vista/?l='.$this->encrypt->encode($g->TopicID).'" style="font-size: 15px;line-height: 18px;"><strong>'.$ort.'</strong>.- '.$g->NameTopic.'</a>
								</div>
								<ul>
								'.$sub.'
								</ul>
							</li>
							';
							$ort++;
						}
					}

					$topics .= '
						</ul>
						</div>
					';
					
				}
				
			}

			$dato['topics'] = $topics;
			//Obtener las unidades y los topics
			
			//Salir de vista previa
			
			$salir = '<a href="'.base_url().'cursosnc/miscursos" class="btn btn-primary">Ir a inicio</a>';
			
			$dato['salir'] = $salir;
			

			$dato['first_lesson'] =' $this->encrypt->encode($gsc[0]->TopicID)';

			$this->load->view('estudiante/materias-vista-alumnos',$dato);
		}

		
	}
	
	
}