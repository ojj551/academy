<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumnosfunc extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$config_email = Array(        
	        'protocol' => 'sendmail',
	        'smtp_host' => 'smtp.gmail.com',
	        'smtp_port' => 25,
	        'smtp_user' => 'SMTP Username',
	        'smtp_pass' => 'SMTP Password',
	        'smtp_timeout' => '4',
	        'mailtype'  => 'html', 
	        'charset'   => 'utf-8',
	        'newline'  => '\r\n',
			'crlf' => '\r\n'
	    );
		$this->load->library('email',$config_email);
	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('alumnos_model');  

		if ( ! $this->session->userdata('UserEmail')){ 
	        redirect(base_url());
	    }
	    $this->Lang = 'es';
	    $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
	    $this->CustomerID = ''; $this->UserTypeID = '';
	    if($usr){
	    	$this->CustomerID = $usr[0]->CustomerID;
	    	$this->UserTypeID = $usr[0]->UserTypeID;
	    	$cm = $this->general_model->AboutCustomerByID($usr[0]->CustomerID);
			$this->LeadType = '';
			if($cm){
				$this->LeadType = $cm[0]->LeadType;
			}
	    }
		if(!$usr){
			$this->session->sess_destroy();
	    	redirect(base_url());
		}
		$this->ExiteRegistro = $this->general_model->GetText($this->Lang,'','existregister');
	}
	function createreference(){
		$random = RandomString();
		$random = trim($random);
		$sq = $this->alumnos_model->ExistReference($random);

		if($sq){
			echo $random;
		}else{
			echo "error";
		}
	}
	function addalumno(){
		
		if($this->CustomerID!=""){

		
			$Reference = trim($this->input->post('idref'));

			$EmailReference = trim($this->input->post('email'));

			$UserTypeID = urlencode(trim($this->input->post('typealm')));
			$UserTypeID = str_replace("+", "%2B",$UserTypeID);
			$UserTypeID = urldecode($UserTypeID);
			$UserTypeID = $this->encrypt->decode($UserTypeID);

			$CareerID = urlencode(trim($this->input->post('carrera')));
			$CareerID = str_replace("+", "%2B",$CareerID);
			$CareerID = urldecode($CareerID);
			$CareerID = $this->encrypt->decode($CareerID);

			$UserSemester = trim($this->input->post('semestre'));

			$DocDate = date('Y-m-d H:i:s');
			/*
			$data = array(
				'DocDate' => $DocDate,
				'DocUserID' => $this->session->userdata('UserID'),
				'Reference' => $Reference,
				'CustomerID' => $this->CustomerID,
				'UserTypeID' => $UserTypeID,
				'EmailReference' => $EmailReference,
				'UserSemester' => $UserSemester,
				'CareerID' => $CareerID
			);
			*/

			
			$data = array(
				'DocDate' => $DocDate,
				'DocUserID' => $this->session->userdata('UserID'),
				'CustomerID' => $this->CustomerID,
				'UserEmail' => $EmailReference,
				'UserReference' => $Reference,
				'UserTypeID' => $UserTypeID,
				'UserSemester' => $UserSemester,
				'CareerID' => $CareerID
			);

			$inst = $this->alumnos_model->AddReferenceAlumno($Reference,$EmailReference,$data);
			if($inst){
				//tipo de usuario
				$UserTypeID = $this->UserTypeID;
        		$typ = $this->general_model->TypeUsers("UserTypeID='$UserTypeID'");
				if($typ){
	        		if($typ[0]->Slug=='teacher'){
	        			$data_r = array(
							'DocDate' => $DocDate,
							'DocUserID' => $this->session->userdata('UserID'),
							'ResponsableID' => $this->session->userdata('UserID'),
							'UserID' => $inst
						);
						$isr = $this->alumnos_model->InsertAsignation($this->session->userdata('UserID'),$inst,$data_r);
	        		}
	        	}
				//enviar correo
				$datam = array(
					'reference' => $Reference
			    );
				$body = $this->load->view('emails/invitacion.php',$datam,TRUE);
				// $this->email->message($body);
				//Enviar correo de bienvenida
        		$asunto = '¡XpertCAD-Academy te espera!';
				// Cada email saliente se tiene que guardar en la base de datos para tener un registro
				$FieldsName = array
				(
					"DocDate" => date('Y-m-d H:i:s'),
					"MessageID" => date('YmdHis')."_".random_code(8, 8),
					"MailSentStatus" => 1,
					"MailFrom" => "\"XpertCAD\" <no-reply@xpertcad.com>",
					"MailTo" => "\"$EmailReference\" <$EmailReference>",
					"MailSubject" => $asunto,
					"MailBody" => $body
				);
				$sv = $this->general_model->SaveMailContacto($FieldsName);
				if($sv){
					//Enviar correo
					$this->email->from('no-reply@xpertcad.com', 'XpertCAD');
					$this->email->to(trim($EmailReference));
					$this->email->subject($asunto);
					$this->email->message($body);

					$send = $this->email->send();
					if($send){
						$wherems = array('DocEntry' => $sv);
						$datams = array(
							'MailSentDate' => date('Y-m-d H:i:s'),
							'MailSentStatus' => 2
						);
						$EJUPT = $this->general_model->UPDATEMAIL($datams,$wherems);
						echo "success";
					}else{
						echo "error al enviar correo al usuario agregado";
					}
				}
			}else{
				//echo $Reference."<br>".$EmailReference;
				echo "Ya exite un usuario con este correo";
			}
			

			//print_r($data);
		}
	}
	function addalumnos(){
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$CustomerID = $usr[0]->CustomerID;
			$data = json_decode(stripslashes($_POST['data']));

			$insert = array();
			$noinsert = array();
			$yaexistproblems = array();
			foreach ($data as $row) {
				$CareerID = $row->carr;
				$CareerID = urlencode($CareerID);
				$CareerID = str_replace("+", "%2B",$CareerID);
				$CareerID = urldecode($CareerID);
				$CareerID = $this->encrypt->decode($CareerID);

				$Level = $row->lv;
				$ProfesorID = $this->session->userdata('UserID');
				
				$cs = $this->alumnos_model->ExistRegister(trim($row->idref),trim($row->Email));
				if($cs){
					if(count($cs)>1){
						array_push($yaexistproblems, trim($row->Email));
					}else{
						$SQLC = "SELECT * FROM bn_groups WHERE CareerID='$CareerID' AND LevelCareer='$Level' AND ProfesorID='$ProfesorID' AND Cancelled=0";
						$rsl = $this->general_model->QUERYS($SQLC);
						if($rsl){
							foreach ($rsl as $vl) {
								$DocDatesb = date('Y-m-d H:i:s');
								$data1 = array(
									'DocDate' => $DocDatesb,
									'DocUserID' => $ProfesorID,
									'UserID' => $cs[0]->UserID,
									'GroupID' => $vl->GroupID
								);
								$ist = $this->alumnos_model->AddUserGroup($cs[0]->UserID,$vl->GroupID,$data1);
								if($ist){
									$DocDatesbsb = date('Y-m-d H:i:s');
									$datasb = array(
										'DocDate' => $DocDatesbsb,
										'UserID' => $cs[0]->UserID,
										'CourseID' => $vl->CourseID,
									);

									$inser = $this->alumnos_model->AddSuscription($vl->CourseID,$cs[0]->UserID,$datasb);
								}
							}
						}
					}
				}else{
					$DocDate = date('Y-m-d H:i:s');

					$data = array(
						'DocDate' => $DocDate,
						'DocUserID' => $this->session->userdata('UserID'),
						'Reference' => trim($row->idref),
						'CustomerID' => $CustomerID,
						'UserTypeID' => trim($row->tipo),
						'SupervisorID' => $this->session->userdata('UserID'),
						'EmailReference' => trim($row->Email),
						'UserSemester' => $Level,
						'CareerID' => $CareerID
					);
					$inst = $this->alumnos_model->AddReferenceAlumno(trim($row->idref),trim($row->Email),$data);
					if($inst){
						array_push($insert, trim($row->Email));
					}else{
						array_push($noinsert, trim($row->Email));
					}
				}
			}

			if (count($noinsert)>0 OR count($yaexistproblems)>0) {
				$correct = '';
				//echo count($insert);
				if(count($insert)>0){
					foreach ($insert as $row) {
						$correct .= '<a>'.$row.'</a><br>';
					}
				}else{
					$correct = '<a>Ninguno</a>';
				}
				$incorrect = '';
				if(count($noinsert)>0){
					foreach ($noinsert as $row) {
						$incorrect .= '<a>'.$row.'</a><br>';
					}
				}else{
					$incorrect = 'Ninguno';
				}

				$ex = '';
				if(count($yaexistproblems)>0){
					foreach ($yaexistproblems as $row) {
						$ex .= '<a>'.$row.'</a><br>';
					}
				}else{
					$ex = 'Ninguno';
				}
				echo '
					<label>Lista de alumnos que se registraron correctamente</label>
					'.$correct.'
					<label>Lista de alumnos que NO se registraron correctamente</label>
					'.$incorrect.'
					<label>Lista de alumnos que ya se encuentraban registrados</label>
					'.$ex.'
				';
			}else{
				echo "success";
			}
		}
		//print_r($data);
	}
	function addalumnos_admin(){
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			
			$data = json_decode(stripslashes($_POST['data']));
			$CustomerID = $data[0]->cutm;
			$CustomerID = urlencode($CustomerID);
			$CustomerID = str_replace("+", "%2B",$CustomerID);
			$CustomerID = urldecode($CustomerID);
			$CustomerID = $this->encrypt->decode($CustomerID);

			$insert = array();
			$noinsert = array();
			$yaexistproblems = array();
			foreach ($data as $row) {
				$CareerID = $row->carr;
				$CareerID = urlencode($CareerID);
				$CareerID = str_replace("+", "%2B",$CareerID);
				$CareerID = urldecode($CareerID);
				$CareerID = $this->encrypt->decode($CareerID);

				$UserTypeID = urlencode(trim($row->tipo));
				$UserTypeID = str_replace("+", "%2B",$UserTypeID);
				$UserTypeID = urldecode($UserTypeID);
				$UserTypeID = $this->encrypt->decode($UserTypeID);

				$Level = $row->lv;
				
				$DocDate = date('Y-m-d H:i:s');

				$data = array(
					'DocDate' => $DocDate,
					'DocUserID' => $this->session->userdata('UserID'),
					'Reference' => trim($row->idref),
					'CustomerID' => $CustomerID,
					'UserTypeID' => $UserTypeID,
					'SupervisorID' => $this->session->userdata('UserID'),
					'EmailReference' => trim($row->Email),
					'UserSemester' => $Level,
					'CareerID' => $CareerID
				);
				$inst = $this->alumnos_model->AddReferenceAlumno(trim($row->idref),trim($row->Email),$data);
				if($inst){
					array_push($insert, trim($row->Email));
				}else{
					array_push($noinsert, trim($row->Email));
				}
			}
		
			if (count($noinsert)>0 OR count($yaexistproblems)>0) {
				$correct = '';
				//echo count($insert);
				if(count($insert)>0){
					foreach ($insert as $row) {
						$correct .= '<a>'.$row.'</a><br>';
					}
				}else{
					$correct = '<a>Ninguno</a>';
				}
				$incorrect = '';
				if(count($noinsert)>0){
					foreach ($noinsert as $row) {
						$incorrect .= '<a>'.$row.'</a><br>';
					}
				}else{
					$incorrect = 'Ninguno';
				}

				$ex = '';
				if(count($yaexistproblems)>0){
					foreach ($yaexistproblems as $row) {
						$ex .= '<a>'.$row.'</a><br>';
					}
				}else{
					$ex = 'Ninguno';
				}
				echo '
					<label>Lista de alumnos que se registraron correctamente</label>
					'.$correct.'
					<label>Lista de alumnos que NO se registraron correctamente</label>
					'.$incorrect.'
					<label>Lista de alumnos que ya se encuentraban registrados</label>
					'.$ex.'
				';
			}else{
				echo "success";
			}
		}
	}
	function ShowTypeUsers(){
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));

		$UserTypeID = '';
		if($usr){
			$UserTypeID = $usr[0]->UserTypeID;

			if($UserTypeID=="1"){
				$sql = "UserTypeName='Alumno' AND Cancelled=0";
				$ty = $this->general_model->TypeUsers($sql);
			}
			if($UserTypeID=="4"){
				$sql = "Cancelled=0";
				$ty = $this->general_model->TypeUsers($sql);
			}

			$listtype = '<option value="">Selecciona un tipo de usuario</option>';
			if($ty){
				foreach ($ty as $row) {
					$UserTypeIDEncryp = $this->encrypt->encode($row->UserTypeID);
					$listtype .= 
					'
					<option value="'.$UserTypeIDEncryp.'">'.$row->UserTypeName.'</option>
					';
				}
			}
			echo $listtype;
		}
	}
	/*
	function ShowMaterias(){
		$gtm = $this->grupos_model->MisGruposCreados($this->session->userdata('UserID'));

		$materias = '<option value="">Selecciona...</option>';
		if($gtm){
			foreach ($gtm as $row) {
				$AC = $this->grupos_model->AboutCarrera($row->CareerID);
				$AM = $this->general_model->AboutMateria($row->CourseID);

				//Tipo de nivel
				$ACM = $this->general_model->AboutCustomer($gtm[0]->CustomerID);

				$TypeNivelName = '';
				if($ACM){
					$TypeNivelID = $ACM[0]->TypeNivelID;
					if($TypeNivelID!=""){
						$SQL = "SELECT * FROM bn_typenivel WHERE TypeNivelID='$TypeNivelID'";
						$s = $this->general_model->QUERYS($SQL);
						if($s){
							$TypeNivelName = $s[0]->TypeNivelName;
						}
					}
				}
				//$CourseIDEncryp = $this->encrypt->encode($row->CourseID);
				$materias .= '
				<option value="'.$row->GroupID.'">#'.$row->GroupID.' - '.$ACM[0]->TypeNivelID.' '.$TypeNivelName.' '.$AC[0]->CareerName.' - '.$AM[0]->CourseName.'</option>
				';
			}
		}
		echo $materias;
	}*/

	function ShowCarreras(){
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$CustomerID = $usr[0]->CustomerID;
			$UserID = $this->session->userdata('UserID');
			$SQL = "SELECT * FROM bn_career AS T1, bn_groups AS T2 WHERE T1.CareerID=T2.CareerID AND T1.CustomerID='$CustomerID' AND T2.ProfesorID='$UserID' AND T2.Cancelled=0 GROUP BY T1.CareerID";
			//$SQL = "SELECT * FROM bn_career WHERE CustomerID='$CustomerID' AND Cancelled=0";
			$c = $this->general_model->QUERYS($SQL);

			$carr = '<option value="">Selecciona...</option>';
			if($c){
				foreach ($c as $row) {
					$CareerIDEncryp = $this->encrypt->encode($row->CareerID);
					$carr .= '
					<option value="'.$CareerIDEncryp.'">'.$row->CareerName.'</option>
					';
				}
			}
			echo $carr;
		}
	}
	function getmateriasbycustomer(){
		$CM = $this->input->post('CM');

		$CustomerID = urlencode($CM);
		$CustomerID = str_replace("+", "%2B",$CustomerID);
		$CustomerID = urldecode($CustomerID);
		$CustomerID = $this->encrypt->decode($CustomerID);

		$gm = $this->alumnos_model->GetCareerByCustomerID($CustomerID);
		$carr = '<option value="">Selecciona carrera/departamento</option>';
		if($gm){
			foreach ($gm as $row) {
				$CareerIDEncryp = $this->encrypt->encode($row->CareerID);
				$carr .= '
					<option value="'.$CareerIDEncryp.'">'.$row->CareerName.'</option>
					';
			}
		}
		echo $carr;
	}
	function SemestreCarrera(){
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$UserID = $this->session->userdata('UserID');
			$vlcar = $this->input->post('vlcar');

			$CareerID = urlencode($vlcar);
			$CareerID = str_replace("+", "%2B",$CareerID);
			$CareerID = urldecode($CareerID);
			$CareerID = $this->encrypt->decode($CareerID);
			
			/*
			//Tipos de usuarios
			$UserTypeID = $usr[0]->UserTypeID;
			if($UserTypeID=="1"){
				$SQL = "SELECT * FROM bn_groups WHERE ProfesorID='$UserID' AND Cancelled=0 AND CareerID='$CareerID' GROUP BY LevelCareer";
				$c = $this->general_model->QUERYS($SQL);
				
				$carr = '<option value="">Selecciona...</option';
				if($c){
					foreach ($c as $row) {
						$carr .= '
						<option value="'.$row->LevelCareer.'">'.$row->LevelCareer.'</option>
						';
					}
				}
			}
			if($UserTypeID=="4"){*/
				$SQL = "SELECT * FROM bn_career WHERE CareerID='$CareerID' AND Cancelled=0";

				$c = $this->general_model->QUERYS($SQL);

				$carr = '<option value="">Selecciona...</option><option value="">No aplica</option>';
				if($c){
					for ($i=1; $i <= $c[0]->CareerTotalNivels; $i++) { 
						$carr .= '
						<option value="'.$i.'">'.$i.'</option>
						';
					}
				}
			//}

			
			echo $carr;
		}
	}
	function asignarmateria(){
		$matE = $this->input->post('mat');
		$CourseID = trim($matE);
		$CourseID = urlencode($CourseID);
		$CourseID = str_replace("+", "%2B",$CourseID);
		$CourseID = urldecode($CourseID);
		$CourseID = $this->encrypt->decode($CourseID);

		$usrE = $this->input->post('usr');
		$UserID = trim($usrE);
		$UserID = urlencode($UserID);
		$UserID = str_replace("+", "%2B",$UserID);
		$UserID = urldecode($UserID);
		$UserID = $this->encrypt->decode($UserID);

		$DocDatesb = date('Y-m-d H:i:s');

		$data = array(
			'DocDate' => $DocDatesb,
			'DocUserID' => $this->session->userdata('UserID'),
			'UserID' => $UserID,
			'CourseID' => $CourseID
		);

		$insert = $this->alumnos_model->AddSuscription($CourseID,$UserID,$data);
		if($insert){
			echo "success";
		}else{
			echo "El alumno ya cuenta con esta materia asignada";
		}
		//echo $CourseID.'----'.$UserID;
	}
	function updateinfobasic(){
		$idID = $this->input->post('id');
		$CourseID = trim($idID);
		$CourseID = urlencode($CourseID);
		$CourseID = str_replace("+", "%2B",$CourseID);
		$CourseID = urldecode($CourseID);
		$CourseID = $this->encrypt->decode($CourseID);

		$CourseRequirements = $this->input->post('reqsvl');
		$CourseRequirements = trim($CourseRequirements);

		$CourseGoals = $this->input->post('objvl');
		$CourseGoals = trim($CourseGoals);

		$SQL = "UPDATE bn_materias SET AsignaturaRequirements='$CourseRequirements', AsignaturaGoals='$CourseGoals' WHERE CourseID='$CourseID'";
		$upt = $this->general_model->QUERYSUPT($SQL);
		echo "success";
		
	}
	function getcustomers(){
		//Vista de customers
		$ctm = $this->general_model->AllCustomer();
		$listclients = '<option value="">Selecciona escuela a la que pertenece</option>';
		if($ctm){
			foreach ($ctm as $row) {
				$CustomerIDEncrypt = $this->encrypt->encode($row->CustomerID);
				$listclients .= '
				<option value="'.$CustomerIDEncrypt.'">'.$row->CustomerName.'</option>
				';
			}
		}
		echo $listclients;
	}
	function TypeUser(){
		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		$CustomerID = $usr[0]->CustomerID;

		$UserTypeID = trim($this->input->post('typeusr'));
		$UserTypeID = urlencode($UserTypeID);
		$UserTypeID = str_replace("+", "%2B",$UserTypeID);
		$UserTypeID = urldecode($UserTypeID);
		$UserTypeID = $this->encrypt->decode($UserTypeID);

		$sllgusertype = $this->general_model->GetSlugUserType($UserTypeID);
		$UserTypeSlug = '';
		if($sllgusertype){
			$UserTypeSlug = $sllgusertype[0]->UserTypeSlug;
		}

		if($UserTypeSlug=='alumn'){
			//Vista de carreras del prof
			$CustomerID = $usr[0]->CustomerID;
			$UserID = $this->session->userdata('UserID');
			$SQL = "SELECT * FROM bn_career WHERE CustomerID='$CustomerID' AND Cancelled=0 GROUP BY CareerID";
			//$SQL = "SELECT * FROM bn_career WHERE CustomerID='$CustomerID' AND Cancelled=0";
			$c = $this->general_model->QUERYS($SQL);

			$carr = '<option value="">Selecciona...</option>';
			if($c){
				foreach ($c as $row) {
					$CareerIDEncryp = $this->encrypt->encode($row->CareerID);
					$carr .= '
					<option value="'.$CareerIDEncryp.'">'.$row->CareerName.'</option>
					';
				}
			}

			echo '
				<label>Matricula ó ID Referencia usuario <b class="red">*</b></label>
		        <input class="form-control idreference" type="text" name="idreference1" id="idreference1">
		        <span>No tengo ninguna de las dos,</span><a class="generateidreference" v="1">generar un ID Referencia unico</a><br>
		        <label>Correo del usuario <b class="red">*</b></label>
		        <input class="form-control emailreference" type="text" name="emailalumn1" id="emailalumn1">
		        <label>Carrera</label>
		        <select class="form-control carrera" id="carrera1">
		        	'.$carr.'
		        </select>
		        <label>Semestre/Cuatrimestre</label>
		        <select class="form-control semestre" id="semestre1">

		        </select>


			';

		}else{
			echo '
				<label>ID Referencia usuario <b class="red">*</b></label>
		        <input class="form-control idreference" type="text" name="idreference1" id="idreference1">
		        <span>No tengo ID Referencia,</span><a class="generateidreference" v="1">generar un ID Referencia unico</a><br>
		        <label>Correo del usuario <b class="red">*</b></label>
		        <input class="form-control emailreference" type="text" name="emailalumn1" id="emailalumn1">
			';
		}

	}
	function getlistalumn(){
		$UserTypeID = trim($this->input->post('typ'));
		$UserTypeID = urlencode($UserTypeID);
		$UserTypeID = str_replace("+", "%2B",$UserTypeID);
		$UserTypeID = urldecode($UserTypeID);
		$UserTypeID = $this->encrypt->decode($UserTypeID);

		$UserID = trim($this->input->post('usrid'));

		$ust = $this->alumnos_model->GetChildrenByUserType($UserTypeID);
		$list = '';
		if($ust){
			foreach ($ust as $vl) {
				$SbUserTypeID = $vl->UserTypeID;
				$alm = $this->alumnos_model->MisUsuarios($SbUserTypeID,$this->CustomerID);
				if($alm){
					foreach ($alm as $row) {
						$UserIDEncryp = $this->encrypt->encode($row->UserID);
						$list .= '
							<option value="'.$UserIDEncryp.'">'.$row->UserFirstName.' '.$row->UserLastName.'</option>
						';
					}
				}
			}
		}
		echo '
			<input type="hidden" value="'.$UserID.'" id="usr_asg" />
			<select class="selectpicker userlist" id="users_asg" data-live-search="true" >
				<option value="">Selecciona...</option>'
				.$list.'
		</select>';
	}
	function addasignacion(){
		$UserID = trim($this->input->post('usr'));
		$UserID = urlencode($UserID);
		$UserID = str_replace("+", "%2B",$UserID);
		$UserID = urldecode($UserID);
		$UserID = $this->encrypt->decode($UserID);

		$UserID2 = trim($this->input->post('usrelg'));
		$UserID2 = urlencode($UserID2);
		$UserID2 = str_replace("+", "%2B",$UserID2);
		$UserID2 = urldecode($UserID2);
		$UserID2 = $this->encrypt->decode($UserID2);

		$DocDate = date('Y-m-d H:i:s');
		$data = array(
			'DocDate' => $DocDate,
			'DocUserID' => $this->session->userdata('UserID'),
			'ResponsableID' => $UserID,
			'UserID' => $UserID2
		);
		
		$isrt = $this->alumnos_model->InsertAsignation($UserID,$UserID2,$data);
		if($isrt){
			echo "success";
		}else{
			echo $this->ExiteRegistro;
		}

		//$UserID = trim($this->input->post('usrid'));
	}
	function addmattoprof(){
		$AsignaturaID = trim($this->input->post('mat'));
		$AsignaturaID = urlencode($AsignaturaID);
		$AsignaturaID = str_replace("+", "%2B",$AsignaturaID);
		$AsignaturaID = urldecode($AsignaturaID);
		$AsignaturaID = $this->encrypt->decode($AsignaturaID);

		$UserID = trim($this->input->post('prof'));
		$UserID = urlencode($UserID);
		$UserID = str_replace("+", "%2B",$UserID);
		$UserID = urldecode($UserID);
		$UserID = $this->encrypt->decode($UserID);
		$usrinf = $this->general_model->AboutUser($UserID);
		$UserName = '';
		if($usrinf){
			$UserName = $usrinf[0]->UserFirstName.' '.$usrinf[0]->UserLastName;
		}

		$CustomerID = trim($this->input->post('ctm'));
		$CustomerID = urlencode($CustomerID);
		$CustomerID = str_replace("+", "%2B",$CustomerID);
		$CustomerID = urldecode($CustomerID);
		$CustomerID = $this->encrypt->decode($CustomerID);

		$CourseStar = trim($this->input->post('star'));
		$CourseEnd = trim($this->input->post('end'));

		//Consultar asignatura
		$asig = $this->alumnos_model->AsignaturaByID($AsignaturaID);
		if($asig){
			$AsignaturaName = $asig[0]->AsignaturaName;
			$AsignaturaRequirements = $asig[0]->AsignaturaRequirements;
			$AsignaturaGoals = $asig[0]->AsignaturaGoals;

			$DocDate = date('Y-m-d H:i:s');
			$data = array(
				'DocDate' => $DocDate,
				'DocUserID' => $this->session->userdata('UserID'),
				'AsignaturaID' => $AsignaturaID,
				//'CourseName' => $AsignaturaName.' by '.$UserName,
				'CourseName' => $AsignaturaName,
				'CourseProfesorID' => $UserID,
				'CustomerID' => $CustomerID,
				'CourseRequirements' => $AsignaturaRequirements,
				'CourseGoals' => $AsignaturaGoals,
				'CourseStar' => $CourseStar,
				'CourseEnd' => $CourseEnd
			);

			$insert = $this->alumnos_model->AddMatProf($AsignaturaID,$UserID,$data);
			if($insert){
				echo "success";
			}else{
				echo "El profesor ya cuenta con esta materia asignada";
			}
		}else{
			echo "Tuvimos problemas al asignar esta materia";
		}
	}
	function SubUsers(){
		$UserID = trim($this->input->post('userid'));
		$UserID = urlencode($UserID);
		$UserID = str_replace("+", "%2B",$UserID);
		$UserID = urldecode($UserID);
		$UserID = $this->encrypt->decode($UserID);

		$CustomerID = trim($this->input->post('ctm'));
		$CustomerID = urlencode($CustomerID);
		$CustomerID = str_replace("+", "%2B",$CustomerID);
		$CustomerID = urldecode($CustomerID);
		$CustomerID = $this->encrypt->decode($CustomerID);

		$u = $this->general_model->AboutUser($UserID);
		if($u){
			$sub = '<option value="">Seleccionar...</option>';
			$UserTypeID = $u[0]->UserTypeID;
			$cld = $this->alumnos_model->UserUnder($UserTypeID,$CustomerID);
			if($cld){
				foreach ($cld as $row) {
					$encrypt = $this->encrypt->encode($row->UserID);
					$sub .= '
						<option value="'.$encrypt.'">'.$row->UserFirstName.' '.$row->UserLastName.'</option>
					';
				}
			}
			echo $sub;
		}
	}
	function asgme(){
		$ResponsableID = trim($this->input->post('usr'));
		$ResponsableID = urlencode($ResponsableID);
		$ResponsableID = str_replace("+", "%2B",$ResponsableID);
		$ResponsableID = urldecode($ResponsableID);
		$ResponsableID = $this->encrypt->decode($ResponsableID);

		$UserID = trim($this->input->post('almtoasig'));
		$UserID = urlencode($UserID);
		$UserID = str_replace("+", "%2B",$UserID);
		$UserID = urldecode($UserID);
		$UserID = $this->encrypt->decode($UserID);

		$CustomerID = trim($this->input->post('ctm'));
		$CustomerID = urlencode($CustomerID);
		$CustomerID = str_replace("+", "%2B",$CustomerID);
		$CustomerID = urldecode($CustomerID);
		$CustomerID = $this->encrypt->decode($CustomerID);

		$DocDate = date('Y-m-d H:i:s');
		$data = array(
			'DocDate' => $DocDate,
			'DocUserID' => $this->session->userdata('UserID'),
			'ResponsableID' => $ResponsableID,
			'UserID' => $UserID
		);
		$insert = $this->alumnos_model->AddMyResponsables($ResponsableID,$UserID,$data);
		if($insert){
			echo "success";
		}else{
			echo "Esta asignacinación ya existe";
		}
	}
	function updateuser(){
		$UserID = trim($this->input->post('usr'));
		$UserID = urlencode($UserID);
		$UserID = str_replace("+", "%2B",$UserID);
		$UserID = urldecode($UserID);
		$UserID = $this->encrypt->decode($UserID);
		$usrinf = $this->general_model->AboutUser($UserID);
		$UserName = '';
		if($usrinf){
			$UserFirstName = trim($this->input->post('name'));
			$UserLastName = trim($this->input->post('app'));

			$data = array(
				'UserFirstName' => $UserFirstName,
				'UserLastName' => $UserLastName
			);

			$where = array('UserID' => $UserID);

			$upt = $this->alumnos_model->UpdateUser($where,$data);

			$enc = $this->encrypt->encode($UserID);
			redirect(base_url().'alumnos/info/?a='.$enc);
			
		}
	}
}
?>