<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unidadfunc extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('materias_model');
		$this->load->model('unidad_model');
		if ( ! $this->session->userdata('UserEmail')){ 
	        redirect(base_url());
	    }
	}
	function bysection(){
		$SectionIDE = $this->input->post('v');

		$SectionID = urlencode($SectionIDE);
		$SectionID = str_replace("+", "%2B",$SectionID);
		$SectionID = urldecode($SectionID);
		$SectionID = $this->encrypt->decode($SectionID);

		$c = $this->unidad_model->AboutUnidad($SectionID);
		if($c){
			echo $c[0]->NameSection;
		}
	}
	function updatename(){
		$SectionIDE = $this->input->post('idunid');

		$SectionID = urlencode($SectionIDE);
		$SectionID = str_replace("+", "%2B",$SectionID);
		$SectionID = urldecode($SectionID);
		$SectionID = $this->encrypt->decode($SectionID);

		$NameSection = $this->input->post('nombre');
		$NameSection = trim($NameSection);

		$SQL = "UPDATE bn_section SET NameSection='$NameSection' WHERE SectionID='$SectionID'";
		$upt = $this->general_model->QUERYSUPT($SQL);
		echo "success";
	}
	function creatematerial(){

		$SectionIDE = $this->input->post('idscm');

		$SectionID = urlencode($SectionIDE);
		$SectionID = str_replace("+", "%2B",$SectionID);
		$SectionID = urldecode($SectionID);
		$SectionID = $this->encrypt->decode($SectionID);

		$MaterialName = $this->input->post('title_matl');
		$MaterialDescription = $this->input->post('dsc_matl');
		$TypeMaterial = $this->input->post('typematvl');
		$videoyoutube = $this->input->post('videoyoutube');

		$target_dir = "";
		$target_file = "";
		$filinsert = "";
		$target_file = "";

		$create = 0;
		if($TypeMaterial!='4'){
			$file = $_FILES["file"];
			$FileSize = $file["size"];

			$DocUserID = $this->session->userdata('UserID');
			$target_dir = "files/".$DocUserID."/".$SectionID;
			$target_file = $target_dir."/".basename($file["name"]);
			//$filinsert = $file["tmp_name"]."/".$file["name"];
			$filinsert = $file["tmp_name"];
			$dato = array();
			$DocDate = date('Y-m-d H:i:s');
			if($FileSize<=2097152){
				$TypeFile = $file["type"];
				$explode = explode("/", $TypeFile);
				if($TypeMaterial=='1'){

					//print_r($file);
					if($explode[0]=='application' || $TypeFile=='text/html'|| $TypeFile=='text/plain'){
						$dato = array(
							'DocDate' => $DocDate,
							'DocUserID' => $this->session->userdata('UserID'),
							'MaterialName' => $MaterialName,
							'MaterialDescription' => $MaterialDescription,
							'TypeMaterial' => $TypeMaterial,
							'MaterialLocation' => $target_file,
							'SectionID' => $SectionID,
						);
					}else{
						echo "Formato de imagen invalido, intentalo nuevamente";
					}
				}
				if($TypeMaterial=='2'){
					if($TypeFile=='image/jpeg'){
						$dato = array(
							'DocDate' => $DocDate,
							'DocUserID' => $this->session->userdata('UserID'),
							'MaterialName' => $MaterialName,
							'MaterialDescription' => $MaterialDescription,
							'TypeMaterial' => $TypeMaterial,
							'MaterialLocation' => $target_file,
							'SectionID' => $SectionID,
						);

					}else{
						echo "Formato de imagen invalido, intentalo nuevamente";
					}
				}
				if($TypeMaterial=='3'){
					if($TypeFile=='image/jpeg'){
						$dato = array(
							'DocDate' => $DocDate,
							'DocUserID' => $this->session->userdata('UserID'),
							'MaterialName' => $MaterialName,
							'MaterialDescription' => $MaterialDescription,
							'TypeMaterial' => $TypeMaterial,
							'MaterialLocation' => $target_file,
							'SectionID' => $SectionID,
						);

					}else{
						echo "Formato de imagen invalido, intentalo nuevamente";
					}
				}
			}else{
				echo "El tamaño del archivo sobrepasa el permitido por el sistema, asegurate que sea menor a 20MB";
			}
			if(count($dato)>0){
				$create = 1;
				//echo "Si tiene datos el array";
			}else{
				//echo "No tiene datos el array";
			}
		}else{
			$MaterialLocation = $this->input->post('videoyoutube');
			$DocDate = date('Y-m-d H:i:s');
			$dato = array(
				'DocDate' => $DocDate,
				'DocUserID' => $this->session->userdata('UserID'),
				'MaterialName' => $MaterialName,
				'MaterialDescription' => $MaterialDescription,
				'TypeMaterial' => $TypeMaterial,
				'MaterialLocation' => $MaterialLocation,
				'SectionID' => $SectionID
			);
		}
		if(count($dato)>0){
			$insert = $this->unidad_model->InsertMaterial($dato);
			if($insert){
				if($create){
					if (!file_exists(FCPATH.$target_dir)) {
						mkdir(FCPATH.$target_dir, 0777, true);
					}
					if (move_uploaded_file($filinsert, FCPATH.$target_file)){
						echo "success";
					}else{
						echo "Hubo problemas al subir el archivo, intenlo nuevamente y de ser continuo el error comunicate a <strong>contacto@xpercatcad.com</strong>";
					}
					//echo $filinsert."<br>". FCPATH.$target_file;
				}else{
					echo "success";
				}
			}else{
				echo "Tuvimo inconvenientes al crear tu material, intenlo nuevamente y de ser continuo el error comunicate a <strong>contacto@xpercatcad.com</strong>";
			}
		}

	}

}