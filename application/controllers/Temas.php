<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temas extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->helper('html');
        $this->load->model('materias_model');
        $this->load->model('temas_model');
        if (!$this->session->userdata('UserID')) {
            redirect(base_url());
        }
        $usrnc = $this->general_model->UserNC($this->session->userdata('UserID'));
        $this->UserTypeID = '';
        $this->CustomerID = '';
        if ($usrnc) {

        } else {
            $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
            if ($usr) {
                $this->UserTypeID = $usr[0]->UserTypeID;
                $this->CustomerID = $usr[0]->CustomerID;
            }
        }
    }

    public function index()
    {
        //$this->method_call =& get_instance();
        $TopicE = $this->input->get('t');

        $TopicID = urlencode($TopicE);
        $TopicID = str_replace("+", "%2B", $TopicID);
        $TopicID = urldecode($TopicID);
        $TopicID = $this->encrypt->decode($TopicID);

        $dato['tabla'] = $this->encrypt->encode('topics');
        $dato['id'] = $this->encrypt->encode('TopicID');

        $dato['UserTypeID'] = $this->UserTypeID;
        $dato['TopicID'] = $this->encrypt->encode($TopicID);

        $c = $this->materias_model->AboutTopic($TopicID);
        if ($c) {
            $dato['NameTopic'] = $c[0]->NameTopic;
            //obtener las categorias de templates
            $gct = $this->temas_model->GetCategorysTemplates();
            $categorias = '';
            if ($gct) {
                foreach ($gct as $row) {
                    $encrypt = $this->encrypt->encode($row->CategoryTemplateID);
                    $categorias .= '
						<div class="list-categ-template animate" vl="' . $encrypt . '">
							<a>' . $row->CategoryTemplateName . '</a>
						</div>
						';
                }
            }
            $dato['categorias'] = $categorias;
            //Obtener templates
            $templ = $this->temas_model->GetTemplatesById($TopicID);
            if ($templ) {
                $tablati = $this->encrypt->encode('topics_templates_items');
                $idti = $this->encrypt->encode('ItemID');
                $loads = '';
                $i = 1;
                $p = 0;

                foreach ($templ as $key => $row) {
                    //Para editar
                    $EncryptItemID = $this->encrypt->encode($row->ItemID);

                    $CategoryTemplateID = $row->CategoryTemplateID;
                    $ctg = $this->temas_model->AboutCategory($CategoryTemplateID);

                    $edt_item = '';
                    if ($ctg) {
                        $CategorySlug = $ctg[0]->CategorySlug;
                        $TemplateID = $row->TemplateID;
                        $Settings = $row->Settings;
                        //if($CategorySlug!='texto' && $CategorySlug!='complementos' && $CategorySlug!='divisiones'){
                        $save = '';
                        if ($TemplateID == '42') {
                            $edt_item = '
								<a class="edit-item-flash" v="' . $EncryptItemID . '" i="' . $row->OrdenItem . '" ct="' . $this->encrypt->encode($CategoryTemplateID) . '"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								';
                        }
                        if ($TemplateID == '45') {
                            $edt_item = '
								<a class="edit-item-puntos" v="' . $EncryptItemID . '" i="' . $row->OrdenItem . '" ct="' . $this->encrypt->encode($CategoryTemplateID) . '"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								';
                        }
                        if ($TemplateID == "49") {

                        }
                        if ($TemplateID != '42' && $TemplateID != '45' && $TemplateID != "49") {
                            $edt_item = '
								<a class="edit-item" v="' . $EncryptItemID . '" i="' . $row->OrdenItem . '" ct="' . $this->encrypt->encode($CategoryTemplateID) . '"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								';
                            //$save = '';
                            $save = '<a class="save-item" v="' . $EncryptItemID . '" i="' . $row->OrdenItem . '"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>';
                        }

                    }
                    //Los items
                    $Items = '';
                    if ($row->Items != "") {
                        $Items .= $row->Items;
                    } else {
                        $UrlFunctionTemplate = $row->UrlFunctionTemplate;
                        $CategoryTemplateID = $row->CategoryTemplateID;
                        $ItemID = $row->ItemID;
                        //Acera de ItemID
                        $gat = $this->temas_model->GetItemByID($ItemID);
                        if ($gat) {
                            $stg_item = $gat[0]->Settings;
                            if ($stg_item != "") {
                                $stgs = $stg_item;
                            } else {
                                $stgs = $Settings;
                            }
                            $ctg = $this->temas_model->AboutCategory($CategoryTemplateID);
                            if ($ctg) {
                                $csbit = $this->temas_model->GetSubItemByItemID($ItemID);
                                $total_sb = $this->temas_model->GetSubItemByItemIDAll($ItemID);
                                if ($csbit) {
                                    if ($TemplateID == '45') {
                                        $Items .= '
											<button class="btn btn-info addpoint" v="' . $this->encrypt->encode($ItemID) . '" tl="' . $total_sb . '">Agregar otro punto</button>
											<button type="button" class="btn btn-secondary changimg" v="' . $this->encrypt->encode($ItemID) . '" >Cambiar imagen de fondo</button>
											<div class="mis-items">
											';
                                        $imgbg = site_url('assets/img/puntos.jpg');
                                        $position = '';
                                        $description = '';

                                        $arrp = array();
                                        foreach ($csbit as $vl) {
                                            if ($vl->Type != 'background') {
                                                $sub_arrb = array(
                                                    'DocEntry' => $this->encrypt->encode($ItemID),
                                                    'Type' => $vl->Type,
                                                    'Valor' => $vl->Valor,
                                                    'OrdenSection' => $row->OrdenItem,
                                                    'Orden' => $vl->Orden,
                                                    'Setting' => $stgs
                                                );
                                                array_push($arrp, $sub_arrb);
                                            } else {
                                                $imgbg = site_url('../files/' . $vl->Valor);
                                            }

                                        }
                                        $agruparp = groupArray($arrp, 'Orden');

                                        foreach ($agruparp as $fila) {

                                            $sb = $fila['groupeddata'];

                                            $Items .= '<div class="points point' . $fila['Orden'] . '" style="' . $sb[2]['Valor'] . '" v="' . $this->encrypt->encode($ItemID) . '" i="' . $fila['Orden'] . '">
														<div class="double-bounce1"></div>
	  													<div class="double-bounce2"></div>
													</div>';

                                        }


                                        $Items .= '
													<img class="imagenps" src="' . $imgbg . '">
													</div>
												';
                                    } else {
                                        $arr = array();
                                        foreach ($csbit as $vl) {
                                            $sub_arr = array(
                                                'DocEntry' => $this->encrypt->encode($vl->DocEntry),
                                                'Type' => $vl->Type,
                                                'Valor' => $vl->Valor,
                                                'OrdenSection' => $row->OrdenItem,
                                                'Orden' => $vl->Orden,
                                                'Setting' => $stgs
                                            );
                                            array_push($arr, $sub_arr);
                                        }
                                        ///Consulta a func.php
                                        $agrupar = groupArray($arr, 'Orden');
                                        $Items .= $UrlFunctionTemplate($agrupar);
                                    }

                                } else {

                                    //if($CategorySlug!='texto' && $CategorySlug!='divisiones'){
                                    if ($TemplateID == '45') {
                                        $Items .= '
											
											<button type="button" class="btn btn-secondary changimg" v="' . $this->encrypt->encode($ItemID) . '" >Cambiar imagen de fondo</button>
											<div class="mis-items">
											';
                                        $imgbg = site_url('assets/img/puntos.jpg');
                                        $Items .= '
											
											<div class="mis-items">
												
												<img class="imagenps" src="' . $imgbg . '">
											</div>
											';
                                    }
                                    //print_r($row);
                                    //$Items .= '<a class="edit-item" v="'.$EncryptItemID.'" i="'.$row->OrdenItem.'" ct="'.$this->encrypt->encode($CategoryTemplateID).'" style="color: initial;">'.$row->TemplateItems.'</a>';
                                    if ($TemplateID == "49") {
                                        $Items .= '
											<div class="text-center" style="background: #dcd7d7;width: 100%;height: 130px;border: 4px solid #a5a6a7;padding-top: 52px;">
												<a href="' . base_url() . 'materias/addexamen/?l=' . $this->encrypt->encode($TopicID) . '&itm=' . $this->encrypt->encode($ItemID) . '"  target="_blank" >Administrar Quiz</a>
											</div>
											';
                                    }

                                    if ($TemplateID != '42' && $TemplateID != '45' && $TemplateID != "49") {
                                        $Items .= '
											<div class="text-center" style="background: #dcd7d7;width: 100%;height: 130px;border: 4px solid #a5a6a7;padding-top: 52px;">
												<a class="edit-item" v="' . $EncryptItemID . '" i="' . $row->OrdenItem . '" ct="' . $this->encrypt->encode($CategoryTemplateID) . '">Agregar contenido </a>
											</div>
											';
                                    }


                                }

                            }
                        }

                    }
                    $Items = stripslashes($Items);


                    if ($i == 1) {
                        if (count($templ) > 1) {
                            $nextv = $templ[$p + 1]->ItemID;
                        } else {
                            $nextv = 0;
                        }
                        $flechas = '
							<a class="change-orden" v="' . $this->encrypt->encode($row->ItemID) . '" oth="' . $this->encrypt->encode($nextv) . '" i="' . $row->OrdenItem . '" typ="down" ky="' . $key . '"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
							';
                    }
                    if ($i == count($templ)) {
                        if ($p - 1 >= 0) {
                            $prev = $templ[$p - 1]->ItemID;
                        } else {
                            $prev = 0;
                        }
                        $nextv = 0;
                        $flechas = '
							<a class="change-orden" v="' . $this->encrypt->encode($row->ItemID) . '" oth="' . $this->encrypt->encode($prev) . '" i="' . $row->OrdenItem . '" typ="up" ky="' . $key . '"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
							';
                    }

                    if ($i != 1 && $i != count($templ)) {
                        $nextv = $templ[$p + 1]->ItemID;
                        $prev = $templ[$p - 1]->ItemID;
                        $flechas = '
							<a class="change-orden" v="' . $this->encrypt->encode($row->ItemID) . '" oth="' . $this->encrypt->encode($nextv) . '" i="' . $row->OrdenItem . '" typ="down" ky="' . $key . '"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
							<a class="change-orden" v="' . $this->encrypt->encode($row->ItemID) . '" oth="' . $this->encrypt->encode($prev) . '" i="' . $row->OrdenItem . '" typ="up" ky="' . $key . '"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
							';
                    }

                    $loads .=
                        '
						<div class="col-xs-12 col-sm-12 col-lg-12 padding0 orden' . $key . ' ">
							<div class="col-xs-12 col-sm-12 col-lg-12 padding0 sectionstm section' . $row->OrdenItem . ' sectionstmnpdd sction-have" i="' . $row->OrdenItem . '" tmp="' . $row->TemplateID . '"><div>' . $Items . '</div>
								
								<a class="add-sction add-after" nxt="' . $this->encrypt->encode($nextv) . '" tpc="' . $this->encrypt->encode($TopicID) . '"><i class="fa fa-plus" aria-hidden="true"></i></a>
								
							</div>
							<div class="bttonsedit">
								' . $flechas . $edt_item . $save . '
								
								<a class="dlt-items" t="' . $tablati . '" id="' . $idti . '" vl="' . $EncryptItemID . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a
							</div>
						</div>
						</div>
						';

                    $i++;
                    $p++;
                }
            } else {

                $loads = '
					<div class="col-xs-12 col-sm-12 col-lg-12 text-center">
						<a class="add-sction" nxt="' . $this->encrypt->encode(0) . '" tpc="' . $this->encrypt->encode($TopicID) . '"><i class="fa fa-plus" aria-hidden="true"></i></a>
					</div>
					';
            }
            $dato['loads'] = $loads;
            //
            //Estilos
            $CourseID = $c[0]->CourseID;
            $stl = $this->materias_model->GetCoursesByID($CourseID);
            $CourseFont = '';
            $ColorPalette1 = '';
            $ColorPalette2 = '';
            $ColorPalette3 = '';
            if ($stl) {
                $font = $stl[0]->CourseFont;
                if ($font != "") {
                    if ($font == 'roboto') {
                        $CourseFont = "'Roboto', sans-serif";
                    }
                    if ($font == 'nunito') {
                        $CourseFont = "'Nunito', sans-serif";
                    }
                    if ($font == 'opensans') {
                        $CourseFont = "'Open Sans', sans-serif";
                    }
                    if ($font == 'lato') {
                        $CourseFont = "'Lato', sans-serif";
                    }

                } else {
                    $CourseFont = "'Open Sans', sans-serif";
                }

                $clr1 = $stl[0]->ColorPalette1;
                if ($clr1 != "") {
                    $ColorPalette1 = $clr1;
                } else {
                    $ColorPalette1 = '#ed8721';
                }
                $clr2 = $stl[0]->ColorPalette2;
                if ($clr2 != "") {
                    $ColorPalette2 = $clr2;
                } else {
                    $ColorPalette2 = '#e5e5e5';
                }
                $clr3 = $stl[0]->ColorPalette3;
                if ($clr3 != "") {
                    $ColorPalette3 = $clr3;
                } else {
                    $ColorPalette3 = '#111111';
                }

            }
            $dato['CourseFont'] = $CourseFont;
            $dato['ColorPalette1'] = $ColorPalette1;
            $dato['ColorPalette2'] = $ColorPalette2;
            $dato['ColorPalette3'] = $ColorPalette3;

            $this->load->view('leccion-single', $dato);
        }
        /*
        }else{
            redirect(base_url().'inicio');
        }*/

    }

    public function texto()
    {
        $TopicE = $this->input->get('tpc');
        $TopicID = urlencode($TopicE);
        $TopicID = str_replace("+", "%2B", $TopicID);
        $TopicID = urldecode($TopicID);
        $TopicID = $this->encrypt->decode($TopicID);

        $TemplateID = trim($this->input->get('t'));

        $ItemID = trim($this->input->get('itm'));
        $ItemID = urlencode($ItemID);
        $ItemID = str_replace("+", "%2B", $ItemID);
        $ItemID = urldecode($ItemID);
        $ItemID = $this->encrypt->decode($ItemID);

        $dato['Topic'] = $TopicE;
        $dato['Template'] = $TemplateID;

        $gtt = $this->temas_model->GetTemplateByTT($TopicID, $ItemID);
        if ($gtt) {

            $content = '';
            foreach ($gtt as $row) {
                $UrlView = $row->UrlView;
                $Items = $row->Items;
                $Items = stripslashes($Items);
                $content .= $Items;
            }
            $UrlView = $gtt[0]->UrlView;
            $dato['content'] = $content;
            //$dato['Orden'] = $gtt[0]->Orden;
        } else {
            $templ = $this->temas_model->GetTemplate($TemplateID);
            if ($templ) {
                $UrlView = $templ[0]->UrlView;
                $dato['content'] = '';
                //
                $SQL = "SELECT * FROM bn_topics_templates_items WHERE TopicID=$TopicID AND Cancelled=0";
                $tt = $this->general_model->QUERYS($SQL);
                if ($tt) {
                    $or = count($tt) + 1;
                } else {
                    $or = 1;
                }
                //$dato['Orden'] = $or;
            }
        }
        $dato['Orden'] = trim($this->input->get('ord'));
        $this->load->view('templates/' . $UrlView, $dato);

    }

    public function video()
    {
        $TopicE = $this->input->get('tpc');
        $TopicID = urlencode($TopicE);
        $TopicID = str_replace("+", "%2B", $TopicID);
        $TopicID = urldecode($TopicID);
        $TopicID = $this->encrypt->decode($TopicID);

        $TemplateID = trim($this->input->get('t'));

        $ItemID = trim($this->input->get('itm'));
        $ItemID = urlencode($ItemID);
        $ItemID = str_replace("+", "%2B", $ItemID);
        $ItemID = urldecode($ItemID);
        $ItemID = $this->encrypt->decode($ItemID);

        $dato['Topic'] = $TopicE;
        $dato['Template'] = $TemplateID;

        $SQL = "SELECT * FROM bn_topics_templates WHERE TemplateID=$ItemID AND Cancelled=0";
        $gtt = $this->general_model->QUERYS($SQL);

        if ($gtt) {

            $content = '';

            $UrlView = $gtt[0]->UrlView;
            $dato['content'] = $content;
            //$dato['Orden'] = $gtt[0]->Orden;
        } else {
            $templ = $this->temas_model->GetTemplate($TemplateID);
            if ($templ) {
                $UrlView = $templ[0]->UrlView;
                $dato['content'] = '';
                //
                $SQL = "SELECT * FROM bn_topics_templates_items WHERE TopicID=$TopicID AND Cancelled=0";
                $tt = $this->general_model->QUERYS($SQL);
                if ($tt) {
                    $or = count($tt) + 1;
                } else {
                    $or = 1;
                }
                //$dato['Orden'] = $or;
            }
        }
        $dato['Orden'] = trim($this->input->get('ord'));
        $this->load->view('templates/' . $UrlView, $dato);
    }

}