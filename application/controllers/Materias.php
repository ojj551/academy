<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materias extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('materias_model');
		$this->load->model('alumnos_model');
		$this->load->model('cursos_model');
		$this->load->model('temas_model');
		$this->load->model('carreras_model');
		if ( ! $this->session->userdata('UserID')){ 
	        redirect(base_url());
	    }

	    $this->actual_link = actual_link();
	    $this->UserTypeID = '';
	    $this->UserNC = 0; 
	    $usrnc = $this->general_model->UserNC($this->session->userdata('UserID'));
	    if($usrnc){
	    	$this->UserNC = 1; 
	    }else{

		    $usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		    $this->UserThings = $usr;
		    $this->UserTypeID = '';
		    $this->CustomerID = '';
			if($usr){
				$CustomerIDP = $usr[0]->CustomerID;
				$this->UserTypeID = $usr[0]->UserTypeID;
				$this->CustomerID = $usr[0]->CustomerID;
			}
			$sllgusertype = $this->general_model->GetSlugUserType($usr[0]->UserTypeID);
			$this->UserTypeSlug = '';
			if($sllgusertype){
				$UserTypeSlug = $sllgusertype[0]->UserTypeSlug;
				$this->UserTypeSlug = $sllgusertype[0]->UserTypeSlug;
			}
			$this->Lang = 'es';
			$cm = $this->general_model->AboutCustomerByID($CustomerIDP);
			$this->LeadType = '';
			if($cm){
				$this->LeadType = $cm[0]->LeadType;
			}
			//Datos para formulario
			$this->FormCancel = $this->general_model->GetText($this->Lang,'','form.cancel');
			//
			$this->FormGuardar = $this->general_model->GetText($this->Lang,'','form.guardar');
			//
			$this->ModalAdd = $this->general_model->GetText($this->Lang,'','modal.add');

			$this->Depto = $this->general_model->GetText($this->Lang,$this->LeadType,'depto');
			$this->FormName = $this->general_model->GetText($this->Lang,'','form.nombre');
		}
	}
	/*tal vez se borre es donde se ve el banner y el temario se reemplazara por admin()*/
	public function prevista(){
		$MateriaIDE = $this->input->get('m');

		$MateriaID = urlencode($MateriaIDE);
		$MateriaID = str_replace("+", "%2B",$MateriaID);
		$MateriaID = urldecode($MateriaID);
		$MateriaID = $this->encrypt->decode($MateriaID);

		
		$dato['MateriaIDEncryp'] = '';
	
		$inf = $this->general_model->AboutMateria($MateriaID);
		if($inf){
			$CourseName = $inf[0]->CourseName;
			$MateriaIDEncryp = $this->encrypt->encode($MateriaID);
			$dato['CourseName'] = $CourseName;
			$dato['MateriaIDEncryp'] = $MateriaIDEncryp;
			$dato['CourseDescription'] = $inf[0]->CourseDescription;
			$dato['UserTypeID'] = $this->UserTypeID;
			$dato['Inc'] = $inf[0]->CourseStar;
			$dato['Fin'] = $inf[0]->CourseEnd;
			$dato['Banner'] = $inf[0]->CourseImage;
			$dato['CourseRequirements'] = $inf[0]->CourseRequirements;
			$dato['CourseGoals'] = $inf[0]->CourseGoals;

			//Obtener las unidades y los topics
			$gsc = $this->materias_model->GetSection($this->session->userdata('UserID'),$MateriaID);
			$list = '';
			if($gsc){
				$tabla = $this->encrypt->encode('topics');
				$id = $this->encrypt->encode('TopicID');
				foreach ($gsc as $vl) {
					$tablasc = $this->encrypt->encode('section');
					$idsc = $this->encrypt->encode('SectionID');

					$TopicIDEncryptUnidad = $this->encrypt->encode($vl->SectionID);
					$head = '
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<h4 class="tpg-relawey"><strong>Unidad '.$vl->OrderSection.',</strong> <a href="'.base_url().'unidad/?u='.$TopicIDEncryptUnidad.'">'.$vl->NameSection.'</a> <a class="addtm" v="'.$TopicIDEncryptUnidad.'" style="font-size: 9px;">Agregar tema</a> <a class="dlt" t="'.$tablasc.'" id="'.$idsc.'" vl="'.$TopicIDEncryptUnidad.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a></h4>
						</div>
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="col-xs-12 col-sm-12 col-lg-12">
						';

					$gut = $this->materias_model->GetSectionTopics($vl->SectionID,false);
					$gutsb = $this->materias_model->GetSectionTopics($vl->SectionID,true);
					$topics = '';
					//Cascada 1
					if($gut){
						
						foreach ($gut as $row) {
							$fc = $this->Get_childrens($row->TopicID);
							foreach ($fc as $k => $vl) {
								$abt = $this->materias_model->AboutTopic($k);
							}
							$childtopics = '<div class="col-xs-12 col-sm-12 col-lg-12">';
							//Cascada 2
							if($gutsb){
								foreach ($gutsb as $sb) {
									if($sb->Parent==$row->TopicID){
										$sbchildtopics = '<div class="col-xs-12 col-sm-12 col-lg-12">';
										//Cascada 3
										if($gutsb){
											foreach ($gutsb as $sbsb) {

												$sbsbchildtopics = '<div class="col-xs-12 col-sm-12 col-lg-12">';
												//cascada 4

												if($gutsb){
													foreach ($gutsb as $sbsbsb) {
														if($sbsbsb->Parent==$sbsb->TopicID){
															$TopicIDEncryptSbSbSb = $this->encrypt->encode($sbsbsb->TopicID);
															$sbchildtopics .= '
															<a href="'.base_url().'temas/?t='.$TopicIDEncryptSbSbSb.'" style="font-size: 20px;"><strong>'.$sbsbsb->OrderTopic.'</strong>.- '.$sbsbsb->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncryptSbSbSb.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncryptSbSbSb.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>';

														}
														
													}
												}
												$sbsbchildtopics .= '</div>';
												if($sbsb->Parent==$sb->TopicID){
													$TopicIDEncryptSbSb = $this->encrypt->encode($sbsb->TopicID);
													$sbchildtopics .= '
													<a href="'.base_url().'temas/?t='.$TopicIDEncryptSbSb.'" style="font-size: 20px;"><strong>'.$sbsb->OrderTopic.'</strong>.- '.$sbsb->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncryptSbSb.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncryptSbSb.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>'.$sbsbchildtopics;

												}
											}
										}

										$sbchildtopics .= '</div>';
										$TopicIDEncryptSb = $this->encrypt->encode($sb->TopicID);
										$childtopics .= '
										<a href="'.base_url().'temas/?t='.$TopicIDEncryptSb.'" style="font-size: 20px;"><strong>'.$sb->OrderTopic.'</strong>.- '.$sb->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncryptSb.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncryptSb.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>'.$sbchildtopics;

									}
								}
							}
							
							$childtopics .= '</div>';
							$TopicIDEncrypt = $this->encrypt->encode($row->TopicID);
							
							$topics .= '
								<a href="'.base_url().'temas/?t='.$TopicIDEncrypt.'" style="font-size: 20px;"><strong>'.$row->OrderTopic.'</strong>.- '.$row->NameTopic.'</a> <a class="addsubtema" v="'.$TopicIDEncrypt.'" style="font-size: 9px;">Agregar sub-tema</a> <a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$TopicIDEncrypt.'" style="color: #F44336;">Eliminar <i class="fa fa-trash-o" aria-hidden="true"></i></a><br>
								'.$childtopics.'
							';
						}
					}
					$footer = '</div></div>';
					$list .= $head.$topics.$footer;
				}
			}
			
			$dato['list'] = $list;
			$btn_exam = ''; $btn_preview = '';
			//Tiene ya examen?
			if($gsc){
				$exm = $this->materias_model->CourseExam($MateriaID);
				if($exm){
					$ExamEncryt = $this->encrypt->encode($exm[0]->QuizID);
					$btn_exam = '<a class="btn btn-primary" href="'.base_url().'materias/examen/?ex='.$ExamEncryt.'">Ver exámen</a>';
				}else{
					$btn_exam = '<a class="btn btn-primary" href="'.base_url().'materias/addexamen/?c='.$MateriaIDEncryp.'">Crear exámen del curso</a>';
				}
				//Vista previa
				$btn_preview = '
				<a href="'.base_url().'materias/vista/?c='.$MateriaIDEncryp.'">
					<button class="btn ripple-infinite btn-gradient btn-info">
				        <div>
				          Vista previa
				        </div>
				    </button>
			    </a><br><br>
				';
			}
			$dato['btn_exam'] = $btn_exam;
			$dato['btn_preview'] = $btn_preview;
			//if($usr[0]->UserTypeID=='1'){
			if($this->UserNC==1){

				$temas = '';

				$get_l = $this->cursos_model->CountTopics($MateriaID,$this->session->userdata('DocEntry'));
				if($get_l){
					$tabla = $this->encrypt->encode('topics');
					$id = $this->encrypt->encode('TopicID');
					foreach ($get_l as $row) {
						$temas .= '
						<li class="col-sm-12 col-md-12 col-lg-12 des-li ui-state-default" v="'.$row->TopicID.'" o="'.$row->OrderTopic.'">
						<div class="col-sm-12 col-md-12 col-lg-12 padding0" style="margin-top:10px;margin-bottom:10px;">
							<div class="col-sm-12 col-md-12 col-lg-12 padding0">
								<input class="form-control edit-lesson " type="text" placeholder="Escribe nombre del tema" value="'.$row->NameTopic.'" v="'.$this->encrypt->encode($row->TopicID).'" />
					
								<div class="btn-group right-option-v1" style="top: 9px;font-size: 33px;">
			                        <i class="icon-options-vertical icons box-v7-menu" data-toggle="dropdown"></i>
			                        <ul class="dropdown-menu" role="menu" style="left: initial; right: 0px;">
			                          <li><a href="'.base_url().'temas/?t='.$this->encrypt->encode($row->TopicID).'"><i class="fa fa-pencil" aria-hidden="true"></i> Administrar contenido</a></li>
			                          <li><a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$this->encrypt->encode($row->TopicID).'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a></li>
			                        </ul>
			                    </div>
							</div>
						</div>
						</li>
						';
					}
					
				}
				$temas .= '
					';
				
				$dato['temas'] = $temas;
				$this->load->view('nc/materias-admin',$dato);
			}else{
				$this->load->view('materias-admin',$dato);
			}
		}
		

	}
	///vista general de curso
	public function admin(){
		$MateriaIDE = $this->input->get('m');

		$MateriaID = urlencode($MateriaIDE);
		$MateriaID = str_replace("+", "%2B",$MateriaID);
		$MateriaID = urldecode($MateriaID);
		$MateriaID = $this->encrypt->decode($MateriaID);
		
		$MaTEncrypt = $this->encrypt->encode($MateriaID);
		$dato['MateriaIDEncryp'] = $this->encrypt->encode($MateriaID);

		$inf = $this->general_model->AboutMateria($MateriaID);
		if($inf){
			if(!isset($_GET['n'])){
				$ntb = 1;
			}else{
				$ntb = $_GET['n'];
			}
			//Generales
			if($ntb==1){
				$CutomerIDEncrypt = md5($this->CustomerID);
				$ProfesorIDEncrypt = md5($inf[0]->CourseProfesorID);
				$MateriaIDEncrypt = md5($MateriaID);
				$Imagen = $this->actual_link.'/files/Customers/'.$CutomerIDEncrypt.'/prof/'.$ProfesorIDEncrypt.'/'.$MateriaIDEncrypt.'/'.$inf[0]->CourseImage;
				$dato['CourseImage'] = $Imagen;
				$dato['CourseDescription'] = $inf[0]->CourseDescription;
				$dato['CourseRequirements'] = $inf[0]->CourseRequirements;
				$dato['CourseGoals'] = $inf[0]->CourseGoals;

				//Styles
				$dato['CourseFont'] = $inf[0]->CourseFont;

				if($inf[0]->ColorPalette1!=""){	$ColorPalette1 = $inf[0]->ColorPalette1; }else{ $ColorPalette1 = '#FFFFFF'; }
				if($inf[0]->ColorPalette2!=""){	$ColorPalette2 = $inf[0]->ColorPalette2; }else{ $ColorPalette2 = '#FFFFFF'; }
				if($inf[0]->ColorPalette3!=""){	$ColorPalette3 = $inf[0]->ColorPalette3; }else{ $ColorPalette3 = '#FFFFFF'; }


				$dato['ColorPalette1'] = $ColorPalette1;
				$dato['ColorPalette2'] = $ColorPalette2;
				$dato['ColorPalette3'] = $ColorPalette3;
				
				$this->load->view('profesor/admin-cursos-generales',$dato);
			}
			//Temarios
			if($ntb==2){
				//Obtener las unidades y los topics
				$gsc = $this->materias_model->GetSection($MateriaID);
				$list = '';

				if($gsc){
					$paint = 0;
					$tabla = $this->encrypt->encode('topics');
					$id = $this->encrypt->encode('TopicID');
					$tablasc = $this->encrypt->encode('section');
					$idsc = $this->encrypt->encode('SectionID');
					$ord = 1;
					foreach ($gsc as $vl) {
						$TopicIDEncryptUnidad = $this->encrypt->encode($vl->SectionID);
						$head = '
							<div class="col-xs-12 col-sm-12 col-lg-12">
								<h4 class="tpg-relawey"><strong>Unidad '.$ord.',</strong> <a href="'.base_url().'unidad/?u='.$TopicIDEncryptUnidad.'">'.$vl->NameSection.'</a> <a class="dlt" t="'.$tablasc.'" id="'.$idsc.'" vl="'.$TopicIDEncryptUnidad.'" style="color: #F44336;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></h4>
							</div>
							<div class="col-xs-12 col-sm-12 col-lg-12">
								<div class="col-xs-12 col-sm-12 col-lg-12">
								<ul class="sortable">
							';

						$gut = $this->materias_model->GetSectionTopics($vl->SectionID,false);
						$gutsb = $this->materias_model->GetSectionTopics($vl->SectionID,true);
						$topics = '';
						//Cascada 1
						if($gut){
							
							foreach ($gut as $g) {
								$topics .= '
									<li class="col-sm-12 col-md-12 col-lg-12 des-li ui-state-default" v="'.$g->TopicID.'" o="'.$g->OrderTopic.'" padding0>
									<div class="col-sm-12 col-md-12 col-lg-12 padding0" style="margin-top:10px;margin-bottom:10px;">
										<div class="col-sm-12 col-md-12 col-lg-12 padding0">
											<input class="form-control edit-lesson " type="text" placeholder="Escribe nombre del tema" value="'.$g->NameTopic.'" v="'.$this->encrypt->encode($g->TopicID).'" />
								
											<div class="btn-group right-option-v1" style="top: 1px;font-size: 26px;">
						                        <i class="icon-options-vertical icons box-v7-menu" data-toggle="dropdown"></i>
						                        <ul class="dropdown-menu" role="menu" style="left: initial; right: 0px;">
						                          <li><a href="'.base_url().'temas/?t='.$this->encrypt->encode($g->TopicID).'"><i class="fa fa-pencil" aria-hidden="true"></i> Administrar contenido</a></li>
						                          <li><a class="vpaint" v="'.$this->encrypt->encode($g->TopicID).'" shw="'.$paint.'"><i class="fa fa-chevron-down" aria-hidden="true"></i> Sub temas</a></li>
						                          <li><a class="dlt" t="'.$tabla.'" id="'.$id.'" vl="'.$this->encrypt->encode($g->TopicID).'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a></li>
						                        </ul>
						                    </div>
										</div>
										<div class="allpaint paint_'.$paint.'">

										</div>
									</div>
									</li>
									';
									$paint++;
							}
						}

						$parentencryt = $this->encrypt->encode(0);
						$footer = '</ul>
							<div>
								<ul>
									<li class="col-sm-12 col-md-12 col-lg-12 ui-state-default">
										<div class="col-sm-12 col-md-12 col-lg-12 padding0" style="margin-top:10px;margin-bottom:10px;">
											<input class="form-control lesson enter-lesson" type="text" placeholder="Escribe nueva lección" autofocus v="'.$MaTEncrypt.'" u="'.$TopicIDEncryptUnidad.'" p="'.$parentencryt.'">
										</div>
									</li>
								</ul>
							</div>
						</div></div>';
						//$list .= $head.$topics.$footer;
						$list .= $head.$topics.$footer;
						$ord++;
					}
				}
				
				$dato['list'] = $list;

				$this->load->view('profesor/admin-cursos-temario',$dato);
			}
			//Alumnos
			if($ntb==3){
				$alc = $this->cursos_model->AlumnosByCourse($MateriaID);
				$alumnos = '';
				if($alc){
					
					foreach ($alc as $row) {
						$UserIDEncrypt = $this->encrypt->encode($row->UserID);
						//
						$ver = '';
						$interacciones_rg = '';
						if($row->ConfirmationID!="" && $row->ConfirmationDetaills!=""){
							$ver = '
							<a href="'.base_url().'alumnos/info/?a='.$UserIDEncrypt.'">Ver</a>
							';
						}else{
							
							if($row->ConfirmationID!=""){
								$interacciones_rg = 'Agrego sus datos, como nombre contraseña, etc. Pero no accedio a la liga que se envio a su correo para poder activar el paso final.';
							}else{
								$interacciones_rg = 'No ha tenido ninguna interacción con el sistema.';
							}
							$ver = '
							<a class="details_inactive" data-toggle="modal" data-target="#myModal" int="'.$interacciones_rg.'">Ver</a>
							';
						}
						//Carrera
						$crr = $this->carreras_model->AboutCareer($row->CareerID);
						$carrera = '';
						if($crr){
							$carrera = $crr[0]->CareerName;
						}
						$cm = $this->general_model->AboutCustomerByID($this->CustomerID);
						if($cm){
							if($cm[0]->TypeNivelID==1){
								$type_nivel = 'Semestre';
							}else{
								$type_nivel = 'Cuatrimestre';
							}
						}
						if($row->UserSemester!=0){
							$dsc = '<b style="font-size:9px;">'.$row->UserSemester.' '.$type_nivel.'</b>';
						}else{
							$dsc = '';
						}

						$alumnos .= '
						<tr>
							<td>'.$row->UserFirstName.'</td>
							<td>'.$row->UserLastName.'</td>
							<td>'.$row->UserEmail.'</td>
							<td>'.$carrera.'<br>'.$dsc.'</td>
							<td>'.$ver.'</td>
						</tr>
						';
					}
				}
				$dato['alumnos'] = $alumnos;
				//obtener usuarios responsables
				$usr = $this->alumnos_model->GetAllUsers($this->session->userdata('UserID'),0);
				$listusers = '';
				if($usr){
					foreach ($usr as $row) {
						$tusr = $this->general_model->AboutUser($row->UserID);

						if($tusr){
							$listusers .= '
							<option value="'.$this->encrypt->encode($row->UserID).'" nm="'.$tusr[0]->UserFirstName.' '.$tusr[0]->UserLastName.'" crs="'.$MaTEncrypt.'">'.$tusr[0]->UserFirstName.' '.$tusr[0]->UserLastName.'</option>
							';
						}
					}
				}
				
				$dato['listusers'] = $listusers;
				$this->load->view('profesor/admin-cursos-alumnos',$dato);
			}

		}

	}
	//Fin vista general de curso
	function Get_childrens($pattern){
		$hijos = $this->materias_model->BD_gethijos($pattern);
		$array_ch = array();
		if($hijos) { //Existen hijos
		
			foreach($hijos as $h){
				$rh = $this->Get_childrens($h->TopicID);
				$array_sb = array();
				if ($rh){ //NO tiene hijos

					$array_sb[$pattern]['chld'] = $rh;
					//$res[$pattern][$h->TopicID] = $rh;
					//$res = 'con hijo';
				}else{
					$array_sb[$pattern][$h->TopicID] = '';
					//$res = $hijos;
					//$res[$pattern][$h->TopicID] = $rh;
				}
				array_push($array_ch, $array_sb);
				//$res = $rh;
			}
		}else { //NO existen
			$array_ch[$pattern][0] = '';
		}
		return $array_ch;
		/*
		$res = array();
		$hijos = $this->materias_model->BD_gethijos($pattern);
		$array_ch = array();
		$array_child = array();
		if($hijos) { //Existen hijos
			
			foreach($hijos as $h){
				$rh = $this->Get_childrens($h->TopicID);
				if ($rh){ //NO tiene hijos
					$arrayName = array(
						'Padre' => $rh[0]->TopicID, 
						'Hijo' => $rh
					);
					
					//$res[$pattern][$h->TopicID] = $rh;
					//$res = 'con hijo';
				}else{
					$arrayName = array(
						'Padre' => $h->TopicID, 
						'Hijo' => ''
					);
					//$res = $hijos;
					//$res[$pattern][$h->TopicID] = $rh;
				}
				array_push($array_child, $arrayName);
				//$res = $rh;
			}
			$array_p = array(
				'Padre' => $pattern,
				'Hijo' => $array_child

			);
			
		}else { //NO existen
			$array_p = array(
				'Padre' => $pattern,
				'Hijo' => ''

			);
		}
		array_push($array_ch, $array_p);
		return $array_ch;*/
	}
	function leccion(){
		$TopicE = $this->input->get('l');

		$TopicID = urlencode($TopicE);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);

		$usr = $this->general_model->AboutUser($this->session->userdata('UserID'));
		if($usr){
			$priv = $this->general_model->GetModuleByTypeUser($usr[0]->UserTypeID,'materias',false);
			$dato['UserTypeID'] = $usr[0]->UserTypeID;
			if($priv){
				$c = $this->materias_model->AboutTopic($TopicID);
				if($c){
					$dato['NameTopic'] = $c[0]->NameTopic;
					$this->load->view('leccion-single',$dato);
				}
			}else{
				redirect(base_url().'inicio');
			}
		}
	}

	//VISTA DE CURSO POR ALUMNOS//
	public function vista(){
		$CourseID = $this->input->get('c');
		$CourseID = urlencode($CourseID);
		$CourseID = str_replace("+", "%2B",$CourseID);
		$CourseID = urldecode($CourseID);
		$CourseID = $this->encrypt->decode($CourseID);

		$inf = $this->general_model->AboutMateria($CourseID);
		if($inf){
			$CourseName = $inf[0]->CourseName;
			$MateriaIDEncryp = $this->encrypt->encode($CourseID);
			$CourseProfesorID = $inf[0]->CourseProfesorID;
			$prf = $this->general_model->AboutUser($CourseProfesorID);
			$ProfesorName = '';
			if($prf){
				$ProfesorName = $prf[0]->UserFirstName.' '.$prf[0]->UserLastName;
			}


			$dato['CourseName'] = $CourseName;
			$dato['MateriaIDEncryp'] = $MateriaIDEncryp;
			$dato['ProfesorName'] = $ProfesorName;
			$dato['Inc'] = $inf[0]->CourseStar;
			$dato['Fin'] = $inf[0]->CourseEnd;
			$dato['Banner'] = $inf[0]->CourseImage;
			$dato['CourseRequirements'] = $inf[0]->CourseRequirements;
			$dato['CourseGoals'] = $inf[0]->CourseGoals;

			//Obtener las unidades y los topics
			$gsc = $this->materias_model->GetSection($this->session->userdata('UserID'),$CourseID);
			$list = '';
			$TopicIDEncrypt = '';
			if($gsc){
				$tabla = $this->encrypt->encode('topics');
				$id = $this->encrypt->encode('TopicID');
				foreach ($gsc as $vl) {
					$tablasc = $this->encrypt->encode('section');
					$idsc = $this->encrypt->encode('SectionID');

					$TopicIDEncryptUnidad = $this->encrypt->encode($vl->SectionID);
					$head = '
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<h4 class="tpg-relawey"><strong class="numberunidad-alumn">Unidad '.$vl->OrderSection.',</strong> <label class="titleunidad-alumn"><i>'.$vl->NameSection.'</i></label> </h4>
						</div>
						<div class="col-xs-12 col-sm-12 col-lg-12">

							<ul class="mini-timeline">
						';

					$gut = $this->materias_model->GetSectionTopics($vl->SectionID,false);
					$gutsb = $this->materias_model->GetSectionTopics($vl->SectionID,true);
					$topics = '';
					//Cascada 1
					if($gut){
						
						foreach ($gut as $row) {
							$fc = $this->Get_childrens($row->TopicID);
							foreach ($fc as $k => $vl) {
								$abt = $this->materias_model->AboutTopic($k);

							}

							
							$childtopics = '';
							/*
							//Cascada 2
							if($gutsb){
								foreach ($gutsb as $sb) {
									if($sb->Parent==$row->TopicID){
										$sbchildtopics = '';
										
										
										//Cascada 3
										if($gutsb){
											foreach ($gutsb as $sbsb) {

												$sbsbchildtopics = '<div class="col-xs-12 col-sm-12 col-lg-12">';
												//cascada 4

												if($gutsb){
													foreach ($gutsb as $sbsbsb) {
														if($sbsbsb->Parent==$sbsb->TopicID){
															$TopicIDEncryptSbSbSb = $this->encrypt->encode($sbsbsb->TopicID);
															$sbchildtopics .= '
															<a href="'.base_url().'temas/?t='.$TopicIDEncryptSbSbSb.'" style="font-size: 20px;"><strong>'.$sbsbsb->OrderTopic.'</strong>.- '.$sbsbsb->NameTopic.'</a> <br>';

														}
														
													}
												}
												$sbsbchildtopics .= '</div>';
												if($sbsb->Parent==$sb->TopicID){
													$TopicIDEncryptSbSb = $this->encrypt->encode($sbsb->TopicID);
													$sbchildtopics .= '
													<a href="'.base_url().'temas/?t='.$TopicIDEncryptSbSb.'" style="font-size: 20px;"><strong>'.$sbsb->OrderTopic.'</strong>.- '.$sbsb->NameTopic.'</a> <br>'.$sbsbchildtopics;

												}
											}
										}

										$sbchildtopics .= '</div>';
										
										
										
										$TopicIDEncryptSb = $this->encrypt->encode($sb->TopicID);
										$childtopics .= '
										<li>
											
											<a href="'.base_url().'temas/?t='.$TopicIDEncryptSb.'" style="font-size: 20px;"><strong>'.$sb->OrderTopic.'</strong>.- '.$sb->NameTopic.'</a>
											
											<ul>'.$sbchildtopics.'</ul>
										</li>
										';
									}
								}
							}
							*/
							
							$TopicIDEncrypt = $this->encrypt->encode($row->TopicID);
							
							$topics .= '
								<li class="mini-timeline-highlight">
								<div class="mini-timeline-panel">
								<a href="'.base_url().'vista/?l='.$TopicIDEncrypt.'" style="font-size: 15px;"><strong>'.$row->OrderTopic.'</strong>.- '.$row->NameTopic.'</a> </div>
								<ul>'.$childtopics.'</ul></li>
							';
						}
					}
					$footer = '</ul></div>';
					$list .= $head.$topics.$footer;
				}
			}

			//Salir de vista previa
			$salir = '';
			if($inf[0]->CourseProfesorID==$this->session->userdata('UserID')){
				$salir = '<a href="'.base_url().'materias/prevista/?m='.$MateriaIDEncryp.'" class="btn btn-primary">Salir de vista previa</a>';
			}
			$dato['salir'] = $salir;
			
			$dato['list'] = $list;

			$dato['first_lesson'] = $TopicIDEncrypt;

			$this->load->view('materias-vista-alumnos',$dato);
		}

		
	}
	//Quiz
	function addexamen(){
		$CourseID = trim($this->input->get('c'));
		$CourseID = urlencode($CourseID);
		$CourseID = str_replace("+", "%2B",$CourseID);
		$CourseID = urldecode($CourseID);
		$CourseID = $this->encrypt->decode($CourseID);

		$TopicID = trim($this->input->get('l'));
		$TopicID = urlencode($TopicID);
		$TopicID = str_replace("+", "%2B",$TopicID);
		$TopicID = urldecode($TopicID);
		$TopicID = $this->encrypt->decode($TopicID);




		//Quiz curso
		if($CourseID!=""){
			$CourseIDEnCrypt = $this->encrypt->encode($CourseID);

			$data['CourseIDEnCrypt'] = $CourseIDEnCrypt;
			$this->load->view('examen-crear',$data);
		}
		//Quiz lección
		if($TopicID!=""){
			$ItemID = trim($this->input->get('itm'));
			$ItemID = urlencode($ItemID);
			$ItemID = str_replace("+", "%2B",$ItemID);
			$ItemID = urldecode($ItemID);
			$ItemID = $this->encrypt->decode($ItemID);

			$check = $this->materias_model->ExistExam($TopicID,$ItemID);
			if($check){
				//foreach ($check as $row) {
				redirect(base_url().'materias/examen/?ex='.$this->encrypt->encode($check[0]->QuizID) . '&qstn=' . urlencode(base64_encode(1)));
				//}
			}else{
				$data['TopicID'] = $this->encrypt->encode($TopicID);
				$data['ItemID'] = $this->encrypt->encode($ItemID);
				$data['CourseIDEnCrypt'] = $this->encrypt->encode($CourseID);

				$this->load->view('profesor/examen-crear',$data);
			}

			/*
			echo "Topic: ".$TopicID."<br>Item: ".$ItemID."<br>";

			$data['CourseIDEnCrypt'] = $this->encrypt->encode($CourseID);
			*/


		}

	}
	function examen(){
		$QuizID = trim($this->input->get('ex'));
		$QuizID = urlencode($QuizID);
		$QuizID = str_replace("+", "%2B",$QuizID);
		$QuizID = urldecode($QuizID);
		$QuizID = $this->encrypt->decode($QuizID);

        $questions1 = array();
		$gq = $this->materias_model->GetQuiz($QuizID);
		if($gq){
			$CourseIDEnCrypt = $this->encrypt->encode($gq[0]->CourseID);
			$TopicIDEnCrypt = $this->encrypt->encode($gq[0]->TopicID);
			$data['CourseIDEnCrypt'] = $CourseIDEnCrypt;
			$data['TopicIDEnCrypt'] = $TopicIDEnCrypt;
			$data['QuizIDEnCryp'] = $this->encrypt->encode($QuizID);
			//Tiene preguntas?
			$qs = $this->materias_model->GetQuestions($QuizID);
			$questions = array();
			if($qs){
				foreach ($qs as $row) {
				    $questions['Orden']     = $row->Orden;
				    $questions['QuestionID']  = $this->encrypt->encode($row->QuestionID);
				    $questions['QuestionName']  = $row->QuestionName;
					$questions1[] = $questions;
				}
			}
			$data['questions']      = $questions1;
            $data['header']         = $this->load->view('cabecera-admin', '', true);
            $data['header']         .= $this->load->view('header-top', '', true);
            $data['header']         .= '<br>';
            $data['header']         .= link_tag('assets/js/editor-master/css/medium-editor.css');
            $data['header']         .= '<br>';
            $data['header']         .= link_tag('assets/js/editor-master/css/themes/default.css');
            $data['footer']         = $this->load->view('footer-admin', '', true);
            $data['mediumEditor']   = site_url('assets/js/editor-master/js/medium-editor.js');
            $this->twig->display('profesor/examen-editar',$data);
		}
	}
}