<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginnc extends CI_Controller {

	function __construct()
	{
		parent::__construct();

	    $this->load->library('pagination');
	    $this->load->helper('html');
		$this->load->model('cursos_model'); 
		$this->load->model('materias_model');

		if ($this->session->userdata('UserID')){ 
	    	redirect(base_url().'cursosnc');
	    }
		
	}

	public function index(){
		if(isset($_GET['u'])){
			$UserID = $_GET['u'];
			$usr = $this->general_model->UserNCL($UserID);
			if($usr){
				$this->session->set_userdata(array(
					'DocEntry' => $usr[0]->DocEntry,
					'UserID' => $usr[0]->UserID,
					'FullName' => $usr[0]->FullName,
					'Depto' => $usr[0]->Depto
	        	));
	        	redirect(base_url().'cursosnc');
			}else{
				$msg['error'] = 'NO EXISTE ESTE USUARIO';
				$this->load->view('nc/error',$msg);
				
			}
		}else{
			$msg['error'] = 'ERROR EN URL';
			$this->load->view('nc/error',$msg);
		}
	}
}