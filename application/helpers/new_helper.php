<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
	
    function menu()
    {
    	$CI = get_instance();
    	$CI->load->library('session');
    	$CI->load->library('encrypt');
		$CI->load->model('general_model');
		$CI->load->model('menu_model'); 
		$CI->load->model('institucion_model');
    	$usr = $CI->general_model->AboutUser($CI->session->userdata('UserID'));
    	$TypeUserID = $usr[0]->UserTypeID;
		$sql = $CI->menu_model->GetModulesByTypeUser($TypeUserID,0);

		$sllgusertype = $CI->general_model->GetSlugUserType($usr[0]->UserTypeID);
		$UserTypeSlug = '';
		if($sllgusertype){
			$UserTypeSlug = $sllgusertype[0]->UserTypeSlug;
		}
		$encryp = $CI->encrypt->encode($usr[0]->CustomerID);
		$adminmdl = '';
		if($sql){
			foreach ($sql as $row) {
				$ClassPadre = $row->ModuleClass;

				$adminmdl .= '
				<li class="ripple">
		          <a class="tree-toggle nav-header">
		            <i class="fa '.$row->ModuleIcon.'" aria-hidden="true"></i> '.$row->ModuleName.'
		            <span class="fa-angle-right fa right-arrow text-right"></span>
		          </a>
		          <ul class="nav nav-list tree">
				'; 
				$chl = $CI->menu_model->GetModulesByTypeUser($TypeUserID,$row->ModuleID);
				if($chl){
					foreach ($chl as $vl) {
						if($vl->ModuleClass=='institucion/vista'){
							$cmm = $CI->institucion_model->GetModuleBySlug('institucion/vista');
							$ModuleID = '';
							if($cmm){
								$ModuleID = $cmm[0]->ModuleID;
							}
							$gcl = $CI->institucion_model->GetMenu($ModuleID,$TypeUserID);
							if($gcl){
								foreach ($gcl as $fila) {
									$adminmdl .= '
										<li><a href="'.base_url().$fila->ModuleClass.'">'.$fila->ModuleName.'</a></li>
										';
								}
							}
						}else{
							$adminmdl .= '
							<li><a href="'.base_url().$vl->ModuleClass.'">'.$vl->ModuleName.'</a></li>
							';
						}
					}
				}
				$adminmdl .= '
						</ul>
		        </li>
				';
			}
		}
        return $adminmdl;
    }   
}