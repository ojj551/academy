<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
	
    function menu_top()
    {
    	$CI = get_instance();
    	$CI->load->library('session');
    	$CI->load->library('encrypt');
		$CI->load->model('general_model');
    	$usr = $CI->general_model->AboutUser($CI->session->userdata('UserID'));

    	$tgglp = '';
    	if($usr){
    		$UserAvatarIcon = $usr[0]->UserAvatarIcon;
    		$UserAvatarImage = $usr[0]->UserAvatarImage;
    		$DocUserID = $CI->session->userdata('UserID');
            $actual_link = "http://$_SERVER[HTTP_HOST]";

    		$target_dir = $actual_link."/file_academy/us/".md5($DocUserID)."/";
    		if($UserAvatarImage!=""){
    			$img = $target_dir.$UserAvatarImage;
    		}
    		if($UserAvatarIcon!=""){
    			$img = site_url("assets/img/avatar/user".$UserAvatarIcon.".png");
    		}
    		if($UserAvatarImage=="" && $UserAvatarIcon==''){
    			$img = site_url("asset/img/avatar.jpg");
    		}
    		$tgglp = '
	    		<img src="'.$img.'" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
	             <ul class="dropdown-menu user-dropdown">
	               <li role="separator" class="divider"></li>
	               <li class="more">
	                <div class="text-center">
	                  <a href="'.base_url().'login/logout"><span class="fa fa-power-off "> Cerrar sesion</span></a>
	                </div>
	              </li>
	        	</ul>
	    	';
    	}
        return $tgglp;
    }   
}