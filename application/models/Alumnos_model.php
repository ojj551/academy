<?php
class Alumnos_model extends CI_Model {

        function ExistReference($random)
        {
            $q = $this ->db->query("SELECT * FROM bn_users WHERE UserReference='$random' AND UserID='' AND Cancelled=0");
            if($q->num_rows()>0){
                return false;
            }else{
                return true;
            }
        }
        function AddReferenceAlumno($Reference,$EmailReference,$data){
   
            $q = $this->db->query("SELECT * FROM bn_users WHERE UserEmail='$EmailReference' AND Cancelled=0 OR UserReference='$Reference' AND Cancelled=0");
            if($q->num_rows()>0){
                return false;
            }else{
                $this->db->insert('bn_users', $data);
    
                return $this->db->insert_id();
            }
        }
        function MisAlumnos($UserID){
            if($UserID=='all'){
                $q = $this ->db->query("SELECT * FROM bn_user_reference WHERE Cancelled=0");
            }else{
                $q = $this ->db->query("SELECT * FROM bn_user_reference WHERE SupervisorID='$UserID' AND Cancelled=0");
            }
            //$q = $this ->db->query("SELECT * FROM bn_user_reference AS T1, bn_users AS T2 WHERE T1.SupervisorID='$UserID' AND T1.Cancelled=0 AND T1.UserID=T2.UserID AND T2.Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }

        function MisUsuarios($UserTypeID,$CustomerID){
            $q = $this ->db->query("SELECT * FROM bn_users WHERE UserTypeID='$UserTypeID' AND CustomerID='$CustomerID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function MisUsuariosAsig($UserTypeID,$CustomerID){
            $q = $this ->db->query("SELECT * FROM bn_users AS T1,bn_responsable_user AS T2 WHERE T1.UserTypeID='$UserTypeID' AND T1.CustomerID='$CustomerID' AND T1.Cancelled=0 AND T1.UserID!=T2.UserID AND T2.Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetAllUsersSearch($ResponsableID,$words,$type){
            $q = $this ->db->query("SELECT * FROM bn_responsable_user AS T1, bn_users AS T2 WHERE T2.UserFirstName LIKE '%$words%' AND T1.ResponsableID='$ResponsableID' AND T1.UserID=T2.UserID AND T1.Cancelled=0 AND T2.Cancelled=0 OR T2.UserLastName LIKE '%$words%' AND T1.ResponsableID='$ResponsableID' AND T1.UserID=T2.UserID AND T1.Cancelled=0 AND T2.Cancelled=0 OR T2.UserEmail LIKE '%$words%' AND T1.ResponsableID='$ResponsableID' AND T1.UserID=T2.UserID AND T1.Cancelled=0 AND T2.Cancelled=0");

            if($q->num_rows()>0){
                if($type==1){
                    return $q->num_rows();
                }else{
                    return $q->result();
                }
            }else{
                return false;
            }
        }
        function GetAllUsers($ResponsableID,$type){
            $q = $this ->db->query("SELECT * FROM bn_responsable_user WHERE ResponsableID='$ResponsableID' AND Cancelled=0");
            if($q->num_rows()>0){
                if($type==1){
                    return $q->num_rows();
                }else{
                    return $q->result();
                }
            }else{
                return false;
            }
        }
        function GetUsers($query,$per_page){
            if ($this->uri->segment(3)==false){
                $uri = 0;
            }
            if ($this->uri->segment(3)=="")
            {
                $uri = 0;
            }
            else
            {
                $uri = $this->uri->segment(3);
            }
           
            $q = $this->db->query($query." LIMIT $uri,$per_page");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }

        function ExistRegister($UserReference,$UserEmail){
             $q = $this->db->query("SELECT * FROM bn_users WHERE UserEmail='$UserEmail' AND Cancelled=0 OR UserReference='$UserReference' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function AddSuscription($CourseID,$UserID,$data){
            $q = $this->db->query("SELECT * FROM bn_user_suscriptions WHERE UserID = '$UserID' AND CourseID='$CourseID' AND Cancelled=0");
            if($q->num_rows()>0){
                return false;
            }else{
                $this->db->insert('bn_user_suscriptions', $data);
                return true;
            }
        }
        function AddUserGroup($UserID,$GroupID,$data){
            $q = $this->db->query("SELECT * FROM bn_user_group WHERE UserID='$UserID' AND GroupID='$GroupID' AND Cancelled=0");
            if($q->num_rows()>0){
                return false;
            }else{
                $this->db->insert('bn_user_group', $data);
                return true;
            }
        }
        function GetSuscriptionUser($UserID){
            $q = $this->db->query("SELECT * FROM bn_user_suscriptions WHERE UserID = '$UserID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetSuscriptionUserByProfesor($UserID,$CourseProfesorID){
            $q = $this->db->query("SELECT * FROM bn_user_suscriptions AS T1,bn_courses AS T2 WHERE T1.UserID='$UserID' AND T2.CourseProfesorID='$CourseProfesorID' AND T1.CourseID=T2.CourseID");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
         function GetSuscriptionUserInfo($UserID,$CustomerID){
            $q = $this->db->query("SELECT * FROM bn_user_suscriptions AS T1,bn_courses AS T2, bn_users AS T3 WHERE T1.UserID='$UserID' AND T3.UserID=$UserID AND T3.CustomerID='$CustomerID' AND T1.CourseID=T2.CourseID AND T1.UserID=T3.UserID");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetCareerByCustomerID($CustomerID){
            $q = $this->db->query("SELECT * FROM bn_career WHERE CustomerID = '$CustomerID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function MyUsers($ResponsableID){
            $q = $this->db->query("SELECT * FROM bn_responsable_user WHERE ResponsableID='$ResponsableID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function MyUsersCreates($DocUserID){
            $q = $this->db->query("SELECT * FROM bn_user_reference WHERE DocUserID='$DocUserID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetChildrenByUserType($UserTypeID){
            $q = $this->db->query("SELECT * FROM bn_usertype WHERE Parent='$UserTypeID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function InsertAsignation($ResponsableID,$UserID,$data){
            $q = $this->db->query("SELECT * FROM bn_responsable_user WHERE ResponsableID = '$ResponsableID' AND UserID='$UserID' AND Cancelled=0");
            if($q->num_rows()>0){
                return false;
            }else{
                $this->db->insert('bn_responsable_user', $data);
                return true;
            }
        }

        function GetUsersByCustomer($CustomerID){
            $q = $this ->db->query("SELECT * FROM bn_users WHERE CustomerID='$CustomerID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetReferencesByCustomer($CustomerID){
            $q = $this ->db->query("SELECT * FROM bn_user_reference WHERE CustomerID='$CustomerID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function AsignaturaByID($AsignaturaID){
            $q = $this ->db->query("SELECT * FROM bn_materias WHERE AsignaturaID=$AsignaturaID AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                 return false;
            }
        }
        function AddMatProf($AsignaturaID,$CourseProfesorID,$data){
            //$q = $this->db->query("SELECT * FROM bn_courses WHERE AsignaturaID = '$AsignaturaID' AND CourseProfesorID='$CourseProfesorID' AND Cancelled=0 AND CourseEnd>=CURDATE()");
            $this->db->select('*');
            $this->db->from('bn_courses');
            $this->db->where('AsignaturaID',$AsignaturaID);
            $this->db->where('CourseProfesorID',$CourseProfesorID);
            $this->db->where('Cancelled',0);
            $this->db->where('CourseEnd>=',$data['CourseStar']);
            $q = $this->db->get();
            if($q->num_rows()>0){
                return false;
            }else{
                $this->db->insert('bn_courses', $data);
                return true;
            }
        }
        function AddMyResponsables($ResponsableID,$UserID,$data){
            $this->db->select('*');
            $this->db->from('bn_responsable_user');
            $this->db->where('ResponsableID',$ResponsableID);
            $this->db->where('UserID',$UserID);
            $this->db->where('Cancelled',0);
            $q = $this->db->get();
            if($q->num_rows()>0){
                return false;
            }else{
                $this->db->insert('bn_responsable_user', $data);
                return true;
            }
        }
        function UserUnder($UserTypeID,$CustomerID){
            $this->db->select('*');
            $this->db->from('bn_usertype as T1');
            $this->db->join('bn_users as T2','T2.UserTypeID=T1.UserTypeID');
            $this->db->where('T1.Parent',$UserTypeID);
            $this->db->where('T1.Cancelled',0);
            $this->db->where('T2.Cancelled',0);
            $this->db->where('T2.ConfirmationID !=','');
            $q = $this->db->get();
            if($q->num_rows()>0){
                return $q->result();
            }else{
                 return false;
            }
        }
        function ProfesorAdvance($UserID){
            $q = $this ->db->query("SELECT * FROM bn_responsable_user AS T1, bn_courses AS T2 WHERE T1.ResponsableID='$UserID' AND T1.Cancelled=0 AND T1.UserID=T2.CourseProfesorID AND T2.Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                 return false;
            }
        }
        function CourseAdvance($CourseID){
            $q = $this ->db->query("SELECT AVG(Progress) AS Total FROM bn_user_suscriptions WHERE CourseID='$CourseID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                 return false;
            }
        }
        function MatByProfesor($UserID,$CustomerID){
            $q = $this ->db->query("SELECT * FROM bn_courses WHERE CourseProfesorID='$UserID' AND CustomerID='$CustomerID' AND Cancelled=0 AND CourseEnd>=CURDATE()");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                 return false;
            }
        }
        function MatByUser($UserID){
            $q = $this ->db->query("SELECT * FROM bn_user_suscriptions AS T1, bn_courses AS T2 WHERE T1.UserID='$UserID' AND T1.Cancelled=0 AND T1.CourseID=T2.CourseID AND CourseEnd>=CURDATE() AND T2.Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                 return false;
            }
        }
        function MatByUserAndProfe($UserID,$CourseProfesorID){
            $q = $this ->db->query("SELECT * FROM bn_user_suscriptions AS T1, bn_courses AS T2 WHERE T1.UserID='$UserID' AND T1.Cancelled=0 AND T2.CourseProfesorID='$CourseProfesorID' AND T1.CourseID=T2.CourseID AND CourseEnd>=CURDATE() AND T2.Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                 return false;
            }
        }
        function AboutUserType($UserTypeID){
            $q = $this->db->query("SELECT * FROM bn_usertype WHERE UserTypeID='$UserTypeID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }

        function UpdateUser($where,$data){
            $this->db->where($where);
            $this->db->update('bn_users ', $data); 
            return true;
        }
        function TopAlumnos($CourseProfesorID){
            $this->db->select('sum(T2.Progress) AS Suma, count(*) as Cantidad, T2.UserID');
            $this->db->from('bn_courses AS T1');
            $this->db->join('bn_user_suscriptions AS T2','T1.CourseID=T2.CourseID');
            $this->db->where('T1.CourseProfesorID',$CourseProfesorID);
            $this->db->where('T2.Cancelled',0);
            $this->db->group_by('T2.UserID');
            $this->db->order_by('Suma', 'DESC');
            $this->db->limit(10);
            $q = $this->db->get();
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
}