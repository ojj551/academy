<?php
class General_model extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        public function QUERYS($query){
            $q = $this->db->query($query);
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        //Actualizar por query
        public function QUERYSUPT($query){
            $q = $this->db->query($query);
        }

        function AboutUser($UserID){
            $this->db->select('*');
            $this->db->from('bn_users');
            $this->db->where('UserID',$UserID);
            $this->db->where('Cancelled',0);
            $q = $this->db->get();
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function UserNCL($UserID){
            $q = $this->db->query("SELECT * FROM bn_nc WHERE MD5(CONCAT(DocEntry,UserID)) = '$UserID' ");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function UserNC($UserID){
            $q = $this->db->query("SELECT * FROM bn_nc WHERE UserID='$UserID' AND CancelStatus=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function UserNCDocEntry($DocEntry){
            $q = $this->db->query("SELECT * FROM bn_nc WHERE DocEntry='$DocEntry' AND CancelStatus=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }

        function TypeUsers($sql){
            $q = $this->db->query("SELECT * FROM bn_usertype WHERE $sql");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function MisMateriasAdmin($CourseProfesorID){
//            $q = $this->db->query("SELECT * FROM bn_courses AS T1, bn_materias AS T2 WHERE T1.CourseProfesorID='$CourseProfesorID' AND T1.Cancelled=0 AND T1.AsignaturaID=T2.AsignaturaID AND T2.Cancelled=0 AND CourseEnd>=CURDATE()");
            $this->db->select('*');
            $this->db->from('bn_courses AS T1');
            $this->db->join('bn_materias AS T2','T1.AsignaturaID=T2.AsignaturaID');
            $this->db->where('T1.CourseProfesorID',$CourseProfesorID);
            $this->db->where('T1.Cancelled',0);
            $this->db->where('T2.Cancelled',0);
            $this->db->where('CourseEnd>=CURDATE()');
            $q = $this->db->get();
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function AboutMateria($CourseID){
            $q = $this->db->query("SELECT * FROM bn_courses WHERE CourseID='$CourseID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function AllMaterias($CustomerID){
            $q = $this->db->query("SELECT * FROM bn_courses WHERE CustomerID='$CustomerID' AND Cancelled=0");

            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }

        }
        function AboutCustomer($DocEntry){
            $q = $this->db->query("SELECT * FROM bn_customers WHERE DocEntry='$DocEntry' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function AboutCustomerByID($CustomerID){
            $this->db->select('*,UNIX_TIMESTAMP(DocDate) as DocDateTimestamp');
            $this->db->where('CustomerID',$CustomerID);
            $this->db->where('Cancelled',0);
            $q = $this->db->get('bn_customers');
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function AllCustomer(){
            $q = $this->db->query("SELECT * FROM bn_customers WHERE Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetModuleByTypeUser($UserTypeID,$ModuleClass,$parent){
            if($parent){
                $q = $this ->db->query("SELECT * FROM bn_module AS T1, bn_usertype_module AS T2 WHERE T1.ModuleClass='$ModuleClass' AND T2.UserTypeID='$UserTypeID' AND T1.Parent!=0 AND T1.ModuleID=T2.ModuleID AND T1.Cancelled=0 AND T2.Cancelled=0");
            }else{
                $q = $this ->db->query("SELECT * FROM bn_module AS T1, bn_usertype_module AS T2 WHERE T1.ModuleClass='$ModuleClass' AND T2.UserTypeID='$UserTypeID' AND T1.Parent=0 AND T1.ModuleID=T2.ModuleID AND T1.Cancelled=0 AND T2.Cancelled=0");
            }
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetSlugUserType($UserTypeID){
            $q = $this->db->query("SELECT * FROM bn_usertype WHERE UserTypeID='$UserTypeID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetText($Lang,$LeadType,$KeyText){
            $q = $this->db->query("SELECT * FROM bn_text WHERE LanguageID='$Lang' AND LeadType='$LeadType' AND KeyText='$KeyText' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result()[0]->Text;
            }else{
                return '';
            }
        }

        function UpdateUser($data,$where){
            $this->db->where($where);
            $this->db->update('bn_users', $data);
            return true;
        }
        public function SaveMailContacto($data){
            $this->db->insert('bn_mail_queue', $data);
            return $this->db->insert_id();
        }
        public function UPDATEMAIL($datams,$wherems){
            $this->db->where($wherems);
            $this->db->update('bn_mail_queue', $datams);
            return true;
        }
    }
?>