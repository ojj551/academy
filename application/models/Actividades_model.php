<?php
class Actividades_model extends CI_Model {

	function SubItemsOrden($UserID,$ItemID,$CategorySlug,$Type){
		$q = $this ->db->query("SELECT * FROM bn_topics_templates_sub_items WHERE ItemID='$ItemID' AND CategorySlug='$CategorySlug' AND Type='$Type' AND Cancelled=0 ORDER BY Orden ASC;");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
	}

	function ExistAnswer($UserID,$ItemID,$CategorySlug){
		$q = $this ->db->query("SELECT * FROM bn_topics_templates_log_activities WHERE UserID='$UserID' AND CategorySlug='$CategorySlug' AND ItemID='$ItemID' AND RightAnswer='1'");// AND Cancelled=0
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
	}
	function AddAnswer($data){
		$this->db->insert('bn_topics_templates_log_activities', $data);
        return true;
	}

	function GetQuizByItem($ItemID){
		$q = $this ->db->query("SELECT * FROM bn_questions WHERE ItemID='$ItemID' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
	}

	function GetAwns($QuestionID,$QuestionAnswerID){
		$this->db->select('a.*, b.ItemID as ItemID1, b.QuizName, c.OptionName, c.OptionExtra');
        $this->db->from('bn_questions as a');
        $this->db->join('bn_quiz as b','a.QuizID = b.QuizID');
        $this->db->join('bn_questions_options as c','a.QuestionID = c.QuestionID');
        $this->db->where('a.QuestionID',$QuestionID);
        $this->db->where('a.Cancelled',0);
        if($QuestionAnswerID != ""){
            $this->db->where('c.OptionID',$QuestionAnswerID);
        }
        $q = $this->db->get();

        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
	}

}
?>