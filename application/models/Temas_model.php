<?php
class Temas_model extends CI_Model {


        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        //
        function GetCategorysTemplates()
        {
            $q = $this ->db->query("SELECT * FROM bn_topics_templates_categorys WHERE Cancelled=0 ORDER BY Orden");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetTemplatesByCategory($CategoryTemplateID){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates WHERE CategoryTemplateID='$CategoryTemplateID' AND Cancelled=0 ORDER BY Orden");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetTemplateByID($TemplateID){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates WHERE TemplateID='$TemplateID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function InsertItemsTemplate($TopicID,$TemplateID,$Orden,$data){
            $q = $this->db->query("SELECT * FROM bn_topics_templates_items WHERE TopicID='$TopicID' AND TemplateID='$TemplateID' AND Orden='$Orden' AND Cancelled=0");
            if($q->num_rows()>0){
                return false;
            }else{
                $this->db->insert('bn_topics_templates_items', $data);
                return true;
            }
        }
        function UpdateItemsTemplate($where,$dataupd){
            $this->db->where($where);
            $this->db->update('bn_topics_templates_items', $dataupd);
            return true;
        }
        function GetTemplatesById($TopicID){
            $this->db->select('*,T1.Orden AS OrdenItem, T2.Orden AS OrdenTemplate');
            $this->db->from('bn_topics_templates_items AS T1');
            $this->db->join('bn_topics_templates as T2', 'T1.TemplateID=T2.TemplateID');
            $this->db->where('T1.TopicID', $TopicID);
            $this->db->where('T1.Cancelled', 0);
            $this->db->where('T2.Cancelled', 0);
            $this->db->order_by('T1.Orden', 'ASC');
            $q = $this->db->get();
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetTemplate($TemplateID){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates WHERE TemplateID=$TemplateID AND Cancelled=0 ORDER BY Orden");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetTemplateValues($TopicID,$TemplateID){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates_items WHERE TopicID=$TopicID AND TemplateID=$TemplateID AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetItemByID($ItemID){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates_items WHERE ItemID='$ItemID' AND  Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetTemplateByTT($TopicID,$TemplateID){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates AS T1, bn_topics_templates_items AS T2 WHERE T1.Cancelled=0 AND T2.ItemID=$TemplateID AND T2.TopicID=$TopicID AND T2.Cancelled=0 AND T1.TemplateID=T2.TemplateID");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
            
        }
        function GetItemsByOrden($TopicID,$Orden){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates_items WHERE TopicID=$TopicID AND Orden=$Orden AND  Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }

        function CreateTemplate($data){

            $this->db->insert('bn_topics_templates', $data);
            return true;
            
        }
        function AboutCategory($CategoryTemplateID)
        {
            $this->db->where('CategoryTemplateID',$CategoryTemplateID);
            $this->db->where('Cancelled',0);
            $q = $this->db->get('bn_topics_templates_categorys');
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function AddSubItems($ItemID,$CategorySlug,$Type,$Orden,$data){
            $q = $this->db->query("SELECT * FROM bn_topics_templates_sub_items WHERE ItemID='$ItemID' AND CategorySlug='$CategorySlug' AND Type='$Type' AND Orden='$Orden' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result()[0]->DocEntry;
            }else{
                $this->db->insert('bn_topics_templates_sub_items', $data);
                return 'success';
            }
        }
        function AddSubItemsBg($ItemID,$CategorySlug,$Type,$data){
            $q = $this->db->query("SELECT * FROM bn_topics_templates_sub_items WHERE ItemID='$ItemID' AND CategorySlug='$CategorySlug' AND Type='$Type' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result()[0]->DocEntry;
            }else{
                $this->db->insert('bn_topics_templates_sub_items', $data);
                return 'success';
            }
        }
        function UpdateSubItemsTemplate($where,$dataupd){
            $this->db->where($where);
            $this->db->update('bn_topics_templates_sub_items', $dataupd);
            return true;
        }

        function GetSubItemByItemID($ItemID){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates_sub_items WHERE ItemID='$ItemID' AND Cancelled=0 ORDER BY Orden");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetSubItemByItemIDAll($ItemID){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates_sub_items WHERE ItemID='$ItemID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->num_rows();
            }else{
                return 0;
            }
        }
        function GetSubItemByItemIDOrden($ItemID,$Orden){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates_sub_items WHERE ItemID='$ItemID' AND Orden='$Orden' AND Cancelled=0 ORDER BY Orden");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }

        function AboutSubItem($DocEntry){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates_sub_items WHERE DocEntry='$DocEntry' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function TopicLoad($TopicID,$UserID){
            $q = $this ->db->query("SELECT * FROM bn_topics_avance WHERE TopicID='$TopicID' AND UserID='$UserID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function InsertAvance($data){
            $this->db->insert('bn_topics_avance', $data);
            return true;
        }
        function UpdateAvance($where,$dataupd){
            $this->db->where($where);
            $this->db->update('bn_topics_avance', $dataupd);
            return true;
        }
        //About template
        function AboutTemplate($TemplateID){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates WHERE TemplateID='$TemplateID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        //Obtener curso de topic
        function GetMyCourse($TopicID){
            $q = $this ->db->query("SELECT T1.CourseID FROM bn_topics AS T1, bn_courses AS T2 WHERE T1.TopicID='$TopicID' AND T1.Cancelled=0 AND T2.CourseEnd >= curdate() AND T2.Cancelled=0 AND T1.CourseID=T2.CourseID");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        //Obtener todos los topics de cursos
        function TopicsIntoCourse($CourseID){
            $q = $this ->db->query("SELECT * FROM bn_topics WHERE CourseID='$CourseID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function UpdateAvanceCourse($data,$where){
            $this->db->where($where);
            $this->db->update('bn_user_suscriptions', $data);
            return true;
        }

        //Actividades
        function AnswerActivities($ItemID,$CategorySlug){
            $q = $this ->db->query("SELECT * FROM bn_topics_templates_log_activities WHERE ItemID='$ItemID' AND CategorySlug='$CategorySlug' AND RightAnswer='1' ORDER BY DocDate DESC");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function TemplateActivities($ItemID,$CategorySlug){
            $this->db->select('*');
            $this->db->from('bn_topics_templates_sub_items');
            $this->db->where('ItemID',$ItemID);
            $this->db->where('CategorySlug',$CategorySlug);
            $this->db->where('Type','title');
            $this->db->where('Cancelled',0);
            $this->db->order_by('RAND()');
            $q = $this->db->get();
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
}