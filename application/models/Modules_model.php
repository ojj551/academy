<?php
class Modules_model extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        function get_modules($Parent){
            if($Parent=='all'){
                $q = $this ->db->query("SELECT * FROM bn_module WHERE Cancelled=0 ORDER BY ModuleName");
            }else{
                $q = $this->db->query("SELECT * FROM bn_module WHERE Parent='$Parent' AND Cancelled=0 ORDER BY ModuleName");
            }
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetModuleByID($ModuleID){
            $q = $this->db->query("SELECT * FROM bn_module WHERE ModuleID='$ModuleID' AND Cancelled=0");
            
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }

        function InsertModule($data){
             $this->db->insert('bn_module', $data);
             return true;
        }

        function get_typeuser(){
            $q = $this ->db->query("SELECT * FROM bn_usertype WHERE Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function GetModulesByTypeUser($UserTypeID,$Parent){
            $q = $this ->db->query("SELECT * FROM bn_usertype_module AS T1, bn_module AS T2 WHERE T1.UserTypeID='$UserTypeID' AND T1.Cancelled=0 AND T1.ModuleID=T2.ModuleID AND T2.Parent='$Parent' AND T2.Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function ModuleUserType($TypeUserID,$ModuleID){
            $q = $this ->db->query("SELECT * FROM bn_usertype_module WHERE UserTypeID='$TypeUserID' AND ModuleID='$ModuleID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function DeleteModuleTypeUser($UserTypeID){
            $q = $this ->db->query("DELETE FROM bn_usertype_module WHERE UserTypeID='$UserTypeID' AND Cancelled=0");
        }
        function InsertModuleTypeUser($UserTypeID,$ModuleID,$data){
            $q = $this ->db->query("SELECT * FROM bn_usertype_module WHERE UserTypeID='$UserTypeID' AND ModuleID='$ModuleID' AND Cancelled=0");
            if($q->num_rows()>0){
                return false;
            }else{
                $this->db->insert('bn_usertype_module', $data);
                return true;
            }
        }

    }
?>