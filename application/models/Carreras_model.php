<?php
class Carreras_model extends CI_Model {

    public $title;
    public $content;
    public $date;

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }

    function AddDepto($CareerName,$CustomerID,$data){
        $q = $this->db->query("SELECT * FROM bn_career WHERE CareerName='$CareerName' AND CustomerID='$CustomerID' AND Cancelled=0");
        if($q->num_rows()>0){
            return false;
        }else{
            $this->db->insert('bn_career', $data);
            return true;
        }
    }
    function GetCareer($CustomerID){
        $q = $this->db->query("SELECT * FROM bn_career WHERE CustomerID='$CustomerID' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function UserCareer($CustomerID,$CareerID){
        $q = $this->db->query("SELECT *, AVG(T2.Progress) FROM bn_users AS T1, bn_user_suscriptions AS T2 WHERE T1.CustomerID='$CustomerID' AND T1.CareerID='$CareerID' AND T1.Cancelled=0 AND T2.Cancelled=0 AND T1.UserID=T2.UserID GROUP BY T2.UserID ORDER BY T2.Progress DESC");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }


    function AboutCareer($CareerID){
        $q = $this->db->query("SELECT * FROM bn_career WHERE CareerID='$CareerID' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function UpdateCareer($where,$data){
        $this->db->where($where);
        $this->db->update('bn_career', $data); 
        return true;
    }

}