<?php

class Materias_model extends CI_Model
{

    public $title;
    public $content;
    public $date;

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function GetMaterias($CustomerID)
    {
        $q = $this->db->query("SELECT * FROM bn_materias WHERE CustomerID='$CustomerID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function Add($nombre, $data)
    {
        $q = $this->db->query("SELECT * FROM bn_materias WHERE AsignaturaName='$nombre' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert('bn_materias', $data);
            return $this->db->insert_id();
        }
    }

    public function UpdateBanner($CourseID, $data)
    {
        $this->db->where('CourseID', $CourseID);
        $this->db->update('bn_courses', $data);
        return true;
    }

    public function AddSection($CourseID, $OrderSection, $data)
    {
        $q = $this->db->query("SELECT * FROM bn_section WHERE OrderSection='$OrderSection' AND CourseID='$CourseID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert('bn_section', $data);
            return $this->db->insert_id();
        }
    }

    public function AddTopics($CourseID, $OrderTopic, $data)
    {
        $q = $this->db->query("SELECT * FROM bn_topics WHERE CourseID='$CourseID' AND OrderTopic='$OrderTopic' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert('bn_topics', $data);
            return $this->db->insert_id();
        }
    }

    public function ADDTopics1($SectionID)
    {
        $q = $this->db->query("select Count(OrderTopic)+1 as Ultimo from bn_topics where Cancelled=0 and Parent=0 and SectionID='$SectionID'");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }


    public function ADDSBTopics1($SectionID, $Parent)
    {
        $q = $this->db->query("select Count(OrderTopic)+1 as Ultimo from bn_topics where Cancelled=0 and Parent='$Parent' and SectionID='$SectionID'");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function GetSection($CourseID)
    {
        $q = $this->db->query("SELECT * FROM bn_section WHERE CourseID='$CourseID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function SectionCourse($CourseID)
    {
        $q = $this->db->query("SELECT * FROM bn_section WHERE CourseID='$CourseID' AND Cancelled=0 ");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function GetSectionTopics($SectionID, $Parent)
    {
        if ($Parent) {
            $q = $this->db->query("SELECT * FROM bn_topics WHERE SectionID='$SectionID' AND Parent!=0 AND Cancelled=0 ORDER BY OrderTopic");
        } else {
            $q = $this->db->query("SELECT * FROM bn_topics WHERE SectionID='$SectionID' AND Parent=0 AND Cancelled=0 ORDER BY OrderTopic");
        }
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function GetSubSectionTopics($SectionID, $TopicID)
    {
        $q = $this->db->query("SELECT * FROM bn_topics WHERE SectionID='$SectionID' AND Parent='$TopicID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function AboutTopic($TopicID)
    {
        $q = $this->db->query("SELECT * FROM bn_topics WHERE TopicID='$TopicID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function BD_gethijos($Parent)
    {
        $q = $this->db->query("SELECT * FROM bn_topics WHERE Parent=$Parent AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function AddMatProf($AsignaturaID, $CourseProfesorID, $data)
    {
        $q = $this->db->query("SELECT * FROM bn_courses WHERE AsignaturaID = '$AsignaturaID' AND CourseProfesorID='$CourseProfesorID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert('bn_courses', $data);
        }

    }

    public function GetMateriasByUserID($CourseProfesorID)
    {
        $q = $this->db->query("SELECT * FROM bn_courses WHERE CourseProfesorID='$CourseProfesorID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function AsignaturaBYID($AsignaturaID)
    {
        $q = $this->db->query("SELECT * FROM bn_materias WHERE AsignaturaID='$AsignaturaID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function AddQuiz($QuizName, $CourseID, $DocUserID, $data)
    {
        $q = $this->db->query("SELECT * FROM bn_quiz WHERE DocUserID = '$DocUserID' AND QuizName='$QuizName' AND CourseID='$CourseID'  AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert('bn_quiz', $data);
            return $this->db->insert_id();
        }
    }

    public function AddQuizLesson($QuizName, $TopicID, $ItemID, $DocUserID, $data)
    {
        $q = $this->db->query("SELECT * FROM bn_quiz WHERE DocUserID = '$DocUserID' AND QuizName='$QuizName' AND TopicID='$TopicID' AND ItemID='$ItemID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert('bn_quiz', $data);
            return $this->db->insert_id();
        }
    }

    public function GetQuiz($QuizID)
    {
        $q = $this->db->query("SELECT * FROM bn_quiz WHERE QuizID='$QuizID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function CourseExam($CourseID)
    {
        $q = $this->db->query("SELECT * FROM bn_quiz WHERE CourseID='$CourseID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    //<editor-fold desc="Questions">
    public function GetQuestions($QuizID)
    {
        $this->db->select('*');
        $this->db->from('bn_questions');
        $this->db->where('QuizID',$QuizID);
        $this->db->where('Cancelled',0);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function OptionQuestion($QuestionID)
    {
        $this->db->select('*');
        $this->db->from('bn_questions_options');
        $this->db->where('QuestionID', $QuestionID);
        $this->db->where('Cancelled', 0);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function AboutQuestion($QuestionID)
    {
        $this->db->select('*');
        $this->db->from('bn_questions');
        $this->db->where('QuestionID', $QuestionID);
        $this->db->where('Cancelled', 0);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function AddQuestion($data)
    {
        $this->db->insert('bn_questions', $data);
        return $this->db->insert_id();
    }

    public function UpdateQuestion($where, $data)
    {
        $this->db->where('QuestionID', $where);
        $this->db->update('bn_questions', $data);
        return true;
    }
    //</editor-fold>

    //<editor-fold desc="Options">

    public function getOption($OptionID)
    {
        $this->db->select('OptionExtra');
        $this->db->from('bn_questions_options');
        $this->db->where('OptionID',$OptionID);
        $q = $this->db->get();
        return $q->result();
    }

    public function AddOptions($Orden, $QuestionID, $UserID, $data)
    {
        $this->db->select('*');
        $this->db->from('bn_questions_options');
        $this->db->where('DocUserID', $UserID);
        $this->db->where('QuestionID', $QuestionID);
        $this->db->where('OptionName', $data['OptionName']);
        $this->db->where('Cancelled', 0);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result()[0]->OptionID;
        } else {
            $this->db->insert('bn_questions_options', $data);
            return $this->db->insert_id();
        }
    }

    public function UpdateOption($data)
    {
        $this->db->update_batch('bn_questions_options', $data, 'OptionID');
        return true;
    }

    public function DeleteOption($OptionID){
        $SQL = $this->db->delete('bn_questions_options', array('OptionID' => $OptionID,'OptionExtra' => ''));
    }
    //</editor-fold>

    public function GetCoursesByAsig($AsignaturaID)
    {
        $q = $this->db->query("SELECT * FROM bn_courses WHERE AsignaturaID='$AsignaturaID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function GetCoursesByID($CourseID)
    {
        $q = $this->db->query("SELECT * FROM bn_courses WHERE CourseID='$CourseID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function UsersSuscription($CourseID, $status)
    {
        $q = $this->db->query("SELECT * FROM bn_user_suscriptions WHERE CourseID='$CourseID' AND Cancelled=0 Group BY UserID");
        if ($q->num_rows() > 0) {
            if ($status == 1) {
                return $q->num_rows();
            } else {
                return $q->result();
            }
        } else {
            return false;
        }
    }

    public function UpdateAsignatura($where, $data)
    {
        $this->db->where($where);
        $this->db->update('bn_materias', $data);
        return true;
    }

    public function UsersIntoAsignature($AsignaturaID)
    {
        $q = $this->db->query("SELECT T1.AsignaturaName,T3.SuscriptionID,T3.DocDate,T3.UserID,T3.Progress,T3.DocUpdateDate FROM bn_materias AS T1, bn_courses AS T2, bn_user_suscriptions AS T3 WHERE T1.AsignaturaID='$AsignaturaID' AND T1.AsignaturaID=T2.AsignaturaID AND T2.Cancelled=0 AND T2.CourseID=T3.CourseID AND T2.Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function ExistExam($TopicID, $ItemID)
    {
        $q = $this->db->query("SELECT * FROM bn_quiz WHERE TopicID='$TopicID' AND ItemID='$ItemID' AND Cancelled=0");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
}

?>