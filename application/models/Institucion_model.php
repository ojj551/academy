<?php
class Institucion_model extends CI_Model {
	public $title;
    public $content;
    public $date;

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    function GetModuleBySlug($ModuleClass)
    {
        $q = $this ->db->query("SELECT * FROM bn_module WHERE ModuleClass='$ModuleClass' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
             return false;
        }
    }
    function GetMenu($ModuleID,$UserTypeID){
        $q = $this ->db->query("SELECT * FROM bn_module AS T1, bn_usertype_module AS T2 WHERE T1.Parent='$ModuleID' AND T1.Cancelled=0 AND T2.UserTypeID='$UserTypeID' AND T2.Cancelled=0 AND T1.ModuleID=T2.ModuleID ORDER BY T1.Orden ASC");
        if($q->num_rows()>0){
            return $q->result();
        }else{
             return false;
        }
    }
    function Asignados(){
        $q = $this ->db->query("SELECT * FROM bn_responsable_user WHERE Cancelled=0 GROUP BY ResponsableID");
        if($q->num_rows()>0){
            return $q->result();
        }else{
             return false;
        }
    }
    function CursosColegas($CustomerID){
        $q = $this ->db->query("SELECT * FROM bn_courses WHERE CustomerID='$CustomerID' AND Cancelled=0 GROUP BY CourseProfesorID");
        if($q->num_rows()>0){
            return $q->result();
        }else{
             return false;
        }
    }
    function AvanceInstitucionGlobal($CustomerID){
        $q = $this ->db->query("SELECT avg(T2.Progress) AS Total FROM bn_courses AS T1, bn_user_suscriptions AS T2 WHERE T1.CustomerID='$CustomerID' AND T1.Cancelled=0 AND T1.CourseID=T2.CourseID AND T2.Cancelled=0");
        if($q->num_rows()>0){
            return $q->result()[0]->Total;
        }else{
             return false;
        }
    } 
    function AllAsignaturas($CustomerID){
        $q = $this ->db->query("SELECT * FROM bn_materias WHERE CustomerID='$CustomerID' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
             return false;
        }
    }
    function AllCarreras($CustomerID){
        $q = $this ->db->query("SELECT * FROM bn_career WHERE CustomerID='$CustomerID' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
             return false;
        }
    }
    function AvgCareer($CareerID){
        $q = $this ->db->query("SELECT AVG(T2.Progress) AS Total FROM bn_users AS T1, bn_user_suscriptions AS T2 WHERE T1.CareerID='$CareerID' AND T1.Cancelled=0 AND T1.UserID=T2.UserID AND T1.Cancelled=0;");
        if($q->num_rows()>0){
            return $q->result();
        }else{
             return false;
        }  
    }
    function AllProfes($CustomerID){
        $q = $this ->db->query("SELECT * FROM bn_users AS T1, bn_usertype AS T2 WHERE T2.UserTypeSlug='profe' AND T2.Cancelled=0 AND T1.UserTypeID=T2.UserTypeID AND T1.CustomerID='$CustomerID' AND T1.Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
             return false;
        }
    }
    function MatByUser($UserID){
        $q = $this ->db->query("SELECT * FROM bn_courses WHERE CourseProfesorID='$UserID' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
             return false;
        }
    }
    function UserUnder($UserTypeID){
        $q = $this ->db->query("SELECT * FROM bn_usertype AS T1, bn_users AS T2 WHERE T1.Parent=$UserTypeID AND T1.Cancelled=0 AND T2.UserTypeID=T1.UserTypeID AND T1.Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
             return false;
        }
    }
    function AllMaterias($CustomerID,$type){
         $q = $this ->db->query("SELECT * FROM bn_materias WHERE CustomerID='$CustomerID' AND Cancelled=0");
        if($q->num_rows()>0){
            if($type==1){
                return $q->num_rows();
            }else{
                return $q->result();
            }
        }else{
             return false;
        }
    }
    function AllProfesInst($CustomerID,$slug,$type){
        $q = $this ->db->query("SELECT * FROM bn_users AS T1, bn_usertype AS T2 WHERE T2.Slug='$slug' AND T1.UserTypeID=T2.UserTypeID");
        if($q->num_rows()>0){
            if($type==1){
                return $q->num_rows();
            }else{
                return $q->result();
            }
        }else{
             return false;
        }

    }
    function GetAllUsers($CustomerID,$type){
        $q = $this ->db->query("SELECT *,date_format(DocDate,'%d-%m-%Y') AS FRegister FROM bn_users WHERE CustomerID='$CustomerID' AND Cancelled=0");
        if($q->num_rows()>0){
            if($type==1){
                return $q->num_rows();
            }else{
                return $q->result();
            }
        }else{
             return false;
        }
    }
    function GetAllUsersSearch($CustomerID,$words,$type){
        $q = $this ->db->query("SELECT *,date_format(DocDate,'%d-%m-%Y') AS FRegister FROM bn_users WHERE UserFirstName LIKE '%$words%' AND CustomerID='$CustomerID' AND Cancelled=0 OR UserLastName LIKE '%$words%' AND CustomerID='$CustomerID' AND Cancelled=0 OR UserEmail LIKE '%$words%' AND CustomerID='$CustomerID' AND Cancelled=0");
        if($q->num_rows()>0){
            if($type==1){
                return $q->num_rows();
            }else{
                return $q->result();
            }
        }else{
             return false;
        }
    }
    function GetUsers($query,$per_page){
        if ($this->uri->segment(3)==false){
            $uri = 0;
        }
        if ($this->uri->segment(3)=="")
        {
            $uri = 0;
        }
        else
        {
            $uri = $this->uri->segment(3);
        }
       
        $q = $this->db->query($query." LIMIT $uri,$per_page");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function HaveThisUser($UserID,$ResponsableID){
        $q = $this ->db->query("SELECT * FROM bn_responsable_user WHERE ResponsableID='$ResponsableID' AND UserID='$UserID' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
             return false;
        }
    }
    function UsuariosDeCarreras($CareerID,$type){
        $q = $this ->db->query("SELECT * FROM bn_users WHERE CareerID='$CareerID' AND Cancelled=0");
        if($q->num_rows()>0){
            if($type==1){
                return $q->num_rows();
            }else{
                return $q->result();
            }
        }else{
             return false;
        }
    }
}
?>