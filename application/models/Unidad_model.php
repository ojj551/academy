<?php
class Unidad_model extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
       
        function AboutUnidad($SectionID){
            $q = $this->db->query("SELECT * FROM bn_section WHERE SectionID='$SectionID' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        function InsertMaterial($data){
            $this->db->insert('bn_section_material', $data);
            return true;
        }
    }
?>