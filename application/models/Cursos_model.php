<?php
class Cursos_model extends CI_Model {

    public $title;
    public $content;
    public $date;

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    function GetCourses($CourseProfesorID)
    {
        $this->db->select('*');
        $this->db->from('bn_courses');
        $this->db->where('CourseProfesorID',$CourseProfesorID);
        $this->db->where('Cancelled',0);
        $this->db->where('CourseEnd >= curdate()');
        $q = $this->db->get();
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function AvanceCourse($CourseID)
    {
        $q = $this ->db->query("SELECT avg(Progress) AS Total FROM bn_user_suscriptions WHERE CourseID='$CourseID' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result()[0]->Total;
        }else{
            return false;
        }
    }
    function AvanceByUser($UserID,$TopicID){
        $q = $this ->db->query("SELECT * FROM bn_topics_avance WHERE TopicID = '$TopicID' AND UserID='$UserID' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function AlumnosByCourse($CourseID)
    {
        $q = $this ->db->query("SELECT * FROM bn_user_suscriptions AS T1, bn_users AS T2 WHERE T1.CourseID=$CourseID AND T1.Cancelled=0 AND T1.UserID=T2.UserID AND T2.Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function LastInteractionUser($UserID){
        $q = $this ->db->query("SELECT * FROM bn_topics_avance WHERE UserID='$UserID' AND Cancelled=0 ORDER BY DateLastInteraction DESC LIMIT 1");
        if($q->num_rows()>0){
            return $q->result()[0]->DateLastInteraction;
        }else{
            return false;
        }
    }

    /************CURSOS POR EMPRESA****************/
    function AddCurso($data){
        $this->db->insert('bn_courses', $data);  
        return true;
    }
    function CountTopics($CourseID,$UserID){
        $q = $this ->db->query("SELECT * FROM bn_topics AS T1, bn_courses AS T2 WHERE T1.CourseID='$CourseID' AND T1.CourseID=T2.CourseID AND T1.Cancelled=0 AND T2.CourseProfesorID='$UserID' ORDER BY OrderTopic ASC");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function CountTopicsChild($CourseID,$Parent,$UserID){
        $q = $this ->db->query("SELECT * FROM bn_topics AS T1, bn_courses AS T2 WHERE T1.CourseID='$CourseID' AND T1.CourseID=T2.CourseID AND T1.Cancelled=0 AND T2.CourseProfesorID='$UserID' AND T1.Parent='$Parent' ORDER BY OrderTopic ASC");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function InsertLesson($data){
        $this->db->insert('bn_topics', $data);  
        return true;
    }
    function UpdateTopic($data,$where){
        $this->db->where($where);
        $this->db->update('bn_topics', $data);
        return true;
    }

    function UpdateCourse($data,$where){
        $this->db->where($where);
        $this->db->update('bn_courses', $data);
        return true;
    }
    function AllUsers(){
        $q = $this ->db->query("SELECT * FROM bn_nc WHERE CancelStatus=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function Deptos(){
        $q = $this ->db->query("SELECT T1.Depto,T2.departmentName FROM bn_nc AS T1, departament AS T2 WHERE T1.Depto=T2.departmentID GROUP BY T1.Depto");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function AllUsersNC(){
        $q = $this ->db->query("SELECT * FROM bn_nc WHERE CancelStatus=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function AddUsersToCurso($UserID,$CourseID,$data){
        $q = $this ->db->query("SELECT * FROM bn_user_suscriptions WHERE UserID='$UserID' AND CourseID='$CourseID' AND Cancelled=0");
        if($q->num_rows()>0){
            return false;
        }else{
            $this->db->insert('bn_user_suscriptions', $data);  
            return true;
        }
    }
    function AlumnosNCByCourse($CourseID)
    {
        $q = $this ->db->query("SELECT * FROM bn_user_suscriptions AS T1, bn_nc AS T2 WHERE T1.CourseID='$CourseID' AND T1.Cancelled=0 AND T1.UserID=T2.DocEntry AND T2.CancelStatus=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function GetMyCourses($DocEntry){
        $q = $this ->db->query("SELECT * FROM bn_user_suscriptions AS T1, bn_courses AS T2 WHERE T1.UserID='$DocEntry' AND T1.Cancelled=0 AND T2.CourseID=T1.CourseID AND T2.Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function GetMyCoursesList($UserID){
        $q = $this ->db->query("SELECT * FROM bn_user_suscriptions AS T1, bn_courses AS T2 WHERE T1.UserID='$UserID' AND T1.Cancelled=0 AND T1.CourseID=T2.CourseID AND T2.Cancelled=0 AND T2.CourseEnd>=CURDATE()");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function TopicsOnCourse($CourseID){
        $q = $this ->db->query("SELECT * FROM bn_topics AS T1 WHERE T1.CourseID='$CourseID' AND T1.Cancelled=0 ORDER BY OrderTopic ASC");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function ChildTopic($Parent){
        $q = $this ->db->query("SELECT * FROM bn_topics WHERE Parent='$Parent' AND Cancelled=0 ORDER BY OrderTopic ASC");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    function AboutTopic($TopicID){
        $q = $this ->db->query("SELECT * FROM bn_topics WHERE TopicID='$TopicID' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
    
    function UserIntoCurso($UserID,$CourseID){
        $q = $this ->db->query("SELECT * FROM bn_user_suscriptions WHERE UserID='$UserID' AND CourseID='$CourseID' AND Cancelled=0");
        if($q->num_rows()>0){
            return $q->result();
        }else{
            return false;
        }
    }
}