<?php
class Login_model extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        //
        function login($email,$password)
        {
            if($email!="" && $password!=""){
                $email = trim($email);
                $password = trim($password);
                $UserPasswd = sha1(MD5(sha1(MD5($password))));
                $q = $this ->db->query("SELECT * FROM bn_users where UserEmail='$email' AND sha1(MD5(sha1(UserPasswd)))='$UserPasswd' AND Cancelled=0");
                if($q->num_rows()>0){
                    return $q->result();
                }else{
                    return false;
                }
            }
        }
        //
        function Matricula($Reference){
            $q = $this->db->query("SELECT * FROM bn_users WHERE UserReference = '$Reference' AND ConfirmationDetaills=''");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        //Si email existe
        function ExitsEmail($UserEmail){
            $q = $this->db->query("SELECT * FROM bn_users WHERE UserEmail = '$UserEmail' AND Cancelled=0");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }
        //Crear nuevo usuario
        function NewUser($data){
            $this->db->insert('bn_users', $data);
            return $this->db->insert_id();
            //return true;
        }
        //Validar confirmación ID
        function ValidateConfimationID($ConfirmationID){
            $q = $this->db->query("SELECT * FROM bn_users WHERE ConfirmationID = '$ConfirmationID' AND ConfirmationDetaills='' AND Cancelled=0 AND VisibleStatus=1");
            if($q->num_rows()>0){
                return $q->result();
            }else{
                return false;
            }
        }

        function AddSuscription($CourseID,$UserID,$data){
            $q = $this->db->query("SELECT * FROM bn_user_suscriptions WHERE UserID = '$UserID' AND CourseID='$CourseID'");
            if($q->num_rows()>0){
                return false;
            }else{
                $this->db->insert('bn_user_suscriptions', $data);
                return true;
            }
        }

        function AddUserGroup($UserID,$GroupID,$data){
            $q = $this->db->query("SELECT * FROM bn_user_group WHERE UserID='$UserID' AND GroupID='$GroupID' AND Cancelled=0");
            if($q->num_rows()>0){
                return false;
            }else{
                $this->db->insert('bn_user_group', $data);
                return true;
            }
        }
        function AboutReference($UserID)
        {
            $q = $this ->db->query("SELECT * FROM bn_users WHERE UserID='$UserID' AND Cancelled=0");
            if($q->num_rows()>0){
                return false;
            }else{
                return true;
            }
        }
        function AboutCode($UserID,$UserRecoveryPassword){
            $q = $this ->db->query("SELECT * FROM bn_users WHERE UserID='$UserID' AND UserRecoveryPassword='$UserRecoveryPassword' AND Cancelled=0");
            if($q->num_rows()>0){
                return true;
            }else{
                return false;
            }
        }
}